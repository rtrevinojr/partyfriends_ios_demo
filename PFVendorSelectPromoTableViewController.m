//
//  PFVendorSelectPromoTableViewController.m
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 12/10/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import "PFVendorSelectPromoTableViewController.h"

#import "PFVendorSelectPromoTableViewCell.h"

#import "PFVendorTabBarController.h"
#import "PFPromotersViewController.h"

#import <Parse/Parse.h>


@implementation PFVendorSelectPromoTableViewController



- (void) viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"PF Vendor Select Promoter Table View Controller     ");
    
    
    PFUser * user = [PFUser currentUser];
    
    
    PFQuery * query = [PFQuery queryWithClassName:@"friends"];
    
    PFRelation * friends = [user relationForKey:@"friends"];
    
    PFQuery * friendsQuery = [friends query];
    
    [friendsQuery findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if (!error) {
            self.friendsList = objects;
        }
        [self.tableView reloadData];
    }];
    
    
    NSLog(@"skjfs \n kfjsk \n ladk \n %@", self.vendorObject);
    
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.friendsList.count;
}

- (PFVendorSelectPromoTableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //UITableViewCell * cell =
    
    PFVendorSelectPromoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"promofriendcell" forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[PFVendorSelectPromoTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"promofriendcell"];
    }
    
    PFUser * friend = [self.friendsList objectAtIndex:indexPath.row];
    
    CALayer *editFriendbtnLayer = [cell.promoterFriendAddButton layer];
    [editFriendbtnLayer setMasksToBounds:YES];
    [editFriendbtnLayer setCornerRadius:10.0f];
    
    [editFriendbtnLayer setBorderWidth:1.0f];
    [editFriendbtnLayer setBorderColor:[[UIColor blackColor] CGColor]];
    
    [cell.promoterFriendAddButton setImage:[UIImage imageNamed:@"add-party"] forState:UIControlStateNormal];
    
    CALayer *imageLayer = cell.promoterFriendChoiceImageView.layer;
    [imageLayer setCornerRadius:5];
    [imageLayer setBorderWidth:1];
    [imageLayer setMasksToBounds:YES];
    [cell.promoterFriendChoiceImageView.layer setCornerRadius:cell.promoterFriendChoiceImageView.frame.size.width/2];
    [cell.promoterFriendChoiceImageView.layer setMasksToBounds:YES];
    
    
    if ([self.vendorList containsObject:friend]) {
        [cell.promoterFriendAddButton setBackgroundColor:[UIColor blueColor]];
        cell.promoterFriendAddButton.tag = 1;
    }
    else {
        [cell.promoterFriendAddButton setBackgroundColor:[UIColor whiteColor]];
        cell.promoterFriendAddButton.tag = 0;
    }
    
    
    if (friend[@"profilePic"] != nil) {
        
        PFFile * pic = [friend objectForKey:@"profilePic"];
        [pic getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
            if (!error) {
                UIImage *image = [UIImage imageWithData:data];
                [cell.promoterFriendChoiceImageView setImage:image];
            }
            else {
                UIImage * defaultimg = [UIImage imageNamed:@"58.png"];
                [cell.promoterFriendChoiceImageView setImage:defaultimg];
            }
        }];
    }
    else {
        UIImage * defaultimg = [UIImage imageNamed:@"PFIcon"];
        [cell.promoterFriendChoiceImageView setImage:defaultimg];
    }
    
    cell.promoterFriendNameLabel.text = [friend objectForKey:@"firstName"];
    
    cell.promoterPFObject = friend;
    //cell.promoterFriendChoiceImageView
    
    return cell;
}


- (IBAction)addPromoter:(id)sender {
    
    NSLog(@"Add Promoter Touch");
    
    PFVendorSelectPromoTableViewCell * cell = (PFVendorSelectPromoTableViewCell *) [[sender superview] superview];
    
    PFObject * promoter = cell.promoterPFObject;
    
    PFVendorTabBarController * container = (PFVendorTabBarController *) self.navigationController.parentViewController;
    
    
    
    NSLog(@"********* Parent VC = %@", container);
    
    if (cell.promoterFriendAddButton.tag == 0) {
        
        [cell.promoterFriendAddButton setBackgroundColor:[UIColor whiteColor]];
    
       PFObject * vendorPFObject = self.vendorObject;
       // container.vendorPromoPFObject;
        NSLog(@"vendor object  select promoter view = %@", vendorPFObject );
        PFRelation *promoters = [self.vendorObject relationForKey:@"promoters"];
        [promoters addObject:promoter];
        [self.vendorObject saveInBackground];
        
        cell.promoterFriendAddButton.tag = 1;
        
    }
    else {
        
        [cell.promoterFriendAddButton setBackgroundColor:[UIColor blueColor]];
        
      //  PFObject * vendorPFObject =
        //container.vendorPromoPFObject;
        //NSLog(@"vendor object  select promoter view = %@", vendorPFObject);
        PFRelation *promoters = [self.vendorObject relationForKey:@"promoters"];
        [promoters removeObject:promoter];
        [self.vendorObject saveInBackground];
        
        cell.promoterFriendAddButton.tag = 0;
    }
    
    //NSLog(@"**** self.tabBarController = %@", self.tabBarController.viewControllers[0].vendorPromoPFObject );
    
    //PFObject * promoObject = self.tabBarController.
    
    //PFRelation *promters = self.tabBarController;
    
    
}



@end
