
// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
Parse.Cloud.define("CheckPassword", function(request, response) {

    var password = request.params.password;

    Parse.User.logIn(request.user.getUsername(), password, {
        success: function(user) 
        {   
            response.success("Password correct.");
        },
        error: function(results, error) {
            response.error("Current password incorrect.");
        }
    });
});

/*Parse.Cloud.define("Update", function(request, response){
   
   var friendrequests = Parse.Object.extend("friendrequest");
   var query = Parse.Query(friendrequests);
   query.equalTo("user1",request.user);
   query.equalTo("status",1);
   query.include("user2");
   query.find({
       
       success:function(results){
           for(var i = 0; i<results.length;i++){
               var friendRequest = results[i];
               var newFriend = friendRequest.get("user2");
               var user = request.user;
               var relation = user.relation("friends");
               relation.add(newFriend);
               
           };
           user.save(null,{
               success:function(user){
                   
                    for(var i = 0; i<results.length;i++){
                    var friendReq = results[i];
                    friendReq.set("status",3);
                    friendReq.save();
               
           }
               response.success("Friends list updated!");
               },
               error:function(user,error){
                   
                   response.error("Failed to add new frinds.")
                   
               }
           });
           
           response.success("Found requests!");
       },
       
       error:function(error){
           response.error("Could not retrieve requests!");
       }
       
   });
   
   var removeBadFriends1 = new Parse.Query("friendrequest");
   removeBadFriends1.equalTo("user1",request.user);
   removeBadFriends1.equalTo("status",4);
   
   var removeBadFriends2 = new Parse.Query("friendrequest");
   removeBadFriends2.equalTo("user2",request.user);
   removeBadFriends2.equalTo("status",4);
   
   var removeBadFriends = new Parse.Query.or(removeBadFriends1,removeBadFriends2);
   removeBadFriends.include("user1");
   removeBadFriends.include("user2");
   removeBadFriends.find({
  success: function(results) {
      if(results.length>0){
          var current = request.user;
          var friendList = current.relation("friends");
          for(var l = 0;results.length > l;l++){
              var badFriendRequest = results[l];
              var user1 = badFriendRequest.get("user1");
              var user2 = badFriendRequest.get("user2");
              
              if(user1.id===current.id){
                  friendList.remove(user2);
              }
              else if(user2.id===current.id){
                  friendList.remove(user1);
              }
          }
          current.save();
      }
  },
  error: function(error) {
    // There was an error.
  }
});
   
   
    
});*/

//Parse.Cloud.define("CheckRole", function(request, response){
    
    

//});

Parse.Cloud.beforeSave("accountinfo", function(request, response) {
    
    var city = request.object.get("city");
    var zip = request.object.get("zip");
    
    request.object.set("city", city.toUpperCase());
    
    if (zip.length != 5) 
        response.error("Please enter a valid zipcode");
    
    response.success();
})

/*
Parse.Cloud.beforeSave("party", function(request, response) {
    
    var geo = request.object.get("location");
    
    var latitude = geo.latitude;
    var longitude = geo.longitude;
    
    var party_id = request.object.get("objectId");
    
    var user = Parse.Object.extend(Parse.User);
    
    var query = new Parse.Query(user);
    
    query.equalTo(request.user);
    query.include("lastParty");

    query.find({
      success: function(results) {
          for (var i = 0; i < results.length; i++) {
              console.log(" " + results[i].get("objectId") + " - " + results[i].get("createdBy")); 
          }
      }
    })
    
})*/

Parse.Cloud.beforeSave("friendrequest", function(request, response) {
    
    var status = request.object.get("status");
    var user1 = request.object.get("user1");
    
    var user2 = request.object.get("user2");
    
    var friendrequest = Parse.Object.extend("friendrequest");
    
    var query = new Parse.Query(friendrequest);
    
    query.equalTo("user1", user1);
    query.equalTo("user2", user2);
    query.equalTo("status", 0);
    
    var isPresent = false;
    
    var updated = false;
    var update = request.object.get("update");
    
    
    if (!updated) {
       
    console.log("update is false");
    
    query.find({
        success: function(results) {
            
            isPresent = true;
            
            //printTable (results);
            
            if(results.length > 0) {
                response.error("Friend Request is pending...");
            
//                alert("objects = " + results.toString());
//                alert("objects = " + (results[0]).toString());
//                var obj_id = (results[0]).get("objectId");
//                alert("objects present = " + obj_id );
            }
            else{
               response.success();
            }
        },
//        error: function(error) {
//            //response.success();
//            alert("error. no matches");
     //  }
    })
    
    }
    else {
        console.log("udpate is true");
        response.success();
           }
    
});


/*Parse.Cloud.beforeSave(Parse.User, function(request, response) {
    
  var username = request.object.get("username");
  //var password = request.object.get("password");
  //var password = request.params.password;
  
  var isValid = true;
  
  for (var i = 0; i < username.length; i++) {
      
      var char = username.charAt(i);
      if (!parseInt(char))   {
          
          isValid = false;
          //response.error("Please enter a valid phone number");
          
      }
  }
  /*
  for (var i = 0; i < username.length; i++) {
      
      var char = username.charAt(i);
  }
  if (password.length < 6) {
      response.error("Please enter a password of at least 6 characters")
  }
  if (password.length > 12) {
      reponse.error("Please enter a password no longer than 12 characters");
  }
  
  
  if (isValid)
    response.success();
  else
      response.error();
  

});
*/

Parse.Cloud.define("Logger", function(request, response) {
  console.log(request.params);
  
  console.log("********** Logger *************");
  
  var party = Parse.Object.extend("party");
    
  var query = new Parse.Query(party);
  
  query.find({
      success: function(results) {
          for (var i = 0; i < results.length; i++) {
              console.log(" " + results[i].get("objectId") + " - " + results[i].get("createdBy")); 
          }
      }
  })
  
  response.success();
});


function printTable (list) {
    
    for (var i = 0; i < list.length; i++) {
        //console.log( " " + list[i].get("objectId") );
        //console.error( " " + list[i].get("objectId") );
        alert (" " + list[i].get("objectId") );
    }
    
}
