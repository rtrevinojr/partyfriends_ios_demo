//
//  PFVendorEditPromoterTableViewCell.h
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 12/9/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PFVendorEditPromoterTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *promoterNameLabel;

@property (weak, nonatomic) IBOutlet UIImageView *promoterProfileImage;

@end
