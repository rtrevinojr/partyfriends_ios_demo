//
//  PFVendorContainerViewController.h
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 12/9/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Parse/Parse.h>


@interface PFVendorContainerViewController : UIViewController

@property (strong, nonatomic) PFObject * vendorPFObject;


@end
