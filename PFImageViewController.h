//
//  PFImageViewController.h
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 11/10/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PFImageViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>


@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;


- (IBAction)takePhoto:(id)sender;


- (IBAction)selectPhoto:(id)sender;


@end
