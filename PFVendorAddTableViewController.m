//
//  PFVendorAddTableViewController.m
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 12/9/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import "PFVendorAddTableViewController.h"

#import "PFVendorAddTableViewCell.h"


@implementation PFVendorAddTableViewController


- (void) viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    
    
    
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}


- (PFVendorAddTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PFVendorAddTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"vendorcell" forIndexPath:indexPath];
    
    if (cell == nil) {
        
        cell = [[PFVendorAddTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle];
        
        //cell = [[PFVendorAddTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle resuseIdentifier:@"vendorcell"];
    }
    
    
    
    
    return cell;
}



@end
