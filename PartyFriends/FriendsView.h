//
//  FriendsView.h
//  PartyFriends
//
//  Created by Joshua Silva  on 8/14/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Parse/Parse.h"

@interface FriendsView : UITableViewController


@property (strong, nonatomic) NSMutableArray * currentFriends;

@property (strong, nonatomic) PFUser * currentUser;

@property (strong, nonatomic) PFRelation * friends;

@property (strong, nonatomic) NSIndexPath* index;

@property (strong, nonatomic) PFRole * rolling;




@end
