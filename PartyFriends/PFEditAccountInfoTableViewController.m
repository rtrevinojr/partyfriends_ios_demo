//
//  PFEditAccountInfoTableViewController.m
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 1/23/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import "PFEditAccountInfoTableViewController.h"

#import <Parse/Parse.h>
#import "UIView+Toast.h"
#import "Reach.h"
#import "Gradient.h"
#import <ParseUI/ParseUI.h>
#import "LocationManagerSingleton.h"


@implementation PFEditAccountInfoTableViewController


- (void) viewDidLoad
{
    [super viewDidLoad];

//    CAGradientLayer * randomLayer = [Gradient gold];
//    
//    randomLayer.frame = CGRectMake(0, -20, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height + 20);
//    
//    [randomLayer setStartPoint:CGPointMake(0.0, 0.5)];
//    [randomLayer setEndPoint:CGPointMake(1.0, 0.5)];
//    
//    
//    [self.navigationController.navigationBar.layer insertSublayer:randomLayer atIndex:2];
    
    self.theStates = @[ @"",@"AL",@"AK",@"AZ",@"AR",@"CA",@"CO",@"CT",@"DE",@"DC",@"FL",@"GA",@"HI",@"ID",@"IL",@"IN",@"IA",@"KS",@"KY",@"LA",@"ME",@"MD",@"MA",@"MI",@"MN",@"MS",@"MO",@"MT",@"NE",@"NV",@"NH",@"NJ",@"NM",@"NY",@"NC",@"ND",@"OH",@"OK",@"OR",@"PA",@"RI",@"SC",@"SD",@"TN",@"TX",@"UT",@"VT",@"VA",@"WA",@"WV",@"WI",@"WY"];
    self.genderArray = @[@"Not Selected", @"M",@"F"];
    
    
    
    
    UIBarButtonItem * saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered
                                                                  target:self
                                                                  action:@selector(saveAccountInfoTouch:)];
    
    self.navigationItem.rightBarButtonItem = saveButton;
    //self.navigationItem.rightBarButtonItem
    

    
    self.firstNameTextField.delegate = self;
    self.lastNameTextField.delegate = self;
    self.phoneTextField.delegate = self;
    self.addressTextField.delegate = self;
    self.cityTextField.delegate = self;
    self.zipcodeTextField.delegate = self;
    self.emailTextField.delegate = self;
    self.invitePin.delegate = self;
    
    [self.bdayDatePicker addTarget:self action:@selector(dateChanged:)
     forControlEvents:UIControlEventValueChanged];
    
    PFUser * user = [PFUser currentUser];
    
    self.datePickerIsShowing = NO;
    self.statePickerIsShowing = NO;
    
    
    
    self.firstNameTextField.text = [user objectForKey:@"firstName"];
    self.lastNameTextField.text = [user objectForKey:@"lastName"];
    self.phoneTextField.text = [user objectForKey:@"username"];
    self.emailTextField.text = [user objectForKey:@"email"];
    self.invitePin.text = [user objectForKey:@"invitePin"];
    
    
    
    CALayer *imageLayer = self.profileImage.layer;
    [imageLayer setCornerRadius:5];
    [imageLayer setBorderWidth:0];
    [imageLayer setMasksToBounds:YES];
    [self.profileImage.layer setCornerRadius:self.profileImage.frame.size.width/2];
    [self.profileImage.layer setMasksToBounds:YES];
    
    if ([user[@"gender" ] isEqualToString:@"M"]) {
        self.userGender = @"M";
        
        [self.genderPicker selectRow:1 inComponent:0 animated:YES];}else if ([user[@"gender" ] isEqualToString:@"F"]){
            [self.genderPicker selectRow:2 inComponent:0 animated:YES];
            self.userGender = @"F";
        }
    else{
        [self.genderPicker selectRow:0 inComponent:0 animated:YES];
            self.userGender = @"Not Selected";
        }
    
    
    
    
    PFObject * photo = user[@"profilePicture"];
    
    if (photo !=nil) {
        [photo fetchInBackgroundWithBlock:^(PFObject * photoObject, NSError * error){
            if (!error) {
                PFFile * pic =[photoObject objectForKey:@"photo"];
                PFImageView * userImage =[PFImageView new];
                userImage.image = [UIImage imageNamed:@"userPic"];
                userImage.file = (PFFile *) pic;
                
                [userImage loadInBackground:^(UIImage * image, NSError * error){
                    if (!error) {
                        image = [self squareImageFromImage:image scaledToSize:image.size.height];
                        [self.profileImage setImage:image];
                    }else {
                        UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
                        [self.profileImage setImage:defaultimg];
                        
                    }
                    
                }];
                
            }
            
            
        }];
    }else{
        
        UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
        [self.profileImage setImage:defaultimg];
        
    }
    
    
//    if (user[@"profilePic"] != nil) {
//        
//        PFFile * pic = [user objectForKey:@"profilePic"];
//        [pic getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
//            if (!error) {
//                UIImage *image = [UIImage imageWithData:data];
//                [self.profileImage setImage:image];
//            }
//            else {
//                UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
//                [self.profileImage setImage:defaultimg];
//            }
//        }];
//    }
//    else {
//        UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
//        [self.profileImage setImage:defaultimg];
//    }
    
    PFQuery * userAccount = [PFUser query];
    [userAccount whereKey:@"objectId" equalTo:[[PFUser currentUser] objectId]];
    [userAccount includeKey:@"accountInfo"];
    
    [userAccount getFirstObjectInBackgroundWithBlock:^(PFObject * object, NSError * error){
        
        if (!error) {
            self.currentUser = (PFUser *)object;
            self.userAccountInfo = [self.currentUser objectForKey:@"accountInfo"];
            
            
            NSLog(@"self.userAccountInfo = %@", self.userAccountInfo);
            
            if (self.userAccountInfo == nil) {
                PFQuery * queryAccount = [PFQuery queryWithClassName:@"accountinfo"];
                [queryAccount whereKey:@"createdBy" equalTo:self.currentUser];
                
                [queryAccount getFirstObjectInBackgroundWithBlock:^(PFObject * object, NSError * error){
                    if (error.code == 101){
                        NSLog(@"error at finding a account row, time to creat a account object!!! ");
                        self.userAccountInfo = [PFObject objectWithClassName:@"accountinfo"];
                        self.userAccountInfo[@"address"] = @"random";
                        NSLog(@"log of the address %@", self.userAccountInfo[@"address"]);
                        
                    }else if(!error){
                        
                        self.userAccountInfo = object;
                        
                        self.cityTextField.text = [self.userAccountInfo objectForKey:@"city"];
                        self.addressTextField.text = [self.userAccountInfo objectForKey:@"address"];
                    self.zipcodeTextField.text = [self.userAccountInfo objectForKey:@"zip"];
                        
                    self.userState = [self.userAccountInfo objectForKey:@"state"];
                    self.stateLabel.text = [self.userAccountInfo objectForKey:@"state"];
                       self.genderLabel.text = [user objectForKey:@"gender"];
                        
                        NSLog(@"..... State Label = %@", self.stateLabel.text);
                        
                        NSDate * userBirth = [self.userAccountInfo objectForKey:@"birthday"];
                        
                        [self.bdayDatePicker setDate:userBirth];
                        
                        
                        
                        NSDateFormatter *newFormatter = [[NSDateFormatter alloc] init];
                        
                        [newFormatter setDateFormat:@"dd-MM-yyyy"];
                        
                        NSString * dateString = [newFormatter stringFromDate:userBirth];
                        
                        self.bdayLabel.text = dateString;
                        
                    }
                    else{
                        //[self.view makeToast:@"Unable to return your info - try again" duration:3 position:CSToastPositionCenter];
                        
                        NSLog(@"ERROR");
                    }}];
            }
            else{
                
                NSLog(@" - self.userState = %@", self.userState);
           
                self.cityTextField.text = [self.userAccountInfo objectForKey:@"city"];
                self.addressTextField.text = [self.userAccountInfo objectForKey:@"address"];
                //self.zipcodeTextField.text = [self.userAccountInfo objectForKey:@"zip"];
                self.genderLabel.text = [user objectForKey:@"gender"];
                
                NSNumber * zip = [self.userAccountInfo objectForKey:@"zip"];
                
                NSString * zipString = [NSString stringWithFormat:@"%@", zip];
                
                self.zipcodeTextField.text = zipString;
              
                self.userState = [self.userAccountInfo objectForKey:@"state"];
                self.stateLabel.text = [self.userAccountInfo objectForKey:@"state"];
//
//                NSLog(@"..... State Label = %@", self.stateLabel.text);
                
                NSDate * userBirth = [self.userAccountInfo objectForKey:@"birthday"];
                
                NSLog(@"User Birth Date ==================== %@", userBirth);
                
                [self.bdayDatePicker setDate:userBirth];
                
                NSDateFormatter *newFormatter = [[NSDateFormatter alloc] init];
                [newFormatter setDateFormat:@"yyyy-MM-dd"];
                NSString * dateString = [newFormatter stringFromDate:userBirth];
                
                NSLog(@"Date String = %@", dateString);
                
                self.bdayLabel.text = dateString;
                int index = [self.theStates indexOfObject:self.userState];
                //NSInteger stateIndex= index;
                //self.zipcodeTextField.text = zipPlace;
                //NSLog(@"zipplace    %u", index);
                
                index = (index > 52) ? 43 : index;
                
                NSLog(@"index = %d", index);
                
                [self.statePicker selectRow:index inComponent:0 animated:YES];

            }
            
            
        }
    }];
    
    
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSDictionary * dic = [NSDictionary new];
    
        NSLog(@"the subviews for the bar %@",self.navigationController.navigationBar.subviews);
    
    [PFCloud callFunctionInBackground:@"VerifyEmail" withParameters:dic];
    
    [self.currentUser fetchInBackgroundWithBlock:^(PFObject * _Nullable user, NSError * _Nullable error) {
        if (!error) {
            
            NSLog(@"cake");
            self.currentUser = (PFUser *) user;
            
            PFObject * profilePhoto = self.currentUser[@"profilePicture"];
            
            [profilePhoto fetchInBackgroundWithBlock:^(PFObject * photoObject, NSError * error){
                if (!error) {
                    NSLog(@"cake1");
                    PFFile * pic =[photoObject objectForKey:@"photo"];
                    PFImageView * userImage =[PFImageView new];
                    userImage.image = [UIImage imageNamed:@"userPic"];
                    userImage.file = (PFFile *) pic;
                    
                    [userImage loadInBackground:^(UIImage * image, NSError * error){
                        if (!error) {
                            [self.profileImage setImage:image];
                        }else{
                             UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
                            [self.profileImage setImage:defaultimg];
                        
                        }
                    }];
                }
            }];
        
        
        }}];
    
    
    

}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    // Background color
  //  view.tintColor = [UIColor blackColor];
   // view.tintColor = [UIColor colorWithRed:(28/255.0) green:(179/255.0) blue:(251/255.0) alpha:1.0];
    
    // Text Color
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[UIColor colorWithRed:(142/255.0) green:(83/255.0) blue:(249/255.0) alpha:1.0]];
    
    // Another way to set the background color
    // Note: does not preserve gradient effect of original header
    // header.contentView.backgroundColor = [UIColor blackColor];
}



- (void) dateChanged:(id)sender{
    // handle date changes
    NSDate * date = [self.bdayDatePicker date];
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd"]; // Date formater yyyy-MM-dd
    NSString *datestring = [dateformate stringFromDate:date];

    NSLog(@"Date String = %@", datestring);
    self.bdayLabel.text = datestring;
}


- (void)showDatePickerCell {
    self.datePickerIsShowing = YES;
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    self.bdayDatePicker.hidden = NO;
    self.bdayDatePicker.alpha = 0.0f;
    
    [UIView animateWithDuration:0.25 animations:^{
        self.bdayDatePicker.alpha = 1.0f;
    }];
}

- (void)hideDatePickerCell {
    self.datePickerIsShowing = NO;
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.bdayDatePicker.alpha = 0.0f;
                     }
                     completion:^(BOOL finished){
                         self.bdayDatePicker.hidden = YES;
                     }];
}

- (void)showStatePickerCell {
    self.statePickerIsShowing = YES;
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    self.statePicker.hidden = NO;
    self.statePicker.alpha = 0.0f;
    
    [UIView animateWithDuration:0.25 animations:^{
        self.statePicker.alpha = 1.0f;
    }];
}

- (void)hideStatePickerCell {
    self.statePickerIsShowing = NO;
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.statePicker.alpha = 0.0f;
                     }
                     completion:^(BOOL finished){
                         self.statePicker.hidden = YES;
                     }];
}

-(void)showGenderPicker {
    self.genderPickerIsShowing = YES;
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    self.genderPicker.hidden = NO;
    self.genderPicker.alpha = 0.0f;
    
    [UIView animateWithDuration:0.25 animations:^{
        self.genderPicker.alpha = 1.0f;
    }];


}

-(void)hideGenderPicker{
    self.genderPickerIsShowing = NO;
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
    [UIView animateWithDuration:0.25
                     animations:^{
                         self.genderPicker.alpha = 0.0f;
                     }
                     completion:^(BOOL finished){
                         self.genderPicker.hidden = YES;
                     }];

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (indexPath.section == 4 && indexPath.row == 1 && self.datePickerIsShowing == NO){
        // hide date picker row
        return 0.0f;
    }
    
    if (indexPath.section == 8 && indexPath.row == 1 && self.statePickerIsShowing == NO) {
        return 0.0f;
    }
    
    if (indexPath.section == 9 && indexPath.row == 1 && self.genderPickerIsShowing == NO) {
        return 0.0f;
    }
    return [super tableView:tableView heightForRowAtIndexPath:indexPath];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"..........didSelectRowAtIndexPath = %@", indexPath);
    NSLog(@"Section = %lu", indexPath.section);
    NSLog(@"Row = %lu", indexPath.row);
    
    if (indexPath.section == 4 && indexPath.row == 0) {
        
        NSLog(@"THE CORRECT ROW");
        
        if (self.datePickerIsShowing){
            [self hideDatePickerCell];
        }else {
            [self showDatePickerCell];
        }
    }
    
    if (indexPath.section == 8 && indexPath.row == 0) {
        
        NSLog(@"THE CORRECT ROW");
        
        if (self.statePickerIsShowing){
            [self hideStatePickerCell];
        }else {
            [self showStatePickerCell];
        }
    }
    if (indexPath.section == 9 && indexPath.row == 0) {
        if (self.genderPickerIsShowing) {
            [self hideGenderPicker];
            
        }else{
            [self showGenderPicker];
        }
    }
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    if(pickerView.tag == 1){
        return 1;
    
    }else{
    return 1;
    }
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView.tag ==1 ) {
    return [self.theStates count];
    }else{
        return [self.genderArray count];
    }

}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (pickerView.tag == 1) {
        
    return [self.theStates objectAtIndex:row];
    }else{
    
        return [self.genderArray objectAtIndex:row];
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    NSLog(@"pickerView pick change");
    
    if (pickerView.tag == 1) {
        
    self.userState = [[NSString alloc] initWithFormat:@"%@", [self.theStates objectAtIndex:row]];
    self.stateLabel.text = self.userState;
        
    }
    if (pickerView.tag ==2) {
        self.userGender = [[NSString alloc] initWithFormat:@"%@", [self.genderArray objectAtIndex:row]];
        self.genderLabel.text = self.userGender;
    }
    
    if (pickerView.tag == 0) {
        
    
    NSDate * date = [self.bdayDatePicker date];

    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"mm-dd-yyyy"]; // Date formater
    NSString *datestring = [dateformate stringFromDate:date];
    
    NSLog(@"Date String = %@", datestring);
    
    }
    
    
}




- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y-200);
    //[self.scrollView setContentOffset:scrollPoint animated:YES];
    [self.tableView setContentOffset:scrollPoint animated:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    //[self.scrollView setContentOffset:CGPointZero animated:YES];
    [self.tableView setContentOffset:CGPointZero animated:YES];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    [self.view endEditing:YES];
    return YES;
}


- (IBAction)saveAccountInfoTouch:(id)sender {
    
    NSLog(@"...............saveAccountInfoTouch");
    [sender setEnabled:NO];
    if ([Reachability internetCheck]) {
        
    
    
    NSDate * birthDay = [self.bdayDatePicker date];
    
    NSLog(@"b date = %@", birthDay);
    
    if (self.firstNameTextField.text != self.currentUser[@"firstName"] ) {
        //NSString * firstNameString = self.firstName.text;
        self.currentUser[@"firstName"] = self.firstNameTextField.text;
    }
    if (self.lastNameTextField.text != self.currentUser[@"lastName"]) {
        NSString * lastNameString = self.lastNameTextField.text;
        self.currentUser[@"lastName"] = lastNameString;
    }
    if (self.phoneTextField.text != self.currentUser.username) {
        NSString * usernameString = self.phoneTextField.text;
        self.currentUser.username = usernameString;
    }
    if (self.addressTextField.text != self.userAccountInfo[@"address"]) {
        NSString * addressString = self.addressTextField.text;
        //self.userAccountInfo[@"address"] = addressString;
    }
    if (self.cityTextField.text != self.userAccountInfo[@"city"]) {
        NSString * cityString = self.cityTextField.text;
       // self.userAccountInfo[@"city"] = cityString;
    }
    
    if (self.stateLabel.text != self.userAccountInfo[@"state"]) {
        NSString * stateString = self.stateLabel.text;
       // self.userAccountInfo[@"state"]  = stateString;
        self.userState = stateString;
    }
    
    

    
    NSString * zip = @"";
    
    if (self.zipcodeTextField.text != self.userAccountInfo[@"zip"]) {
    NSString * zipString = [self.zipcodeTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSNumberFormatter * zipCode = [NSNumberFormatter new];
    zipCode.numberStyle = NSNumberFormatterDecimalStyle;
    zip = [zipCode numberFromString:self.zipcodeTextField.text];
    NSString * zipcodeString = self.zipcodeTextField.text;
        
        //self.userAccountInfo[@"zip"] = zip;
    
    }
    
        
                    NSLog(@"................................. %@ \n %@ \n %@ \n %@ \n %@ \n %@ \n %@ \n %@-%@  \n %@ \n %@ \n %@ \n tjjhkjh", self.currentUser[@"firstName"], self.currentUser[@"lastName"], self.currentUser.username, self.userAccountInfo[@"address"],birthDay, self.userAccountInfo[@"city"], self.currentUser[@"gender"],self.userGender, self.userAccountInfo[@"state"],self.userAccountInfo[@"zip"], self.currentUser.email, self.currentUser[@"invitePin"]);
        
    NSLog(@"***********************************");
    
    if (self.emailTextField.text != self.currentUser.email) {
        NSString * emailString = self.emailTextField.text;
        self.currentUser.email = emailString;
    }
    
    if (self.phoneTextField.text.length !=10){
        UIAlertView * numberView = [[UIAlertView alloc] initWithTitle:@"Phone Number" message:@"there are more than 10 digtis in the Phone Number field" delegate:nil cancelButtonTitle:@"thank you" otherButtonTitles: nil];
        [numberView show];
        

        [sender setEnabled:YES];
    }else {
        if ((self.zipcodeTextField.text.length != 5 && self.zipcodeTextField.text.length > 0)) {
            UIAlertView * zipcodeAlert = [[UIAlertView alloc] initWithTitle:@"Zipcode" message:@"Zipcode field must have only 5 digits" delegate:nil cancelButtonTitle:@"thank you" otherButtonTitles:nil];
            [zipcodeAlert show];
            [sender setEnabled:YES];
        }else{
            if (self.invitePin.text.length !=4) {
                UIAlertView * pinAlert = [[UIAlertView alloc] initWithTitle:@"Zipcode" message:@"Your invite pin must be 4 digits long" delegate:nil cancelButtonTitle:@"thank you" otherButtonTitles:nil];
                [pinAlert show];
                [sender setEnabled:YES];
            }
            else{
                self.currentUser[@"invitePin"] = self.invitePin.text;
            self.userAccountInfo.ACL =[PFACL ACLWithUser:self.currentUser];
                if (birthDay != nil) {
            self.userAccountInfo[@"birthday"] = birthDay;
                }
                if (zip != nil) {
            self.userAccountInfo[@"zip"] = zip;
                }
                if (self.userState != nil) {
            self.userAccountInfo[@"state"] = self.userState;
                }
                if (self.userGender != nil) {
                self.currentUser[@"gender"] = self.userGender;
                }if (self.addressTextField.text.length !=0 || self.addressTextField.text != nil) {
                    self.userAccountInfo[@"address"] = self.addressTextField.text;
                }
                if (self.cityTextField.text.length != 0 || self.cityTextField.text != nil) {
                    self.userAccountInfo[@"city"] = self.cityTextField.text;
                }
                
           // self.userAccountInfo[@"gender"] = self.userGender;
            
            NSLog(@"........ User State = %@", self.userState);
            
            NSLog(@"On key press user = %@", self.currentUser.objectId);
            [self.currentUser saveInBackgroundWithBlock:^(BOOL saveStatus, NSError * error){
                NSLog(@"On key press object = %@", self.userAccountInfo.objectId);
                
                if (!error) {
                    NSLog(@"the account+the currentUser was saved");
                    
                    if(self.userAccountInfo!= nil){
                        
                        [self.userAccountInfo saveInBackgroundWithBlock:^(BOOL status, NSError * error){
                            if (!error) {
                                //[self.view makeToast:@"Info Saved" duration:2 position:CSToastPositionCenter];
                                [sender setEnabled:YES];
                                
                                UIAlertView * accountSave = [[UIAlertView alloc] initWithTitle:@"PartyFriends" message:@"Your account has been saved" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                [accountSave show];
                                
                                NSString * addressString = [NSString stringWithFormat:@"%@ %@ %@", self.userAccountInfo[@"address"], self.zipcodeTextField.text , self.userAccountInfo[@"state"]];
                                
                                
                                CLGeocoder * geocoder = [CLGeocoder new];
                                [geocoder geocodeAddressString:addressString completionHandler:^(NSArray *placemarks, NSError *error) {
                                    if (error) {
                                        NSLog(@"%@", error);
                                        
                                    }else {
                                        CLPlacemark * placemark = [placemarks lastObject];
                                        CLLocationCoordinate2D homeCord = CLLocationCoordinate2DMake(placemark.location.coordinate.latitude, placemark.location.coordinate.longitude);
                                        
                                        CLRegion * homeRegion = [[CLCircularRegion alloc] initWithCenter:homeCord radius:200 identifier:@"home"];
                                        
                                        
                                        [[LocationManagerSingleton sharedSingleton].locationManager startMonitoringForRegion:homeRegion];
                                        
                                        
                                        
                                    }
                                }];
                                
                                
                                
                                
                             
                                
                            }else {
                                if (error.code == 141) {
                                    
                                    
                                    UIAlertController * missingDetails = [UIAlertController alertControllerWithTitle:@"For Your Protection" message:@"Please fill in the following fields. address, state, city, and zipcode for a better accuracy " preferredStyle:UIAlertControllerStyleAlert];
                                    
                                    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Later", @"Cancel action") style:UIAlertActionStyleCancel handler:nil];
                                    UIAlertAction * agreedAction = [UIAlertAction actionWithTitle:@"Okay!" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                        [self.addressTextField becomeFirstResponder];
                                    }];
                                    [missingDetails addAction:agreedAction];
                                    [missingDetails addAction:cancelAction];
                                    
                                    [self presentViewController:missingDetails animated:YES completion:nil];
                                  
                                    
                                    
                                    
                                }else{
                                [sender setEnabled:YES];
                                NSLog(@"the accountInfo did not save!!! ERROR=%@ .... %ld", error, (long)error.code);
                                UIAlertView * accountNotSaved = [[UIAlertView alloc] initWithTitle:@"PartyFriends" message:@"Your account was not saved" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                [accountNotSaved show];
                                [sender setEnabled:YES];
                            }
                            }
                            
                        }];
                    }
                    
                }else{[sender setEnabled:YES];
                    NSLog(@"currentUser was not saved!");
                }
                
                
            }];
            
            [sender setEnabled:YES];
            NSLog(@"the accountInfo was able to save! ");
        } }}
    
    
    }else if (![Reachability internetCheck]) {
        
        [self.view makeToast:@"Try again: No internet connection" duration:3 position:CSToastPositionCenter];
    }
    
}
- (UIImage *)squareImageFromImage:(UIImage *)image scaledToSize:(CGFloat)newSize {
    CGAffineTransform scaleTransform;
    CGPoint origin;
    
    if (image.size.width > image.size.height) {
        CGFloat scaleRatio = newSize / image.size.height;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(-(image.size.width - image.size.height) / 2.0f, 0);
    } else {
        CGFloat scaleRatio = newSize / image.size.width;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(0, -(image.size.height - image.size.width) / 2.0f);
    }
    
    CGSize size = CGSizeMake(newSize, newSize);
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(size, YES, 0);
    } else {
        UIGraphicsBeginImageContext(size);
    }
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextConcatCTM(context, scaleTransform);
    
    [image drawAtPoint:origin];
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}


@end
