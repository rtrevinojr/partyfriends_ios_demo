//
//  PFPromotersViewController.h
//  PartyFriends
//
//  Created by Joshua Silva  on 1/14/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
@interface PFPromotersViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UIButton *add;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) PFObject * vendorPFObject;

@property (strong, nonatomic) NSMutableArray * promoterList;

@end
