//
//  TablesecondViewController.h
//  PartyFriends
//
//  Created by Joshua Silva  on 7/11/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>



@interface PFTextTableVIew : UITableViewController


@property (strong, nonatomic) PFRelation *friendsRelation;
@property (strong, nonatomic) NSArray * friends;

@end
