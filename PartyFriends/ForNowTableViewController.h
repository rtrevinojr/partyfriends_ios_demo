//
//  ForNowTableViewController.h
//  PartyFriends
//
//  Created by Joshua Silva  on 7/10/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface ForNowTableViewController : UITableViewController





@property (nonatomic, strong) NSArray * allUsers;
@property (nonatomic, strong) PFUser *currentUser;
@property (nonatomic, strong) NSMutableArray *friends;
@property (nonatomic, strong) NSMutableArray *friendsAgain;
@property (nonatomic, strong) PFObject * placeObject;


-(BOOL) isFriend: (PFUser *) user;

@end
