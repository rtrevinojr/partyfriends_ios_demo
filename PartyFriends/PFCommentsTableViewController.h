//
//  PFCommentsTableViewController.h
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 11/23/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Parse/Parse.h>


@interface PFCommentsTableViewController : UITableViewController <UITextFieldDelegate>


@property (strong, nonatomic) PFObject * pfParty;

@property (strong, nonatomic) NSString *userPush;

@property (strong, nonatomic) NSArray * commentsArray;


@property (weak, nonatomic) UITextField * commentTextFieldTest;

@property (weak, nonatomic) IBOutlet UIToolbar *commentToolbar;

@property (weak, nonatomic) IBOutlet UIToolbar *commentTextField;

@property (weak, nonatomic) IBOutlet UIToolbar *PostCommentBtn;

@end
