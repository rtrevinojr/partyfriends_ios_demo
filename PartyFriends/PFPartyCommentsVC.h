//
//  PFPartyCommentsVC.h
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 1/8/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>


#import <Parse/Parse.h>


@interface PFPartyCommentsVC : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UITextViewDelegate>

@property (strong, nonatomic) PFObject * pfPost;

@property (strong, nonatomic) PFObject * commentObject;

@property (weak, nonatomic) IBOutlet UITextView *cellCommentTextView;

@property (strong, nonatomic) NSMutableArray * commentsList;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *commentActionView;

//@property (weak, nonatomic) IBOutlet UITextView *commentTextView;

@property (weak, nonatomic) IBOutlet UIButton *commentPostButton;

@property (weak, nonatomic) IBOutlet UITextView *commentTextView2;

@property (strong, nonatomic) UIRefreshControl * refreshControl;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end
