//
//  PFHostingDetailViewController.m
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 11/16/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import "PFHostingDetailViewController.h"
#import "Gradient.h"
#import <Parse/Parse.h>


@implementation PFHostingDetailViewController


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    self.currentUser = [PFUser currentUser];
    
    self.allFriendsRelation = [[PFUser currentUser] objectForKey:@"friends"];
    NSLog(@"the number of sublayers, %@",self.navigationController.navigationBar.layer.sublayers);
    
    
    NSMutableArray * sublayers = [NSMutableArray arrayWithArray:self.navigationController.navigationBar.layer.sublayers];
    
    
    
    self.navigationController.navigationBar.layer.sublayers = sublayers;
    
    CAGradientLayer * whiteLayer = [Gradient white];
    
    CAGradientLayer * randomLayer = [Gradient gold];
    
    whiteLayer.frame = CGRectMake(0, -20, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height + 20);
    
    randomLayer.frame = CGRectMake(0, -20, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height + 20);
    
    [whiteLayer setStartPoint:CGPointMake(0.0, 0.5)];
    [whiteLayer setEndPoint:CGPointMake(1.0, 0.5)];
    [randomLayer setStartPoint:CGPointMake(0.0, 0.5)];
    [randomLayer setEndPoint:CGPointMake(1.0, 0.5)];
    
    NSLog(@"the number of sublayers, %@",self.navigationController.navigationBar.layer.sublayers);
    

    
    NSLog(@"the number of sublayers, %@",self.navigationController.navigationBar.layer.sublayers);
    
    self.tableView.delegate = self;
    
    self.allFriends = [NSArray new];
    
    self.pickFriends = [NSMutableArray new];
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithImage:[UIImage imageNamed:@"arrow-left"]
                                   style:UIBarButtonItemStylePlain
                                   target:self action:@selector(goBack)];
    
    UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithTitle:@"Create" style:UIBarButtonItemStyleBordered
                                                                  target:self
                                                                  action:@selector(savePartyTouch:)];
    
    
    self.navigationItem.leftBarButtonItem = backButton;
    
    self.navigationItem.rightBarButtonItem = barButton;
    
    
    [self.view addGestureRecognizer:tap];
    
    tap.cancelsTouchesInView = NO;
    
    [self.tableView setUserInteractionEnabled:YES];
    [self.tableView setAllowsSelection:YES];
    
}


- (void) goBack {
    
    NSLog(@"Back");
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    
    PFQuery * query7 = [PFUser query];
    [query7 includeKey:@"myRole"];
    [query7 findObjectsInBackgroundWithBlock:^(NSArray * objects, NSError * error) {
        
        self.currentUser = objects[0];
        self.rolling = [self.currentUser objectForKey:@"myRole"];
        if (self.rolling == NULL) {
            
            [PFCloud callFunctionInBackground:@"CheckRole" withParameters:@{@"":@""} block:^(NSString * results, NSError * error){
                
                if (!error) {
                    PFQuery * query5 = [PFUser query];
                    [query5 includeKey:@"myRole"];
                    [query5 findObjectsInBackgroundWithBlock:^(NSArray * objects, NSError * error){
                        self.currentUser = objects[0];
                        self.rolling = [self.currentUser objectForKey:@"myRole"];
                    }];
                    
                    
                }
                
            }];
        }
      
    }
     ];
    
    PFQuery * query = [self.allFriendsRelation query];
    [query orderByAscending:@"firstName"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error){
            
            NSLog(@"error %@ %@", error, [error userInfo]);
            
            
        }
        else{
            self.allFriends = objects;
            
            NSLog(@"**** PF Hosting ****");
            NSLog(@"%@", self.allFriends );
            NSLog(@"Count = %lu", self.allFriends.count);
            
            [self.tableView reloadData];
            
        }
        
    }];
    
    NSLog(@"**** PF Hosting ****");
    NSLog(@"%@", self.allFriends );
    NSLog(@"Count = %lu", self.allFriends.count);

    
    NSCalendar * day = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate * today = [NSDate date];
    NSDateComponents * comps = [NSDateComponents new];
    [comps setDay:1];
    NSDate * max = [day dateByAddingComponents:comps toDate:today options:0];
    [comps setHour:-3];
    
    [self.hostingPartyDatePicker setMinimumDate:today];
    [self.hostingPartyDatePicker setMaximumDate:max];
    
    //[self.tableView reloadData];
}

-(void)dismissKeyboard {
    [self.headlineTextField resignFirstResponder];
    [self.descriptionTextField resignFirstResponder];
    [self.addressTextField resignFirstResponder];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSString *) tableView: (UITableView *) tableView titleForHeaderInSection:(NSInteger)section {
    NSString * result = @"Friends";
    return result;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.allFriends.count;
}

- (UITableViewCell *) tableView: (UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * cellIdentifier = @"host";
    
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
//    if (cell == nil) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
//    }
//    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    
    PFUser * user = [self.allFriends objectAtIndex:indexPath.row];
    
    cell.textLabel.text = user[@"firstName"];
    //cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    if ([self.pickFriends containsObject:user.objectId]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
    
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"tableView didSelectRowAtIndexPath ");
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UITableViewCell * cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    PFUser * user = [self.allFriends objectAtIndex:indexPath.row];
    
    if (cell.accessoryType == UITableViewCellAccessoryNone) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [self.pickFriends addObject:user];
    }else {
        cell.accessoryType = UITableViewCellAccessoryNone;
        [self.pickFriends removeObject:user];
    }
}


- (IBAction)savePartyTouch:(id)sender {
    
    NSString * name = self.headlineTextField.text;
    NSString * description = self.descriptionTextField.text;
    NSString * address = self.addressTextField.text;
    
    NSDate * eventDate = [self.hostingPartyDatePicker date];
    
    PFACL * acl = [PFACL new];
    

    
    
    PFObject * event = [PFObject objectWithClassName:@"invitations"];
    
    PFRelation * friendsRel = [PFRelation alloc];
    
    friendsRel = [event relationForKey:@"friends"];
    
    
    for (int i = 0; i < self.pickFriends.count; i++) {
        [friendsRel addObject:[self.pickFriends objectAtIndex:i]];
        
        [acl setReadAccess:YES forUser:[self.pickFriends objectAtIndex:i]];
    }
    
    [acl setReadAccess:YES forUser:[PFUser currentUser]];
    [acl setWriteAccess:YES forUser:[PFUser currentUser]];
    
//    for (PFUser * user in self.pickFriends) {
//        [friendsRel addObject:user];
//    }
    
    
    //CLLocationCoordinate2D coordinate;
    
    CLGeocoder * geocoder = [CLGeocoder new];
    
    __block PFGeoPoint * geoPoint = [PFGeoPoint new];
    
    [geocoder geocodeAddressString:address
                 completionHandler:^(NSArray* placemarks, NSError* error){
                     //for (CLPlacemark* aPlacemark in placemarks)
                     //{
                         // Process the placemark.
                     
                     CLPlacemark * aPlacemark = [placemarks lastObject];
                     
                        CLLocation * coordinate = [aPlacemark location];
                     
                     CLLocationCoordinate2D cood = [coordinate coordinate];
                     
                         geoPoint = [PFGeoPoint geoPointWithLatitude:cood.latitude longitude:cood.longitude];
                     
                     NSLog(@"*** GEOPOINT = %@", geoPoint);
                     
                         event[@"location"] = geoPoint;
                     
                     [event save];
                     //}
                 }];
    
    
    // PFGeoPoint *geoPoint = [PFGeoPoint geoPointWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    
    //event[@"location"] = geoPoint;
    
    event[@"headline"] = name;
    
    event[@"description"] = description;
    
    event[@"address"] = address;
    
    event[@"date"] = eventDate;
    
    event[@"createdBy"] = [PFUser currentUser];
    
    [event setACL:acl];
    
    [event saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            // The object has been saved.
            NSLog(@"party saved");
        } else {
            // There was a problem, check error.description
            NSLog(@"party not saved");
        }
    }];
    
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}





@end
