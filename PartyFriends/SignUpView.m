//
//  SignUpView.m
//  PartyFriends
//
//  Created by Joshua Silva  on 6/24/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import "SignUpView.h"
#import "UIView+Toast.h"
#import "Reach.h"


@interface SignUpView ()

@end

@implementation SignUpView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.lastName.delegate = self;
    self.firstName.delegate = self;
    self.phoneNumber.delegate = self;
    self.password.delegate = self;
    self.passwordConfirm.delegate = self;
    self.eMail.delegate = self;
    self.confirmEmail.delegate = self;
    self.genderPicker.delegate = self;
    self.invitePin.delegate = self;
    
    
    self.confirmPhoneNumber.delegate = self;
    
    self.genderString = [[NSArray alloc] initWithObjects:@"Not Selected", @"M", @"F", nil];
    if (self.firstFriend ==nil) {
        self.firstFriend = [PFUser currentUser];
    }
    
    //self.firstFriend = [PFUser currentUser];
    if (self.firstFriend != nil) {
        
        if ([PFUser currentUser]) {
            [PFUser logOutInBackgroundWithBlock:^(NSError * error){
            
                if (error) {
                    UIAlertView * errorLogOutView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Unable to sign up at the moment." delegate:nil cancelButtonTitle:@"Try again" otherButtonTitles: nil];
                   
                    [self.navigationController popViewControllerAnimated:YES];
                    [errorLogOutView show];
                
                }else {
                
                NSLog(@"halog out from user!");
                }
                
                
                    
            
            }];
        }
    }
    
    self.activityIndicator.color = [UIColor colorWithRed:(142/255.0) green:(83/255.0) blue:(249/255.0) alpha:1.0];
    self.activityIndicator.hidden = YES;
    
    //[PFUser logOut];
    
}

-(void) activityOffAction{
    self.activityIndicator.hidden = YES;
    [self.activityIndicator stopAnimating];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
    
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return [self.genderString count];
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    self.gender = [[NSString alloc] initWithFormat:@"%@", [self.genderString objectAtIndex:row]];
    
    
}



-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return [self.genderString objectAtIndex:row];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backgroundTap:(id)sender {
    
    
        [self.view endEditing:YES];
    
    
}



- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y-200);
    [self.scrollView setContentOffset:scrollPoint animated:YES];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self.scrollView setContentOffset:CGPointZero animated:YES];
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)newPF:(id)sender {
    PFUser * firstFriend = [PFUser currentUser];
   // [self performSegueWithIdentifier:@"SecondSignUp" sender:self];
    if ([Reachability internetCheck]) {

        [sender setEnabled:NO];


    
    [PFUser logOut];
    
    
    NSString * newPassword = [self.password.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString * newConPassword = [self.passwordConfirm.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString * userGender = self.gender;
    
    
    NSString *firstname = [self.firstName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *lastname = [self.lastName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *username = [self.phoneNumber.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString * phone = [self.phoneNumber.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString * confirmPhone = [self.confirmPhoneNumber.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
//    if (![phone isEqualToString:confirmPhone]) {
//        UIAlertView *error = [[UIAlertView alloc] initWithTitle:@"PartyFriends" message:@"Please enter Phone Numbers that are identical" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [error show];

    
//        return;
//    }
    
    // trimming of email
    
    /*NSString * eMail = [self.eMail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString * confirmEmail = [self.confirmEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];*/
    
    
    
    NSRegularExpression * capitalRegex = [NSRegularExpression regularExpressionWithPattern:@"[A-Z]" options:0 error:nil];
    NSRegularExpression * numberRegex = [NSRegularExpression regularExpressionWithPattern:@"[0-9]" options:0 error:nil];
    NSRegularExpression * letterRegex = [NSRegularExpression regularExpressionWithPattern:@"[a-z]" options:0 error:nil];
    

    
    NSLog(@"Number of capital letters %ld", (unsigned long)[capitalRegex matchesInString:newPassword options:0 range:NSMakeRange(0, newPassword.length)].count );
    
    /*NSUInteger lettercount = (unsigned long)[letterRegex matchesInString:newPassword options:0 range:NSMakeRange(0, newPassword.length)].count;
    NSUInteger passwordcount = (unsigned long)[capitalRegex matchesInString:newPassword options:0 range:NSMakeRange(0, newPassword.length)].count; */
    
    NSUInteger numbercountPassword = (unsigned long)[numberRegex matchesInString:newPassword options:0 range: NSMakeRange(0, newPassword.length)].count;
    NSUInteger lowerCaseUsername = (unsigned long)[letterRegex matchesInString:username options:0 range:NSMakeRange(0, username.length)].count;
    NSUInteger upperCaseUsername = (unsigned long)[capitalRegex matchesInString:username options:0 range:NSMakeRange(0, username.length)].count;
    
    
    if ([firstname length ]==0 || [lastname length]== 0 || [username length]== 0 || [newPassword length] == 0 ) {
        UIAlertView *error = [[UIAlertView alloc] initWithTitle:@"Missing a field" message:@"please double check if all fields are filled in and the password matches" delegate:nil cancelButtonTitle:@"Thank you" otherButtonTitles: nil];
        [error show];
        [sender setEnabled:YES];
        [self activityOffAction];
        
    }else {
        if (![phone isEqualToString:confirmPhone]) {
            UIAlertView *error = [[UIAlertView alloc] initWithTitle:@"PartyFriends" message:@"Please enter Phone Numbers that are identical" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [error show];
            [sender setEnabled:YES];
            [self activityOffAction];
            return;
            
        }else {
        if (lowerCaseUsername != 0 || upperCaseUsername != 0 ) {
            UIAlertView * numberCheck = [[UIAlertView alloc] initWithTitle:@"Phone Number" message:@"There might be a letter in your number" delegate:nil cancelButtonTitle:@"okay" otherButtonTitles:nil];
            [numberCheck show];
            [sender setEnabled:YES];
            [self activityOffAction];
        }else {
            if (![self.eMail.text isEqualToString: self.confirmEmail.text]) {
                UIAlertView * emailCheck = [[UIAlertView alloc] initWithTitle:@"Email" message:@"your emails don't match each other" delegate:nil cancelButtonTitle:@"Mistakes happen ;)" otherButtonTitles:nil];
                [emailCheck show];
                [sender setEnabled:YES];
                [self activityOffAction];
            }else{
        if (![newPassword isEqualToString:newConPassword]) {
            UIAlertView * error = [[UIAlertView alloc]initWithTitle:@"Passwords don't match" message:@"Happens to the best of us" delegate:nil cancelButtonTitle:@"Thank you" otherButtonTitles: nil];
            [error show];
            [sender setEnabled:YES];
            [self activityOffAction];
        }else {
    if (newPassword.length <= 6|| newPassword.length >=12) {
        UIAlertView * theShitagain = [[UIAlertView alloc] initWithTitle:@"Password error" message:@"Password must be between 8 and 12 chacaters long" delegate:nil cancelButtonTitle:@"thank you :)" otherButtonTitles:nil];
        [theShitagain show];
        [sender setEnabled:YES];
        [self activityOffAction];
    }
    else{
    
    if (numbercountPassword == 0) {
        UIAlertView * theShit = [[UIAlertView alloc] initWithTitle:@"Password error" message:@"Please double check if your password has atleast a number" delegate:nil cancelButtonTitle:@"safety first!" otherButtonTitles:nil];
            [theShit show];
        [sender setEnabled:YES];
        [self activityOffAction];
    }
    else {
        if ([self.gender isEqualToString:@"Gender"]) {
            UIAlertView * genderAlert = [[UIAlertView alloc] initWithTitle:@"Gender" message:@"Please select a gender" delegate:nil cancelButtonTitle:@"Thank you" otherButtonTitles: nil];
            [genderAlert show];
            [sender setEnabled:YES];
            [self activityOffAction];
        }else{
            if (self.invitePin.text.length != 4) {
                UIAlertView * inviteAlert = [[UIAlertView alloc] initWithTitle:@"Invite Pin" message:@"Your pin can only be 4 digits" delegate:nil cancelButtonTitle:@"Thank you" otherButtonTitles: nil];
                [inviteAlert show];
                [sender setEnabled:YES];
                [self activityOffAction];
            }else{

            NSNumberFormatter * pinCode = [NSNumberFormatter new];
            pinCode.numberStyle = NSNumberFormatterDecimalStyle;
            NSNumber * pin = [pinCode numberFromString:self.invitePin.text];
 
        NSLog(@" checking to for the firstFriend:%@ ..... then for the gender:%@ or the other gender: %@",self.firstFriend,self.gender, userGender);
        
            PFUser * newUser = [PFUser user];
             //newUser[@"firstFriend"] = false;
            PFRelation * friend = [newUser relationForKey:@"friends"];
            [friend addObject:self.firstFriend];
            newUser.username = username;
            newUser.password = newPassword;
            newUser.email = _eMail.text;
            newUser[@"firstName"] = firstname;
            newUser[@"lastName"] = lastname;
            newUser[@"invitedBy"] = self.firstFriend;
                if (self.gender == nil || [self.gender isEqualToString:@"Not Selected"]) {
                }else{
                newUser[@"gender"] = self.gender;
                }
            newUser[@"status"] = @0;
            newUser[@"invitePin"] = self.invitePin.text;
        
            [newUser signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (error) {
                    
                    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"sorry" message:[error.userInfo objectForKey:@"error"] delegate:nil cancelButtonTitle:@"Please try again" otherButtonTitles: nil];
                    [alertview show];
                    [sender setEnabled:YES];
                    [self activityOffAction];
                    
                }
                else{
                    [sender setEnabled:YES];
                    [self activityOffAction];
                    [self performSegueWithIdentifier:@"SecondSignUp" sender:self];}
            }];
    
        }
    
    }
    }
    }
    }}
        }
        } }



    }else if (![Reachability internetCheck]) {
        
        [self.view makeToast:@"Try again: No internet connection" duration:3 position:CSToastPositionCenter];
    }
    
}





-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    [self.view endEditing:YES];
    
    return YES;
}



- (IBAction)cancelSignUp:(id)sender {
    
    [PFUser logOut];
    
}
             
             
@end
