//
//  PFCommentsTableViewCell.h
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 12/1/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Parse/Parse.h>


@interface PFCommentsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *comment;

@property (weak, nonatomic) IBOutlet UIImageView *commentSenderProfilePic;

@property (weak, nonatomic) IBOutlet UITextView *commentTextView;

@property (weak, nonatomic) IBOutlet UILabel *commentAuthor;
@property (strong, nonatomic) IBOutlet UILabel *timeStamp;

@property (strong, nonatomic) PFObject * commentObject;

@property (weak, nonatomic) IBOutlet UIButton *editCommentButton;

@end
