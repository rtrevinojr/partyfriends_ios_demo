//
//  PFVendorListTableViewController.m
//  PartyFriends
//
//  Created by Gilberto Silva on 12/2/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import "PFVendorListTableViewController.h"

#import "PFVendorContainerViewController.h"

#import "PFVendorTabBarController.h"


@interface PFVendorListTableViewController ()

@property (strong, nonatomic) PFObject * vendorObject;

@end

@implementation PFVendorListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.currentUser = [PFUser currentUser];

    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)viewDidAppear:(BOOL)animated{
    PFRelation *vendorAccounts = [self.currentUser relationForKey:@"vendors"];
    PFQuery *query = [vendorAccounts query];
    [query orderByAscending:@"createdAt"];
    [query findObjectsInBackgroundWithBlock:^(NSArray * objects, NSError * error){
        if(!error){
            self.myVendorAccounts = [NSMutableArray new];
            NSLog(@"vendors = %@",objects);
            self.myVendorAccounts = [[NSMutableArray alloc] initWithArray:objects];
            
            NSLog(@"vendorsAccounts = %@",self.myVendorAccounts);
            [self.tableView reloadData];
            
            
        }
    }];}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSUInteger count = self.myVendorAccounts.count ;
    NSLog(@"%tu",count);
    return self.myVendorAccounts.count + 1;
}

- (IBAction)editVendorProfile:(id)sender {
    
    PFVendorListTableViewCell * cell = (PFVendorListTableViewCell *) [[sender superview] superview];
    //NSString * objId = cell.objectId;
    
    //NSLog(@"*** Btn press. cell.objectId = %@", objId);
    //NSLog(@"*** cell.partyObject = %@", cell.partyObject);
    
    self.vendorObject = cell.vendorPFObject;
    
    //PFQuery * query = [PFQuery queryWithClassName:@"comments"];
    
    //self.selectedMessage = cell.partyObject;
    
    //[self performSegueWithIdentifier:@"vendoredit" sender:self];
    
    
}

- (PFVendorListTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PFVendorListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"vendorcell" forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[PFVendorListTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"vendorcell"];
    }
    
    // Configure the cell...
  
    if(indexPath.row == self.myVendorAccounts.count){
        cell.nameLabel.hidden = true;
        cell.editButton.hidden = true;
        cell.createNewButton.hidden = false;
    }
    else{
        PFObject *account = [PFObject objectWithClassName:@"vendor"];
        account = self.myVendorAccounts[[indexPath row]];
        
        cell.vendorPFObject = account;
        
        cell.createNewButton.hidden = true;
        cell.nameLabel.hidden = false;
        cell.editButton.hidden = false;
        cell.nameLabel.text = account[@"businessName"];
    }
    
    return cell;
}

-(IBAction)backToVendorAccount:(UIStoryboardSegue *) segue{


    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"vendoredit"]) {
        
        //PFVendorContainerViewController * vendorVC = [segue destinationViewController];
        
        PFVendorTabBarController * vendorVC = [segue destinationViewController];
        
        vendorVC.vendorPromoPFObject = self.vendorObject;
        
    }
}


@end
