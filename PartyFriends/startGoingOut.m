//
//  startGoingOut.m
//  PartyFriends
//
//  Created by Joshua Silva  on 8/6/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import "startGoingOut.h"

@interface startGoingOut ()

@end

@implementation startGoingOut



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor colorWithWhite:1.0 alpha:1.0  ];
    
    
    self.friends.delegate = self;

    
    
    self.waf = [[NSArray alloc] initWithObjects:@"waffles", @"pancakes", @"flapjacks", nil];
    

}

- (void)viewWillAppear:(BOOL)animated{
    
    

    NSCalendar * day = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate * today = [NSDate date];
    NSDateComponents * comps = [NSDateComponents new];
    [comps setDay:1];
    NSDate * max = [day dateByAddingComponents:comps toDate:today options:0];
    [comps setHour:-3];
   //NSDate * min = [day dateByAddingComponents:comps toDate:today options:0];
    
    
    [self.setTimer setMinimumDate:today];
    [self.setTimer setMaximumDate:max];
    

}



-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
   // [self.scrollView layoutIfNeeded];
    self.scrollView.contentSize = self.contentView.bounds.size;
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    
}


-(void)localNotificationGoingOut:(NSDate *)firetime {
    UILocalNotification * notification = [UILocalNotification  new];
    
    notification.fireDate = firetime;
    notification.alertBody = @"Party Time!";
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notification ];
    
    

}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return [self.waf count];
}


 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     
         static NSString * cellIdentifier = @"cell";
     
 UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
 
 // Configure the cell...
     
     cell.textLabel.text = [self.waf objectAtIndex:indexPath.row];
     
     
 return cell;
 }

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    NSLog(@"thank you god for everytihg!!!!!!#@#@#@");
    
}






/*

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)beginGoingOut:(id)sender {
    
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.timeZone = [NSTimeZone defaultTimeZone];
    dateFormatter.timeStyle = NSDateFormatterShortStyle;
    dateFormatter.dateStyle = NSDateFormatterShortStyle;
    
    NSString * dataString = [dateFormatter stringFromDate:self.setTimer.date    ];
    NSLog(@"the time you'd like to go out %@", dataString);
    [self localNotificationGoingOut:self.setTimer.date];
    
}
@end
