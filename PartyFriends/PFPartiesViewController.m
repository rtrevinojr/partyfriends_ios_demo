//
//  PFPartiesViewController.m
//  PartyFriends
//
//  Created by Joshua Silva  on 4/21/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import "PFPartiesViewController.h"
#import "PFPartiesTabBarViewController.h"

@interface PFPartiesViewController ()
@end
PFPartiesTabBarViewController * taBBar;
@implementation PFPartiesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc]
                                  initWithImage:[UIImage imageNamed:@"add-party"]
                                  style:UIBarButtonItemStylePlain
                                  target:self action:@selector(addPartyTouch)];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithImage:[UIImage imageNamed:@"arrow-left"]
                                   style:UIBarButtonItemStylePlain
                                   target:self action:@selector(goBack)];
    
    self.navigationItem.leftBarButtonItem = backButton;
    
    UIBarButtonItem * backBtn = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:self action:@selector(goBack)];
    
    self.navigationItem.rightBarButtonItem = addButton;
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}


- (IBAction)setIndex:(id)sender {
    
    switch (self.controlSegment.selectedSegmentIndex)
    {
        case 0:
            //First selected
            NSLog(@"segmented control = 0");
            [taBBar indexOne];
            //[self.tableView reloadData];
            //[self viewDidLoad];
            break;
        case 1:
            //Second Segment selected
            NSLog(@"segmented control = 1");
            [taBBar indexTwo];

            break;
                    case 2:
            NSLog(@"segmented control = 2");
            [taBBar indexThree];
                        break;
            
        default:
            break;
    }
    
}

- (void) addPartyTouch {
    NSLog(@"addPartyTouch");
    [self performSegueWithIdentifier:@"hostingdetail" sender:self];
}

- (void) goBack {
    
    [self performSegueWithIdentifier:@"backFromParty" sender:self];
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
