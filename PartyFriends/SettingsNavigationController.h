//
//  SettingsNavigationController.h
//  PartyFriends
//
//  Created by Joshua Silva  on 2/17/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsNavigationController : UINavigationController

@end
