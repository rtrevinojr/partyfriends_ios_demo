
//
//  AccountView.m
//  PartyFriends
//
//  Created by Joshua Silva  on 8/14/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import "AccountView.h"
#import "UIView+Toast.h"

@interface AccountView ()

@end

@implementation AccountView

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.currentUser == nil) {
        
    self.currentUser = [PFUser currentUser];
    }
    
    NSLog(@"getting something %@", self.accountInfo[@"address"]);
    
    self.firstName.delegate = self;
    self.lastName.delegate = self;
    self.phoneNumber.delegate = self;
    self.address.delegate = self;
    self.city.delegate = self;
    self.zipcode.delegate = self;
    self.email.delegate = self;
    self.password.delegate = self;
    self.comfirmPassword.delegate = self;
    self.state.delegate = self;
    
    
     //  self.theStates = [[NSArray alloc]initWithObjects: @"",@"AL",@"AK",@"AZ",@"AR",@"CA",@"CO",@"CT",@"DE",@"DC",@"FL",@"GA",@"HI",@"ID",@"IL",@"IN",@"IA",@"KS",@"KY",@"LA",@"ME",@"MD",@"MA",@"MI",@"MN",@"MS",@"MO",@"MT",@"NE",@"NV",@"NH",@"NJ",@"NM",@"NY",@"NC",@"ND",@"OH",@"OK",@"OR",@"PA",@"RI",@"SC",@"SD",@"TN",@"TX",@"UT",@"VT",@"VA",@"WA",@"WV",@"WI",@"WY",nil];
    
           self.theStates = @[ @"",@"AL",@"AK",@"AZ",@"AR",@"CA",@"CO",@"CT",@"DE",@"DC",@"FL",@"GA",@"HI",@"ID",@"IL",@"IN",@"IA",@"KS",@"KY",@"LA",@"ME",@"MD",@"MA",@"MI",@"MN",@"MS",@"MO",@"MT",@"NE",@"NV",@"NH",@"NJ",@"NM",@"NY",@"NC",@"ND",@"OH",@"OK",@"OR",@"PA",@"RI",@"SC",@"SD",@"TN",@"TX",@"UT",@"VT",@"VA",@"WA",@"WV",@"WI",@"WY"];
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:NO];
    
    
   // [self.state selectRow:34 inComponent:0 animated:YES];
    
    PFQuery * userAccount = [PFUser query];
    
    [userAccount whereKey:@"objectId" equalTo:[[PFUser currentUser] objectId]];
    
    [userAccount includeKey:@"accountInfo"];
    
    [userAccount getFirstObjectInBackgroundWithBlock:^(PFObject * object, NSError * error){
        
        if(!error){
            self.currentUser = (PFUser *)object;
            self.accountInfo = [self.currentUser objectForKey:@"accountInfo"];
            if (self.accountInfo == nil) {
                PFQuery * queryAccount = [PFQuery queryWithClassName:@"accountinfo"];
                [queryAccount whereKey:@"createdBy" equalTo:self.currentUser];
                
                [queryAccount getFirstObjectInBackgroundWithBlock:^(PFObject * object, NSError * error){
                    if (error.code == 101){
                        NSLog(@"error at finding a account row, time to creat a account object!!! ");
                        self.accountInfo = [PFObject objectWithClassName:@"accountinfo"];
                        
                    }else if(!error){
                        
                        
                        NSLog(@"this is going through the account query");
                        self.accountInfo = object;
                        NSLog(@"accountinfo query object = %@",self.accountInfo);
                        self.city.text = self.accountInfo[@"city"];
                        self.address.text = self.accountInfo[@"address"];
                        self.zipcode.text = self.accountInfo[@"zipcode"];
                        NSDate * userBirth = self.accountInfo[@"birthday"];
                        [self.birthPicker setDate:userBirth];
                        
                        
                    }
                    else{
                        [self.view makeToast:@"Unable to return your info - try again" duration:3 position:CSToastPositionCenter];
                    }}];
            }else{
                NSNumber * zipcodeNumber = self.accountInfo[@"zip"];
                            NSString * zipPlace = [NSString stringWithFormat:@"%@",zipcodeNumber];
                //self.zipcode.text = zipPlace;
            NSLog(@"is this wprking %@", self.accountInfo);
            self.city.text = self.accountInfo[@"city"];
            self.address.text = self.accountInfo[@"address"];
            NSDate * userBD = self.accountInfo[@"birthday"];
            [self.birthPicker setDate:userBD];
            
            NSLog(@"is number there?!?!? : %@", self.accountInfo[@"zip"]);
            NSString * userState = self.accountInfo[@"state"];
                
            int index =[self.theStates indexOfObject:userState];
                //NSInteger stateIndex= index;
             //self.zipcode.text = zipPlace;
            NSLog(@"zipplace12313123123    %u", index);
            [self.state selectRow:index inComponent:0 animated:YES];
           self.zipcode.text = zipPlace;
        
            }
        }
//        else if (error.code == 101 || object == nil) {
//            NSLog(@"time to check the accountinfo table");
//        
//    
//    
//    PFQuery * queryAccount = [PFQuery queryWithClassName:@"accountinfo"];
//    [queryAccount whereKey:@"createdBy" equalTo:self.currentUser];
//    
//    [queryAccount getFirstObjectInBackgroundWithBlock:^(PFObject * object, NSError * error){
//        if (!error || error.code == 101){
//            NSLog(@"error at finding a account row, time to creat a account object!!! ");
//        }else{
//            
//            
//            NSLog(@"this is going through the account query");
//            self.accountInfo = object;
//            
//                self.city.placeholder = self.accountInfo[@"city"];
//                self.address.placeholder = self.accountInfo[@"address"];
//                self.zipcode.placeholder = self.accountInfo[@"zipcode"];
//            NSDate * userBirth = self.accountInfo[@"birthday"];
//            [self.birthPicker setDate:userBirth];
//            NSString * userState = self.accountInfo[@"state"];
//            int index = [self.theStates indexOfObject:userState];
//            [self.state selectRow:34 inComponent:0 animated:YES];
//            
//            
//        }}];}else{
//            
//           
//            self.accountInfo = object;
//            NSLog(@"checking the viewWillApperadfhjash :%@ ",self.accountInfo);
//            self.city.text = self.accountInfo[@"city"];
//            self.address.placeholder = self.accountInfo[@"address"];
//            self.zipcode.placeholder = self.accountInfo[@"zipcode"];
//            NSLog(@"wheres the code going through?!?!?!");
//        }
        
        
        
    }];
    
    self.firstName.text = self.currentUser[@"firstName"];
    self.lastName.text = self.currentUser[@"lastName"];
    self.phoneNumber.text = self.currentUser.username;
   
    self.email.text = self.currentUser.email;
    NSLog(@"ahfhajfhajhfahfhasdjfhalhflakhfkjahfklhalkfhaklhfklajhf");
    

    if (self.accountInfo[@"birthday"]!=nil) {
        
    }
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
    
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return [self.theStates count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return [self.theStates objectAtIndex:row];
    
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    self.userState = [[NSString alloc] initWithFormat:@"%@", [self.theStates objectAtIndex:row]];
    
    
}



- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y-200);
    [self.scrollView setContentOffset:scrollPoint animated:YES];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self.scrollView setContentOffset:CGPointZero animated:YES];
    
}

- (IBAction)tapBackground:(id)sender {
    
    [self.view endEditing:YES];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)theChange:(id)sender {
    
    [sender setEnabled:NO];
    NSLog(@"is the change button being pressed");
    
    NSDate * birthDay = [self.birthPicker date];
    //NSString * userState =
    if (self.firstName.text != self.currentUser[@"firstName"]) {
        NSString * firstNameString = self.firstName.text;
        self.currentUser[@"firstName"] = firstNameString;
    } if (self.lastName.text != self.currentUser[@"lastName"]) {
        NSString * lastNameString = self.lastName.text;
        self.currentUser[@"lastName"] = lastNameString;
    }
    if (self.phoneNumber.text != self.currentUser.username) {
        NSString * usernameString = self.phoneNumber.text;
        self.currentUser.username = usernameString;
    }
    if (self.address.text != self.accountInfo[@"address"]) {
       
        NSString * addressString = self.address.text;
        self.accountInfo[@"address"] = addressString;
    }
    if (self.city.text != self.accountInfo[@"city"]) {
        NSString * cityString = self.city.text;
        self.accountInfo[@"city"] = cityString;
    }
   // if (self.zipcode.text != self.accountInfo[@"zip"]) {
        NSString * zipString = [self.zipcode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        NSNumberFormatter * zipCode = [NSNumberFormatter new];
        zipCode.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber * zip = [zipCode numberFromString:self.zipcode.text];
        NSString * zipcodeString = self.zipcode.text;
    
    //}
    if (self.email.text != self.currentUser.email) {
        NSString * emailString = self.email.text;
        self.currentUser.email = emailString;
    }
    if (self.phoneNumber.text.length !=10){
        UIAlertView * numberView = [[UIAlertView alloc] initWithTitle:@"Phone Number" message:@"there are more than 10 digtis in the Phone Number field" delegate:nil cancelButtonTitle:@"thank you" otherButtonTitles: nil];
        [numberView show];
    }else {
        if (self.zipcode.text.length != 5 || self.zipcode.text == nil || [self.zipcode.text isEqualToString:@""]) {
            UIAlertView * zipcodeAlert = [[UIAlertView alloc] initWithTitle:@"Zipcode" message:@"Zipcode field must have only 5 digits" delegate:nil cancelButtonTitle:@"thanks" otherButtonTitles:nil];
            [zipcodeAlert show];
        }else{
    self.accountInfo.ACL =[PFACL ACLWithUser:self.currentUser];
    self.accountInfo[@"birthday"] = birthDay;
     self.accountInfo[@"zip"] = zip;
            self.accountInfo[@"state"] = self.userState;

            NSLog(@"On key press user = %@", self.currentUser.objectId);
            [self.currentUser saveInBackgroundWithBlock:^(BOOL saveStatus, NSError * error){
                NSLog(@"On key press object = %@", self.accountInfo.objectId);

                if (!error) {
                    NSLog(@"the account+the currentUser was saved");
                    
                    if(self.accountInfo!= nil){
                    
                    [self.accountInfo saveInBackgroundWithBlock:^(BOOL status, NSError * error){
                        if (!error) {
                                                [self.view makeToast:@"Info Saved" duration:2 position:CSToastPositionCenter];
                            [sender setEnabled:YES];
                        }else {
                            [sender setEnabled:YES];
                            NSLog(@"the accountInfo did not save!!! ERROR=%@ .... %ld", error, (long)error.code);
                        }
                        
                    }];
                }
                
                }else{[sender setEnabled:YES];
                    NSLog(@"currentUser was not saved!");
                }
                
                
            }];
            
            NSLog(@"the accountInfo was able to save! ");
        }}
}



-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    [self.view endEditing:YES];
    
    return YES;
}
- (IBAction)passwordChange:(id)sender {
    

    
}
@end
