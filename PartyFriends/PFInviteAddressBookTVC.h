//
//  PFInviteAddressBookTVC.h
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 11/5/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MessageUI/MessageUI.h>
#import <Parse/Parse.h>


@interface PFInviteAddressBookTVC : UITableViewController <MFMessageComposeViewControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) PFUser * currentUser;
@property(strong, nonatomic) NSMutableArray * friends;
@property(strong, nonatomic) NSArray * steps;
@property (strong, nonatomic) NSString * check;



@end
