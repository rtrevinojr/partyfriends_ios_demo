//
//  PFVendorCreateViewController.m
//  PartyFriends
//
//  Created by Gilberto Silva on 12/2/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import "PFVendorCreateViewController.h"

#import "PFVendorTabBarController.h"

#import <Parse/Parse.h>


@interface PFVendorCreateViewController ()

@property (strong, nonatomic) PFObject* vendorCreatePFObject;
@property (weak, nonatomic) IBOutlet UIButton *saveVendorProfileBtn;

@end

@implementation PFVendorCreateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
    
    self.navigationItem.rightBarButtonItem = nil;
    
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
    [self.navigationItem.rightBarButtonItem setImage:nil];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    tap.cancelsTouchesInView = NO;
    
    //PFVendorTabBarController * container = (PFVendorTabBarController *) self.navigationController.parentViewController;
    
    PFVendorTabBarController * container = (PFVendorTabBarController *) self.parentViewController;
    
    NSLog(@"********* Parent VC = %@", container);
    
    self.vendorCreatePFObject = container.vendorPromoPFObject;
    
    
    [self.businessName setDelegate:self];
    [self.address setDelegate:self];
    [self.city setDelegate:self];
    [self.zipCode setDelegate:self];
    [self.vendorDescription setDelegate:self];
    [self.businessPhone setDelegate:self];
    [self.phone setDelegate:self];
    [self.businessEmail setDelegate:self];
    [self.email setDelegate:self];
    [self.businessFax setDelegate:self];
    [self.fax setDelegate:self];
    [self.website setDelegate:self];
    
    self.businessName.text = [self.vendorCreatePFObject objectForKey:@"businessName"];
    self.address.text = [self.vendorCreatePFObject objectForKey:@"address"];
    self.city.text = [self.vendorCreatePFObject objectForKey:@"city"];
    self.zipCode.text = [self.vendorCreatePFObject objectForKey:@"zip"];
    self.vendorDescription.text = [self.vendorCreatePFObject objectForKey:@"description"];
    self.businessPhone.text = [self.vendorCreatePFObject objectForKey:@"businessPhone"];
    self.phone.text = [self.vendorCreatePFObject objectForKey:@"phone"];
    self.businessEmail.text = [self.vendorCreatePFObject objectForKey:@"businessEmail"];
    self.email.text = [self.vendorCreatePFObject objectForKey:@"email"];
    self.businessFax.text = [self.vendorCreatePFObject objectForKey:@"businessFax"];
    self.fax.text = [self.vendorCreatePFObject objectForKey:@"fax"];
    self.website.text = [self.vendorCreatePFObject objectForKey:@"website"];
    
    //self.pfVendorScrollView.contentSize = CGSizeMake(3000, 2000);
    
    //[self.pfVendorScrollView setContentSize:CGSizeMake(320, self.view.frame.size.height* 1.5)];
    
    [self.pfVendorScrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height* 2.0)];
    
    self.statePicker.delegate = self;
    self.states = [[NSArray alloc]initWithObjects: @"AL",@"AK",@"AZ",@"AR",@"CA",@"CO",@"CT",@"DE",@"DC",@"FL",@"GA",@"HI",@"ID",@"IL",@"IN",@"IA",@"KS",@"KY",@"LA",@"ME",@"MD",@"MA",@"MI",@"MN",@"MS",@"MO",@"MT",@"NE",@"NV",@"NH",@"NJ",@"NM",@"NY",@"NC",@"ND",@"OH",@"OK",@"OR",@"PA",@"RI",@"SC",@"SD",@"TN",@"TX",@"UT",@"VT",@"VA",@"WA",@"WV",@"WI",@"WY",nil];
    
    // Do any additional setup after loading the view.
}



- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSString * objectId = self.vendorCreatePFObject.objectId;
    
    PFQuery * query = [PFQuery queryWithClassName:@"vendor"];
    
    [query whereKey:@"objectId" equalTo:objectId];
    
    NSArray * objectList = [query findObjects];
    
    self.vendorCreatePFObject = [objectList firstObject];
    
    
    //self.vendorCreatePFObject
    
    
    [self saveVendorProfileTouch:nil];
    
    
    [self viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
-(void)viewDidLayoutSubviews
{
    
    [self.pfVendorScrollView setContentSize:CGSizeMake(320, self.view.frame.size.height* 1.5)];
    
    // this will pick height automatically from device's height and multiply it with 1.5
}
 */

-(void)dismissKeyboard {
   
    [self.businessName resignFirstResponder];
    [self.address resignFirstResponder];
    [self.city resignFirstResponder];
    [self.zipCode resignFirstResponder];
    [self.vendorDescription resignFirstResponder];
    [self.businessPhone resignFirstResponder];
    [self.phone resignFirstResponder];
    [self.businessEmail resignFirstResponder];
    [self.email resignFirstResponder];
    [self.businessFax resignFirstResponder];
    [self.fax resignFirstResponder];
    [self.website resignFirstResponder];
    
    
}



- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
    
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return [self.states count];
}


-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    self.userState = [[NSString alloc] initWithFormat:@"%@", [self.states objectAtIndex:row]];

}



-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return [self.states objectAtIndex:row];
    
}

- (IBAction)saveVendorProfileTouch:(id)sender {
    
    if (self.businessName.text != nil)
        self.vendorCreatePFObject[@"businessName"] = self.businessName.text;
    if (self.address.text != nil)
        self.vendorCreatePFObject[@"address"] = self.address.text;
    if (self.city.text != nil)
        self.vendorCreatePFObject[@"city"] = self.city.text;
    if (self.zipCode.text != nil)
        self.vendorCreatePFObject[@"zip"] = self.zipCode.text;
    if (self.vendorDescription.text != nil)
        self.vendorCreatePFObject[@"description"] = self.vendorDescription.text;
    if (self.businessPhone.text != nil)
        self.vendorCreatePFObject[@"businessPhone"] = self.businessPhone.text;
    if (self.phone.text != nil)
        self.vendorCreatePFObject[@"phone"] = self.phone.text;
    if (self.businessEmail.text != nil)
        self.vendorCreatePFObject[@"businessEmail"] = self.businessEmail.text;
    if (self.email.text != nil)
        self.vendorCreatePFObject[@"email"] = self.email.text;
    if (self.businessFax.text != nil)
        self.vendorCreatePFObject[@"businessFax"] = self.businessFax.text;
    if (self.fax.text != nil)
        self.vendorCreatePFObject[@"fax"] = self.fax.text;
    if (self.website.text != nil)
        self.vendorCreatePFObject[@"website"] = self.website.text;
    
    [self.vendorCreatePFObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            // The object has been saved.
            NSLog(@"vendor profile saved");
        } else {
            // There was a problem, check error.description
            NSLog(@"vendor profile not saved");
        }
    }];
    
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)create:(id)sender {
    
    NSString *descriptionPlaceHolder = @"Description";
    if(descriptionPlaceHolder == self.vendorDescription.text){
        
    }


                     
                     
                     NSString * zipString = [self.zipCode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                     NSString * phoneString = [self.businessPhone.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                     
                     NSRegularExpression * capitalRegex = [NSRegularExpression regularExpressionWithPattern:@"[A-Z]" options:0 error:nil];
                     NSRegularExpression * numberRegex = [NSRegularExpression regularExpressionWithPattern:@"[0-9]" options:0 error:nil];
                     NSRegularExpression * letterRegex = [NSRegularExpression regularExpressionWithPattern:@"[a-z]" options:0 error:nil];
                     
                     
                     NSUInteger phoneNumberletter = (unsigned long)[letterRegex matchesInString:phoneString options:0 range:NSMakeRange(0, phoneString.length)].count;
                     NSUInteger phoneNumberCap = (unsigned long)[capitalRegex matchesInString:phoneString options:0 range:NSMakeRange(0, phoneString.length)].count;
                   
                     NSUInteger zipCheckLetter = (unsigned long)[letterRegex matchesInString:zipString options:0 range:NSMakeRange(0, zipString.length)].count;
                     NSUInteger zipCheckCap = (unsigned long)[capitalRegex matchesInString:zipString options:0 range:NSMakeRange(0, zipString.length)].count;
                     
                     //NSUInteger zipCheck = (unsigned long)[numberRegex matchesInString:zipString options:0 range: NSMakeRange(0, zipString.length)].count;
                     
                     if([zipString length] != 0 && self.businessName.text.length != 0 && self.businessPhone.text.length != 0 && self.city.text.length != 0 && self.address.text.length != 0 && self.businessEmail.text.length != 0){
                     if ([zipString length] == 5 && zipCheckLetter == 0 && zipCheckCap == 0) {
                     
                     if (phoneString.length == 10 && phoneNumberCap == 0 && phoneNumberletter == 0) {
                         
                         NSNumberFormatter * zipCode = [NSNumberFormatter new];
                         zipCode.numberStyle = NSNumberFormatterDecimalStyle;
                         NSNumber * zip = [zipCode numberFromString:self.zipCode.text];
                         
                         NSString *address = [NSString stringWithFormat:@"%@ %@ %@ %@", self.address.text,self.city.text, self.userState, self.zipCode.text];
                         CLGeocoder * geocoder = [CLGeocoder new];
                         
                         __block PFGeoPoint * geoPoint = [PFGeoPoint new];
                         
                         
                         [geocoder geocodeAddressString:address
                                      completionHandler:^(NSArray* placemarks, NSError* error){
                                          //for (CLPlacemark* aPlacemark in placemarks)
                                          //{
                                          // Process the placemark.
                                          
                                          CLPlacemark * aPlacemark = [placemarks lastObject];
                                          
                                          CLLocation * coordinate = [aPlacemark location];
                                          
                                          CLLocationCoordinate2D cood = [coordinate coordinate];
                                          
                                          geoPoint = [PFGeoPoint geoPointWithLatitude:cood.latitude longitude:cood.longitude];
                                          
                                          NSLog(@"*** GEOPOINT = %@", geoPoint);
                                          
                                          
                           
                                          PFACL *acl  = [PFACL new];
                                          [acl setPublicReadAccess:true];
                                          [acl setPublicWriteAccess:true];
                                          
                                          
                                          
                                          PFObject *vendorAccount = [PFObject objectWithClassName:@"vendor"];
                                          vendorAccount[@"businessName"] = self.businessName.text;
                                          vendorAccount[@"address"] = self.address.text;
                                          vendorAccount[@"city"] = self.city.text;
                                          vendorAccount[@"state"] = self.userState;
                                          vendorAccount[@"zip"] = zip;
                                          vendorAccount[@"description"] = self.vendorDescription.text;
                                          vendorAccount[@"businessPhone"] = self.businessPhone.text;
                                          vendorAccount[@"phone"] = self.phone.text;
                                          vendorAccount[@"businessEmail"] = self.businessEmail.text;
                                          vendorAccount[@"email"] = self.email.text;
                                          vendorAccount[@"businessFax"] = self.businessFax.text;
                                          vendorAccount[@"fax"] = self.fax.text;
                                          vendorAccount[@"website"] = self.website.text;
                                          vendorAccount[@"location"] = geoPoint;
                                          [vendorAccount setACL:acl];
                                          [vendorAccount saveInBackgroundWithBlock:^(BOOL succeded, NSError * error){
                                                 if(!error)
                                                 {
                                                     
                                                     [self performSegueWithIdentifier:@"exitView" sender:self];
                                                     
                                                 }
                                                 else{
                                                     
                                                     UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error" message:[error.userInfo objectForKey:@"error"] delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
                                                     [alertview show];
                                                     
                                                 }
                                                 
                                             }];
                                           }];

                         
                     }
                     else{
                         
                         UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Business Phone number should be 10 digits only. No characters." delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
                         [alertview show];
                     }
                     }
                     else{
                         UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Zip should be 5 digits." delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
                         [alertview show];
                     }
                     }
                     else{
                         UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Make sure all required fields are filled." delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
                         [alertview show];
                     }
                
     
    

    //[self backButton:nil];
    
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex  {
   if(buttonIndex == [alertView cancelButtonIndex]){
        //[self performSegueWithIdentifier:@"exitView" sender:self];
 
    }
}

- (IBAction)backButton:(id)sender {
}

@end
