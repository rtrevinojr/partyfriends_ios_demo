//
//  TableViewController.h
//  PartyFriends
//
//  Created by Joshua Silva  on 6/28/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewController : UITableViewController

@end
