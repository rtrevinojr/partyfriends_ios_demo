//
//  PFContactUsView.h
//  PartyFriends
//
//  Created by Joshua Silva  on 3/30/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PFContactUsView : UIViewController

@property (strong, nonatomic) NSString * whereFrom;
@property (strong, nonatomic) IBOutlet UITextView *textView;

@property (strong, nonatomic) IBOutlet UILabel *elPaso;


@end
