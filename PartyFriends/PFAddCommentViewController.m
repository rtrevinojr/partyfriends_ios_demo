//
//  PFAddCommentViewController.m
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 11/23/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import "PFAddCommentViewController.h"

#import <Parse/Parse.h>


@implementation PFAddCommentViewController


- (void) viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"PFAddCommentVC viewDidLoad self.partyObject = %@", self.partyObject);
    
    
    self.commentTextView.delegate = self;
    
    PFObject * commentObj = self.commentObject;
    
    NSLog(@"**** PFAddCommentViewController Edit. self.commentObject = %@", commentObj);
    
    self.commentTextView.text = [commentObj objectForKey:@"comment"];
    
    
    UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithTitle:@"Delete" style:UIBarButtonItemStyleBordered
                                                target:self
                                                                  action:@selector(deleteCommentTouch:)];
    
//    
//    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
//                                   initWithImage:[UIImage imageNamed:@"arrow-left"]
//                                   style:UIBarButtonItemStylePlain
//                                   target:self action:@selector(goBack)];
//    
//    
//    self.navigationItem.leftBarButtonItem = backButton;
    
   // UIImage *image = [[UIImage imageNamed:@"GoldBackButton"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    UIButton * backButtonView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [backButtonView addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [backButtonView setBackgroundImage:[UIImage imageNamed:@"GoldBackButton"] forState:UIControlStateNormal];
    
    UIBarButtonItem * backBUTTON = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
    self.navigationItem.leftBarButtonItem = backBUTTON;
    
    self.navigationItem.rightBarButtonItem = barButton;
    
    
    
//    UIButton * deleteButtonView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
//    [backButtonView addTarget:self action:@selector(deleteCommentTouch:) forControlEvents:UIControlEventTouchUpInside];
//    [backButtonView setBackgroundImage:[UIImage imageNamed:@"refreshGold"] forState:UIControlStateNormal];
//    
//    UIBarButtonItem * delete = [[UIBarButtonItem alloc] initWithCustomView:deleteButtonView];
//    
//    
//    self.navigationItem.rightBarButtonItem = delete;
    

}




- (void) postComment
{
    NSLog(@"postComment method");
}


- (void) goBack
{
    NSLog(@"Back");
    [self.navigationController popViewControllerAnimated:YES];
}



- (void) viewWillAppear: (BOOL) animated
{
    [super viewWillAppear: animated];
    
    [self.commentTextView becomeFirstResponder];
    
}

-(IBAction) textFieldDoneEditing : (id) sender {
    [sender resignFirstResponder];
    [sender becomeFirstResponder];
}

- (IBAction)deleteCommentTouch:(id)sender {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"PartyFriends"
                                  message:@"Are you sure you want to delete this comment?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   PFQuery *query = [PFQuery queryWithClassName:@"comment"];
                                   
                                   [query whereKey:@"objectId" equalTo:[self.commentObject objectId]];
                                   
                                   [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                                       if (!object) {
                                           NSLog(@"The getFirstObject request failed.");
                                       } else {
                                           NSLog(@"Successfully retrieved the object.");
                                           [object deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                                               if (succeeded && !error) {
                                                   NSLog(@"Image deleted from Parse");
                                                   [self.navigationController popViewControllerAnimated:YES];
                                               } else {
                                                   NSLog(@"error: %@", error);
                                               }
                                           }];
                                       }
                                   }];
                                   
                                   //Handel your yes please button action here
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    UIAlertAction* cancelButton = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   //Handel your yes please button action here
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    [alert addAction:okButton];
    [alert addAction:cancelButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    

}

- (IBAction)postComment:(id)sender {
    
    NSLog(@"postComment ");
    
//    PFObject * message = [PFObject objectWithClassName:@"comment"];
//    
//    PFRelation * friendsRel = [PFRelation alloc];
//    
//    friendsRel = [message relationForKey:@"party"];
//    
//    NSString * message_string = [NSString stringWithFormat:@"%@", self.commentTextView.text];
//    
//    message[@"comment"] = message_string;
//    
//    if (self.partyObject != nil) {
//        message[@"party"] =  self.partyObject;
//    }
//    else {
//        NSLog(@"************* self.partyObject is nil");
//    }
    
    
    if ([self.commentTextView.text isEqualToString:@""]) {
        
        NSLog(@"comment is nil");
        
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"PartyFriends"
                                      message:@"Your comment is empty"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       //Handel your yes please button action here
                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                       
                                   }];
        
        [alert addAction:okButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        return;
    }
    
    
    
    PFQuery * query = [PFQuery queryWithClassName:@"comment"];
    
    [query whereKey:@"objectId" equalTo:self.commentObject.objectId];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        
        if (!error) {
            PFObject * comment = [objects firstObject];
            
            comment[@"comment"] = self.commentTextView.text;
            
            [comment saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                if (!error) {
                    NSLog(@"message save in background");
                    
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }];
        }
    }];
    

//    [message saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
//        if (!error) {
//            NSLog(@"message save in background");
//        }
//    }];
    
    //[self dismissViewControllerAnimated: YES completion: nil];
    //[self.navigationController popViewControllerAnimated:YES];
    
}

/*
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    NSLog(@"textViewShouldBeginEditing");
    
    if (textView == self.commentTextView) {
        return NO;
    }
    return YES;
}
*/

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return textView.text.length + (text.length - range.length) <= 140;
}

- (void) textViewDidChange:(UITextView *)textView
{
    NSLog(@"textViewDidChange");
}



- (void) textViewDidEndEditing:(UITextView *)textView
{
    NSLog(@"textViewDidEndEditing");
}


@end
