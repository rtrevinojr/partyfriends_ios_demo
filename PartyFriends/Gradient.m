//
//  Gradient.m
//  PartyFriends
//
//  Created by Joshua Silva  on 2/26/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import "Gradient.h"

@implementation Gradient



+(CAGradientLayer *) random{
    
    UIColor * color = [UIColor colorWithRed:(0/255.0)green:(100/255.0)blue:(200/255.0)alpha:1.0];
    UIColor * other = [UIColor colorWithRed:(200/255.0)green:(0/255.0)blue:(0/255.0)alpha:1.0];
    
    NSArray * colors = [NSArray arrayWithObjects:(id)color.CGColor, other.CGColor,nil];
    NSNumber * stopOne = [NSNumber  numberWithFloat:0.2];
    NSNumber * stopTwo = [NSNumber numberWithFloat:.8];
    
    NSArray * locations = [NSArray arrayWithObjects:stopOne, stopTwo, nil];
    
    CAGradientLayer * headLayer = [CAGradientLayer layer];
    
    headLayer.colors = colors;
    
    headLayer.locations = locations;
    
    return headLayer;
    
}

+(CAGradientLayer *) gold{
    //The brown used in on android
    // UIColor * color = [UIColor colorWithRed:(204/255.0)green:(139/255.0)blue:(105/255.0)alpha:1.0];
    
    // Aimming for a darker brown
    UIColor * color = [UIColor colorWithRed:(139/255.0)green:(69/255.0)blue:(19/255.0)alpha:1.0];
    
    UIColor * other = [UIColor colorWithRed:252.0/255.0 green:194/255.0 blue:0 alpha:.6];
    
    UIColor * color2 = [UIColor colorWithRed:(0/255.0)green:(0/255.0)blue:(0/255.0)alpha:1.0];
    
    // light brown
    //[UIColor colorWithRed:(204/255.0)green:(139/255.0)blue:(105/255.0)alpha:1.0];
    
    //    UIColor * color = [UIColor colorWithRed:(139/255.0)green:(105/255.0)blue:(20/255.0)alpha:204.0];
    //    UIColor * other = [UIColor colorWithRed:139/255.0 green:105/255.0 blue:20/255.0 alpha:204.0];
    //    UIColor * color2 = [UIColor colorWithRed:(122/255.0)green:(255/255.0)blue:(215/255.0)alpha:0];
    
    NSArray * colors = [NSArray arrayWithObjects:(id)color.CGColor, other.CGColor,color.CGColor, nil];
    NSNumber * stopOne = [NSNumber  numberWithFloat:0.2];
    NSNumber * stopTwo = [NSNumber numberWithFloat:.5];
    NSNumber * stopThree = [NSNumber numberWithFloat:0.8];
    
    NSArray * locations = [NSArray arrayWithObjects:stopOne, stopTwo,stopThree, nil];
    
    CAGradientLayer * headLayer = [CAGradientLayer layer];
    
    headLayer.colors = colors;
    
    headLayer.locations = locations;
    
    return headLayer;
    
    
    
}

+(CAGradientLayer *) goldTwo{
    //The brown used in on android
    // UIColor * color = [UIColor colorWithRed:(204/255.0)green:(139/255.0)blue:(105/255.0)alpha:1.0];
    
    // Aimming for a darker brown
    UIColor * color = [UIColor colorWithRed:(0/255.0)green:(0/255.0)blue:(0/255.0)alpha:1.0];
    UIColor * other = [UIColor colorWithRed:252.0/255.0 green:194/255.0 blue:0 alpha:1];
    UIColor * color2 = [UIColor colorWithRed:(0/255.0)green:(0/255.0)blue:(0/255.0)alpha:1.0];
    
    // light brown
    //[UIColor colorWithRed:(204/255.0)green:(139/255.0)blue:(105/255.0)alpha:1.0];
    
    //    UIColor * color = [UIColor colorWithRed:(139/255.0)green:(105/255.0)blue:(20/255.0)alpha:204.0];
    //    UIColor * other = [UIColor colorWithRed:139/255.0 green:105/255.0 blue:20/255.0 alpha:204.0];
    //    UIColor * color2 = [UIColor colorWithRed:(122/255.0)green:(255/255.0)blue:(215/255.0)alpha:0];
    
    NSArray * colors = [NSArray arrayWithObjects:(id)color.CGColor, other.CGColor,color2.CGColor, nil];
    NSNumber * stopOne = [NSNumber  numberWithFloat:-1.0];
    NSNumber * stopTwo = [NSNumber numberWithFloat:.5];
    NSNumber * stopThree = [NSNumber numberWithFloat:2.0];
    
    NSArray * locations = [NSArray arrayWithObjects:stopOne, stopTwo,stopThree, nil];
    
    CAGradientLayer * headLayer = [CAGradientLayer layer];
    
    headLayer.colors = colors;
    
    headLayer.locations = locations;
    
    return headLayer;
    
    
    
}

+(CAGradientLayer *) white{
    
    UIColor * color = [UIColor colorWithRed:(255/255.0)green:(255/255.0)blue:(255/255.0)alpha:1.0];
    UIColor * other = [UIColor colorWithRed:(0/255.0)green:(0/255.0)blue:(0/255.0)alpha:1.0];
    
    NSArray * colors = [NSArray arrayWithObjects:(id)color.CGColor, other.CGColor,nil];
    NSNumber * stopOne = [NSNumber  numberWithFloat:0.2];
    NSNumber * stopTwo = [NSNumber numberWithFloat:.8];
    
    NSArray * locations = [NSArray arrayWithObjects:stopOne, stopTwo, nil];
    
    CAGradientLayer * headLayer = [CAGradientLayer layer];
    
    headLayer.colors = colors;
    
    headLayer.locations = locations;
    
    return headLayer;
    
}


+(CAGradientLayer *) black{
    
    UIColor * color = [UIColor colorWithRed:(0/255.0)green:(0/255.0)blue:(0/255.0)alpha:1.0];
    UIColor * other = [UIColor colorWithRed:(0/255.0)green:(0/255.0)blue:(0/255.0)alpha:1.0];
    
    NSArray * colors = [NSArray arrayWithObjects:(id)color.CGColor, other.CGColor,nil];
    NSNumber * stopOne = [NSNumber  numberWithFloat:0.2];
    NSNumber * stopTwo = [NSNumber numberWithFloat:.8];
    
    NSArray * locations = [NSArray arrayWithObjects:stopOne, stopTwo, nil];
    
    CAGradientLayer * headLayer = [CAGradientLayer layer];
    
    headLayer.colors = colors;
    
    headLayer.locations = locations;
    
    return headLayer;
    
}


@end
