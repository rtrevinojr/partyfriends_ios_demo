//
//  tabBarController.m
//  PartyFriends
//
//  Created by Joshua Silva  on 8/4/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import "tabBarController.h"
#import "MapView.h"
#import "NewsFeed.h"
#import "PFRefresh.h"

@interface tabBarController ()

@end

@implementation tabBarController {
    
    MapView * childViewMap;
    
    NewsFeed * childNewsFeed;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    
    [self.tabBar setHidden:YES];
    childViewMap = [[self viewControllers ] firstObject];

   // childNewsFeed = [[self viewControllers] objectAtIndex:1];
    
   // UINavigationController * nav = (UIViewController *) childNewsFeed;
    //NewsFeed * NewsTVC = (NewsFeed *)nav.topViewController;
    
    
    [self refreshAll];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];

    [self refreshAll];


}

-(void)refreshAll{

    NSMutableDictionary * dictionary = [NSMutableDictionary new];
    int batteryLevel = [[UIDevice currentDevice] batteryLevel] * 100;
    NSNumber * battery = [NSNumber numberWithInt:batteryLevel];
    self.postObjectArray = [NSMutableArray new];
    self.partyObjectArray = [NSMutableArray new];
//    dictionary[@"batteryPercentage"] = battery;
    //@{@"batteryPercentage": battery}
    NSLog(@"Before the cloudFunction the battery value %@ %d ", battery, batteryLevel);
    [PFCloud callFunctionInBackground:@"Refresh" withParameters:@{@"batteryPercentage": battery} block:^(NSArray * objects, NSError * error) {
        NSLog(@"right after the cloudFunction");
        
        if (error){
            UINavigationController * nav = [[self viewControllers] objectAtIndex:1];
            NewsFeed * NewsTVC = (NewsFeed *)nav.topViewController;
            
            NSLog(@"theres an erRor %@", error);
            [NewsTVC failedRefresh];
            [childNewsFeed failedRefresh];
            
        }else{
            
            self.refreshArray = [NSMutableArray new];
            
            NSLog(@"Feed query objects count = %lu",[objects count]);
            self.refreshArray = [NSMutableArray arrayWithArray:objects];
//            self.postObjectArray = [NSMutableArray arrayWithCapacity:[self.refreshArray count]];
//            self.partyObjectArray = [NSMutableArray arrayWithCapacity:[self.refreshArray count]];
            
            NSLog(@"log the object %lu", (unsigned long)[self.postObjectArray count]);
            
            //childViewMap.allArray = [NSMutableArray arrayWithArray:self.refreshArray];
            self.pinsArray = [NSMutableArray new];
            self.cellsArray = [NSMutableArray new];
           
            if(self.refreshArray.count != 0 || self.refreshArray != nil){
//           self.pinsArray[0] = self.refreshArray[0];
//            self.cellsArray[0] = self.refreshArray[0];
                PFUser * userUpdate = self.refreshArray[0];
                NSNumber * request = userUpdate[@"request"];
                if ([request isEqual: @0]) {
                    ActionView * toAction = (ActionView *) [self parentViewController];
                    
                    [toAction clearedUpdate];
                    //[toAction updateCheck];
                    
                }else{
                    ActionView * toAction = (ActionView *) [self parentViewController];
                    
                    //[toAction clearedUpdate];
                    [toAction updateCheck];
                    
                }
                
                }
            
            NSLog(@"refreshArray %lu", (unsigned long)[self.refreshArray count]);
            
            NSMutableArray *fetchObjects = [NSMutableArray new];
            for (int i =0 ; [self.refreshArray count] >i; i++) {
                PFUser * user = self.refreshArray[i];
                
                PFObject * party = user[@"lastParty"];
                
                if(party != nil){
                    
                    [fetchObjects addObject:party];
                
                }
                
                PFObject * post = user[@"lastPost"];
                
                if(post != nil){
                    
                    [fetchObjects addObject:post];
                    
                }
                
                if ([user[@"status"] isEqual:@1] || [user[@"status"] isEqual:@2]) {
                    [self.pinsArray addObject:user];
                }
                NSLog(@"looking for NULL %@ \n %@ \n %@",user, party, post);
                
                PFRefresh * pfObject = [[PFRefresh alloc] initWithObject:i user:user party:party post:post];
                
                NSLog(@"new PFObject %@", pfObject);
                
                
                [self.cellsArray addObject:pfObject];
                
                
                
                

                
            }
            
            
            UINavigationController * nav = [[self viewControllers] objectAtIndex:1];
            NewsFeed * NewsTVC = (NewsFeed *)nav.topViewController;
            
            NewsTVC.allArray = [NSMutableArray arrayWithArray:self.cellsArray];
            //  NewsTVC.allArray = [NSMutableArray arrayWithArray:self.refreshArray];
            
            [NewsTVC refreshTable];
            
            [childViewMap customPins:self.pinsArray];
            //getting the currentUser's update of their party and changing the "edited the current location" text field
//            PFObject * currentUserParty = userUpdate[@"lastParty"];
//            ActionView * toAction = (ActionView *) [self parentViewController];
//            if (currentUserParty[@"headline"] == nil || [currentUserParty[@"headline"] isEqualToString:@""]) {
//                toAction.editLocation.placeholder = @"Where are you?";
//            }else{
//            toAction.editLocation.placeholder =
//                currentUserParty[@"headline"];
//            }
            
            
            

            
//            int l = (int) [self.refreshArray count];
//            
//            int m = (int) [self.refreshArray count];
//
//            bool isPartyIndex = false;
//            bool isPostIndex = false;
//            
//            for(int i = (int)([self.refreshArray count] - 1); i >= 0; i--){
//                
//                PFUser * checkingUsers = [self.refreshArray objectAtIndex:i];
////                [self.postObjectArray addObject:[NSNull null]];
//               // [self.partyObjectArray addObject:[NSNull null]];
//                
//                [self.partyObjectArray setObject:[NSNull null] atIndexedSubscript:i];
//                [self.postObjectArray setObject:[NSNull null] atIndexedSubscript:(i)];
//                
//                PFObject * party = checkingUsers[@"lastParty"];
//                
//                PFObject * post = checkingUsers[@"lastPost"];
//
//                
//                
//                if(party != nil && !isPartyIndex){
//                    
//                    l = i;
//                    isPartyIndex = true;
//                    NSLog(@"Index l = %d", l);
//                    
//                }
//                if(post != nil && !isPostIndex){
//                    
//                    m = i;
//                    isPostIndex = true;
//                    NSLog(@"Index m = %d", m);
//                    
//                }
//                
//                if(isPostIndex && isPartyIndex){
//                    
//                    break;
//                    
//                }
//                
//            }
//            
//            if(m > l){
//                
//                l = m;
//                
//            }
//            
//            
//            for (int i = 0; [self.refreshArray count]>i; i++){
//                PFUser * checkingUsers = [self.refreshArray objectAtIndex:i];
////                if(checkingUsers.objectId != [[PFUser currentUser] objectId]){
////            if(checkingUsers[@"lastParty"] != nil) {
////                if ([checkingUsers[@"status"]  isEqual: @2] || [checkingUsers[@"status"] isEqual: @1]){
////                    self.cellsArray[l] = self.refreshArray[i];
////                    l++;
////                }
////            }
//                [self.cellsArray addObject:checkingUsers];
//                
//                        __block PFObject * party = checkingUsers[@"lastParty"];
//                        __block PFObject * post = checkingUsers[@"lastPost"];
//                
//        
//                
////                [self.partyObjectArray addObject:party];
////                [self.postObjectArray addObject:post];
//
//
//                        [party fetchInBackgroundWithBlock:^(PFObject * object, NSError * error){
//                
//                
//                            if (!error) {
//                                
//                                party = object;
////                                [self.partyObjectArray addObject:party];
//                                //[self.partyObjectArray setObject:party atIndexedSubscript:i];
//                                
//                                if(party != nil && ([checkingUsers[@"status"] isEqual:@1] || [checkingUsers[@"status"] isEqual:@2])){
//                                    
//                                    [self.pinsArray addObject:checkingUsers]                         ;
//                                    [self.partyObjectArray setObject:party atIndexedSubscript:i];
//                                    
//                                    NSLog(@"Fetch party, have pin for user %@", checkingUsers[@"firstName"]);
//                                    
//                                    
//                                }
//                                
//
//                                
//                                if (l == i) {
//                                
//                                    NSLog(@"Fetch party complete, i = %d", i);
//
//                                    UINavigationController * nav = [[self viewControllers] objectAtIndex:1];
//                                    NewsFeed * NewsTVC = (NewsFeed *)nav.topViewController;
//                                
//                                    NewsTVC.allArray = [NSMutableArray arrayWithArray:self.cellsArray];
//                                    //  NewsTVC.allArray = [NSMutableArray arrayWithArray:self.refreshArray];
//                                    [NewsTVC refreshTable];
//                                    [childViewMap customPins:self.pinsArray];
//                                                                
//                                }
//                                
//
//                                
//                                childViewMap.allArray = [NSMutableArray arrayWithArray:self.pinsArray];
//                                
//                                
//                            
//                                
//                
//                            }
//                            else{
//                                        
//                                            NSLog(@"Fetch party at i = %d, error = %@",i, error);
//                                        
//                                        }
//                            
//                                                            NSLog(@"self.refreshArray count = %lu, i = %d", [self.refreshArray count], i);
//                            
//
//                
//                            
//
//                
//                            
//                        }];
//                
//                [post fetchInBackgroundWithBlock:^(PFObject * object, NSError * error){
//                    
//                    if(!error){
//                        
//                        post = object;
//                        NSLog(@"thePOST %@", post);
//                        
//                        [self.postObjectArray setObject:post atIndexedSubscript:i];
//                   //[self.postObjectArray objectAtIndex:i];
////                        [self.postObjectArray setObject:post atIndexedSubscript:i];
//                        //[self.postObjectArray addObject:post];
//                        NSLog(@"theRoast %@", self.postObjectArray);
//                        if (m == i) {
//                            
//                            NSLog(@"Fetch post complete, i = %d", i);
//                            UINavigationController * nav = [[self viewControllers] objectAtIndex:1];
//                            NewsFeed * NewsTVC = (NewsFeed *)nav.topViewController;
//                            
//                            NewsTVC.allArray = [NSMutableArray arrayWithArray:self.cellsArray];
//                            NewsTVC.postObjectArray = [NSMutableArray arrayWithArray:self.postObjectArray];
//                            NSLog(@"theRoast %@", self.postObjectArray);
//                            //  NewsTVC.allArray = [NSMutableArray arrayWithArray:self.refreshArray];
//                            [NewsTVC refreshTable];
//                            [childViewMap customPins:self.pinsArray];
//                            
//                        }
//                        
//                    }
//                    
//                }];
//
//                
////                }
//            
//
//                
//            
//            }
            
            
         

//            bool inArray = false;
//
//            for (int i = 0; [self.refreshArray count]>i; i++) {
//                PFUser * checkingUsersAgain = [self.refreshArray objectAtIndex:i];
////                if (checkingUsersAgain[@"lastParty"] == nil) {
////                    self.cellsArray[l] = self.refreshArray[i];
////                    l++;
////                    
////                }
//                for(int n = 0; [self.cellsArray count] > n; n++){
//                    PFUser * finalUser = [self.cellsArray objectAtIndex:n];
//                    if([[checkingUsersAgain objectId] isEqualToString:[finalUser objectId]]){
//                        inArray = true;
//                    }
//                  //  NSLog(@"BOOL = %@/%@", [checkingUsersAgain objectId],[finalUser objectId]);
//                }
//                if(!inArray){
//                    [self.cellsArray addObject:checkingUsersAgain];
//                    // NSLog(@"Offline or party nil user = %@", checkingUsersAgain.objectId);
//                }
//                inArray = false;
//
//            }
            
            
            /*childViewMap.allArray = [NSMutableArray arrayWithArray:self.pinsArray];
            //[childViewMap newData];
           [childViewMap customPins];
             */
            
            
//            UINavigationController * nav = [[self viewControllers] objectAtIndex:1];
//            NewsFeed * NewsTVC = (NewsFeed *)nav.topViewController;
//            
//            NewsTVC.allArray = [NSMutableArray arrayWithArray:self.cellsArray];
//            //  NewsTVC.allArray = [NSMutableArray arrayWithArray:self.refreshArray];
//            
//            [NewsTVC refreshTable];
//            
//            //childViewMap.allArray = [NSMutableArray arrayWithArray:self.pinsArray];
//            
//            
//            NSLog(@"is this ever being called?!?!?!?!? \n \n \n \n \n \n \n \n \n \n \n");
            
            
            
        }
        
    }];
    
//            if (self.onlineUsers != nil) {
//                
//                int l = 1;
//                
//                for (int i =0; [self.onlineUsers count]>i; i++) {
//                    PFUser * checkingUsers = [self.onlineUsers objectAtIndex:i];
//                    NSLog(@"l = %lu, PFUser = %@", i, checkingUsers);
//                    if(checkingUsers[@"lastParty"] != nil) {
//                        if ([checkingUsers[@"status"]  isEqual: @2] || [checkingUsers[@"status"] isEqual: @1]) {
//                            self.who[l] = self.onlineUsers[i];
//                            l++;
//                        }
//                    }
//                }
//                
//                for (int i =0; [self.onlineUsers count]>i; i++) {
//                    PFUser * checkingUsersAgain = [self.onlineUsers objectAtIndex:i];
//                    if (checkingUsersAgain[@"lastParty"] == nil) {
//                        NSLog(@" this is %@ and their number is %@", checkingUsersAgain[@"firstName"], checkingUsersAgain.username);
//                        self.who[l] = self.onlineUsers[i];
//                        l++;
//                    }
//                }
//            }
            


    
    

}

-(void)removeKeyboard{
        ActionView * toAction = (ActionView *) [self parentViewController];

    [toAction byeKeyboard];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)toZoom{

    [childViewMap ZoomUser];
    NSLog(@"is this passing here?!?!");
    
    [self setSelectedIndex:0];
    
}

-(void)toTable{
    
    [self setSelectedIndex:1];


}

-(void)refresh{
    UINavigationController * nav = [[self viewControllers] objectAtIndex:1];
    NewsFeed * NewsTVC = (NewsFeed *)nav.topViewController;
    
    [NewsTVC refreshTable];
    //[childNewsFeed refreshTable];
    
    
}

-(void)privateParty{

        ActionView * toAction = (ActionView *) [self parentViewController];
    
    [toAction toPrivateParty];

}

-(void)refreshBothTabs{

    UINavigationController * nav = [[self viewControllers] objectAtIndex:1];
    NewsFeed * NewsTVC = (NewsFeed *)nav.topViewController;
    
    [NewsTVC newData];
    [childNewsFeed newData];
    
    
}

-(void)refreshAction{
    ActionView * toAction = (ActionView *) [self parentViewController];
    
    
}

-(void)segueToComments{
    ActionView * toAction = (ActionView *) [self parentViewController];
    toAction.commentObject = self.commentObject;
    [toAction toCommentsView];

}

- (void) segueToCommentHistory
{
    ActionView * toAction = (ActionView *) [self parentViewController];
    
    toAction.commentObject = self.commentObject;
    
    [toAction toCommentHistoryView];
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"toPartyFeed"]) {
        UINavigationController * nav = [segue destinationViewController];
        PFCommentsTableViewController * commentsTVC = (PFCommentsTableViewController *)nav.topViewController;
        
        commentsTVC.pfParty = self.commentObject;
        
    }
    
  //  UINavigationController * nav = [segue destinationViewController];
   // NewsFeed * NewsTVC = (NewsFeed *)nav.topViewController;

}


@end
