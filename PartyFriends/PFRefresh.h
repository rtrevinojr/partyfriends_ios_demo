//
//  PFRefresh.h
//  PartyFriends
//
//  Created by Joshua Silva  on 5/9/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>


@interface PFRefresh : NSObject

@property int position;

@property (strong, nonatomic) PFUser * user;

@property (strong, nonatomic) PFObject * party;

@property (strong, nonatomic) PFObject * post;

- (id)initWithObject:(int )status
user:(PFUser *)aUser
party:(PFObject *)aParty post:(PFObject *) aPost;


@end
