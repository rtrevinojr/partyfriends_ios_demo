//
//  2ndSignUpView.h
//  PartyFriends
//
//  Created by Joshua Silva  on 6/24/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Parse/Parse.h"

@interface _ndSignUpView : UIViewController <UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>


@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) PFUser * currentUser;
@property (strong, nonatomic) PFObject *accountInfo;

@property (strong, nonatomic) IBOutlet UITextField *address;
@property (strong, nonatomic) IBOutlet UITextField *zipCode;
@property (strong, nonatomic) IBOutlet UITextField *city;

@property (strong, nonatomic) IBOutlet UIPickerView *statePicker;
@property (strong, nonatomic) IBOutlet UIDatePicker *birthday;

@property (strong, nonatomic) NSArray * theStates;
@property (strong, nonatomic) NSString * userState;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UITextView *privacyStatemant;

@property (strong, nonatomic) IBOutlet UIButton *whyButton;

@property (strong, nonatomic) IBOutlet UIButton *gotitButton;
@property (strong, nonatomic) IBOutlet UILabel *homeLabel;

@end
