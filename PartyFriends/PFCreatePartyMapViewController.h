//
//  PFCreatePartyMapViewController.h
//  PartyFriends
//
//  Created by Joshua Silva  on 4/25/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <AddressBookUI/AddressBookUI.h>

@interface PFCreatePartyMapViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *searchField;

- (IBAction)dropPinButton:(id)sender;
@property (weak, nonatomic) IBOutlet MKMapView * mapView;

@property (strong, nonatomic) IBOutlet UILabel *addressLabel;
@property (strong, nonatomic) CLLocation * usersLocation;




@end
