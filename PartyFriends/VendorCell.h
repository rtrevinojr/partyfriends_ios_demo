//
//  VendorCell.h
//  VendorProfile
//
//  Created by Gilberto Silva on 12/1/15.
//  Copyright © 2015 Gilberto Silva. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Parse/Parse.h"

@interface VendorCell : UITableViewCell

@property (strong, nonatomic) PFObject * vendorCellObject;
//@property (strong, nonatomic) PFObject * currentUser;
@property (strong, nonatomic)IBOutlet UIButton * createNewButton;
@property (strong, nonatomic)IBOutlet UIButton * editButton;
@property (strong, nonatomic)IBOutlet UILabel * nameLabel;
@property (strong, nonatomic)IBOutlet UIImageView * vendorPic;

@end
