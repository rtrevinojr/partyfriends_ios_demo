//
//  PFAddContactsTableViewController.swift
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 11/4/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

import UIKit

import Foundation

import Contacts
import ContactsUI


@available(iOS 9.0, *)
class PFAddContactsTableViewController: UITableViewController {
    
    
    //    @IBAction func sendMessage(sender: AnyObject) {
    //
    //        var messageVC = MFMessageComposeViewController()
    //
    //        messageVC.body = "Enter a message";
    //        messageVC.recipients = ["Enter tel-nr"]
    //        messageVC.messageComposeDelegate = self;
    //
    //        self.presentViewController(messageVC, animated: false, completion: nil)
    //    }
    
    var ContactsArr : [CNContact] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.tableView.delegate = self
        
        //fetch ()
        
        self.ContactsArr = self.findContacts()
        
        print(ContactsArr.count)
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func fetch () {
        
        let predicate = CNContact.predicateForContactsMatchingName("trevino");
        let keysToFetch = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey, CNContactEmailAddressesKey];
        
        let store = CNContactStore ();
        
        do {
            let contacts = try store.unifiedContactsMatchingPredicate(predicate, keysToFetch: keysToFetch)
            for contact in contacts {
                print("\(contact.givenName) \(contact.familyName)")
            }
            ContactsArr = contacts
        }
        catch {
            print("catch")
        }
    }
    
    
    
    func findContacts () -> [CNContact] {
        
        let store = CNContactStore ();
        
        let keyToFetch = [CNContactFormatter.descriptorForRequiredKeysForStyle(.FullName), CNContactPhoneNumbersKey]
        
        let fetchRequest = CNContactFetchRequest(keysToFetch: keyToFetch)
        
        var contacts = [CNContact] ()
        
        
        do {
            try store.enumerateContactsWithFetchRequest(fetchRequest, usingBlock: { (let contact, let stop) -> Void in contacts.append(contact)
            })
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
        
        return contacts
        
        
    }
    
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return 1;
        return ContactsArr.count
    }
    
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> DTexContactTableViewCell {
        
        let cellid: String = "DTexContactCell"
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellid, forIndexPath:indexPath) as! DTexContactTableViewCell
        
        // no "if" - the cell is guaranteed to exist
        // ... do stuff to the cell here ...
        
        let contact = ContactsArr[indexPath.row] as CNContact
        
        print(contact)
        
        cell.ContactNameLabel.text = contact.givenName
        
        //
        //          var phoneNumber: String!
        //        for phone in contact.phoneNumbers {
        //
        //            if phone.label == CNLabelPhoneNumberMobile {
        //                phoneNumber = phone.value as! CNContact
        //                break
        //            }
        //        }
        //
        //        var emailAddress: String!
        //        for email in contact.emailAddresses {
        //            if email.label == CNLabelEmailiCloud {
        //                emailAddress = email.value as! String
        //                break
        //            }
        //        }
        
        
        var numberArray = [String] ()
        
        for number in contact.phoneNumbers {
            
            let phoneNumber = number.value as! CNPhoneNumber
            
            numberArray.append(phoneNumber.stringValue)
        }
        
        //var comma: String = ","
        
        if (numberArray.count > 0) {
            cell.ContactPhoneLabel.text = numberArray[0]
        }
        else {
            cell.ContactPhoneLabel.text = "0000000000"
        }
        
        
        //        if let phoneNumberLabel = cell.ContactPhoneLabel {
        //            var numberArray = [String] ()
        //            for number in contact.phoneNumbers {
        //                let phoneNumber = number.value as! CNPhoneNumber
        //                numberArray.append(phoneNumber.stringValue)
        //            }
        //            phoneNumberLabel.text = numberArray[0]
        //       }
        //        if (contact.isKeyAvailable(CNContactPhoneNumbersKey)) {
        //            print("\(contact.phoneNumbers)")
        //            
        //            cell.ContactPhoneLabel.text = ""
        //        }
        //        else {
        //            cell.ContactPhoneLabel.text = "0000000000"
        //        }
        
        return cell
    }
*/
    
}

