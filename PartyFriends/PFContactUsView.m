//
//  PFContactUsView.m
//  PartyFriends
//
//  Created by Joshua Silva  on 3/30/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import "PFContactUsView.h"

@implementation PFContactUsView

-(void) viewDidLoad{
    
    [super viewDidLoad];
    
    
}

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    
    self.textView.clipsToBounds = YES;
    self.textView.layer.cornerRadius = 10.0f;
    
    if ([self.whereFrom isEqualToString:@"forgot"]) {
        self.textView.text = @"Contact us at \n contact@partyfriendsapp.com";
        self.elPaso.text = @"";
    }else{
    self.elPaso.text = @"#ItsAllGoodElPaso \n#ElPasoKnowsHowToParty";
    }
}

-(void ) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    self.whereFrom = @"";
    
}


@end