//
//  PFGoingOutTableViewCell.h
//  PartyFriends
//
//  Created by Joshua Silva  on 5/5/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PFGoingOutTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *profileView;
@property (strong, nonatomic) IBOutlet UILabel *userName;

@end
