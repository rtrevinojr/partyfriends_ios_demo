//
//  PFVendorListTableViewCell.h
//  PartyFriends
//
//  Created by Gilberto Silva on 12/2/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Parse/Parse.h>


@interface PFVendorListTableViewCell : UITableViewCell

@property(strong, nonatomic)IBOutlet UILabel *nameLabel;
@property(strong, nonatomic)IBOutlet UIButton *createNewButton;
@property(strong, nonatomic)IBOutlet UIButton *editButton;

@property (strong, nonatomic) PFObject * vendorPFObject;

@end
