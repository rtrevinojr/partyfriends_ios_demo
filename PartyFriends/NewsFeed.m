//
//  NewsFeed.m
//  PartyFriends
//
//  Created by Joshua Silva  on 6/27/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import "NewsFeed.h"
#import "MapView.h"
#import "SettingsView.h"
#import "UIView+Toast.h"
#import "Reach.h"
#import "PFNewsFeedTableViewCell.h"
#import "Gradient.h"
#import <ParseUI/ParseUI.h>
#import <CoreLocation/CoreLocation.h>
#import "LocationManagerSingleton.h"
#import "NSDate+NVTimeAgo.h"

#import "PFCommentsTableViewController.h"


@interface NewsFeed ()

@end

BOOL waff;

@implementation NewsFeed {
    
    tabBarController * tabbar;
    CLLocationCoordinate2D cood;
    
}


- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if( 0 == buttonIndex ){ //cancel button
        self.current[@"gender"] = @"F";
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        // NSLog(@"this shows this is a female ekkkk!!");
        [self.current saveInBackground];
        
    } else if ( 1 == buttonIndex ){
        self.current[@"gender"] = @"M";
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        // NSLog(@"this shows this is a male yay!!! ");
        [self.current saveInBackground];
        
    }
    
}

- (void) hostTouch {
    
    NSLog(@"refreshTouch");
    
    [self refreshTable];
    
    
}

- (void) mapTouch
{
    NSLog(@"mapTouch");
    [self.tabBarController setSelectedIndex:0];
}

-(void)refreshAll{
    
    [tabbar refreshAll];
    
}


- (void)refreshTable {
    
    if (self.allArray == nil) {
        self.allArray = [NSMutableArray arrayWithArray:tabbar.cellsArray];
        
        //        self.userObjectArray = [NSMutableArray arrayWithArray:tabbar.userObjectArray];
    }
    
    
    
    
    //TODO: refresh your data
    [self.refreshControl endRefreshing];
    
    
    
    
    
    [self.tableView reloadData];
    
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //    if (self.allArray == nil) {
    //
    //    self.allArray = [[NSMutableArray alloc] init];
    //    }
    self.tableView.estimatedRowHeight = 200.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    //[self refreshAll];
    [self refreshTable];
    //[self.tableView reloadData];
    //[self.lowBar setBackgroundColor:[UIColor blackColor]];
    self.refreshControl = [[UIRefreshControl alloc]init];
    [self.tableView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(refreshAll) forControlEvents:UIControlEventValueChanged];
    
    self.leftEdgeView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
    
    CGSize barSize = CGSizeMake(self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height +20);
    
    CAGradientLayer * blackLayer = [Gradient black];
    blackLayer.frame =CGRectMake(0, 0, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height +  20);
    
    [blackLayer setStartPoint:CGPointMake(0.0, 0.0)];
    [blackLayer setEndPoint:CGPointMake(0.0, 0.0)];
    
    UIGraphicsBeginImageContext(barSize);
    [blackLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * blackImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    CAGradientLayer * randomLayer = [Gradient gold];
    //CAGradientLayer *
    
    randomLayer.frame = CGRectMake(0, -20, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height +20 );
    
    [randomLayer setStartPoint:CGPointMake(0.0, 0.0)];
    [randomLayer setEndPoint:CGPointMake(0.0, 0.0)];
    // the orginal sizes
    //    [randomLayer setStartPoint:CGPointMake(0.0, 0.5)];
    //    [randomLayer setEndPoint:CGPointMake(1.0, 0.5)];
    UIGraphicsBeginImageContext(barSize);
    [randomLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * anotherImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //[self.navigationController.navigationBar.layer insertSublayer:randomLayer atIndex:1];
    
    // [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"PartyFriendsLogoTrim3.png"] forBarMetrics:UIBarMetricsDefault];
    
    //CGSize barSize = CGSizeMake(self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height +20);
    UIGraphicsBeginImageContextWithOptions(barSize, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    size_t gradientNumberOfLocations = 3;
    CGFloat gradientLocations[3] = {1.0,0.5, 0.0};
    CGFloat gradientComponents[12] ={(0/255.0), (0/255.0), (0/255.0), 1.0, 252.0/255.0,194/255.0, 0 ,.5, 0, 0, 0, 1.0,};
    
    CGGradientRef gradient = CGGradientCreateWithColorComponents(colorSpace, gradientComponents, gradientLocations, gradientNumberOfLocations);
    
    CGContextDrawLinearGradient(context, gradient, CGPointMake(0.0, 0.0), CGPointMake(0.0, 12.0), 0);
    UIImage * imageCheck = UIGraphicsGetImageFromCurrentImageContext();
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
    UIGraphicsEndImageContext();
    
    UIImage * randomImage = [UIImage imageNamed:@"PartyFriendsLogoTrim3.png"];
    
    
    
    
    NSLog(@" dollin %@ %@\n \n \n %@", randomImage, imageCheck, anotherImage);
    //  [self.navigationController.navigationBar setBackgroundImage:blackImage forBarMetrics:UIBarMetricsDefault];
    
    //[self.navigationController.navigationBar setBackgroundImage:anotherImage forBarMetrics:UIBarMetricsDefault];
    // [self.navigationController.navigationBar setBackgroundColor:[UIColor blackColor]];
    
    UIScreenEdgePanGestureRecognizer *pan = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(panToHeatMap:)];
    [pan setEdges:UIRectEdgeLeft];
    [pan setDelegate:self];
    //pan.cancelsTouchesInView=NO;
    [self.view addGestureRecognizer:pan];
    
    self.tableView.delegate = self;
    
    self.navigationController.delegate = self;
    
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
    
    //    UIBarButtonItem * refreshButton = [[UIBarButtonItem alloc]
    //                                     initWithImage:[UIImage imageNamed:@"refresh-2-selected"]
    //                                     style:UIBarButtonItemStylePlain
    //                                     target:self action:@selector(toPrivateParty)];
    
    UIButton * rightBut = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBut setFrame:CGRectMake(0.0, 0.0, 30.0, 30.0)];
    [rightBut addTarget:self action:@selector(toPrivateParty) forControlEvents:UIControlEventTouchUpInside];
    [rightBut setImage:[UIImage imageNamed:@"solidGold"] forState:UIControlStateNormal];
    //UIBarButtonItem * rightButton = [[UIBarButtonItem alloc] initWithCustomView:rightBut];
    
    UIButton * leftBut = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftBut setFrame:CGRectMake(0.0, 0.0, 30.0, 30.0)];
    [leftBut addTarget:self action:@selector(mapTouch) forControlEvents:UIControlEventTouchUpInside];
    [leftBut setImage:[UIImage imageNamed:@"goldMap"] forState:UIControlStateNormal];
    UIBarButtonItem * leftButton = [[UIBarButtonItem alloc] initWithCustomView:leftBut];
    
    
    //    UIBarButtonItem * refreshButton = [[UIBarButtonItem alloc]
    //                                       initWithImage:[UIImage imageNamed:@"solidGold"]
    //                                       style:UIBarButtonItemStylePlain
    //                                       target:self action:@selector(toPrivateParty)];
    
    UIBarButtonItem * mapButton = [[UIBarButtonItem alloc]
                                   initWithImage:[UIImage imageNamed:@"map-location-selected"]
                                   style:UIBarButtonItemStylePlain
                                   target:self action:@selector(mapTouch)];
    
    
    
    
    [mapButton setTintColor:[UIColor purpleColor]];
    //[refreshButton setTintColor:[UIColor purpleColor]];
    
    //self.navigationItem.rightBarButtonItem = rightButton;
    
    self.navigationItem.leftBarButtonItem = leftButton;
    
    //self.navigationItem.rightBarButtonItem
    
    //trying fot the gradient gold background
    CAGradientLayer * tableLayer = [Gradient gold];
    //CAGradientLayer *
    
    tableLayer.frame = CGRectMake(0, 0, self.tableView.frame.size.width, self.tableView.frame.size.height );
    
    [tableLayer setStartPoint:CGPointMake(-1.5, 0.0)];
    [tableLayer setEndPoint:CGPointMake(2.5, 0.0)];
    
    //[self.tableView.layer insertSublayer:tableLayer atIndex:2];
    
    UIColor * goldColor = [UIColor colorWithRed:0.988f green:0.76f blue:0 alpha:1];
    // [self.tableView.layer insertSublayer:randomLayer atIndex:0];
    // [self.tableView  setSeparatorColor:[UIColor colorWithRed:252.0/255.0 green:194/255.0 blue:0 alpha:.5]];
    
    [[self.tableView layer] setBorderColor:[[UIColor colorWithRed:252.0/255.0 green:194/255.0 blue:0 alpha:1] CGColor]];
    // [[self.tableView layer] setBorderColor:[[UIColor purpleColor] CGColor]];
    [[self.tableView layer] setBorderWidth:1.1];
    
    self.current=[PFUser currentUser];
    
    NSLog(@"%@",_current);
    
//    if (self.current[@"gender"] == nil) {
//        UIAlertView * AlertView = [[UIAlertView alloc] initWithTitle:@"Missing gender" message:@"You were a PartyFriend before we requested your gender" delegate:self cancelButtonTitle:@"Female" otherButtonTitles:@"Male",nil];
//        [AlertView show];
//        
//    }
    
    self.currentPFs = [[PFUser currentUser] objectForKey:@"friends"];
    
    NSString *name=[self.current objectForKey:@"firstName"];
    self.myGeo = [self.current objectForKey:@"location"];
    float waf = self.myGeo.latitude;
    float waf2 = self.myGeo.longitude;
    
    self.yourCord = [[CLLocation alloc]initWithLatitude:self.myGeo.latitude longitude:self.myGeo.longitude];
    NSLog(@" %f %f  ", waf, waf2);
    
    self.yourName.text = name;
    
    self.yourLocation.text = [NSString stringWithFormat:@"%f %f", waf, waf2];
    
    NSLog(@"%@", self.myGeo);
    
    [self reverseGeocode:self.yourCord];
    
    self.waffles = @[@"waffles", @"pancakes", @"syrup!"];
    
    
    self.whoElse = [NSMutableArray new];
    self.who = [NSMutableArray new];
    
    tabbar = (tabBarController *) self.tabBarController;
    
    
    //    PFQuery *query1 = [PFQuery queryWithClassName:@"friendrequest"];
    //    [query1 whereKey:@"user2" equalTo:self.current];
    //    [query1 whereKey:@"status" equalTo:@4];
    //
    //    PFQuery *query2 = [PFQuery queryWithClassName:@"friendrequest"];
    //    [query2 whereKey:@"user1" equalTo:self.current];
    //    [query2 whereKey:@"status" equalTo:@4];
    //
    //    PFQuery* queryBoth =[PFQuery orQueryWithSubqueries:@[query2, query1]];
    //    [queryBoth orderByAscending:@"createdAt"];
    //    [queryBoth includeKey:@"user1"];
    //    [queryBoth includeKey:@"user2"];
    //    [queryBoth findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
    //        if (error) {
    //            NSLog(@"Error 1");
    //            NSLog(@"%@%@",error,error.userInfo);
    //        }
    //        else {
    //            NSLog(@"/n %@",objects);
    //            self.badFriends=objects;
    //            NSUInteger n =[_badFriends count];
    //            NSLog(@"%lu",(unsigned long)_badFriends.count);
    //            NSUInteger i=0;
    //
    //            for (i=0; n>=i; i++) {
    //                NSLog(@"%@",_badFriends[i]);
    //                PFObject *request=_badFriends[i];
    //                PFUser *user1=request[@"user1"];
    //                PFUser *user2=request[@"user2"];
    //                PFRelation *removeFriend=[self.current relationForKey:@"friends"];
    //
    //                if (user1==self.current) {
    //                    NSLog(@"%@",user2);
    //                    [removeFriend removeObject:user2];
    //                    [self.current saveInBackground];
    //                    request[@"status"]= @5;
    //                    [request saveInBackground];
    //
    //                }
    //                else if (user1!=self.current){
    //
    //                    NSLog(@"%@ 2",user1);
    //                    [removeFriend removeObject:user1];
    //                    [self.current saveInBackground];
    //                    request[@"status"] = @5;
    //                    [request saveInBackground];
    //
    //                }
    //
    //                [self.current saveEventually];
    //                NSLog(@"badfriends removed.");
    //            }
    //        }
    //    }];
    
    
    
    //    PFQuery * querySelf = [PFUser query];
    //    [querySelf whereKey:@"objectId" equalTo:self.current.objectId];
    //    [querySelf includeKey:@"lastParty"];
    //    [querySelf getFirstObjectInBackgroundWithBlock:^(PFObject * selfObject, NSError * error){
    //
    //        if (!error) {
    //
    //            self.who[0]= selfObject;
    //
    //            PFQuery * query = [self.currentPFs query];
    //            [query includeKey:@"lastParty"];
    //            [query orderByDescending:@"updatedAt"];
    //            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
    //                if (error) {
    //                    NSLog(@"error %@ %@", error, [error userInfo]);
    //                }
    //                else if ([objects count]==0) {
    //                    NSLog(@"*** NewsFeed.m - query return no objects");
    //                    self.who = [NSMutableArray arrayWithObjects:@"no", nil];
    //                }
    //                else {
    //                    self.onlineUsers = objects;
    //
    //                    if (self.onlineUsers != nil) {
    //
    //                        int l = 1;
    //
    //                        for (int i =0; [self.onlineUsers count]>i; i++) {
    //                            PFUser * checkingUsers = [self.onlineUsers objectAtIndex:i];
    //                            NSLog(@"l = %lu, PFUser = %@", i, checkingUsers);
    //                            if(checkingUsers[@"lastParty"] != nil) {
    //                                if ([checkingUsers[@"status"]  isEqual: @2] || [checkingUsers[@"status"] isEqual: @1]) {
    //                                    self.who[l] = self.onlineUsers[i];
    //                                    l++;
    //                                }
    //                            }
    //                        }
    //
    //                        for (int i =0; [self.onlineUsers count]>i; i++) {
    //                            PFUser * checkingUsersAgain = [self.onlineUsers objectAtIndex:i];
    //                            if (checkingUsersAgain[@"lastParty"] == nil) {
    //                                NSLog(@" this is %@ and their number is %@", checkingUsersAgain[@"firstName"], checkingUsersAgain.username);
    //                                self.who[l] = self.onlineUsers[i];
    //                                l++;
    //                            }
    //                        }
    //                    }
    //                    [self.tableView reloadData];
    //                }
    //
    //            }];}else{
    //                NSLog(@"Error when querying self for the party");
    //
    //       }
    //
    //    }];
    
    
    
    
    
    cood = [LocationManagerSingleton sharedSingleton].locationManager.location.coordinate;
    
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}






- (IBAction)tugAction:(id)sender {
    
    PFNewsFeedTableViewCell * cell = (PFNewsFeedTableViewCell *) [[sender superview] superview];
    
    
    
    
    if ([self.current[@"status"]  isEqual: @1] || [self.current[@"status"] isEqual: @2]) {
        
        PFObject * tug = [PFObject objectWithClassName:@"tug"];
        
        tug[@"user1"] = self.current;
        tug[@"user2"] = cell.userObject;
        
        [tug saveInBackgroundWithBlock:^(BOOL success, NSError * error){
            
            if (!error) {
                NSString * tugged = [NSString stringWithFormat:@"You Tugged %@", cell.userObject[@"firstName"]];
                [self.view makeToast:tugged duration:2 position:CSToastPositionCenter];
            }else{
                NSString * errorMessage = [[error userInfo] objectForKey:NSLocalizedDescriptionKey];
                if ([errorMessage containsString:@"Tug sent"]) {
                    [self.view makeToast:errorMessage duration:2 position:CSToastPositionCenter];
                }else if ([errorMessage containsString:@"You must wait"]){
                    [self.view makeToast:errorMessage duration:2 position:CSToastPositionCenter];
                }else{
                    NSString * errorString = [NSString stringWithFormat:@"%@ Error code: %ld", errorMessage, (long)error.code];
                    [self.view makeToast:errorString duration:2 position:CSToastPositionCenter];
                    
                }
                
                
                UIAlertView * tugFailed = [[UIAlertView alloc] initWithTitle:@"Tug failed" message:error.userInfo delegate:nil cancelButtonTitle:@"okay" otherButtonTitles: nil];
                
                //[tugFailed show];
            }
            
        }];
    }else{
        
        UIAlertView * offlineCheck = [[UIAlertView alloc] initWithTitle:@"You're offline" message:@"You have to be on Party or Exclusive Mode to tug someone" delegate:nil cancelButtonTitle:@"Got it!" otherButtonTitles: nil];
        
        [offlineCheck show];
        
    }
    
}


- (IBAction)commentHistoryTouch:(id)sender {
    
    PFNewsFeedTableViewCell * cell = (PFNewsFeedTableViewCell *) [[sender superview] superview];
    
    NSString * objId = cell.objectId;
    NSLog(@"****************** Btn press. cell.objectId = %@", objId);
    NSLog(@"*** cell.partyObject = %@", cell.postObject);
    
    self.selectedMessage = cell.postObject;
    
    tabbar.commentObject = cell.postObject;
    
    [tabbar segueToCommentHistory];
}

-(void)toPrivateParty{
    
    NSDate *now = [NSDate date];
    NSDate *sevenDaysAgo = [now dateByAddingTimeInterval:-7*24*60*60];
    NSDate * mintues = [now dateByAddingTimeInterval:-60];
    NSLog(@"7 days ago: %@", sevenDaysAgo);
    NSTimeInterval differentTime = [now timeIntervalSinceDate:sevenDaysAgo];
    NSTimeInterval nowTime = [now timeIntervalSinceDate:mintues];
    
    //    if ([now alertFormatCheck:differentTime]) {
    //        NSLog(@"something is wrong, it shouldnt show");
    //    }
    //    if ([now alertFormatCheck:nowTime]) {
    //        NSLog(@"please work god %@ %d", [sevenDaysAgo formattedAsTimeAgo], [sevenDaysAgo alertTime]);
    //    }
    
    
    if ([now alertTimeCheck:mintues]) {
        NSLog(@"something is wrong, it shouldnt show");
    }
    if ([now alertTimeCheck:sevenDaysAgo]) {
        NSLog(@"please work god %@ %d", [sevenDaysAgo formattedAsTimeAgo], [sevenDaysAgo alertTime]);
    }
    
    
    //[tabbar privateParty];
    
}


- (IBAction)commentBtnTouch:(id)sender {
    
    NSLog(@"comment Button Touch");
    
    PFNewsFeedTableViewCell * cell = (PFNewsFeedTableViewCell *) [[sender superview] superview];
    
    NSLog(@"PFNewsFeedTableViewCell = %@", cell);
    
    
    NSString * objId = cell.objectId;
    NSLog(@"*** Btn press. cell.objectId = %@", objId);
    NSLog(@"*** cell.partyObject = %@", cell.partyObject);
    
    
    
    //PFQuery * query = [PFQuery queryWithClassName:@"comments"];
    
    self.selectedMessage = cell.partyObject;
    tabbar.commentObject  = cell.partyObject;
    
    // [self performSegueWithIdentifier:@"comments" sender:self];
    
    [tabbar segueToComments];
    [self refreshAll];
}

/*
 -(void) viewWillAppear:(BOOL)animated {
 
 NSLog(@"NewsFeed.m - viewWillAppear ");
 
 [super viewWillAppear:NO];
 
 PFQuery *query1 = [PFQuery queryWithClassName:@"friendrequest"];
 [query1 whereKey:@"user2" equalTo:self.current];
 [query1 whereKey:@"status" equalTo:@4];
 
 PFQuery *query2 = [PFQuery queryWithClassName:@"friendrequest"];
 [query2 whereKey:@"user1" equalTo:self.current];
 [query2 whereKey:@"status" equalTo:@4];
 
 PFQuery* queryBoth =[PFQuery orQueryWithSubqueries:@[query2, query1]];
 [queryBoth orderByAscending:@"createdAt"];
 [queryBoth includeKey:@"user1"];
 [queryBoth includeKey:@"user2"];
 [queryBoth findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
 if (error) {
 NSLog(@"Error 1");
 NSLog(@"%@%@",error,error.userInfo);
 }
 else {
 NSLog(@"/n %@",objects);
 self.badFriends=objects;
 NSUInteger n =[_badFriends count];
 NSLog(@"%lu",(unsigned long)_badFriends.count);
 NSUInteger i=0;
 
 for (i=0; n>=i; i++) {
 NSLog(@"%@",_badFriends[i]);
 PFObject *request=_badFriends[i];
 PFUser *user1=request[@"user1"];
 PFUser *user2=request[@"user2"];
 PFRelation *removeFriend=[self.current relationForKey:@"friends"];
 
 if (user1==self.current) {
 NSLog(@"%@",user2);
 [removeFriend removeObject:user2];
 [self.current saveInBackground];
 request[@"status"]= @5;
 [request saveInBackground];
 
 }
 else if (user1!=self.current){
 
 NSLog(@"%@ 2",user1);
 [removeFriend removeObject:user1];
 [self.current saveInBackground];
 request[@"status"] = @5;
 [request saveInBackground];
 
 }
 
 [self.current saveEventually];
 NSLog(@"badfriends removed.");
 }
 }
 }];
 
 PFQuery * query = [self.currentPFs query];
 [query includeKey:@"lastParty"];
 [query orderByAscending:@"firstName"];
 [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
 if (error) {
 NSLog(@"error %@ %@", error, [error userInfo]);
 }
 else if ([objects count]==0) {
 NSLog(@"*** NewsFeed.m - query return no objects");
 self.who = [NSMutableArray arrayWithObjects:@"no", nil];
 }
 else {
 self.onlineUsers = objects;
 
 if (self.onlineUsers != nil) {
 
 int l = 0;
 
 for (int i =0; [self.onlineUsers count]>i; i++) {
 PFUser * checkingUsers = [self.onlineUsers objectAtIndex:i];
 NSLog(@"l = %lu, PFUser = %@", i, checkingUsers);
 if(checkingUsers[@"lastParty"] != nil) {
 if ([checkingUsers[@"status"]  isEqual: @2] || [checkingUsers[@"status"] isEqual: @1]) {
 self.who[l] = self.onlineUsers[i];
 l++;
 }
 }
 }
 
 for (int i =0; [self.onlineUsers count]>i; i++) {
 PFUser * checkingUsersAgain = [self.onlineUsers objectAtIndex:i];
 if (checkingUsersAgain[@"lastParty"] == nil) {
 NSLog(@" this is %@ and their number is %@", checkingUsersAgain[@"firstName"], checkingUsersAgain.username);
 self.who[l] = self.onlineUsers[i];
 l++;
 }
 }
 }
 [self.tableView reloadData];
 }
 
 }];
 
 
 NSLog(@"this for showing whats inside of the Array = %@", [self.onlineUsers objectAtIndex:0]);
 NSLog(@"this is checking the online users! %@ ", self.onlineUsers);
 NSLog(@"the number of things %lu", (unsigned long)self.onlineUsers.count);
 
 [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(OrientationDidChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
 
 NSLog(@"   %@ ", self.onlineUsers);
 
 //   if (self.onlineUsers != nil) {
 //        int l = 0;
 //        for (int i =0; [self.onlineUsers count]>i; i++) {
 //            PFUser * checkingUsers = [self.onlineUsers objectAtIndex:i];
 //            //if(checkingUsers[@"lastParty"] != nil){
 //            if ([checkingUsers[@"status"]  isEqual: @2] || [checkingUsers[@"status"] isEqual: @1]) {
 //                self.who[l] = self.onlineUsers[i];
 //                l++;
 //
 //            }
 //        }for (int i =0; [self.onlineUsers count]>i; i++) {
 //          PFUser * checkingUsersAgain = [self.onlineUsers objectAtIndex:i];
 //          if (checkingUsersAgain[@"lastParty"] == nil) {
 //          self.who[l] = self.onlineUsers[i];
 //          l++;
 //          }
 //
 //          }
 //
 //    }
 
 }
 
 */

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    //NSLog(@"NewsFeed.m - numberOfRowsInSection count = %lu", [self.who count]);
    //return [self.who count];
    return [self.allArray count];
}

- (NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    return @"Party Feed";
    
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 0.0;
    
}


/*
 -(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
 {
 
 UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 1000)];
 
 UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, tableView.frame.size.width - 10, 15)];
 
 [label setFont:[UIFont fontWithName:@"Helvetica Neue" size:16]];
 [label setText:@"PARTY FEED"];
 [label setTintColor:[UIColor purpleColor]];
 [label setTextColor:[UIColor purpleColor]];
 
 label.textAlignment = NSTextAlignmentCenter;
 
 [view setBackgroundColor:[UIColor whiteColor]];
 
 //[[view layer] setBorderColor:[[UIColor purpleColor] CGColor]];
 //
 /*[[view layer] setBorderWidth:1.1];
 
 
 [view addSubview:label];
 
 return view;
 
 }
 */


- (PFNewsFeedTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    static NSString *CellIdentifier = @"NewsFeedCell";
    PFNewsFeedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    
    
    
    if (cell == nil) {
        cell = [[PFNewsFeedTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    //[cell.tugButton setTitleColor:[UIColor colorWithRed:252.0/255.0 green:194/255.0 blue:0 alpha:0.5] forState:UIControlStateNormal];
    
    if ([[self.allArray objectAtIndex:0] isEqual:@"no"]) {
        
        cell.profileName.text = @"No friends yet";
        
    }
    else {
        
        PFRefresh * refresh = [self.allArray objectAtIndex:indexPath.row];
        PFUser * name = refresh.user;
        PFObject * party = refresh.party;
        PFObject * post = refresh.post;
        // NSLog(@"waffles %@", self.who);
        [cell.tugButton setHidden:NO];
        cell.alertLabel.text = @"";
        cell.movingLabel.text = @"";
        if ([name.objectId isEqualToString:self.current.objectId]) {
            [cell.tugButton setHidden:YES];
            //[cell.commentButton setHidden:YES];
            cell.alertLabel.text= @"";
            cell.movingLabel.text = @"";
            cell.profileSubheadline.text = @"";
            
            
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.userObject = name;
        cell.profileName.text = name[@"firstName"];
        cell.pointsLabel.text = [NSString stringWithFormat:@"%@", name[@"score"]];
        cell.profileHeadline.text = @"";
        cell.commentButton.hidden = true;
        cell.partyCommentCount.hidden = true;
        [cell.tugButton setTitleColor:[UIColor purpleColor] forState:UIControlStateNormal];
        cell.profileHeadline.text = @"";
        cell.profileHeadline.hidden = false;
        cell.profileSubheadline.text = @"";
        cell.profileSubheadline.hidden = false;
        cell.thridText.text = @"";
        cell.thridText.hidden = false;
        cell.batteryImage.hidden = YES;
        cell.batteryNumber.hidden = YES;
        cell.batteryImage.image = [UIImage imageNamed:@"battery100120x120 px.png"];
        NSNumber * batteryNumber = name[@"batteryPercentage"];
        //NSString * numberString = [NSString string]
        cell.batteryNumber.text = [NSString stringWithFormat:@"%ld%%", (long)[batteryNumber integerValue]];
        
        if ([batteryNumber intValue] > 75) {
            cell.batteryImage.image =[UIImage imageNamed:@"battery100120x120 px.png"];
            
        }else if ([batteryNumber intValue] > 50){
            cell.batteryImage.image = [UIImage imageNamed:@"battery75120x120px.png"];
            
            
        }else if ([batteryNumber intValue] > 25){
            cell.batteryImage.image = [UIImage imageNamed:@"battery50120x120px.png"];
            
            
        }else if ([batteryNumber intValue] > 15){
            cell.batteryImage.image = [UIImage imageNamed:@"battery25120x120px.png"];
            
        }else if ([batteryNumber intValue] > 5){
            
            cell.batteryImage.image = [UIImage imageNamed:@"battery10120x120px.png"];
        }else{
            
            cell.batteryImage.image = [UIImage imageNamed:@"battery0120x120px.png"];
        }
        
        
        
        //        if ([name[@"status"]  isEqual: @2]) {
        //            cell.statusButton.image = [UIImage imageNamed:@"BlueDot"];
        //            cell.profileName.textColor = [UIColor blackColor];
        //        }
        //        else if ([name[@"status"]  isEqual: @1]) {
        //            cell.statusButton.image = [UIImage imageNamed:@"RedDot"];
        //            cell.profileName.textColor = [UIColor blackColor];
        //        }
        //        else {
        cell.statusButton.image = [UIImage imageNamed:@"whiteBallon"];
        cell.profileName.textColor = [UIColor blackColor];
        
        
        //        }
        
        NSDate * now = [NSDate date];
        NSNumber * status = name[@"status"];
        NSDate * logDate = nil;
        
        
        
        if ([name[@"status"]  isEqual: @2] && party != nil) {
            cell.statusButton.image = [UIImage imageNamed:@"BlueBallon"];
            cell.profileName.textColor = [UIColor blackColor];
            logDate = name[@"logIn"];
            cell.batteryImage.hidden = NO;
            cell.batteryNumber.hidden = NO;
        }
        else if ([name[@"status"]  isEqual: @1]) {
            cell.statusButton.image = [UIImage imageNamed:@"RedBallon"];
            cell.profileName.textColor = [UIColor blackColor];
            logDate = name[@"logIn"];
            cell.batteryNumber.hidden = NO;
            cell.batteryImage.hidden = NO;
        }
        else if([name[@"status"]  isEqual: @0] || ([name[@"status"]  isEqual: @2] && party == nil)){
            
            logDate = name[@"logOut"];
            
            
        }
        
        
        NSString * headline = nil;
        NSString * subHeadline = nil;
        NSString * secondSubHeadline = nil;
        NSString * timeStampString = nil;
        NSString * placeName = nil;
        NSString * applePlaceMark = nil;
        NSString * address = nil;
        NSString * distance  = nil;
        NSString * googlePlacesID = nil;
        NSDate * postCreatedAt = nil;
        NSDate * partyCreatedAt = nil;
        NSDate * locationUpdatedAt = nil;
        NSDate * googleTimeStamp = nil;
        int hasParty = 0;
        
        
        
        
        
        
        
        
        //
        //        __block PFObject * party = name[@"lastParty"];
        //        [party fetchInBackgroundWithBlock:^(PFObject * object, NSError * error){
        
        
        //            if (!error) {
        //                party = object;
        //                cell.partyObject = party;
        //
        ////                if ([cell.partyObject objectForKey:@"commentCount"]) {
        ////
        ////
        ////                    NSNumber * commentCnt = [cell.partyObject objectForKey:@"commentCount"];
        ////
        ////                    NSString * commentCntS = [NSString stringWithFormat:@"%@", commentCnt];
        ////                    if (commentCnt == nil) {
        ////                        cell.partyCommentCount.text =[NSString stringWithFormat:@"0"];
        ////                    }else{
        ////
        ////                        cell.partyCommentCount.text = commentCntS;
        ////                        NSLog(@"Cell Party Comment Count = %@", commentCntS);
        ////                        NSLog(@"cell Party Object = %@", name[@"lastParty"]);
        ////
        ////
        ////                    }
        ////                }
        //            }
        
        //        PFObject * party = [self.partyObjectArray objectAtIndex:indexPath.row];
        
        if(party != nil){
            [party fetchIfNeeded];
            cell.partyObject = party;
            //                cell.commentButton.hidden = false;
            //                cell.partyCommentCount.hidden = false;
            //
            //                NSNumber * commentCount = party[@"commentCount"];
            //
            //                NSString * commentString = [NSString stringWithFormat:@"%@", commentCount];
            //                cell.partyCommentCount.text = commentString;
            //                if (commentCount == nil){
            //                    cell.partyCommentCount.text =[NSString stringWithFormat:@"0"];
            //                }
            
            
            
            
            
            
            
            //
            //                if (party[@"headline"] != nil) {
            //                    //cell.detailTextLabel.text = name[@"headline"];
            //                    cell.profileHeadline.text = party[@"headline"];
            //                }else {
            //                    if (party[@"applePlaceMark"] != nil) {
            //                        cell.profileHeadline.text = party[@"applePlaceMark"];
            //
            //                    }else{
            //
            
            
            hasParty = 1;
            
            if(([name[@"status"] isEqual:@1] || [name[@"status"] isEqual:@2]) || name == self.current){
                
                partyCreatedAt = [party createdAt];
//                Boolean googlePlaces = party[@"googlePlaces"];
                
                if(party[@"googlePlaces"]){
                
                googleTimeStamp = party[@"googlePlacesTimeStamp"];
                placeName = party[@"googlePlaceName"];
                googlePlacesID = party[@"googlePlacesId"];
                address = party[@"googlePlacesAddress"];
                
                }
                else{
                    
                    applePlaceMark = party[@"applePlaceMark"];
                    address = party[@"address"];
                    
                }
                
                PFGeoPoint * nameLocation = party[@"location"];
                locationUpdatedAt = party[@"updatedLocationAt"];

                
                CLLocation * location = [[CLLocation alloc]initWithLatitude:nameLocation.latitude longitude:nameLocation.longitude];
                
                //                        cell.profileSubheadline.text = [self locationDistanceString:location];
                
                if(name != self.current){
                    
                    distance = [self locationDistanceString:location];
                    
                }
                
            }
            
            
            
            
            
            
            //  // selffaw
            //
            //
            //                       // [self locationDistanceString:location];
            //
            //
            //                    CLGeocoder * waffles = [CLGeocoder new];
            ////                    [waffles reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
            ////                        if (error) {
            ////                            NSLog(@"Error %@", error.description);
            ////                            cell.profileHeadline.text = @"nil";
            ////
            ////                        } else {
            ////                            CLPlacemark * placemark = [placemarks lastObject];
            ////                            if (placemark.areasOfInterest[0] != nil) {
            ////                                cell.profileHeadline.text = [NSString stringWithFormat:@"%@", placemark.areasOfInterest[0]];
            ////                            }
            ////                            else {
            ////
            ////                                cell.profileHeadline.text = placemark.name;
            ////                            }
            ////                        }
            ////                    }];
            //                    }
            //                }
            
            
            
        }
        //            else{
        //                cell.profileHeadline.text = @"nil";
        //            }
        
        //            __block PFObject * post = name[@"lastPost"];
        //            [post fetchInBackgroundWithBlock:^(PFObject * object, NSError * error){
        //
        //                if(!error){
        //
        //                    post = object;
        //                    cell.postObject = post;
        //
        //                }
        
        //        PFObject * post = [self.postObjectArray objectAtIndex:indexPath.row];
        
        if(post != nil){
            
            [post fetchIfNeeded];
            
            cell.postObject = post;
            
            cell.commentButton.hidden = false;
            cell.partyCommentCount.hidden = false;
            if (post[@"commentCount"] != nil || post[@"commentCount"] != [NSNull null]) {
                
                
                
                NSNumber * commentCount = post[@"commentCount"];
                
                if(commentCount != nil){
                    
                    NSString * commentString = [NSString stringWithFormat:@"%@", commentCount];
                    cell.partyCommentCount.text = commentString;
                }}
            
            postCreatedAt = [post createdAt];
            timeStampString = [postCreatedAt formattedAsTimeAgo];
            
            
            headline = post[@"headline"];
            
            if([headline isEqualToString:@""]){
                
                headline = nil;
                
            }
            
        }
        else{
            
            if ([name[@"status"]  isEqual: @1] || ([name[@"status"]  isEqual: @2] && party != nil)) {
                
                logDate = self.current[@"logIn"];
                
                timeStampString = [logDate formattedAsTimeAgo];
            }
            else if ([name[@"status"]  isEqual: @0] || ([name[@"status"]  isEqual: @2] && party == nil)) {
                
                logDate = self.current[@"logOut"];
                
                if(logDate == nil){
                    
                    logDate = [self.current updatedAt];
                    
                }
                
                timeStampString = [logDate formattedAsTimeAgo];
            }
            
        }
        if(name != [PFUser currentUser]){
            
            
            if(name[@"alertMessage"] == nil){
            
            NSString * alertMessage = [self messageAlert:status hasParty: hasParty newLocation:partyCreatedAt updatedLocation:locationUpdatedAt postDate:postCreatedAt logDate:logDate];
            
            if(alertMessage != nil){
                
                cell.alertLabel.text = alertMessage;
                if (party != nil) {
                   
                }
                
            }
            else{
                
                cell.alertLabel.text = @"";
                cell.movingLabel.text = @"";
                
            }
            }
            else{
                NSDate * messageTimeStamp = name[@"alertMessageTimeStamp"];
                NSTimeInterval alertMessageDifference = [now timeIntervalSinceDate:messageTimeStamp];
                NSNumber * messageTime = [NSNumber numberWithDouble:alertMessageDifference/60];
                
                if([messageTime integerValue] <= 15){
                    
                    //show message
                    
                    cell.alertLabel.text = name[@"alertMessage"];
                    
                }
                
                
            }
            
            if ([[name objectForKey:@"moving"] boolValue]) {
                cell.movingLabel.text = @"moving...";
            }
            
        }
        
        //places
        
        
        
        
        if(placeName != nil){
            
            NSTimeInterval timeDifference = [now timeIntervalSinceDate:googleTimeStamp];
            
            if (timeDifference <= 2592000) {
                
                if([placeName isEqualToString:@""]){
                    placeName = nil;
                }
                if(address != nil){
                    
                    if([address isEqualToString:@""]){
                        
                        address = nil;
                        
                    }
                    
                }
            }
            else{
                
                placeName = nil;
                googlePlacesID = nil;
                address = nil;
                
            }
            
        }
        
        //apple
        
        if(applePlaceMark != nil){
            
            if([applePlaceMark isEqualToString:@""]){
                applePlaceMark = nil;
            }
            
        }
        
        if(address != nil){
            
            if([address isEqualToString:@""]){
                
                address = nil;
                
            }
            
        }
        
        
        
        //android
        
        
        
        //            }];
        
        
        //        }];
        
        if(name == self.current){
            
            if(placeName != nil || applePlaceMark != nil || address != nil){
                
                
                // places
                if(placeName != nil){
                    
                    if(address != nil){
                        
                        subHeadline = placeName;
                        secondSubHeadline = address;
                        
                    }
                    else{
                        
                        subHeadline = placeName;
                        
                    }
                    
                }
                
                //apple
                
                if(applePlaceMark != nil){
                    
                    if(address != nil){
                        
                        subHeadline = applePlaceMark;
                        secondSubHeadline = address;
                        
                    }
                    else{
                        
                        subHeadline = applePlaceMark;
                        
                    }
                    
                }
                
                if(subHeadline == nil && address != nil){
                    
                    subHeadline = address;
                    
                }
                
            }
            
            if (timeStampString != nil) {
                
                if(subHeadline != nil){
                    
                    if(secondSubHeadline != nil){
                        
                        if(headline != nil){
                            
                            //                                    viewHolder.headline.setTypeface(null, Typeface.NORMAL);
                            //                                    viewHolder.headline.setTextColor(Color.parseColor("#FF010101"));
                            //                                    viewHolder.headline.setText(headline);
                            //                                    //                                viewHolder.subHeadline.setText(subHeadline + " " + secondSubHeadline);
                            //                                    viewHolder.subHeadline.setText(subHeadline);
                            //                                    viewHolder.timeStamp.setText(timeStampString);
                            
                            cell.profileHeadline.font = [UIFont systemFontOfSize:13.0];
                            cell.profileHeadline.textColor = [UIColor blackColor];
                            
                            cell.profileHeadline.text = headline;
                            cell.profileSubheadline.text = subHeadline;
                            cell.thridText.text = timeStampString;
                            
                        }
                        
                        else{
                            
                            //                                    viewHolder.headline.setText(subHeadline);
                            //                                    viewHolder.subHeadline.setText(secondSubHeadline);
                            //                                    viewHolder.timeStamp.setText(timeStampString);
                            
                            cell.profileHeadline.text = subHeadline;
                            cell.profileSubheadline.text = secondSubHeadline;
                            cell.thridText.text = timeStampString;
                            
                        }
                        
                    }
                    else{
                        
                        if(headline != nil){
                            
                            //                                    viewHolder.headline.setTypeface(null, Typeface.NORMAL);
                            //                                    viewHolder.headline.setTextColor(Color.parseColor("#FF010101"));
                            //                                    viewHolder.headline.setText(headline);
                            //                                    viewHolder.subHeadline.setText(subHeadline);
                            //                                    viewHolder.timeStamp.setText(timeStampString);
                            
                            cell.profileHeadline.font = [UIFont systemFontOfSize:13.0];
                            cell.profileHeadline.textColor = [UIColor blackColor];
                            
                            cell.profileHeadline.text = headline;
                            cell.profileSubheadline.text = subHeadline;
                            cell.thridText.text = timeStampString;
                            
                        }
                        
                        else{
                            
                            //                                    viewHolder.headline.setText(subHeadline);
                            //                                    viewHolder.subHeadline.setText(timeStampString);
                            //                                    viewHolder.timeStamp.setVisibility(View.INVISIBLE);
                            
                            cell.profileHeadline.text = subHeadline;
                            cell.profileSubheadline.text = timeStampString;
                            cell.thridText.hidden = true;
                            
                        }
                        
                    }
                    
                }
                else{
                    
                    if(headline != nil){
                        
                        //                                viewHolder.headline.setTypeface(null, Typeface.NORMAL);
                        //                                viewHolder.headline.setTextColor(Color.parseColor("#FF010101"));
                        //                                viewHolder.headline.setText(headline);
                        //                                viewHolder.subHeadline.setText(timeStampString);
                        //                                viewHolder.timeStamp.setVisibility(View.INVISIBLE);
                        
                        cell.profileHeadline.font = [UIFont systemFontOfSize:13.0];
                        cell.profileHeadline.textColor = [UIColor blackColor];
                        
                        cell.profileHeadline.text = headline;
                        cell.profileSubheadline.text = timeStampString;
                        cell.thridText.hidden = true;
                        
                    }
                    
                    else{
                        
                        //                                viewHolder.headline.setVisibility(View.INVISIBLE);
                        //                                viewHolder.subHeadline.setText(timeStampString);
                        //                                viewHolder.timeStamp.setVisibility(View.INVISIBLE);
                        
                        cell.profileHeadline.hidden = true;
                        cell.profileSubheadline.text = secondSubHeadline;
                        cell.thridText.hidden = true;
                        
                    }
                    
                }
                
            }
            else {
                
                if(subHeadline != nil){
                    
                    if(secondSubHeadline != nil){
                        
                        if(headline != nil){
                            
                            //                                    viewHolder.headline.setTypeface(null, Typeface.NORMAL);
                            //                                    viewHolder.headline.setTextColor(Color.parseColor("#FF010101"));
                            //                                    viewHolder.headline.setText(headline);
                            //                                    viewHolder.subHeadline.setText(subHeadline);
                            //                                    viewHolder.timeStamp.setText(secondSubHeadline);
                            
                            cell.profileHeadline.font = [UIFont systemFontOfSize:13.0];
                            cell.profileHeadline.textColor = [UIColor blackColor];
                            
                            cell.profileHeadline.text = headline;
                            cell.profileSubheadline.text = subHeadline;
                            cell.thridText.text = secondSubHeadline;
                            
                        }
                        
                        else{
                            
                            //                                    viewHolder.headline.setText(subHeadline);
                            //                                    viewHolder.subHeadline.setText(secondSubHeadline);
                            //                                    viewHolder.timeStamp.setVisibility(View.INVISIBLE);
                            
                            cell.profileHeadline.text = subHeadline;
                            cell.profileSubheadline.text = secondSubHeadline;
                            cell.thridText.hidden = true;
                            
                        }
                        
                    }
                    else{
                        
                        if(headline != nil){
                            
                            //                                    viewHolder.headline.setTypeface(null, Typeface.NORMAL);
                            //                                    viewHolder.headline.setTextColor(Color.parseColor("#FF010101"));
                            //                                    viewHolder.headline.setText(headline);
                            //                                    viewHolder.subHeadline.setText(subHeadline);
                            //                                    viewHolder.timeStamp.setVisibility(View.INVISIBLE);
                            
                            cell.profileHeadline.font = [UIFont systemFontOfSize:13.0];
                            cell.profileHeadline.textColor = [UIColor blackColor];
                            
                            cell.profileHeadline.text = headline;
                            cell.profileSubheadline.text = subHeadline;
                            cell.thridText.hidden = true;
                            
                        }
                        
                        else{
                            
                            //                                    viewHolder.headline.setText(subHeadline);
                            //                                    viewHolder.subHeadline.setVisibility(View.INVISIBLE);
                            //                                    viewHolder.timeStamp.setVisibility(View.INVISIBLE);
                            
                            cell.profileHeadline.text = subHeadline;
                            cell.profileSubheadline.hidden = true;
                            cell.thridText.hidden = true;
                            
                        }
                        
                    }
                    
                }
                else{
                    
                    if(headline != nil){
                        
                        //                                viewHolder.headline.setTypeface(null, Typeface.NORMAL);
                        //                                viewHolder.headline.setTextColor(Color.parseColor("#FF010101"));
                        //                                viewHolder.headline.setText(headline);
                        //                                viewHolder.subHeadline.setVisibility(View.INVISIBLE);
                        //                                viewHolder.timeStamp.setVisibility(View.INVISIBLE);
                        
                        cell.profileHeadline.font = [UIFont systemFontOfSize:13.0];
                        cell.profileHeadline.textColor = [UIColor blackColor];
                        
                        cell.profileHeadline.text = headline;
                        cell.profileSubheadline.hidden = true;
                        cell.thridText.hidden = true;
                        
                    }
                    
                    else{
                        
                        //                                viewHolder.headline.setText(mContext.getString(R.string.hello_partyfriends));
                        //                                viewHolder.subHeadline.setVisibility(View.INVISIBLE);
                        //                                viewHolder.timeStamp.setVisibility(View.INVISIBLE);
                        
                        cell.profileHeadline.text = @"Hello PartyFriends";
                        cell.profileSubheadline.hidden = true;
                        cell.thridText.hidden = true;
                        
                    }
                    
                }
                
            }
            
            
        }
        else{
            
            if(placeName != nil || applePlaceMark != nil || address != nil || distance != nil){
                
                if(placeName != nil){
                    
                    if(address != nil){
                        
                        if(distance != nil){
                            
                            //                                    subHeadline = placeName + @" " + distance;
                            subHeadline = [NSString stringWithFormat:@"%@ %@",placeName, distance];
                            secondSubHeadline = address;
                            
                        }
                        else{
                            
                            subHeadline = placeName;
                            secondSubHeadline = address;
                            
                        }
                        
                    }
                    else{
                        
                        if(distance != nil){
                            
                            subHeadline = [NSString stringWithFormat:@"%@ %@",placeName, distance];
                            
                        }
                        else{
                            
                            subHeadline = placeName;
                            
                        }
                        
                    }
                    
                }
                
                if(applePlaceMark != nil){
                    
                    if(address != nil){
                        
                        if(distance != nil){
                            
                            //                                    subHeadline = applePlaceMark + @" " + distance;
                            subHeadline = [NSString stringWithFormat:@"%@ %@",applePlaceMark, distance];
                            
                            secondSubHeadline = address;
                            
                        }
                        else{
                            
                            subHeadline = applePlaceMark;
                            secondSubHeadline = address;
                            
                        }
                    }
                    else{
                        
                        if(distance != nil){
                            
                            //                                    subHeadline = applePlaceMark + @" " + distance;
                            subHeadline = [NSString stringWithFormat:@"%@ %@",applePlaceMark, distance];
                            
                            
                        }
                        else{
                            
                            subHeadline = applePlaceMark;
                            
                        }
                    }
                    
                }
                
                if(subHeadline == nil && (address != nil || distance != nil)){
                    
                    if(address != nil){
                        
                        if(distance != nil){
                            
                            //                                    subHeadline = address + distance;
                            
                            subHeadline = [NSString stringWithFormat:@"%@ %@",address, distance];
                            
                            
                        }
                        else{
                            
                            subHeadline = address;
                            
                        }
                        
                    }
                    else{
                        
                        subHeadline = distance;
                        
                    }
                    
                }
                
            }
            
            if (timeStampString != nil) {
                
                if(subHeadline != nil){
                    
                    if(secondSubHeadline != nil){
                        
                        if(headline != nil){
                            
                            //                                    viewHolder.headline.setTypeface(null, Typeface.NORMAL);
                            //                                    viewHolder.headline.setTextColor(Color.parseColor("#FF010101"));
                            //                                    viewHolder.headline.setText(headline);
                            //                                    viewHolder.subHeadline.setText(subHeadline);
                            //                                    viewHolder.timeStamp.setText(timeStampString);
                            
                            cell.profileHeadline.font = [UIFont systemFontOfSize:13.0];
                            cell.profileHeadline.textColor = [UIColor blackColor];
                            
                            cell.profileHeadline.text = headline;
                            cell.profileSubheadline.text = subHeadline;
                            cell.thridText.text = timeStampString;
                            
                        }
                        
                        else{
                            
                            //                                    viewHolder.headline.setText(subHeadline);
                            //                                    viewHolder.subHeadline.setText(secondSubHeadline);
                            //                                    viewHolder.timeStamp.setText(timeStampString);
                            
                            cell.profileHeadline.text = subHeadline;
                            cell.profileSubheadline.text = secondSubHeadline;
                            cell.thridText.text = timeStampString;
                            
                        }
                        
                    }
                    else{
                        
                        if(headline != nil){
                            
                            //                                    viewHolder.headline.setTypeface(null, Typeface.NORMAL);
                            //                                    viewHolder.headline.setTextColor(Color.parseColor("#FF010101"));
                            //                                    viewHolder.headline.setText(headline);
                            //                                    viewHolder.subHeadline.setText(subHeadline);
                            //                                    viewHolder.timeStamp.setText(timeStampString);
                            
                            cell.profileHeadline.font = [UIFont systemFontOfSize:13.0];
                            cell.profileHeadline.textColor = [UIColor blackColor];
                            
                            cell.profileHeadline.text = headline;
                            cell.profileSubheadline.text = subHeadline;
                            cell.thridText.text = timeStampString;
                            
                        }
                        
                        else{
                            
                            //                                    viewHolder.headline.setText(subHeadline);
                            //                                    viewHolder.subHeadline.setText(timeStampString);
                            //                                    viewHolder.timeStamp.setVisibility(View.INVISIBLE);
                            
                            cell.profileHeadline.text = subHeadline;
                            cell.profileSubheadline.text = timeStampString;
                            cell.thridText.hidden = true;
                            
                        }
                        
                    }
                    
                }
                else{
                    
                    if(headline != nil){
                        
                        //                                viewHolder.headline.setTypeface(null, Typeface.NORMAL);
                        //                                viewHolder.headline.setTextColor(Color.parseColor("#FF010101"));
                        //                                viewHolder.headline.setText(headline);
                        //                                viewHolder.subHeadline.setText(timeStampString);
                        //                                viewHolder.timeStamp.setVisibility(View.INVISIBLE);
                        
                        cell.profileHeadline.font = [UIFont systemFontOfSize:13.0];
                        cell.profileHeadline.textColor = [UIColor blackColor];
                        
                        cell.profileHeadline.text = headline;
                        cell.profileSubheadline.text = timeStampString;
                        cell.thridText.hidden = true;
                        
                    }
                    
                    else{
                        
                        //                                viewHolder.headline.setVisibility(View.INVISIBLE);
                        //                                viewHolder.subHeadline.setText(timeStampString);
                        //                                viewHolder.timeStamp.setVisibility(View.INVISIBLE);
                        
                        cell.profileHeadline.hidden = true;
                        cell.profileSubheadline.text = timeStampString;
                        cell.thridText.hidden = true;
                        
                    }
                    
                }
                
            }
            else {
                
                if(subHeadline != nil){
                    
                    if(secondSubHeadline != nil){
                        
                        // check here to see if distance should be bellow placeName/appleName
                        
                        if(headline != nil){
                            
                            //                                    viewHolder.headline.setTypeface(null, Typeface.NORMAL);
                            //                                    viewHolder.headline.setTextColor(Color.parseColor("#FF010101"));
                            //                                    viewHolder.headline.setText(headline);
                            //                                    viewHolder.subHeadline.setText(subHeadline);
                            //                                    viewHolder.timeStamp.setText(secondSubHeadline);
                            
                            cell.profileHeadline.font = [UIFont systemFontOfSize:13.0];
                            cell.profileHeadline.textColor = [UIColor blackColor];
                            
                            cell.profileHeadline.text = headline;
                            cell.profileSubheadline.text = subHeadline;
                            cell.thridText.text = secondSubHeadline;
                            
                        }
                        
                        else{
                            
                            //                                    viewHolder.headline.setText(subHeadline);
                            //                                    viewHolder.subHeadline.setText(secondSubHeadline);
                            //                                    viewHolder.timeStamp.setVisibility(View.INVISIBLE);
                            
                            cell.profileHeadline.text = subHeadline;
                            cell.profileSubheadline.text = secondSubHeadline;
                            cell.thridText.hidden = true;
                            
                        }
                        
                    }
                    else{
                        
                        if(headline != nil){
                            
                            //                                    viewHolder.headline.setTypeface(null, Typeface.NORMAL);
                            //                                    viewHolder.headline.setTextColor(Color.parseColor("#FF010101"));
                            //                                    viewHolder.headline.setText(headline);
                            //                                    viewHolder.subHeadline.setText(subHeadline);
                            //                                    viewHolder.timeStamp.setVisibility(View.INVISIBLE);
                            
                            cell.profileHeadline.font = [UIFont systemFontOfSize:13.0];
                            cell.profileHeadline.textColor = [UIColor blackColor];
                            
                            cell.profileHeadline.text = headline;
                            cell.profileSubheadline.text = subHeadline;
                            cell.thridText.hidden = true;
                            
                        }
                        
                        else{
                            
                            //                                    viewHolder.headline.setText(subHeadline);
                            //                                    viewHolder.subHeadline.setVisibility(View.INVISIBLE);
                            //                                    viewHolder.timeStamp.setVisibility(View.INVISIBLE);
                            
                            cell.profileHeadline.text = subHeadline;
                            cell.profileSubheadline.hidden = true;
                            cell.thridText.hidden = true;
                            
                        }
                        
                    }
                    
                }
                else{
                    
                    if(headline != nil){
                        
                        //                                viewHolder.headline.setTypeface(null, Typeface.NORMAL);
                        //                                viewHolder.headline.setTextColor(Color.parseColor("#FF010101"));
                        //                                viewHolder.headline.setText(headline);
                        //                                viewHolder.subHeadline.setVisibility(View.INVISIBLE);
                        //                                viewHolder.timeStamp.setVisibility(View.INVISIBLE);
                        
                        cell.profileHeadline.font = [UIFont systemFontOfSize:13.0];
                        cell.profileHeadline.textColor = [UIColor blackColor];
                        
                        cell.profileHeadline.text = headline;
                        cell.profileSubheadline.hidden = true;
                        cell.thridText.hidden = true;
                        
                    }
                    
                    else{
                        
                        //                                viewHolder.headline.setText(mContext.getString(R.string.hello_partyfriends));
                        //                                viewHolder.subHeadline.setVisibility(View.INVISIBLE);
                        //                                viewHolder.timeStamp.setVisibility(View.INVISIBLE);
                        
                        cell.profileHeadline.text = @"Hello PartyFriends";
                        cell.profileSubheadline.hidden = true;
                        cell.thridText.hidden = true;
                        
                    }
                    
                }
                
            }
            
        }
        
        
        
        
        CALayer *imageLayer = cell.profileImage.layer;
        [imageLayer setCornerRadius:5];
        [imageLayer setBorderWidth:0];
        [imageLayer setMasksToBounds:YES];
        [cell.profileImage.layer setCornerRadius:cell.profileImage.frame.size.width/2];
        [cell.profileImage.layer setMasksToBounds:YES];
        
        PFObject * photo = name[@"profilePicture"];
        
        
        if(photo != nil){
            [photo fetchInBackgroundWithBlock:^(PFObject *object, NSError *error){
                if(!error){
                    
                    PFFile * pic = [object objectForKey:@"photo"];
                    PFImageView * friendImage = [PFImageView new];
                    friendImage.image = [UIImage imageNamed:@"nigger"];
                    friendImage.file = (PFFile *) pic;
                    
                    [friendImage loadInBackground:^(UIImage * image, NSError * error){
                        if (!error) {
                            
                            image = [self squareImageFromImage:image scaledToSize:image.size.height];
                            
                          //  image = [self imageByCroppingImage:image toSize:CGSizeMake(image.size.width, image.size.height)];
                            [cell.profileImage setImage:image];
                            
                        }else{
                            
                            
                            
                        }
                        
                        
                    }];
                    //                    [pic getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                    //                        if (!error) {
                    //                            UIImage *image = [UIImage imageWithData:data];
                    //                            [self imageByCrop:image];
                    //
                    //                            [cell.profileImage setImage:image];
                    //                        }
                    //                        else {
                    //                            UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
                    //                            [cell.profileImage setImage:defaultimg];
                    //                        }
                    //                    }];
                }
                
            }];
        }
        
        
        //        if (name[@"photo"] != nil) {
        //
        //        }
        else {
            UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
            [cell.profileImage setImage:defaultimg];
        }
        
        NSLog(@"HEADLINE == %@", [name objectForKey:@"headline"]);
        NSLog(@"%d", [name[@"headline"] isEqualToString:@""]);
        
        //    if (name[@"headline"] != nil) {
        //        //cell.detailTextLabel.text = name[@"headline"];
        //        cell.profileHeadline.text = name[@"headline"];
        //    }else {
        //
        //        PFGeoPoint *nameLocation = name[@"location"];
        //
        //        CLLocation * location = [[CLLocation alloc]initWithLatitude:nameLocation.latitude longitude:nameLocation.longitude];
        //
        //        CLGeocoder * waffles = [CLGeocoder new];
        //        [waffles reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        //            if (error) {
        //                NSLog(@"Error %@", error.description);
        //                cell.profileHeadline.text = @"nil";
        //
        //            } else {
        //                CLPlacemark * placemark = [placemarks lastObject];
        //                if (nameLocation == nil) {
        //                    cell.profileHeadline.text = [NSString stringWithFormat:@"past bed time!"];
        //                }
        //                else {
        //                cell.profileHeadline.text = placemark.name;
        //                }
        //            }
        //        }];
        //    }
        
        
        
        //////   fetching the Party headline before
        
        
        
        
        //        PFObject * party = name[@"lastParty"];
        //        [party fetchInBackgroundWithBlock:^(PFObject * object, NSError * error){
        //
        //
        //            if (!error) {
        //
        //
        //            cell.partyObject = party;
        //
        //            if ([cell.partyObject objectForKey:@"commentCount"]) {
        //
        //
        //
        //                NSNumber * commentCnt = [cell.partyObject objectForKey:@"commentCount"];
        //
        //                NSString * commentCntS = [NSString stringWithFormat:@"%@", commentCnt];
        //                if (commentCnt == nil) {
        //                    cell.partyCommentCount.text =[NSString stringWithFormat:@"0"];
        //                }else{
        //
        //                    cell.partyCommentCount.text = commentCntS;
        //
        //                    NSLog(@"Cell Party Comment Count = %@", commentCntS);
        //
        //                    NSLog(@"cell Party Object = %@", name[@"lastParty"]);
        //                }
        //            }}
        //
        //            if(party != nil){
        //                cell.commentButton.hidden = false;
        //                cell.partyCommentCount.hidden = false;
        //                NSNumber * commentCount = party[@"commentCount"];
        //
        //                NSString * commentString = [NSString stringWithFormat:@"%@", commentCount];
        //                cell.partyCommentCount.text =commentString;
        //                if (commentCount == nil){
        //                    cell.partyCommentCount.text =[NSString stringWithFormat:@"0"];
        //                }
        //
        //
        //
        //                if ([name[@"status"]  isEqual: @2]) {
        //                    cell.statusButton.image = [UIImage imageNamed:@"BlueBallon"];
        //                    cell.profileName.textColor = [UIColor blackColor];
        //
        //                }
        //                else if ([name[@"status"]  isEqual: @1]) {
        //                    cell.statusButton.image = [UIImage imageNamed:@"RedBallon"];
        //                    cell.profileName.textColor = [UIColor blackColor];
        //
        //
        //
        //
        //
        //
        //
        //                }
        //
        //
        //
        //                if (party[@"headline"] != nil) {
        //                    //cell.detailTextLabel.text = name[@"headline"];
        //                    cell.profileHeadline.text = party[@"headline"];
        //                }else {
        //
        //                    PFGeoPoint *nameLocation = party[@"location"];
        //
        //                    CLLocation * location = [[CLLocation alloc]initWithLatitude:nameLocation.latitude longitude:nameLocation.longitude];
        //
        //                    CLGeocoder * waffles = [CLGeocoder new];
        //                    [waffles reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        //                        if (error) {
        //                            NSLog(@"Error %@", error.description);
        //                            cell.profileHeadline.text = @"nil";
        //
        //                        } else {
        //                            CLPlacemark * placemark = [placemarks lastObject];
        //                            if (nameLocation == nil) {
        //                                cell.profileHeadline.text = [NSString stringWithFormat:@"past bed time!"];
        //                            }
        //                            else {
        //                                cell.profileHeadline.text = placemark.name;
        //                            }
        //                        }
        //                    }];
        //                }
        //
        //
        //
        //            }
        ////            else{
        ////                cell.profileHeadline.text = @"nil";
        ////            }
        //
        //        }];
        //        cell.partyObject = party;
        //
        //        if ([cell.partyObject objectForKey:@"commentCount"]) {
        //
        //
        //
        //       NSNumber * commentCnt = [cell.partyObject objectForKey:@"commentCount"];
        //
        //        NSString * commentCntS = [NSString stringWithFormat:@"%@", commentCnt];
        //        if (commentCnt == nil) {
        //            cell.partyCommentCount.text =[NSString stringWithFormat:@"0"];
        //        }else{
        //
        //        cell.partyCommentCount.text = commentCntS;
        //
        //        NSLog(@"Cell Party Comment Count = %@", commentCntS);
        //
        //        NSLog(@"cell Party Object = %@", name[@"lastParty"]);
        //        }
        //        }
        
    }
    
    
    
    return cell;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    PFNewsFeedTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    PFRefresh * userObject =[self.allArray objectAtIndex:indexPath.row];
    PFUser * user = userObject.user;
    if ([user[@"status"] isEqual:@2] || [user[@"status"] isEqual:@1]){
        
        PFObject * party = user[@"lastParty"];
        PFGeoPoint * location = party[@"location"];
        if (location != nil) {
            
            NSUInteger number = indexPath.row;
            NSLog(@"IndexAT  if this is even working %lu \n ??!?!??!?!?!?!??!?!?!?!?!??!?!?!?!?!",(unsigned long)number);
            
            
            //tabbar.numberPin = indexPath.row;
            
            
            // NSLog(@"this seeing the users object id = %@",party[@"createdBy"]);
            
            
            
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
            //        NSLog(@" this is the user:%@ and looking for their location: %@", user[@"firstName"], user[@"location"]);
            //        NSLog(@"is this the location now %@", location );
            //        // location = nil;
            tabbar.friendsLocation = location;
            tabbar.pinUser = user;
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            
            [self.tabBarController setSelectedIndex:0];}
        
    }
    
    
}


-(void)reverseGeocode:(CLLocation *) location {
    CLGeocoder * geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        
        NSLog(@"Finding address");
        if (error) {
            NSLog(@"Error %@", error.description);
            
        }else {
            CLPlacemark * placemark = [placemarks lastObject];
            self.yourLocation.text = placemark.name;
        }
    }];
}


-(void)OrientationDidChange:(NSNotification*)notification
{
    UIDeviceOrientation Orientation=[[UIDevice currentDevice]orientation];
    
    if(Orientation == UIInterfaceOrientationPortrait)
    {
        [self byeToView:nil];
    }
    
    else if(Orientation == UIDeviceOrientationPortraitUpsideDown)
    {
        [self performSegueWithIdentifier:@"addView" sender:self];
    }
    
    
}

-(void)newData{
    self.allArray = tabbar.refreshArray;
    
    NSLog(@"\n LOGGING \n to SEE if \n REFRESH \n is PASSING \n through %@", self.allArray);
    
}


-(IBAction)byeToView: (UIStoryboardSegue *) segue{
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"toHeatMap"]) {
        MapView * viewController = (MapView *)segue.destinationViewController;
        
        viewController.currentUsersLocation = self.yourCord;
    }
    
    if ([segue.identifier isEqualToString:@"toSettingsView"]) {
        SettingsView * Settings = (SettingsView *)segue.destinationViewController;
        
    }
    
    if ([segue.identifier isEqualToString:@"comments"]) {
        
        NSLog(@"comments segue from NewsFeed.m");
        
        
        PFCommentsTableViewController * commentsTVC = [segue destinationViewController];
        
        // commentsTVC.pfParty = self.selectedMessage;
        
    }
    
}

- (IBAction)logout:(id)sender {
    
    [PFUser logOut];
    
}

-(void)failedRefresh{
    [self.view makeToast:@"Couldn't refresh - Try again" duration:2.5 position:CSToastPositionCenter];
    [self.refreshControl endRefreshing];
    
    
}

-(void)panToHeatMap:(UIGestureRecognizer*)sender
{
    if (sender.state == UIGestureRecognizerStateEnded) {
        
        [self.tabBarController setSelectedIndex:0];
    }
    
}

- (UIImage *)imageByCroppingImage:(UIImage *)image toSize:(CGSize)size
{
    // not equivalent to image.size (which depends on the imageOrientation)!
    double refWidth = CGImageGetWidth(image.CGImage);
    double refHeight = CGImageGetHeight(image.CGImage);
    
    double x = (refWidth - size.width) / 2.0;
    double y = (refHeight - size.height) / 2.0;
    
    CGRect cropRect = CGRectMake(x, y, size.height, size.width);
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    
    UIImage *cropped = [UIImage imageWithCGImage:imageRef scale:0.0 orientation:image.imageOrientation];
    CGImageRelease(imageRef);
    
    return cropped;
}

-(UIImage *) imageByCrop:(UIImage * )image{
    
    // Create the image from a png file
    //    UIImage *image = [UIImage imageNamed:@"prgBinary.jpg"];
    //    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    
    // Get size of current image
    CGSize size = [image size];
    
    // Frame location in view to show original image
    //    [imageView setFrame:CGRectMake(0, 0, size.width, size.height)];
    //    [[self view] addSubview:imageView];
    //    [imageView release];
    
    // Create rectangle that represents a cropped image
    // from the middle of the existing image
    CGRect rect = CGRectMake(size.width / 4, size.height / 4 ,
                             (size.width / 2), (size.height / 2));
    
    // Create bitmap image from original image data,
    // using rectangle to specify desired crop area
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], rect);
    UIImage *img = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    // Create and show the new image from bitmap data
    //    imageView = [[UIImageView alloc] initWithImage:img];
    //    [imageView setFrame:CGRectMake(0, 200, (size.width / 2), (size.height / 2))];
    //    [[self view] addSubview:imageView];
    //    [imageView release];
    
    return img;
}

-(NSString *) locationDistanceString:(CLLocation*) partyObjectsLocation{
    CLLocation * currentUserLocation = [[CLLocation alloc] initWithLatitude:cood.latitude longitude:cood.longitude];
    
    CLLocationDistance distanceFromFriend = [currentUserLocation distanceFromLocation:partyObjectsLocation];
    double mToMi = distanceFromFriend/1609.344;
    NSString * stringDistance = [NSString new];
    
    if (mToMi < 1.0) {
        if (mToMi <0.0947) {
            stringDistance = [NSString stringWithFormat:@"500 feet away"];
        }else if (mToMi < 0.0473){
            stringDistance = [NSString stringWithFormat:@"250 feet away"];
            
        }else if(mToMi < 0.0189){
            stringDistance = [NSString stringWithFormat:@"100 feet away"];
        }else if (mToMi < 0.00946){
            stringDistance = [NSString stringWithFormat:@"near by"];
        }else{
            stringDistance = [NSString stringWithFormat:@"Less than a mile"];
        }
    }else{
        
        stringDistance = [NSString stringWithFormat:@"%.2f miles away", mToMi];
    }
    
    
    
    return stringDistance;
    
}

- (NSString *)messageAlert:(NSNumber *)status hasParty:(int)hasParty newLocation:(NSDate *)newLocation updatedLocation:(NSDate *) updatedLocation postDate:(NSDate *) postDate logDate:(NSDate *)logDate{
    NSString * alertMessage = nil;
    NSDate * now = [NSDate date];
    
    NSTimeInterval newOrUpdate;
    NSNumber *locationDifference = nil;
    
    NSNumber * updatedDifference = nil;
    
    NSNumber * logDateDifference = nil;
    
    NSNumber * postDateDifference = nil;
    
    //    NSTimeInterval newOrUpdate = [newLocation timeIntervalSinceDate:updateLocation];
    //    int mintues = newOrUpdate/60;
    
    
    //    if ([now alertTimeCheck:newLocation]) {
    //        message = @"New Location";
    //
    //    }else if ([now alertTimeCheck:updateLocation]){
    //        if (newOrUpdate > 15) {
    //
    //        }
    //    message = @"Updated Location";
    //    }
    //
    //    if ([now alertTimeCheck:logDate]){
    //
    //        if(statusNumber == 0){
    //
    //            //alertMessage = mContext.getString(R.string.offline);
    //            message = @"Offline";
    //        }
    //        else if(statusNumber == 1){
    //
    //            //alertMessage = mContext.getString(R.string.party_mode);
    //            message = @"PartyMode";
    //        }
    //        else if(statusNumber == 2){
    //
    //            //alertMessage = mContext.getString(R.string.exclusive);
    //            message = @"Exclusive";
    //        }
    //
    //    }
    //
    //    if ([now alertTimeCheck:PostDate]) {
    //        message = @"New Post";
    //    }
    
    if(newLocation != nil){
        
        newOrUpdate = [now timeIntervalSinceDate:newLocation];
        locationDifference = [NSNumber numberWithDouble: newOrUpdate/60];
        //        NSLog(@"locationDifference = %@l", locationDifference);
        //
        //        dateDifferenceCalculator = new DateDifferenceCalculator(mContext, newLocation);
        //
        //        if(dateDifferenceCalculator.differenceInDays() == 0 && dateDifferenceCalculator.remainingHours() == 0){
        
        //            locationDifference = dateDifferenceCalculator.remainingMins();
        
        if ([locationDifference integerValue] >= 15) {
            
            locationDifference = nil;
            
        }
        
        if(locationDifference != nil){
            
            //                alertMessage = mContext.getString(R.string.new_location);
            alertMessage = @"New Location!";
            
        }
        //        }
        
    }
    if(updatedLocation != nil){
        
        
        newOrUpdate = [now timeIntervalSinceDate:updatedLocation];
        updatedDifference =[NSNumber numberWithDouble: newOrUpdate/60];
        //        NSLog(@"updatedDifference = %@l", updatedDifference);
        
        
        //        dateDifferenceCalculator = new DateDifferenceCalculator(mContext, updatedLocation);
        //
        //        if(dateDifferenceCalculator.differenceInDays() == 0 && dateDifferenceCalculator.remainingHours() == 0){
        
        if(locationDifference != nil){
            
            if(updatedDifference < locationDifference){
                
                locationDifference = updatedDifference;
                //                    alertMessage = mContext.getString(R.string.location_updated);
                alertMessage = @"Location updated";
                
                
            }
            
        }
        else{
            
            locationDifference = updatedDifference;
            
            if ([locationDifference integerValue] >= 15) {
                
                locationDifference = nil;
                
            }
            
            if(locationDifference != nil){
                
                //                    alertMessage = mContext.getString(R.string.location_updated);
                alertMessage = @"Location updated";
                
                
            }
            
        }
        //        }
        
    }
    
    if(logDate != nil){
        
        newOrUpdate = [now timeIntervalSinceDate:logDate];
        logDateDifference = [NSNumber numberWithDouble: newOrUpdate/60];
        //        NSLog(@"logDateDifference = %@l", logDateDifference);
        
        
        //        dateDifferenceCalculator = new DateDifferenceCalculator(mContext, logDate);
        //
        //        if(dateDifferenceCalculator.differenceInDays() == 0 && dateDifferenceCalculator.remainingHours() == 0){
        
        //            logDateDifference =  dateDifferenceCalculator.remainingMins();
        
        if ([logDateDifference integerValue] >= 15) {
            
            logDateDifference = nil;
            
        }
        
        //        }
        
    }
    
    if(logDateDifference != nil && locationDifference != nil){
        
        if(logDateDifference <= locationDifference){
            
            locationDifference = logDateDifference;
            
            if([status isEqual:@0] && ([status isEqual:@2] && hasParty == 0)){
                
                //                alertMessage = mContext.getString(R.string.offline);
                alertMessage = @"Offline";
                
                
            }
            else if([status isEqual:@1]){
                
                //                alertMessage = mContext.getString(R.string.party_mode);
                alertMessage = @"PartyMode";
                
                
            }
            else if([status isEqual:@2] && hasParty == 1){
                
                //                alertMessage = mContext.getString(R.string.exclusive);
                alertMessage = @"Exclusive";
                
                
            }
            
        }
        
    }
    
    else if(logDateDifference != nil){
        
        locationDifference = logDateDifference;
        if([status isEqual:@0] || ([status isEqual:@2] && hasParty == 0)){
            
            //                alertMessage = mContext.getString(R.string.offline);
            alertMessage = @"Offline";
            
            
        }
        else if([status isEqual:@1]){
            
            //                alertMessage = mContext.getString(R.string.party_mode);
            alertMessage = @"PartyMode";
            
            
        }
        else if([status isEqual:@2] && hasParty == 1){
            
            //                alertMessage = mContext.getString(R.string.exclusive);
            alertMessage = @"Exclusive";
            
            
        }
    }
    
    if(postDate != nil){
        
        //        dateDifferenceCalculator = new DateDifferenceCalculator(mContext, postDate);
        //
        //        if(dateDifferenceCalculator.differenceInDays() == 0 && dateDifferenceCalculator.remainingHours() == 0){
        
        newOrUpdate = [now timeIntervalSinceDate:postDate];
        postDateDifference = [NSNumber numberWithInt: newOrUpdate/60];
        //        NSLog(@"postDateDifference = %@l", postDateDifference);
        
        
        //            postDateDifference =  dateDifferenceCalculator.remainingMins();
        
        if ([postDateDifference integerValue]>= 15) {
            
            postDateDifference = nil;
            
            
            
        }
        //        }
    }
    
    if(postDateDifference != nil && locationDifference != nil){
        
        if(postDateDifference < locationDifference){
            
            //            alertMessage = mContext.getString(R.string.new_post);
            alertMessage = @"New Post!";
            
            
        }
        
    }
    
    else if(postDateDifference != nil){
        
        //        alertMessage = mContext.getString(R.string.new_post);
        alertMessage = @"New Post!";
        
        
    }
    
    
    return alertMessage;
}


- (void)scrollViewWillBeginDragging:(UIScrollView *)activeScrollView{
    
    [tabbar removeKeyboard];
    
}

- (UIImage *)squareImageFromImage:(UIImage *)image scaledToSize:(CGFloat)newSize {
    CGAffineTransform scaleTransform;
    CGPoint origin;
    
    if (image.size.width > image.size.height) {
        CGFloat scaleRatio = newSize / image.size.height;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(-(image.size.width - image.size.height) / 2.0f, 0);
    } else {
        CGFloat scaleRatio = newSize / image.size.width;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(0, -(image.size.height - image.size.width) / 2.0f);
    }
    
    CGSize size = CGSizeMake(newSize, newSize);
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(size, YES, 0);
    } else {
        UIGraphicsBeginImageContext(size);
    }
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextConcatCTM(context, scaleTransform);
    
    [image drawAtPoint:origin];
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}
//- (UIImage *)imageByCroppingImage:(UIImage *)image toSize:(CGSize)size
//{
//    double x = (image.size.width - size.width) / 2.0;
//    double y = (image.size.height - size.height) / 2.0;
//    
//    CGRect cropRect = CGRectMake(x, y, size.height, size.width);
//    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
//    
//    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
//    CGImageRelease(imageRef);
//    
//    return cropped;
//}




@end
