//
//  tabbarViewController.h
//  VendorProfile
//
//  Created by Gilberto Silva on 11/30/15.
//  Copyright © 2015 Gilberto Silva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface tabbarViewController : UITabBarController

-(void)indexOne;
-(void)indexTwo;
-(void)indexThree;
-(void)fetchVendor;

@property (strong, nonatomic) PFObject * vendorObject;


@end
