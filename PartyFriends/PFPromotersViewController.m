//
//  PFPromotersViewController.m
//  PartyFriends
//
//  Created by Joshua Silva  on 1/14/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import "PFPromotersViewController.h"
#import "tabbarViewController.h"
#import "PFVendorEditPromoterTableViewCell.h"
#import "PFVendorSelectPromoTableViewController.h"

@interface PFPromotersViewController ()

@end

@implementation PFPromotersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /*UIBarButtonItem *addButton = [[UIBarButtonItem alloc]
                                  initWithImage:[UIImage imageNamed:@"add-party"]
                                  style:UIBarButtonItemStylePlain
                                  target:self action:@selector(addPromoTouch)];
    
    self.navigationItem.rightBarButtonItem = addButton;
    
    self.navigationController.navigationItem.rightBarButtonItem = addButton;
    */
    //[self.navigationItem.rightBarButtonItem setEnabled:NO];
    //[self.navigationItem.rightBarButtonItem setImage:nil];
    
    //PFVendorTabBarController * container = self.parentViewController;
    
    //tabbar = (tabbarViewController * ) self.tabBarController;
    tabbarViewController * container = (tabbarViewController *) self.tabBarController;
    
    
    NSLog(@"********* Parent VC = %@", container);
    
    self.vendorPFObject = container.vendorObject;
    
    NSLog(@"********* %@", self.vendorPFObject);
    
    
    //PFQuery * query = [PFQuery queryWithClassName:@"vendor"];
    
    PFRelation * promoters = [self.vendorPFObject relationForKey:@"promoters"];
    PFQuery *query = [promoters query];
    
    self.promoterList = [NSMutableArray new];
    
    
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if (!error) {
            self.promoterList = [NSMutableArray arrayWithArray:objects];
        }
        
        NSLog(@"*********** Promo List");
        NSLog(@"%@", self.promoterList);
        
        [self.tableView reloadData];
    }];
    
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:NO];
    
    [self viewDidLoad];
}

- (void) addPromoTouch
{
    NSLog(@"addPromoTouch");
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.promoterList.count;
}


- (PFVendorEditPromoterTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PFVendorEditPromoterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"promocell" forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[PFVendorEditPromoterTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"promocell"];
    }
    
    
    PFObject * promo = [self.promoterList objectAtIndex:indexPath.row];
    
    
    
    
    CALayer *imageLayer = cell.promoterProfileImage.layer;
    [imageLayer setCornerRadius:5];
    [imageLayer setBorderWidth:1];
    [imageLayer setMasksToBounds:YES];
    [cell.promoterProfileImage.layer setCornerRadius:cell.promoterProfileImage.frame.size.width/2];
    [cell.promoterProfileImage.layer setMasksToBounds:YES];
    
    
    if (promo[@"profilePic"] != nil) {
        
        PFFile * pic = [promo objectForKey:@"profilePic"];
        [pic getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
            if (!error) {
                UIImage *image = [UIImage imageWithData:data];
                [cell.promoterProfileImage setImage:image];
            }
            else {
                UIImage * defaultimg = [UIImage imageNamed:@"58.png"];
                [cell.promoterProfileImage setImage:defaultimg];
            }
        }];
    }
    else {
        UIImage * defaultimg = [UIImage imageNamed:@"PFIcon"];
        [cell.promoterProfileImage setImage:defaultimg];
    }
    
    
    cell.promoterNameLabel.text = [promo objectForKey:@"firstName"];
    
    return cell;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"pro"]) {
        
        PFVendorSelectPromoTableViewController * destVC = (PFVendorSelectPromoTableViewController *) [segue destinationViewController];
        
        destVC.vendorList = self.promoterList;
        destVC.vendorObject = self.vendorPFObject;
    }
    
}

-(IBAction)backToPromo:(UIStoryboardSegue *) segue{
    
    NSLog(@"random!!!");
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
