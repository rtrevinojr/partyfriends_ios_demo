//
//  LoginView.m
//  PartyFriends
//
//  Created by Joshua Silva  on 6/25/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import "LoginView.h"
#import "Reach.h"
#import <Parse/Parse.h>
#import "UIView+Toast.h"

@interface LoginView ()

@end

@implementation LoginView

- (void)viewDidLoad {
    [super viewDidLoad];

    self.username.delegate= self;
    self.password.delegate = self;
    self.navigationController.navigationBarHidden = YES;
    
    PFUser * user = [PFUser currentUser];
    
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:(142/255.0) green:(83/255.0) blue:(249/255.0) alpha:1.0];
    
    [self.view setTintColor:[UIColor colorWithRed:(142/255.0) green:(83/255.0) blue:(249/255.0) alpha:1.0]];
    
    self.activityIndicator.color = [UIColor colorWithRed:(142/255.0) green:(83/255.0) blue:(249/255.0) alpha:1.0];
    self.activityIndicator.hidden = YES;
   // [self.login setHighlighted:YES];
    
}

-(void) activityOffAction{
    self.activityIndicator.hidden = YES;
    [self.activityIndicator stopAnimating];
    

}


-(void) viewDidAppear:(BOOL)animated
{
    PFUser * currentUser = [PFUser currentUser];
    if (currentUser) {
        NSLog(@"current user: %@", currentUser.username);
        [self performSegueWithIdentifier:@"striaghtAction" sender:self];
    }
}

-(void) viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES];
    [super viewWillAppear:YES];


}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    [self.view endEditing:YES];
    
    return YES;
}



- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y-200);
    [self.scrollView setContentOffset:scrollPoint animated:YES];
    
    
}



- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self.scrollView setContentOffset:CGPointZero animated:YES];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)login:(id)sender {
    

    
    if ([Reachability internetCheck]) {
        [sender setEnabled:NO];
        [self.activityIndicator startAnimating];
        self.activityIndicator.hidden = NO;
    
    
    NSString * username = [self.username.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString * password = [self.password.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([username length] == 0 || [password length]==0) {
        UIAlertView * error = [[UIAlertView alloc] initWithTitle:@"Empty Fields" message:@"You need to put in your stuff!" delegate: nil cancelButtonTitle:@"Try again" otherButtonTitles: nil];
        [error show];
        [sender setEnabled:YES];
        
        [self activityOffAction];
        
    }else{
        [PFUser logInWithUsernameInBackground:username password:password block:^(PFUser *user, NSError *error) {
            if (error) {
                UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Sorry" message:[error.userInfo objectForKey:@"error"] delegate:nil cancelButtonTitle:@"Try again!!" otherButtonTitles:nil];
                [alertview show];
                [sender setEnabled:YES];
                [self activityOffAction];
            }
            else {
                // Store the deviceToken in the current installation and save it to Parse.
                PFInstallation *currentInstallation = [PFInstallation currentInstallation];
                NSLog(@"this at the log on to check if the current user is shoeing on time %@", [PFUser currentUser]);
                //[currentInstallation removeObjectForKey:@"createdBy"];
                currentInstallation[@"createdBy"] = [PFUser currentUser];
                //  [currentInstallation setDeviceTokenFromData:deviceToken];
                [currentInstallation saveInBackgroundWithBlock:^(BOOL secuess, NSError * error){
                    if (!error) {
                        NSLog(@"the installation was a good");
                        
                        [self performSegueWithIdentifier:@"straightAction" sender:sender];
                        [sender setEnabled:YES];
                        [self activityOffAction];
                    }else{
                        [currentInstallation saveEventually];
                        NSLog(@"ERROROROROROROOROROROROROROOROROROROOR");
                        [self performSegueWithIdentifier:@"straightAction" sender:sender];
                        [sender setEnabled:YES];
                        [self activityOffAction];
                    }
                    
                }];
            }
            
            
            
            
        }];
        
        
    }
    }else if (![Reachability internetCheck]){
    
        [self.view makeToast:@"Try again: No internet connection" duration:3 position:CSToastPositionCenter];
    
    }
}

-(IBAction)backToLogin:(UIStoryboardSegue *) segue{
    
    NSLog(@"random!!!");
    
}

- (IBAction)backgroundTap:(id)sender {
    
    [self.view endEditing:YES];
    
}


-(IBAction)byeToView: (UIStoryboardSegue *) segue{
    
    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
