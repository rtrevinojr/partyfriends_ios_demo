//
//  ViewController.h
//  VendorProfile
//
//  Created by Gilberto Silva on 11/30/15.
//  Copyright © 2015 Gilberto Silva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>


@interface ViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *vendorAccounts;
@property (strong, nonatomic) PFUser *currentUser;
@property (strong, nonatomic) PFObject * vendorObject;
- (IBAction)backButton:(id)sender;
@property (strong, nonatomic) IBOutlet UINavigationBar *navigationBar;

@end