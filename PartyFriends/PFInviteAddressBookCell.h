//
//  PFInviteAddressBookCell.h
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 11/5/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PFInviteAddressBookCell : UITableViewCell


@property (strong, nonatomic) NSString * cellObjectId;


@property (weak, nonatomic) IBOutlet UILabel *ContactNameLabel;


@property (weak, nonatomic) IBOutlet UILabel *ContactPhoneLabel;

@property (weak, nonatomic) IBOutlet UIButton *ContactInviteBtn;


@end
