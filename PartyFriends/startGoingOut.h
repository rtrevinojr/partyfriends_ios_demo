//
//  startGoingOut.h
//  PartyFriends
//
//  Created by Joshua Silva  on 8/6/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Parse/Parse.h"

@interface startGoingOut : UIViewController <UIScrollViewDelegate,UITableViewDataSource, UITabBarDelegate, UITableViewDelegate>


@property (strong, nonatomic) IBOutlet UITextField *editingLocation;


@property (strong, nonatomic) IBOutlet UIDatePicker *setTimer;

@property (strong, nonatomic) IBOutlet UITableView *friends;

@property (strong, nonatomic) NSArray * waf;


- (IBAction)beginGoingOut:(id)sender;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

//-(BOOL) isFriend:(PFUser *) user;


@property (strong, nonatomic) IBOutlet UIView *contentView;


-(void)localNotificationGoingOut: (NSDate * ) firetime;




@end
