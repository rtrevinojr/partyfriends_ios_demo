//
//  PFCreatePartyMapViewController.m
//  PartyFriends
//
//  Created by Joshua Silva  on 4/25/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import "PFCreatePartyMapViewController.h"
#import "LocationManagerSingleton.h"



@interface PFCreatePartyMapViewController ()

@end

@implementation PFCreatePartyMapViewController

float spanA = 0.00725;
float spanO = 0.00725;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    MKCoordinateRegion region;
    region.center.latitude = [LocationManagerSingleton sharedSingleton].locationManager.location.coordinate.latitude;
    region.center.longitude = [LocationManagerSingleton sharedSingleton].locationManager.location.coordinate.longitude;
    region.span = MKCoordinateSpanMake(spanA, spanO);
    
    [self.mapView setRegion:region animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)localSearch:(NSString *)placeName{

    [self.mapView removeAnnotations:self.mapView.annotations];
    
    MKLocalSearchRequest *searchRequest = [[MKLocalSearchRequest alloc] init];
    [searchRequest setNaturalLanguageQuery:placeName];
    
    CLLocationCoordinate2D parisCenter = CLLocationCoordinate2DMake([LocationManagerSingleton sharedSingleton].locationManager.location.coordinate.latitude, [LocationManagerSingleton sharedSingleton].locationManager.location.coordinate.longitude);
    MKCoordinateRegion parisRegion = MKCoordinateRegionMakeWithDistance(parisCenter, 15000, 15000);
    [searchRequest setRegion:parisRegion];
    
    // Create the local search to perform the search
    MKLocalSearch *localSearch = [[MKLocalSearch alloc] initWithRequest:searchRequest];
    
    
    
    [localSearch startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error) {
        if (!error) {
            for (MKMapItem *mapItem in [response mapItems]) {
                NSLog(@"Name: %@, MKAnnotation title: %@", [mapItem name], [[mapItem placemark] title]);
                NSLog(@"Coordinate: %f %f", [[mapItem placemark] coordinate].latitude, [[mapItem placemark] coordinate].longitude);
                // Should use a weak copy of self
                [self.mapView addAnnotation:[mapItem placemark] ];
            }
        } else {
            NSLog(@"Search Request Error: %@", [error localizedDescription]);
        }
    }];

}

- (IBAction)waffles:(id)sender {
    
    [self localSearch:self.searchField.text];
    
//    CLGeocoder * geocoder = [CLGeocoder new];
//    [geocoder geocodeAddressString:self.searchField.text completionHandler:^(NSArray *placemarks, NSError *error) {
//        if (error) {
//            NSLog(@"%@", error);
//            
//        }else {
//            CLPlacemark * placemark = [placemarks lastObject];
//            
//            MKCoordinateRegion region;
//            region.center.latitude = placemark.location.coordinate.latitude;
//            region.center.longitude = placemark.location.coordinate.longitude;
//            region.span = MKCoordinateSpanMake(spanA, spanO);
//            
//            [self.mapView setRegion:region animated:YES];
//            
//            
//            self.addressLabel.text= [NSString stringWithFormat:@" lat is %f \n log is %f",region.center.latitude, region.center.longitude ];
//            
//            
//            
//            
//            
//            
//            
//        }
//    }];

    
    /*
     [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
     
     if ( ([[UIDevice currentDevice] orientation] ==  UIDeviceOrientationPortrait)  )
     {
     // do something for Portrait mode
     }
     else if(([[UIDevice currentDevice] orientation] == UIDeviceOrientationPortraitUpsideDown) )
     {
     
     [self performSegueWithIdentifier:@"seg" sender:sender];
     
     }*/
    
    
}

- (IBAction)pinDrop:(id)sender {
    
    CLLocation * mapLocation = [[CLLocation alloc] initWithLatitude:self.mapView.centerCoordinate.latitude longitude:self.mapView.centerCoordinate.longitude];
    
    [self reverseGeocode:mapLocation];
    
    
}

- (IBAction)myLocation:(UIButton *)sender {
    
    
    self.usersLocation = [LocationManagerSingleton sharedSingleton].locationManager.location;
    //A quick NSlog to show us that location data is
    MKCoordinateRegion region;
    region.center.latitude = [LocationManagerSingleton sharedSingleton].locationManager.location.coordinate.latitude;
    region.center.longitude = [LocationManagerSingleton sharedSingleton].locationManager.location.coordinate.latitude;
    region.span = MKCoordinateSpanMake(spanA, spanO);
    [self.mapView setRegion:region animated:YES];
    

    
    [self reverseGeocode:self.usersLocation];
    
}



-(void)reverseGeocode:(CLLocation *) location {
    CLGeocoder * geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        
        NSLog(@"Finding address");
        if (error) {
            NSLog(@"Error %@", error.description);
            
        }else {
            CLPlacemark * placemark = [placemarks lastObject];
            self.addressLabel.text = [NSString stringWithFormat:@"%@", ABCreateStringWithAddressDictionary(placemark.addressDictionary, NO)];
            
            
        }
    }];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)dropPinButton:(id)sender {
    
    
}
@end
