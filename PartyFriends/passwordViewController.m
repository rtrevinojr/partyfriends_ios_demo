//
//  passwordViewController.m
//  PartyFriends
//
//  Created by Joshua Silva  on 9/4/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import "passwordViewController.h"
#import "Gradient.h"

@interface passwordViewController ()

@end

@implementation passwordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    

    
    self.currentUser = [PFUser currentUser];
    
    
    CAGradientLayer * randomLayer = [Gradient gold];
    
    randomLayer.frame = CGRectMake(0, -20, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height + 20);
    
    [randomLayer setStartPoint:CGPointMake(0.0, 0.5)];
    [randomLayer setEndPoint:CGPointMake(1.0, 0.5)];
    self.oldPassword.delegate = self;
    self.newpass.delegate = self;
    self.comfirm.delegate = self;
    
    
   // [self.navigationController.navigationBar.layer insertSublayer:randomLayer atIndex:1];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
        NSLog(@"the subviews for the bar %@",self.navigationController.navigationBar.subviews);
            NSLog(@"the number of sublayers, %@",self.navigationController.navigationBar.layer.sublayers);
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    [self.view endEditing:YES];
    
    return YES;
}



- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y-100);
    [self.scrollView setContentOffset:scrollPoint animated:YES];
    
    
}



- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self.scrollView setContentOffset:CGPointZero animated:YES];
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)passwordChange:(id)sender {
    
    NSString * pass = self.newpass.text;
    NSString * comfirmPass = self.comfirm.text;
    NSString * oldPass = self.oldPassword.text;
    NSString * username = self.currentUser.username;
    
    NSRegularExpression * numberRegex = [NSRegularExpression regularExpressionWithPattern:@"[0-9]" options:0 error:nil];
    
        NSUInteger numbercount = (unsigned long)[numberRegex matchesInString:pass options:0 range: NSMakeRange(0, pass.length)].count;
    
    
    
    [PFUser logInWithUsernameInBackground:username password:oldPass block:^(PFUser *user, NSError *error) {
        if (error) {
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Incorrect password" message:[error.userInfo objectForKey:@"error"] delegate:nil cancelButtonTitle:@"Try again!" otherButtonTitles:nil];
            [alertview show];
            [sender setEnabled:YES];
            //[self activityOffAction];
        }
        else {
            
            
            if (![pass isEqualToString:comfirmPass]) {
                UIAlertView * error = [[UIAlertView alloc]initWithTitle:@"Passwords don't match" message:@"Your new passwords do not match." delegate:nil cancelButtonTitle:@"Thank you." otherButtonTitles: nil];
                [error show];
            }else {
                if (pass.length <= 6|| pass.length >=12) {
                    UIAlertView * theShitagain = [[UIAlertView alloc] initWithTitle:@"Password error" message:@"Password must be between 6 and 12 chacaters long" delegate:nil cancelButtonTitle:@"thank you :)" otherButtonTitles:nil];
                    [theShitagain show];
                }
                else{
                    
                    if (numbercount == 0) {
                        UIAlertView * theShit = [[UIAlertView alloc] initWithTitle:@"Password Error" message:@"Please double check if your password has atleast a number" delegate:nil cancelButtonTitle:@"safety first!" otherButtonTitles:nil];
                        [theShit show];
                    }else{
                        
                        
                        self.currentUser.password = pass;
                        NSLog(@"password should be passed to parse at this point!");
                        
                        
                        [self.currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                            if (error) {
                                UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"sorry" message:[error.userInfo objectForKey:@"Error connection, was unable to save new password."] delegate:nil cancelButtonTitle:@" Try Again!" otherButtonTitles: nil];
                                [alertview show];
                            }
                            else{
                                
                                [self.navigationController popToRootViewControllerAnimated:YES];
                                
                                
                            }
                        }];
                        
                    }
                }}
            
            
        
        
        
        }
        }];
    
    
//    [PFCloud callFunctionInBackground:@"CheckPassword" withParameters:@{@"password":oldPass} block:^(NSString * results, NSError * error){
//    
//        if(error){
//            if(error.code == 141){
//                UIAlertView * incorrectPass = [[UIAlertView alloc] initWithTitle:@"Incorrect password" message:@"Old password is incorrect" delegate:nil cancelButtonTitle:@"try again" otherButtonTitles:nil];
//                [incorrectPass show];
//                
//                
//                
//                }else{
//                    UIAlertView * anotherError = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Old password is incorrect" delegate:nil cancelButtonTitle:@"try again" otherButtonTitles:nil];
//                    [anotherError show];
//            
//            }
//           // NSLog(@"the resutls: %@", results);
//            NSLog(@"ERROR: %@ $$$ ERROR number: %ld", error, (long)error.code);
//            
//        }else {
//            NSLog(@"ERROR: %@ $$$ ERROR number: %ld", error, (long)error.code);
//        
//
//  
//    
//   
//    if (![pass isEqualToString:comfirmPass]) {
//        UIAlertView * error = [[UIAlertView alloc]initWithTitle:@"Passwords don't match" message:@"Your passwords doesn't match" delegate:nil cancelButtonTitle:@"it happens to the best of us" otherButtonTitles: nil];
//        [error show];
//    }else {
//        if (pass.length <= 8|| pass.length >=12) {
//            UIAlertView * theShitagain = [[UIAlertView alloc] initWithTitle:@"password error" message:@"password must be between 8 and 12 chacaters long" delegate:nil cancelButtonTitle:@"thank you :)" otherButtonTitles:nil];
//            [theShitagain show];
//        }
//        else{
//            
//            if (numbercount == 0) {
//                UIAlertView * theShit = [[UIAlertView alloc] initWithTitle:@"password error" message:@"please double check if your password has atleast a number" delegate:nil cancelButtonTitle:@"safety first!" otherButtonTitles:nil];
//                [theShit show];
//            }else{
//                
//                
//                self.currentUser.password = pass;
//                NSLog(@"password should be passed to parse at this point!");
//                
//                
//                [self.currentUser signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//                    if (error) {
//                        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"sorry" message:[error.userInfo objectForKey:@"error"] delegate:nil cancelButtonTitle:@" Try Again!!" otherButtonTitles: nil];
//                        [alertview show];
//                    }
//                    else{
//                      
//                        [self.navigationController popToRootViewControllerAnimated:YES];
//                    
//                    
//                    }
//                }];
//            
//            }
//        }}
//        
//    
//        }
//        
//        
//        
//    }];
    
}

- (IBAction)backgroundTap:(id)sender {
    
    [self.view endEditing:YES];
    
}




@end
