//
//  PFCreateVendorViewController.m
//  PartyFriends
//
//  Created by Joshua Silva  on 1/10/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import "PFCreateVendorViewController.h"

@interface PFCreateVendorViewController ()

@end

@implementation PFCreateVendorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.currentUser = [PFUser currentUser];
    
    self.businessName.delegate = self;
    self.descriptionView.delegate = self;
    self.address.delegate = self;
    self.businessPhone.delegate = self;
    self.phone.delegate = self;
    self.businessPhone.delegate = self;
    self.confirmEmail.delegate = self;
    self.businessFax.delegate = self;
    self.faxNumber.delegate = self;
    self.website.delegate = self;
    self.zip.delegate= self;
    
    self.theStates = [[NSArray alloc]initWithObjects: @"AL",@"AK",@"AZ",@"AR",@"CA",@"CO",@"CT",@"DE",@"DC",@"FL",@"GA",@"HI",@"ID",@"IL",@"IN",@"IA",@"KS",@"KY",@"LA",@"ME",@"MD",@"MA",@"MI",@"MN",@"MS",@"MO",@"MT",@"NE",@"NV",@"NH",@"NJ",@"NM",@"NY",@"NC",@"ND",@"OH",@"OK",@"OR",@"PA",@"RI",@"SC",@"SD",@"TN",@"TX",@"UT",@"VT",@"VA",@"WA",@"WV",@"WI",@"WY",nil];
    
    
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
    
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return [self.theStates count];
    
}


-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    self.userState = [[NSString alloc] initWithFormat:@"%@", [self.theStates objectAtIndex:row]];
    
    
}



-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return [self.theStates objectAtIndex:row];
    
}



- (IBAction)createVendor:(id)sender {
    
    
    
    
    
    NSString * zipString = [self.zip.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString * phoneString = [self.businessPhone.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSRegularExpression * capitalRegex = [NSRegularExpression regularExpressionWithPattern:@"[A-Z]" options:0 error:nil];
    NSRegularExpression * numberRegex = [NSRegularExpression regularExpressionWithPattern:@"[0-9]" options:0 error:nil];
    NSRegularExpression * letterRegex = [NSRegularExpression regularExpressionWithPattern:@"[a-z]" options:0 error:nil];
    
    
    NSUInteger phoneNumberletter = (unsigned long)[letterRegex matchesInString:phoneString options:0 range:NSMakeRange(0, phoneString.length)].count;
    NSUInteger phoneNumberCap = (unsigned long)[capitalRegex matchesInString:phoneString options:0 range:NSMakeRange(0, phoneString.length)].count;
    
    NSUInteger zipCheckLetter = (unsigned long)[letterRegex matchesInString:zipString options:0 range:NSMakeRange(0, zipString.length)].count;
    NSUInteger zipCheckCap = (unsigned long)[capitalRegex matchesInString:zipString options:0 range:NSMakeRange(0, zipString.length)].count;
    
    //NSUInteger zipCheck = (unsigned long)[numberRegex matchesInString:zipString options:0 range: NSMakeRange(0, zipString.length)].count;
    
    if([zipString length] != 0 && self.businessName.text.length != 0 && self.businessPhone.text.length != 0 && self.city.text.length != 0 && self.address.text.length != 0 && self.businessEmail.text.length != 0){
        if ([zipString length] == 5 && zipCheckLetter == 0 && zipCheckCap == 0) {
            
            if (phoneString.length == 10 && phoneNumberCap == 0 && phoneNumberletter == 0) {
                
                NSNumberFormatter * zipCode = [NSNumberFormatter new];
                zipCode.numberStyle = NSNumberFormatterDecimalStyle;
                NSNumber * zip = [zipCode numberFromString:self.zip.text];
                
                NSString *address = [NSString stringWithFormat:@"%@ %@ %@ %@", self.address.text,self.city.text, self.userState, self.zip.text];
                CLGeocoder * geocoder = [CLGeocoder new];
                
                __block PFGeoPoint * geoPoint = [PFGeoPoint new];
                
                
                [geocoder geocodeAddressString:address
                             completionHandler:^(NSArray* placemarks, NSError* error){
                                 //for (CLPlacemark* aPlacemark in placemarks)
                                 //{
                                 // Process the placemark.
                                 
                                 CLPlacemark * aPlacemark = [placemarks lastObject];
                                 
                                 CLLocation * coordinate = [aPlacemark location];
                                 
                                 CLLocationCoordinate2D cood = [coordinate coordinate];
                                 
                                 geoPoint = [PFGeoPoint geoPointWithLatitude:cood.latitude longitude:cood.longitude];
                                 
                                 NSLog(@"*** GEOPOINT = %@", geoPoint);
                                 
                                 
                                 
                                 PFACL *acl  = [PFACL new];
                                 [acl setPublicReadAccess:true];
                                 [acl setPublicWriteAccess:true];
                                 
                                 
                                 
                                 PFObject *vendorAccount = [PFObject objectWithClassName:@"vendor"];
                                 vendorAccount[@"businessName"] = self.businessName.text;
                                 vendorAccount[@"address"] = self.address.text;
                                 vendorAccount[@"city"] = self.city.text;
                                 vendorAccount[@"state"] = self.userState;
                                 vendorAccount[@"zip"] = zip;
                                 vendorAccount[@"description"] = self.descriptionView.text;
                                 vendorAccount[@"businessPhone"] = self.businessPhone.text;
                                 vendorAccount[@"phone"] = self.phone.text;
                                 vendorAccount[@"businessEmail"] = self.businessEmail.text;
                                 vendorAccount[@"email"] = self.businessEmail.text;
                                 vendorAccount[@"businessFax"] = self.businessFax.text;
                                 vendorAccount[@"fax"] = self.faxNumber.text;
                                 vendorAccount[@"website"] = self.website.text;
                                 vendorAccount[@"location"] = geoPoint;
                                 [vendorAccount setACL:acl];
                                 [vendorAccount saveInBackgroundWithBlock:^(BOOL succeded, NSError * error){
                                     if(!error)
                                     {
                                         
                                         [self performSegueWithIdentifier:@"exitView" sender:self];
                                         
                                     }
                                     else{
                                         
                                         UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error" message:[error.userInfo objectForKey:@"error"] delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
                                         [alertview show];
                                         
                                     }
                                     
                                 }];
                             }];
                
                
            }
            else{
                
                UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Business Phone number should be 10 digits only. No characters." delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
                [alertview show];
            }
        }
        else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Zip should be 5 digits." delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
            [alertview show];
        }
    }
    else{
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Make sure all required fields are filled." delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
        [alertview show];
    }
    
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)backgroundTap:(id)sender {
    
    [self.view endEditing:YES];
    
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    [self.view endEditing:YES];
    
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
