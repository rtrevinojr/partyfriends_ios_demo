//
//  PFHostingTableViewController.m
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 11/16/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import "PFHostingTableViewController.h"

#import "PFHostingTableViewCell.h"

#import "PFHostingProfileDetailViewController.h"
#import "Gradient.h"

#import <Parse/Parse.h>


@implementation PFHostingTableViewController




- (IBAction)setIndex:(id)sender {
    
    switch (self.partyTypeSelection.selectedSegmentIndex)
    {
        case 0:
            //First selected
            NSLog(@"segmented control = 0");
            _invitationMode = @"Invites";
            [self.tableView reloadData];
            //[self viewDidLoad];
            break;
        case 1:
            //Second Segment selected
            NSLog(@"segmented control = 1");
            self.invitationMode = @"Hostings";
            [self.tableView reloadData];
            //[self viewDidLoad];
            break;
        default:
            self.invitationMode = @"Hostings";
            [self.tableView reloadData];
            break; 
    }
    
}


- (void)refreshTable {
    //TODO: refresh your data
    [self.refreshControl endRefreshing];
    [self.tableView reloadData];
}
 

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    
    //[tap setCancelsTouchesInView:NO];
    
    CGSize barSize = CGSizeMake(self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height +20);
    
    CAGradientLayer * whiteLayer = [Gradient white];
    
    CAGradientLayer * randomLayer = [Gradient gold];
    
    whiteLayer.frame = CGRectMake(0, -20, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height + 20);
    
    randomLayer.frame = CGRectMake(0, -20, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height + 20);
    
    [whiteLayer setStartPoint:CGPointMake(0.0, 0.5)];
    [whiteLayer setEndPoint:CGPointMake(1.0, 0.5)];
    //    [randomLayer setStartPoint:CGPointMake(0.0, 0.5)];
    //    [randomLayer setEndPoint:CGPointMake(1.0, 0.5)];
    
    [randomLayer setStartPoint:CGPointMake(-0.5, 0.0)];
    [randomLayer setEndPoint:CGPointMake(1.5, 0.0)];
    UIGraphicsBeginImageContext(barSize);
    [randomLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * anotherImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.navigationController.navigationBar setBackgroundImage:anotherImage forBarMetrics:UIBarMetricsDefault];
       // [self.navigationController.navigationBar.layer insertSublayer:randomLayer below:self.navigationController.navigationBar.layer];
    //[self.navigationController.navigationBar.layer insertSublayer:whiteLayer atIndex:1];
        NSLog(@"the number of sublayers, %@",self.navigationController.navigationBar.layer.sublayers);

        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:(142/255.0) green:(83/255.0) blue:(249/255.0) alpha:1.0];

    
    self.refreshControl = [[UIRefreshControl alloc]init];
    [self.tableView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    
    self.partyTypeSelection.selectedSegmentIndex = 0;
    
    self.invitationMode = @"Invites";
    
    self.tableView.delegate = self;
    
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc]
                                   initWithImage:[UIImage imageNamed:@"add-party"]
                                   style:UIBarButtonItemStylePlain
                                   target:self action:@selector(addPartyTouch)];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                  initWithImage:[UIImage imageNamed:@"arrow-left"]
                                  style:UIBarButtonItemStylePlain
                                  target:self action:@selector(goBack)];
    
    self.navigationItem.leftBarButtonItem = backButton;
    
    UIBarButtonItem * backBtn = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:self action:@selector(goBack)];
    
    self.navigationItem.rightBarButtonItem = addButton;
    

    
    //[self.navigationItem.backBarButtonItem setAction:@selector(goBack)];
    
    
    PFQuery * query = [PFQuery queryWithClassName:@"invitations"];
    [query includeKey:@"createdBy"];
    
    [query whereKey:@"createdBy" equalTo:[PFUser currentUser]];
    
    
    //[query whereKey:@"createdBy" notEqualTo:[PFUser currentUser]];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if (!error) {
            self.hostingList = objects;
            //self.inviteList = objects;
            [self.tableView reloadData];
            
            NSLog(@"hostlistDOG %lu", (unsigned long)self.hostingList.count);
        }
        else {
            NSLog(@"Query Error");
        }
        //[self.tableView reloadData];
    }];
    
    PFQuery * queryInvites = [PFQuery queryWithClassName:@"invitations"];
    
    [queryInvites includeKey:@"createdBy"];
    
    [queryInvites whereKey:@"createdBy" notEqualTo:[PFUser currentUser]];
    
    [queryInvites findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        
        if (!error) {
            self.inviteList = objects;
            NSLog(@"hostlistDOG %lu", (unsigned long)self.inviteList.count);
            
        }
        else {
            NSLog(@"query error");
        }
        [self.tableView reloadData];
    }];
    
    NSLog(@"****** List count ********");
    NSLog(@"%lu", self.hostingList.count);
    NSLog(@"%lu", self.inviteList.count);
    
    
    [self.tableView reloadData];
    
    
    
}

- (void) goBack {
    
    [self performSegueWithIdentifier:@"backFromParty" sender:self];
    
    //[self.navigationController popViewControllerAnimated:YES];
}

- (void) viewWillAppear:(BOOL)animated {
    
    NSLog(@"viewWillAppear");
    
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
    
    //[self viewDidLoad];
    
}

- (void) viewDidAppear:(BOOL)animated
{
    NSLog(@"PFHostingTableViewController. - viewDidAppear");
    
    //[self viewDidLoad];
    
    [super viewDidAppear:animated];
    
    [self.tableView reloadData];
}

- (void) addPartyTouch {
    
    NSLog(@"addPartyTouch");
    
    [self performSegueWithIdentifier:@"hostingdetail" sender:self];
}


- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"reaching accessoryButtonTappedForRowWithIndexPath:");
    [self performSegueWithIdentifier:@"hostprofiledetail" sender:[self.tableView cellForRowAtIndexPath:indexPath]];
}


- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if ([self.invitationMode isEqualToString:@"Invites"]) {
        return self.inviteList.count;
    }
    else if ([self.invitationMode isEqualToString:@"Hostings"]){
        return self.hostingList.count;
    }
    else {
        return self.inviteList.count;
    }
}

- (NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString * result = @"Events";
    return result;
}

- (PFHostingTableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * cellIdentifier = @"inviteCell";
    
    PFHostingTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    
    PFUser * user = [PFUser currentUser];
    
    if ([self.invitationMode isEqualToString:@"Invites"]) {
        
        PFObject * obj = [self.inviteList objectAtIndex:indexPath.row];
        //cell.textLabel.text = [obj objectForKey:@"headline"];
        PFUser * createdBy = [obj objectForKey:@"createdBy"];
        
        NSString * createdByName = [createdBy objectForKey:@"firstName"];
        
        cell.hostingCellHeadlineLabel.text = [obj objectForKey:@"headline"];
        cell.hostingCellCreatedByLabel.text = createdByName;
        
         if (createdBy[@"profilePic"] != nil) {
         
             PFFile * pic = [createdBy objectForKey:@"profilePic"];
         
        
             [pic getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                 if (!error) {
                     UIImage *image = [UIImage imageWithData:data];
             
                     CALayer *imageLayer = cell.hostingCellProfileImage.layer;
                     [imageLayer setCornerRadius:5];
                     [imageLayer setBorderWidth:1];
                     [imageLayer setMasksToBounds:YES];
                     [cell.hostingCellProfileImage.layer setCornerRadius:cell.hostingCellProfileImage.frame.size.width/2];
                     [cell.hostingCellProfileImage.layer setMasksToBounds:YES];
                     [cell.hostingCellProfileImage setImage:image];
                 }
                 else {
         
                     UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
         
                     CALayer *imageLayer = cell.hostingCellProfileImage.layer;
                     [imageLayer setCornerRadius:5];
                     [imageLayer setBorderWidth:1];
                     [imageLayer setMasksToBounds:YES];
                     [cell.hostingCellProfileImage.layer setCornerRadius:cell.hostingCellProfileImage.frame.size.width/2];
                     [cell.hostingCellProfileImage.layer setMasksToBounds:YES];
                     [cell.hostingCellProfileImage setImage:defaultimg];
                 }
                 [self.tableView reloadData];
             }];
         
         }
         else {
             UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
         
             CALayer *imageLayer = cell.hostingCellProfileImage.layer;
             [imageLayer setCornerRadius:5];
             [imageLayer setBorderWidth:1];
             [imageLayer setMasksToBounds:YES];
             [cell.hostingCellProfileImage.layer setCornerRadius:cell.hostingCellProfileImage.frame.size.width/2];
             [cell.hostingCellProfileImage.layer setMasksToBounds:YES];
             [cell.hostingCellProfileImage setImage:defaultimg];
         }
         
        
        
        
        
    }
    else if ([self.invitationMode isEqualToString:@"Hostings"]){
        
        PFObject * obj = [self.hostingList objectAtIndex:indexPath.row];
        
        PFUser * createdBy = [obj objectForKey:@"createdBy"];
        
        NSString * createdByName = [createdBy objectForKey:@"firstName"];
        
        cell.hostingCellHeadlineLabel.text = [obj objectForKey:@"headline"];
        cell.hostingCellCreatedByLabel.text = createdByName;
        
        PFUser * user = [PFUser currentUser];
        
        
        if (user[@"profilePic"] != nil) {
            
            PFFile * pic = [user objectForKey:@"profilePic"];
            
            
            
            [pic getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                if (!error) {
                    UIImage *image = [UIImage imageWithData:data];
                    
                    CALayer *imageLayer = cell.hostingCellProfileImage.layer;
                    [imageLayer setCornerRadius:5];
                    [imageLayer setBorderWidth:1];
                    [imageLayer setMasksToBounds:YES];
                    [cell.hostingCellProfileImage.layer setCornerRadius:cell.hostingCellProfileImage.frame.size.width/2];
                    [cell.hostingCellProfileImage.layer setMasksToBounds:YES];
                    [cell.hostingCellProfileImage setImage:image];
                }
                else {
                
                    UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
                    
                    CALayer *imageLayer = cell.hostingCellProfileImage.layer;
                    [imageLayer setCornerRadius:5];
                    [imageLayer setBorderWidth:1];
                    [imageLayer setMasksToBounds:YES];
                    [cell.hostingCellProfileImage.layer setCornerRadius:cell.hostingCellProfileImage.frame.size.width/2];
                    [cell.hostingCellProfileImage.layer setMasksToBounds:YES];
                    [cell.hostingCellProfileImage setImage:defaultimg];
                }
            }];
            
        }
        else {
            UIImage * defaultimg = [UIImage imageNamed:@"PFIcon"];
            
            CALayer *imageLayer = cell.hostingCellProfileImage.layer;
            [imageLayer setCornerRadius:5];
            [imageLayer setBorderWidth:1];
            [imageLayer setMasksToBounds:YES];
            [cell.hostingCellProfileImage.layer setCornerRadius:cell.hostingCellProfileImage.frame.size.width/2];
            [cell.hostingCellProfileImage.layer setMasksToBounds:YES];
            [cell.hostingCellProfileImage setImage:defaultimg];
        }
        
        
        
        //cell.textLabel.text = [obj objectForKey:@"headline"];
    }
    else {
        
        PFObject * obj = [self.inviteList objectAtIndex:indexPath.row];
        
        //cell.textLabel.text = [obj objectForKey:@"headline"];
        PFUser * createdBy = [obj objectForKey:@"createdBy"];
        
        NSString * createdByName = [createdBy objectForKey:@"firstName"];
        
        cell.hostingCellHeadlineLabel.text = [obj objectForKey:@"headline"];
        cell.hostingCellCreatedByLabel.text = createdByName;
        
    }

    
    return cell;
    
}



- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //[super tableView:tableView didSelectRowAtIndexPath:indexPath];
    
    NSLog(@"PFHostingTVC. didSelectRowAtIndexPath");
    
    [self performSegueWithIdentifier:@"hostprofiledetail" sender:self];
}
 

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"hostprofiledetail"]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        //NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        NSLog(@"***** indexPath = %@", indexPath);
        
        PFObject * object = nil;
        if ([self.invitationMode isEqualToString:@"Invites"])
            object = [self.inviteList objectAtIndex:indexPath.row];
        else
            object = [self.hostingList objectAtIndex:indexPath.row];

        NSString * invite_id = [object objectId];
        NSLog(@"invitation string = %@", invite_id);
        PFQuery *query = [PFQuery queryWithClassName:@"invitations"];
      
        [query whereKey:@"objectId" equalTo:invite_id];
        [query orderByAscending:@"createdAt"];
        PFHostingProfileDetailViewController *detailViewController = [segue destinationViewController];
        detailViewController.pfObject = [query getFirstObject];
    }
}

@end
