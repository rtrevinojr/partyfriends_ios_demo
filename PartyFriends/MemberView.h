//
//  MemberView.h
//  PartyFriends
//
//  Created by Joshua Silva  on 6/24/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Parse/Parse.h"

@interface MemberView : UIViewController <UITextFieldDelegate>


@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;


@property (strong, nonatomic) IBOutlet UITextField *membersNumber;

@property (strong, nonatomic) IBOutlet UITextField *membersPin;

@property (strong, nonatomic) IBOutlet UITextField *membersPassword;

@property (nonatomic, strong) NSString * userNumber;

@property (nonatomic, strong) PFUser * firstFriendMember;

- (IBAction)newPF:(id)sender;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
