//
//  TESTING.h
//  PartyFriends
//
//  Created by Joshua Silva  on 8/28/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Parse/Parse.h"

@interface StartGoingOutView : UIViewController <UIScrollViewDelegate, UITableViewDataSource, UITabBarDelegate, UITextFieldDelegate, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapGesture;
@property (strong, nonatomic) IBOutlet UISwitch *allSwitch;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UITextField *editLocation;
@property (strong, nonatomic) IBOutlet UIDatePicker *setWhen;


@property (strong, nonatomic) NSArray * beforePicked;
@property (strong, nonatomic) IBOutlet UIView *contain;
@property (strong, nonatomic) NSArray * waffles;

@property (strong, nonatomic) NSArray * allFriends;
@property (strong, nonatomic) PFRelation * allFriendsRelation;
@property (strong, nonatomic) PFUser * currentUser;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray * pickFriends;
@property (strong, nonatomic) NSMutableArray * unpickedFriends;
@property (strong, nonatomic) NSMutableArray * wantedFriends;

@property (strong, nonatomic) PFRole * rolling;

@property (strong, nonatomic) PFRole * role;
@property (strong, nonatomic) PFRelation * userRelation;
@property (strong, nonatomic) PFRelation * exclusiveRelation;

@property (strong, nonatomic) UIImageView * cancelX;
@property (strong, nonatomic) PFRelation * pickedFriendsRelation;
@property (strong, nonatomic) PFSession * sessionToken;

- (IBAction)cancel:(id)sender;



@end
