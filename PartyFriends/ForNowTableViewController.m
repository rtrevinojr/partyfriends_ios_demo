//
//  ForNowTableViewController.m
//  PartyFriends
//
//  Created by Joshua Silva  on 7/10/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import "ForNowTableViewController.h"

@interface ForNowTableViewController ()

@end

@implementation ForNowTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    PFQuery * query = [PFUser query];
    [query orderByAscending: @"username"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error){
            NSLog(@"error: %@ %@", error,[error userInfo]);
        
            
        }
        else {
            
            self.allUsers = objects;
            // NSLog(@"%@", self.allUsers);
            [self.tableView reloadData];
            
        }
    }];
    
    self.currentUser = [PFUser currentUser];
    
    
  /*  UIView * topSwipe= [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    topSwipe.backgroundColor = [UIColor blueColor];
    [self.view addSubview:topSwipe];
    
    */
    
    
    
}






#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    return [self.allUsers count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIndentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIndentifier forIndexPath:indexPath];
    
    PFUser *user = [self.allUsers objectAtIndex:indexPath.row];
    cell.textLabel.text  = user[@"username"];
    
    if ([self isFriend:user]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}


-(void)tableView: (UITableView *) tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    UITableViewCell * Cell = [tableView cellForRowAtIndexPath:indexPath];
    
    PFUser *user = [self.allUsers objectAtIndex:indexPath.row];
    
    
    
    
    PFRelation *friendsRelations = [self.currentUser relationForKey:@"friends"];
    
    //just adding
    // PFRelation *friendss = [self.currentUser relationForKey:@"waffles"];
    
    
    
    if ([self isFriend:user]){
        //1.Remve the checkmark
        Cell.accessoryType = UITableViewCellAccessoryNone;
        
        
        //2.remove from the array of friends
        for (PFUser *friend in self.friends){
            
            if ([friend.objectId isEqualToString:user.objectId])
                [self.friends removeObject:friend];
            break;
        }
        
        //3.Remove from the back end
        
        [friendsRelations removeObject:user];
        
        
        
    }else {
        // [friendss addObject:user];
        Cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [self.friends addObject: user];
        [friendsRelations addObject:user];
        NSLog(@"this is what it gives me %@", user);
        
        
    }
    [self.currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(error){
            
            NSLog(@"Error %@ %@ ", error, [error userInfo] );
            
        }
    }];
    
}


#pragma mark - waffles

-(BOOL) isFriend: (PFUser *) user {
    for (PFUser *friend in self.friends){
        
        if ([friend.objectId isEqualToString:user.objectId])
            return YES;
        
    }
    
    return NO;
}





/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
