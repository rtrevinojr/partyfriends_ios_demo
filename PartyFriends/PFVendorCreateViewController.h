//
//  PFVendorCreateViewController.h
//  PartyFriends
//
//  Created by Gilberto Silva on 12/2/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Parse/Parse.h"

@interface PFVendorCreateViewController : UIViewController<UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>

@property (strong, nonatomic)IBOutlet UITextField * businessName;
@property (strong, nonatomic)IBOutlet UITextField * address;
@property (strong, nonatomic) IBOutlet UITextField *city;
@property (strong, nonatomic) IBOutlet UITextField *zipCode;
@property (strong, nonatomic) IBOutlet UIPickerView *statePicker;
@property (strong, nonatomic)IBOutlet UITextView * vendorDescription;
@property (strong, nonatomic)IBOutlet UITextField * businessPhone;
@property (strong, nonatomic)IBOutlet UITextField * phone;
@property (strong, nonatomic)IBOutlet UITextField * businessEmail;
@property (strong, nonatomic)IBOutlet UITextField * email;
@property (strong, nonatomic)IBOutlet UITextField * businessFax;
@property (strong, nonatomic)IBOutlet UITextField * fax;
@property (strong, nonatomic)IBOutlet UITextField * website;
@property (strong, nonatomic)IBOutlet UIButton *createButton;

@property (weak, nonatomic) IBOutlet UIScrollView *pfVendorScrollView;


@property (strong, nonatomic) NSArray * states;
@property (strong, nonatomic) NSString * userState;

@end
