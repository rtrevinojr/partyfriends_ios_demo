//
//  passwordViewController.h
//  PartyFriends
//
//  Created by Joshua Silva  on 9/4/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Parse/Parse.h"

@interface passwordViewController : UIViewController <UITextFieldDelegate>



@property (strong, nonatomic) PFUser * currentUser;

@property (strong, nonatomic) IBOutlet UITextField *newpass;
@property (strong, nonatomic) IBOutlet UITextField *comfirm;
@property (strong, nonatomic) IBOutlet UITextField *oldPassword;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;


- (IBAction)passwordChange:(id)sender;




@end
