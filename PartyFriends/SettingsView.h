//
//  SettingsView.h
//  PartyFriends
//
//  Created by Joshua Silva  on 7/27/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Gradient.h"
#import "Parse/Parse.h"
#import <CoreMotion/CoreMotion.h>


@interface SettingsView : UIViewController

- (IBAction)logout:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *accountButton;
@property (strong, nonatomic) IBOutlet UILabel *numberCount;

@property(strong, nonatomic) CMMotionManager * motionCheck;
@property (strong, nonatomic) PFRelation * friends;
@property (strong, nonatomic) NSMutableArray * allFriends;
@property (strong, nonatomic) IBOutlet UILabel *pinNumber;
@property (strong, nonatomic) IBOutlet UIView *countView;



@property (strong, nonatomic) PFUser * currentUser;
@property (strong, nonatomic) PFObject * accountInfo;
@property (strong, nonatomic) NSString * missingInfo;

- (IBAction)uber:(id)sender;

@end
