//
//  BeginningView.m
//  PartyFriends
//
//  Created by Joshua Silva  on 6/24/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import "BeginningView.h"
#import "Parse/Parse.h"
#import "ActionView.h"
#import "LocationManagerSingleton.h"
#import "StartGoingOutView.h"
#import <QuartzCore/QuartzCore.h>

@interface BeginningView ()

@end

@implementation BeginningView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.locationManager= [CLLocationManager new];
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.delegate = self;
    self.Turorial.hidden = YES;
    [self.Turorial setEnabled:NO];
    
    [self.locationManager requestAlwaysAuthorization];
    
    if (![CLLocationManager locationServicesEnabled]){
        NSLog(@"location services are disabled");
        return;
    }
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied){
        NSLog(@"location services are blocked by the user");
        return;
    }
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways){
        NSLog(@"location services are enabled");
    }
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined){
        NSLog(@"about to show a dialog requesting permission");
    }
    
    NSLog(@" hahah %@", self.locationManager.location);
    self.currentUser=[PFUser currentUser];
    NSLog(@"%@",self.currentUser);
    
    self.allFriendsRelation = [[PFUser currentUser]
                               objectForKey:@"friends"];
    
    self.noteView.clipsToBounds = YES;
    self.noteView.layer.cornerRadius = 10.0f;
    self.noteView.layer.masksToBounds = YES;
    
}


-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:NO];
    
    if ([self.currentUser[@"status"] isEqual:@0]) {
        self.userStatus = @"WH";
        
    }
    if ([self.currentUser[@"status"] isEqual:@1]) {
        self.userStatus = @"PM";
    }
    if ([self.currentUser[@"status"] isEqual:@2]) {
        self.userStatus = @"GO";
    }
    
    
    PFQuery * query = [self.allFriendsRelation query];
    [query orderByAscending:@"firstName"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error){
            
            NSLog(@"error %@ %@", error, [error userInfo]);
            
            
        }
        else{
            self.allFriends = objects;
            
            
            
        }
        
    }];
    
    
    [self.scrollView flashScrollIndicators];
    
    
    
    NSString * roleName = [NSString stringWithFormat:@"Role-%@", self.currentUser.objectId];
    
    PFQuery *qquery = [PFRole query];
    [qquery whereKey:@"name" equalTo:roleName];
    [qquery findObjectsInBackgroundWithBlock:^(NSArray * objects, NSError * error){
        NSLog(@"here");
        if (error.code == 101 || objects.count==0) {
            NSLog(@"here2");
            
            PFACL *acl = [PFACL ACL];
            [acl setPublicReadAccess:false];
            [acl setPublicWriteAccess:false];
            [acl setReadAccess:true forUser:[PFUser currentUser]];
            [acl setWriteAccess:true forUser:[PFUser currentUser]];
            //NSString * roleName = [NSString stringWithFormat:@"Role-%@", self.currentUser.objectId];
            PFRole *role = [PFRole roleWithName:roleName];
            [role setACL:acl];
            [role saveInBackgroundWithBlock:^(BOOL succeeded, NSError * error){
                
                if (!error) {
                    PFQuery *query = [PFRole query];
                    [query whereKey:@"name" equalTo:roleName];
                    [query findObjectsInBackgroundWithBlock:^(NSArray * objects, NSError * error){
                        NSLog(@"double checking");
                        if (error) {
                            NSLog(@"double checking");
                        }else if(objects.count>0){
                            NSLog(@"double checking2");
                            
                            
                            self.rolling = objects[0];
                            
                            
                        }else{
                            
                            
                            
                            
                            
                        }
                        
                        
                        
                        
                        
                        
                        
                        
                    }];
                    
                    
                    
                    
                    
                    
                }
                
            }];
        }else{
            self.rolling = objects[0];
            
        }
        
        
        
        
        
    }];




}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)whatsHappening:(id)sender {
    
    self.currentUser[@"status"]=@0;
    
    
    [self.currentUser saveInBackground];
    
    self.userStatus = @"WH";
    
    
    
    [self performSegueWithIdentifier:@"action" sender:self];




}

- (IBAction)partyMode:(id)sender {
    
    
    
    
    
   /* NSString * roleName = [NSString stringWithFormat:@"Role-%@", self.currentUser.objectId];
    
    PFQuery *query = [PFRole query];
    [query whereKey:@"name" equalTo:roleName];
    [query findObjectsInBackgroundWithBlock:^(NSArray * objects, NSError * error){
        NSLog(@"here");
        if (error.code == 101 || objects.count==0) {
            NSLog(@"here2");
            
            PFACL *acl = [PFACL ACL];
            [acl setPublicReadAccess:false];
            [acl setPublicWriteAccess:false];
            [acl setReadAccess:true forUser:[PFUser currentUser]];
            [acl setWriteAccess:true forUser:[PFUser currentUser]];
            //NSString * roleName = [NSString stringWithFormat:@"Role-%@", self.currentUser.objectId];
            PFRole *role = [PFRole roleWithName:roleName];
            [role setACL:acl];
            [role saveInBackgroundWithBlock:^(BOOL succeeded, NSError * error){
                
                if (!error) {
                    PFQuery *query = [PFRole query];
                    [query whereKey:@"name" equalTo:roleName];
                    [query findObjectsInBackgroundWithBlock:^(NSArray * objects, NSError * error){
                        NSLog(@"double checking");
                        if (error) {
                            NSLog(@"double checking");
                        }else if(objects.count>0){
                            NSLog(@"double checking2");
                            
                            
                            self.rolling = objects[0];
                            PFACL *myAcl = [PFACL ACL];
                            [myAcl setWriteAccess:NO forRole:self.rolling];
                            [myAcl setReadAccess:YES forRole:self.rolling];
                            [myAcl setPublicReadAccess:NO];
                            [myAcl setPublicReadAccess:NO];
                            [myAcl setReadAccess:YES forUser:self.currentUser];
                            [myAcl setWriteAccess:YES forUser:self.currentUser];
                            PFRelation * relation=[PFRelation new];
                            relation= self.rolling.users;
                            
                            PFObject * party = [PFObject objectWithClassName:@"party"];
                            
                            self.userLocation = [LocationManagerSingleton sharedSingleton].locationManager.location;
                            
                            PFGeoPoint * currentLocation  = [PFGeoPoint geoPointWithLocation:self.userLocation];
                            
                            party[@"location"] =currentLocation;
                            party[@"createdBy"] = self.currentUser;
                            party.ACL = myAcl;
                            [party saveInBackgroundWithBlock:^(BOOL suedfbj, NSError * error) {
                                if (error){
                                    
                                    NSLog(@"ajhdjahds  %@", error);
                                    
                                }
                                else{
                                    
                                    
                                }
                                
                            } ];
                            
                        }else{
                            
                        }
                        
                        
                        
                        
                        
                        
                        
                        
                    }];
                    
                    
                    
                    
                    
                    
                }
                
            }];
        }else{
            self.rolling = objects[0];
            PFACL *myAcl = [PFACL ACL];
            [myAcl setWriteAccess:NO forRole:self.rolling];
            [myAcl setReadAccess:YES forRole:self.rolling];
            [myAcl setPublicReadAccess:NO];
            [myAcl setPublicReadAccess:NO];
            [myAcl setReadAccess:YES forUser:self.currentUser];
            [myAcl setWriteAccess:YES forUser:self.currentUser];
            PFRelation * relation=[PFRelation new];
            relation= self.rolling.users;
            
            PFObject * party = [PFObject objectWithClassName:@"party"];
            
            self.userLocation = [LocationManagerSingleton sharedSingleton].locationManager.location;
            
            PFGeoPoint * currentLocation  = [PFGeoPoint geoPointWithLocation:self.userLocation];
            
            party[@"location"] =currentLocation;
            party[@"createdBy"] = self.currentUser;
            party.ACL = myAcl;
            [party saveInBackgroundWithBlock:^(BOOL suedfbj, NSError * error) {
                if (error){
                    
                    NSLog(@"%@", error);
                    
                }
                else{
                    
                    
                }
                
            } ];        }
        
        
        
        
        
    }];*/
    
    for (int i =0; [self.allFriends count] > i ; i++) {
        PFUser * user = [self.allFriends objectAtIndex:i];
        [self.rolling.users addObject:user];
      //  NSLog(@"waafflleess %lu", (unsigned long)[self.roll objectAtIndex:i]);
        i++;
        
        
    }
    
    
    self.location = [LocationManagerSingleton sharedSingleton].locationManager.location;//self.locationManager.location;
    NSLog(@"latitude %f", self.locationManager.location.coordinate.latitude);
    NSLog(@"latitude %f", self.locationManager.location.coordinate.longitude);
    self.currentUser[@"status"]=@1;
    
    PFGeoPoint * currentLocation  = [PFGeoPoint geoPointWithLocation:self.location];
    //[self.current setObject:currentLocation forKey:@"loCation"];
        //self.current [@"location"] = [LocationManagerSingleton sharedSingleton].locationManager.location;
    
    [self.rolling saveInBackgroundWithBlock:^(BOOL sdfjdf, NSError * error){
        
        if (error) {
            NSLog(@"asajdka is there an error %ld", (long)error.code);
        }
        
        else{
            NSLog(@"waffles agaub!!!");
            
        }
        
        
    }
     
     
     
     ];
    
    [self.currentUser saveInBackground];
    
    self.userStatus = @"PM";
    
    
   [self performSegueWithIdentifier:@"action" sender:self];
    
}







- (IBAction)goingOut:(id)sender {
    
    self.currentUser[@"status"]=@2;
    [self.currentUser saveInBackground];
    self.userStatus = @"GO";
    
    
    [self performSegueWithIdentifier:@"action" sender:self];
    
    
    
}

- (IBAction)gotIt:(id)sender {
    [self performSegueWithIdentifier:@"action" sender:self];
    
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"action"]) {
        ActionView * viewController = (ActionView *)segue.destinationViewController;
        
        
        viewController.userStatus = self.userStatus;
        
       // if ([segue.identifier isEqualToString:@"toGoingOut"]) {
         //   GoingOutView * secondViewController = ()
       // }
        
        
        
        
        //[[CLLocation alloc] initWithLatitude:self.yourCord.coordinate.latitude longitude:self.yourCord.coordinate.latitude];
    }


}






@end
