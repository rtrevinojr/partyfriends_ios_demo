//
//  PFPartyCommentsVC.m
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 1/8/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import "PFPartyCommentsVC.h"

#import "PFCommentsTableViewCell.h"

//#import "PFPartyCommentsViewController.h"

#import "PFPartyCommentsTableViewCell.h"

#import "PFAddCommentViewController.h"


#import <Parse/Parse.h>


@implementation PFPartyCommentsVC

- (IBAction)reload:(id)sender {
    
    //[self.tableView reloadData];
    
    NSLog(@"reload.............................refresh Table");
    
    //[self requeryTable];
    
    self.refreshControl = [[UIRefreshControl alloc]init];
    [self.tableView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    CGPoint scrollPoint = CGPointMake(0, textView.frame.origin.y+220
                                      );
    [self.scrollView setContentOffset:scrollPoint animated:YES];
    
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    
    [self.scrollView setContentOffset:CGPointZero animated:YES];
}

- (void)refreshTable {
    //TODO: refresh your data
    
    NSLog(@"refreshTable.........................");
    
    [self.refreshControl endRefreshing];
    
    [self requeryTable];
    
    //[self.tableView reloadData];
}

- (void) requeryTable
{
    
    NSLog(@"requeryTable.........................");
    
    PFRelation * comments = [[self pfPost] objectForKey:@"comments"];
    
    
    self.commentsList = [NSMutableArray new];
    
    
    // self.tableView.delegate = self;
    
    //self.commentTextView.delegate = self;
    
    
    PFQuery * query = [comments query];
    [query includeKey:@"createdBy"];
    
    [query orderByAscending:@"createdAt"];
    
    
    //self.commentsList = [query findObjects];
    
    
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        
        self.commentsList = objects;
        if (objects.count == 0){
            [self.commentsList addObject:@"Zero"];
        }
        
        NSLog(@"*** comments Array in background count = %luu \n and the Array that comes back %lu uhuhuhuh !!!!", self.commentsList.count, objects.count);

        
        NSLog(@"the array after the finding nothing%@", self.commentsList);
        
        [self.tableView reloadData];
    }];
    
    [self.tableView reloadData];
    
    [self.tableView setContentOffset:CGPointMake(0, CGFLOAT_MAX + 100)];
    
}


- (void) viewDidLoad
{
    [super viewDidLoad];
    self.commentTextView2.delegate = self;
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:(142/255.0) green:(83/255.0) blue:(249/255.0) alpha:1.0];
    
    self.refreshControl = [[UIRefreshControl alloc]init];
    [self.tableView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithImage:[UIImage imageNamed:@"arrow-left"]
                                   style:UIBarButtonItemStylePlain
                                   target:self action:@selector(goBack)];
    
    
    
    //self.navigationItem.leftBarButtonItem = backButton;
    
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc]
                                  initWithImage:[UIImage imageNamed:@"add-party"]
                                  style:UIBarButtonItemStylePlain
                                  target:self action:@selector(addCommentTouch)];
    
    //self.navigationItem.rightBarButtonItem = addButton;
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    
    NSLog(@"PFPartyCommentsViewController viewDidLoad. PFObject = %@", self.pfPost);
    
    
    PFRelation * comments = [[self pfPost] objectForKey:@"comments"];
    
    
    // self.tableView.delegate = self;
    
    PFQuery * query = [comments query];
    [query includeKey:@"createdBy"];
    [query orderByAscending:@"createdAt"];
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        
        self.commentsList = objects;
        if (objects.count == 0){
            [self.commentsList addObject:@"Zero"];
        }
        
        NSLog(@"*** comments Array in background count = %luu", self.commentsList.count);
        NSLog(@"%@", self.commentsList);
        
        [self.tableView reloadData];
    }];
    
    
    
}

- (void) viewWillAppear:(BOOL)animated
{
    NSLog(@"viewDidAppear PFPartyCommentsVC");
    
    
    [self requeryTable];
}

-(void)dismissKeyboard {
    [self.commentTextView2 resignFirstResponder];
    //[self.tableView setContentOffset:CGPointMake(0, CGFLOAT_MAX + 100)];
}

-(BOOL) textViewShouldReturn:(UITextView *)textView
{
    
    [self.commentTextView2 resignFirstResponder];
    //[self.tableView setContentOffset:CGPointMake(0, CGFLOAT_MAX + 100)];
    return YES;
}

- (IBAction)postCommentButton:(id)sender {
    
    [self addCommentTouch];
}


- (void) addCommentTouch
{
    NSLog(@".........................postComment ");
    
    NSString * message_string = [NSString stringWithFormat:@"%@", self.commentTextView2.text];
    
    self.commentTextView2.text = @"";
    
    if ([message_string isEqualToString:@""]) {
        
        NSLog(@"comment is nil");
        
        
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@"PartyFriends"
                                      message:@"Your comment is empty"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        //Handel your yes please button action here
                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                        
                                    }];
        
        [alert addAction:okButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        return;
    }
    
    NSLog(@"comment is NOT nil");
    
    PFObject * message = [PFObject objectWithClassName:@"comment"];
    
    PFRelation * friendsRel = [PFRelation alloc];
    
    friendsRel = [message relationForKey:@"comments"];
    
    NSLog(@"*** comment = %@", self.commentTextView2);
    
    
    
    message[@"comment"] = message_string;
    
    if (self.pfPost != nil) {
        message[@"party"] =  self.pfPost;
    }
    else {
        NSLog(@"************* self.partyObject is nil");
    }
    
    
    
    [message saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        if (!error) {
            NSLog(@"message save in background");
            
            [self requeryTable];
            
        }else {
            PFRelation * comments = [[self pfPost] objectForKey:@"comments"];
            
            PFQuery * query = [comments query];
            [query includeKey:@"createdBy"];
            
            [query orderByAscending:@"createdAt"];
            [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
                
                self.commentsList = objects;
                
                NSLog(@"*** comments /n  Array in background /n count = %luu", self.commentsList.count);
                
                NSLog(@"%@", self.commentsList);
                
                
                
                [self.tableView reloadData];
                //  [self viewDidLoad];
            }];}
        
    }];
    
    //[self requeryTable];
    
    
    //    if (self.tableView.contentSize.height > self.tableView.frame.size.height) {
    //        CGPoint offset = CGPointMake(0, self.tableView.contentSize.height - self.tableView.frame.size.height);
    //        [self.tableView setContentOffset:offset animated:YES];
    //    }
    
    [self.commentTextView2 resignFirstResponder];
    
    //    PFRelation * comments = [[self pfParty] objectForKey:@"comments"];
    //
    //    PFQuery * query = [comments query];
    //    [query includeKey:@"createdBy"];
    //
    //    [query orderByAscending:@"createdAt"];
    //
    //
    //    //self.commentsList = [query findObjects];
    //
    //
    //    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
    //
    //        self.commentsList = objects;
    //
    //        NSLog(@"*** comments Array in background count = %luu", self.commentsList.count);
    //
    //        NSLog(@"%@", self.commentsList);
    //
    //        [self.tableView reloadData];
    //    }];
    //
    //    [self.tableView reloadData];
    
}

//- (void) viewDidAppear:(BOOL)animated
//{
//    [super viewDidAppear:animated];
//
//    [self.tableView reloadData];
//}

/*
 - (void) viewDidAppear:(BOOL)animated
 {
 [super viewDidAppear:animated];
 if (self.tableView.contentSize.height > self.tableView.frame.size.height) {
 CGPoint offset = CGPointMake(0, self.tableView.contentSize.height - self.tableView.frame.size.height);
 [self.tableView setContentOffset:offset animated:YES];
 }
 }
 */

/*
 -(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
 {
 //if([text isEqualToString:@"\n"])
 [textView resignFirstResponder];
 return YES;
 }
 */

/*
 -(void)textViewDidBeginEditing:(UITextView *)textView{
 
 [UIView beginAnimations:nil context:NULL];
 [UIView setAnimationDuration:0.5];
 [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:self.commentActionView cache:YES];
 self.commentActionView.frame = CGRectMake(10, 50, 300, 200);
 [UIView commitAnimations];
 
 NSLog(@"Started editing target!");
 
 }
 
 -(void)textViewDidEndEditing:(UITextView *)textView
 {
 [UIView beginAnimations:nil context:NULL];
 [UIView setAnimationDuration:0.5];
 [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:self.commentActionView cache:YES];
 self.commentActionView.frame = CGRectMake(10, 150, 300, 200);
 [UIView commitAnimations];
 }
 */


/*
 - (void)textViewDidBeginEditing:(UITextView *)textView
 {
 [self animateTextView: YES];
 }
 
 - (void)textViewDidEndEditing:(UITextView *)textView
 {
 [self animateTextView:NO];
 }
 */

/*
 -(void)textViewDidBeginEditing:(UITextView *)textView
 {
 if (textView == self.commentTextView)
 {
 [UIView beginAnimations:nil context:NULL];
 [UIView setAnimationDelegate:self];
 [UIView setAnimationDuration:0.5];
 [UIView setAnimationBeginsFromCurrentState:YES];
 self.view.frame = CGRectMake(self.view.frame.origin.x , (self.view.frame.origin.y - 80), self.view.frame.size.width, self.view.frame.size.height);
 
 //        self.commentActionView.frame = CGRectMake(self.commentActionView.frame.origin.x, (self.commentActionView.frame.origin.y - 80), self.commentActionView.frame.size.width, self.commentActionView.frame.size.height);
 
 [UIView commitAnimations];
 }
 }
 
 -(void)textViewDidEndEditing:(UITextView *)textView
 {
 if (textView == self.commentTextView)
 {
 [UIView beginAnimations:nil context:NULL];
 [UIView setAnimationDelegate:self];
 [UIView setAnimationDuration:0.5];
 [UIView setAnimationBeginsFromCurrentState:YES];
 self.view.frame = CGRectMake(self.view.frame.origin.x , (self.view.frame.origin.y + 80), self.view.frame.size.width, self.view.frame.size.height);
 
 //        self.commentActionView.frame = CGRectMake(self.commentActionView.frame.origin.x, (self.commentActionView.frame.origin.y + 80), self.commentActionView.frame.size.width, self.commentActionView.frame.size.height);
 
 [UIView commitAnimations];
 }
 }
 */

- (void) animateTextView:(BOOL) up
{
    
    // Get the size of the keyboard.
    //    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //Given size may not account for screen rotation
    //    int height = MIN(keyboardSize.height,keyboardSize.width);
    //    int width = MAX(keyboardSize.height,keyboardSize.width);
    
    const int movementDistance = 10; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    int movement= movement = (up ? -movementDistance : movementDistance);
    NSLog(@"%d",movement);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.commentActionView.frame, 0, movement);
    [UIView commitAnimations];
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return textView.text.length + (text.length - range.length) <= 140;
}

- (UIImage *)squareImageFromImage:(UIImage *)image scaledToSize:(CGFloat)newSize {
    CGAffineTransform scaleTransform;
    CGPoint origin;
    
    if (image.size.width > image.size.height) {
        CGFloat scaleRatio = newSize / image.size.height;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(-(image.size.width - image.size.height) / 2.0f, 0);
    } else {
        CGFloat scaleRatio = newSize / image.size.width;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(0, -(image.size.height - image.size.width) / 2.0f);
    }
    
    CGSize size = CGSizeMake(newSize, newSize);
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(size, YES, 0);
    } else {
        UIGraphicsBeginImageContext(size);
    }
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextConcatCTM(context, scaleTransform);
    
    [image drawAtPoint:origin];
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}



- (void) goBack {
    
    NSLog(@"Back");
    
    //[self performSegueWithIdentifier:@"unwindToAction" sender:self];
    //[self.navigationController popViewControllerAnimated:YES];
}


- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"number of rows in section = %lu", self.commentsList.count);
    return self.commentsList.count;
}

- (PFCommentsTableViewCell *) tableView: (UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"..............................................indexPath = %lu", indexPath.row);
    
    static NSString * cellIdentifier = @"com";
    
    PFCommentsTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    //PFCommentsTableViewCell * cell = [[PFCommentsTableViewCell alloc] init];
    
    //    PFCommentsTableViewCell * cell = [[PFCommentsTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    if ([[self.commentsList objectAtIndex:0] isEqual:@"Zero"]) {
    cell.commentAuthor.text = @"";
    cell.commentTextView.text = @"No Comments yet";
        [cell.editCommentButton setHidden:YES];
    }else {
    if (cell == nil) {
        
        NSLog(@"cell is NULL...............................................................");
        
        cell = [[PFCommentsTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    PFObject * comments = [self.commentsList objectAtIndex:indexPath.row];
    
    PFUser * user = [comments objectForKey:@"createdBy"];
    
    PFUser * currentUser = [PFUser currentUser];
    
    if ([user.objectId isEqualToString:currentUser.objectId]) {
        [cell.editCommentButton setHidden:NO];
        [cell.editCommentButton setEnabled:YES];
    }
    else {
        [cell.editCommentButton setHidden:YES];
        [cell.editCommentButton setEnabled:NO];
    }
    
    cell.commentObject = comments;
    
    NSLog(@"createdBy = %@", comments);
    
    
    NSString * comment_str = comments[@"comment"];
    
    CGSize stringSize = [comment_str sizeWithFont:[UIFont boldSystemFontOfSize:15] constrainedToSize:CGSizeMake(320, 9999) lineBreakMode:UILineBreakModeWordWrap];
    
    NSLog(@"******************");
    NSLog(@"cell string size = %f", stringSize.height);
    
    cell.commentTextView.font = [UIFont systemFontOfSize:11.0];
    cell.commentTextView.text = comment_str;
    cell.commentTextView.editable = NO;
    [cell.commentTextView setScrollEnabled:NO];
    
    //cell.comment.text = comment_str;
    
    //cell.commentTextView.text = [comments objectForKey:@"comment"];
    
    cell.commentAuthor.text = [user objectForKey:@"firstName"];
    
    
    if (user[@"profilePic"] != nil) {
        
        PFFile * pic = [user objectForKey:@"profilePic"];
        
        
        [pic getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
            if (!error) {
                UIImage *image = [UIImage imageWithData:data];
                
                CALayer *imageLayer = cell.commentSenderProfilePic.layer;
                [imageLayer setCornerRadius:5];
                [imageLayer setBorderWidth:1];
                [imageLayer setMasksToBounds:YES];
                [cell.commentSenderProfilePic.layer setCornerRadius:cell.commentSenderProfilePic.frame.size.width/2];
                [cell.commentSenderProfilePic.layer setMasksToBounds:YES];
                [cell.commentSenderProfilePic setImage:image];
            }
            else {
                
                UIImage * defaultimg = [UIImage imageNamed:@"58.png"];
                
                CALayer *imageLayer = cell.commentSenderProfilePic.layer;
                [imageLayer setCornerRadius:5];
                [imageLayer setBorderWidth:1];
                [imageLayer setMasksToBounds:YES];
                [cell.commentSenderProfilePic.layer setCornerRadius:cell.commentSenderProfilePic.frame.size.width/2];
                [cell.commentSenderProfilePic.layer setMasksToBounds:YES];
                [cell.commentSenderProfilePic setImage:defaultimg];
            }
        }];
        
    }
    else {
        UIImage * defaultimg = [UIImage imageNamed:@"PFIcon"];
        
        CALayer *imageLayer = cell.commentSenderProfilePic.layer;
        [imageLayer setCornerRadius:5];
        [imageLayer setBorderWidth:1];
        [imageLayer setMasksToBounds:YES];
        [cell.commentSenderProfilePic.layer setCornerRadius:cell.commentSenderProfilePic.frame.size.width/2];
        [cell.commentSenderProfilePic.layer setMasksToBounds:YES];
        [cell.commentSenderProfilePic setImage:defaultimg];
    }
    
    }
    return cell;
    
    
}

#define MAXLength 50



- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= MAXLength || returnKey;
}

/*- (BOOL)textView:(UITextView *) textView shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString * )string{

    NSUInteger oldLength = [textView.text length];
    NSUInteger replaceLenght = [string length];
    NSUInteger rangeLenght = range.length;
    
    NSUInteger newLenght = oldLength - rangeLenght + replaceLenght;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    

    return newLenght <= MAXLength || returnKey;
}*/


/*
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return textView.text.length + (text.length - range.length) <= 300
    ;
}
 */

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    PFCommentsTableViewCell * cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    PFObject * object = cell.commentObject;
    
    PFUser * user = [object objectForKey:@"createdBy"];
    
    PFUser * currentUser = [PFUser currentUser];
    
    if ([user.objectId isEqualToString:[currentUser objectId]])
        return YES;
    else
        return NO;
   
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    NSLog(@"........................................ Delete comment.");
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //remove the deleted object from your data source.
        //If your data source is an NSMutableArray, do this
        
        PFObject * comment = [self.commentsList objectAtIndex:indexPath.row];
        
        [self.commentsList removeObjectAtIndex:indexPath.row];
        
        
        [tableView reloadData]; // tell table to refresh now
        
        PFQuery *query = [PFQuery queryWithClassName:@"comment"];
        
        //[query whereKeyDoesNotExist:@"likedImage"];
        
        [query whereKey:@"objectId" equalTo:[comment objectId]];
        
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (!object) {
                NSLog(@"The getFirstObject request failed.");
            } else {
                NSLog(@"Successfully retrieved the object.");
                [object deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    if (succeeded && !error) {
                        NSLog(@"Image deleted from Parse");
                    } else {
                        NSLog(@"error: %@", error);
                    }
                }];
            }
        }];
        
        //PFUser * user = [PFQuery queryWithClassName:@"Users"];
        
        //[query getFirstObject];
        
        
        
        
        // *** decrement comment counter
    }
}

- (IBAction)editCommentTouch:(id)sender {
    
    PFCommentsTableViewCell * cell = (PFCommentsTableViewCell *) [[sender superview] superview];
    
    self.commentObject = cell.commentObject;
    
    
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"addcomment"]) {
        PFAddCommentViewController * commentVC = [segue destinationViewController];
        commentVC.commentObject = self.commentObject;
        
        NSLog(@"***************** SEGUE. self.commentObject = %@", self.commentObject);
    }
}




@end
