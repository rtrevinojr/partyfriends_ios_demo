//
//  PFGoingOutTableViewCell.m
//  PartyFriends
//
//  Created by Joshua Silva  on 5/5/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import "PFGoingOutTableViewCell.h"

@implementation PFGoingOutTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
