//
//  PFHostingProfileDetailViewController.m
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 11/19/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import "PFHostingProfileDetailViewController.h"

@implementation PFHostingProfileDetailViewController



- (void) viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithTitle:@"Post" style:UIBarButtonItemStyleBordered
                                                                  target:self
                                                                  action:@selector(postComment:)];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithImage:[UIImage imageNamed:@"arrow-left"]
                                   style:UIBarButtonItemStylePlain
                                   target:self action:@selector(goBack)];
    
    self.navigationItem.leftBarButtonItem = backButton;
    
    
    
    self.hostingHeadline.text = [self.pfObject objectForKey:@"headline"];
    
    NSDate * hostdate = [self.pfObject objectForKey:@"date"];
    
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd HH:mm"]; // Date formater
    NSString *date = [dateformate stringFromDate:hostdate]; // Convert date to string
    NSLog(@"date :%@",date);
    NSMutableArray * sublayers = [NSMutableArray arrayWithArray:self.navigationController.navigationBar.layer.sublayers];
    
    [sublayers removeObjectAtIndex:1];
    
    self.navigationController.navigationBar.layer.sublayers = sublayers;
    
    //NSDate *currentDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
   NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit | NSMinuteCalendarUnit | NSWeekdayCalendarUnit | NSTimeZoneCalendarUnit fromDate:hostdate]; // Get necessary date components
    
    NSInteger month = [components month]; //gives you month
    NSInteger day = [components day]; //gives you day
    NSInteger year = [components year]; // gives you year
    
    NSInteger hour = [components hour];
    
    NSInteger minute = [components minute];
    
    NSInteger weekday = [components weekday];
    
    NSInteger timezone = [components timeZone];
    
    NSString * temp = [NSString stringWithFormat:@"Months:%d Day:%d Year:%d Hour:%d Minute:%d Weekday:%d TimeZone:%d",[components month],  [components day],[components year], hour, minute, weekday, timezone];
    
    NSLog(@"************ DATE = %@",temp);
    
    NSString * month_s = [PFHostingProfileDetailViewController getMonthFromDate:month];

    NSString * day_s = [PFHostingProfileDetailViewController getDayFromDate:day];
    NSString * year_s = [NSString stringWithFormat:@"%ld", year];
    
    NSString * weekday_s = [PFHostingProfileDetailViewController getWeekDayFromDate:weekday];
    
    NSString * hour_s = [PFHostingProfileDetailViewController getHourFromDate:hour];
    NSString * min_s = [NSString stringWithFormat:@"%ld", minute];
    
    NSString *dateResult = @"";
    NSString * timeResult = @"";
    
    dateResult = [[[[[[[dateResult stringByAppendingString:weekday_s]
                                   stringByAppendingString:@" " ]
                                stringByAppendingString:month_s]
                                stringByAppendingString:@" "]
                                stringByAppendingString:day_s]
                                stringByAppendingString:@" "]
                                stringByAppendingString:year_s];
    
    timeResult = [[[[[timeResult stringByAppendingString:hour_s]
                            stringByAppendingString:@":"]
                            stringByAppendingString:min_s]
                            stringByAppendingString:@" "]
                            stringByAppendingString:[PFHostingProfileDetailViewController getPostAfterFromDate:hour]];
    
    
    self.hostingDescriptionTextView.text = [self.pfObject objectForKey:@"description"];
    
    self.hostingDate.text = dateResult;
    self.hostingTime.text = timeResult;
    
    PFGeoPoint * geo = [self.pfObject objectForKey:@"location"];
    
    
    PFUser * user = [self.pfObject objectForKey:@"createdBy"];
    
    NSString * usertext = [user objectForKey:@"firstName"];
    
    
    self.hostingHost.text = [NSString stringWithFormat:@"Hosted By %@", usertext];
    
    
    MKCoordinateRegion region;
    region.center.latitude = geo.latitude;
    region.center.longitude = geo.longitude;
    region.span.latitudeDelta = 0.005f;
    region.span.longitudeDelta = 0.005f;
    [self.hostingMapView setRegion:region animated:YES];
    
    // Add an annotation
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    //point.coordinate = userLocation.coordinate;
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = geo.latitude;
    coordinate.longitude = geo.longitude;
    point.coordinate = coordinate;
    point.title = [self.pfObject objectForKey:@"headline"];
    //point.subtitle = _AddressLabel.text;
    [self.hostingMapView addAnnotation:point];
    [self.hostingMapView selectAnnotation:point animated:YES];
    
    
}

- (void) goBack {
    
    NSLog(@"Back");
    
    [self.navigationController popViewControllerAnimated:YES];
}

+ (NSString *) getDayFromDate: (NSInteger) day
{
    NSString * result = [NSString stringWithFormat:@"%ld", (long)day];
    return result;
}

+ (NSString *) getMonthFromDate: (NSInteger) month
{
    switch (month) {
        case 1:
            return @"January";
            break;
        case 2:
            return @"February";
        case 3:
            return @"March";
        case 4:
            return @"April";
        case 5:
            return @"May";
        case 6:
            return @"June";
        case 7:
            return @"July";
        case 8:
            return @"August";
        case 9:
            return @"September";
        case 10:
            return @"October";
        case 11:
            return @"November";
        case 12:
            return @"December";
        default:
            return @"Month Unknown";
            break;
    }
}

+ (NSString *) getWeekDayFromDate: (NSInteger) weekday
{
    switch (weekday) {
        case 1:
            return @"Sunday";
            break;
        case 2:
            return @"Monday";
        case 3:
            return @"Tuesday";
        case 4:
            return @"Wednesday";
        case 5:
            return @"Thursday";
        case 6:
            return @"Friday";
        case 7:
            return @"Saturday";
        default:
            return @"Weekday Unknown";
            break;
    }
}

+ (NSString *) getHourFromDate: (NSInteger) hour
{
    if (hour > 12)
        hour -= 12;
    
    return [NSString stringWithFormat:@"%ld", hour];
}

+ (NSString *) getPostAfterFromDate: (NSInteger) hour
{
    if (hour > 12)
        return @"PM";
    else
        return @"AM";
}


@end
