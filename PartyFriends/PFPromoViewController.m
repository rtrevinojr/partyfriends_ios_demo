//
//  PFPromoViewController.m
//  PartyFriends
//
//  Created by Joshua Silva  on 2/22/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import "PFPromoViewController.h"
#import "PFVendorEditPromoterTableViewCell.h"
#import "PFVendorSelectPromoTableViewController.h"
#import "PFVendorInfoTab.h"

@interface PFPromoViewController (){

    tabbarViewController * tabbar;
}

@end

@implementation PFPromoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    tabbar = (tabbarViewController * ) self.tabBarController;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    self.vendorPFObject = tabbar.vendorObject;
    
    PFRelation * promoters = [self.vendorPFObject relationForKey:@"promoters"];
    PFQuery *query = [promoters query];
    
    self.promoterList = [NSMutableArray new];
    
    
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if (!error) {
            self.promoterList = objects;
        }
        
        NSLog(@"*********** Promo List");
        NSLog(@"%@", self.promoterList);
        
        [self.tableView reloadData];
    }];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.promoterList.count;
}


- (PFVendorEditPromoterTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PFVendorEditPromoterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"promocell" forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[PFVendorEditPromoterTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"promocell"];
    }
    
    
    PFObject * promo = [self.promoterList objectAtIndex:indexPath.row];
    
    
    
    
    CALayer *imageLayer = cell.promoterProfileImage.layer;
    [imageLayer setCornerRadius:5];
    [imageLayer setBorderWidth:1];
    [imageLayer setMasksToBounds:YES];
    [cell.promoterProfileImage.layer setCornerRadius:cell.promoterProfileImage.frame.size.width/2];
    [cell.promoterProfileImage.layer setMasksToBounds:YES];
    
    
    if (promo[@"profilePic"] != nil) {
        
        PFFile * pic = [promo objectForKey:@"profilePic"];
        [pic getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
            if (!error) {
                UIImage *image = [UIImage imageWithData:data];
                [cell.promoterProfileImage setImage:image];
            }
            else {
                UIImage * defaultimg = [UIImage imageNamed:@"58.png"];
                [cell.promoterProfileImage setImage:defaultimg];
            }
        }];
    }
    else {
        UIImage * defaultimg = [UIImage imageNamed:@"PFIcon"];
        [cell.promoterProfileImage setImage:defaultimg];
    }
    
    
    cell.promoterNameLabel.text = [promo objectForKey:@"firstName"];
    
    return cell;
}

-(IBAction)backToPromo:(UIStoryboardSegue *) segue{
    
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"selectpromofriends"]) {
        
        UINavigationController * nav = [segue destinationViewController];
        
        PFVendorSelectPromoTableViewController * destVC = (PFVendorSelectPromoTableViewController *)nav.topViewController;
        
        destVC.vendorList = self.promoterList;
    }
    
}


@end
