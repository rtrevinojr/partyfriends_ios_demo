//
//  ProfileContainerViewController.m
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 10/19/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import "ProfileContainerViewController.h"

#import "MapView.h"

#import "LocationManagerSingleton.h"
#import "PFCommentHistoryVC.h"

#import "Parse/Parse.h"

#import "PFPartyCommentsVC.h"
#import <ParseUI/ParseUI.h>

#import <QuartzCore/QuartzCore.h>



@interface ProfileContainerViewController ()

@property (strong, nonatomic) PFObject * comObject;

- (IBAction)Cancel:(id)sender;


@end

@implementation ProfileContainerViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    fetchName = [PFProfileInfomation nameObj];
    
    
    
    CALayer *callbtnLayer = [self.view layer];
    [callbtnLayer setMasksToBounds:YES];
    [callbtnLayer setCornerRadius:30.0f];
    
    self.view.layer.borderColor = [UIColor blackColor].CGColor;
    self.view.layer.borderWidth = 2.0f;
    
    // border radius
    //[self.view.layer setCornerRadius:30.0f];
    //self.view.layer.masksToBounds = YES;
    
    // drop shadow
    [self.view.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.view.layer setShadowOpacity:0.8];
    [self.view.layer setShadowRadius:3.0];
    [self.view.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    //[self.view setUserInteractionEnabled:YES]
    
    self.currentUser = [PFUser currentUser];
    
    //PFObject * friend;
    
    PFFile * pic = [self.currentUser objectForKey:@"profilePic"];
    

    
    [pic getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        if (!error) {
            UIImage *image = [UIImage imageWithData:data];
            
            CALayer *imageLayer = self.profilePartyPic.layer;
            [imageLayer setCornerRadius:5];
            [imageLayer setBorderWidth:1];
            [imageLayer setMasksToBounds:YES];
            
            [self.profilePartyPic.layer setCornerRadius:self.profilePartyPic.frame.size.width/2];
            
            [self.profilePartyPic.layer setMasksToBounds:YES];
            
            [self.profilePartyPic setImage:image];
        }
    }];
    
    //this.layer.borderColor = [UIColor blackColor].CGcolor;
    //this.layer.borderWidth = 10;
    [self.editButton setTintColor:[UIColor whiteColor]];
    
    
    NSLog(@"viewDidLoad ProfileContainerViewController");
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    self.NameLabel.text = fetchName.friendsName;

    NSLog(@"%@", self.labelString);
    
    self.NameLabel.text = self.labelString;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)Cancel:(id)sender {
    
    MapView * parent = (MapView *) [self parentViewController];
    
    [parent.mapView setUserInteractionEnabled:YES];
    [parent.mapView setScrollEnabled:YES];
    
    
    parent.mapView.alpha = 1;
    
    [parent sendContainerToBack];
    parent.mapView_.settings.scrollGestures = YES;
    
}

- (IBAction)callUber:(id)sender {
    
    CLLocationCoordinate2D cood;
    
    cood = [LocationManagerSingleton sharedSingleton].locationManager.location.coordinate;
    
    
    NSString * client_id = @"L58ExalNrphCN8FtEvGEOP2MRVM3QvXc&action";
    
    NSString * server_toker = @"jlnT1TewykCf5US51fU9-BD8CBlGr4jzPN0oM479";
    
    NSString * pickup_lat = [NSString stringWithFormat:@"%f", cood.latitude];
    
    NSString * pickup_long = [NSString stringWithFormat:@"%f", cood.longitude];
    
    NSLog(@"pick up latitude = %@", pickup_lat);
    NSLog(@"pick up longitude = %@", pickup_long);
    
    NSString * pickup_nickname = self.currentUser[@"firstName"];
    
    NSString * pickup_address;
    
    NSString * dropoff_lat;
    
    NSString * dropoff_long;
    
    NSString * dropoff_nickname;
    
    NSString * dropoff_address;
    
    NSString * product_id = @"";
    
    
    NSString * basic_url = @"uber://?action=setPickup&pickup=my_location";
    
    
    NSString * test_url = @"uber://?client_id=L58ExalNrphCN8FtEvGEOP2MRVM3QvXc&action=setPickup&pickup[latitude]=37.775818&pickup[longitude]=-122.418028&pickup[nickname]=UberHQ&pickup[formatted_address]=1455%20Market%20St%2C%20San%20Francisco%2C%20CA%2094103&dropoff[latitude]=37.802374&dropoff[longitude]=-122.405818&dropoff[nickname]=Coit%20Tower&dropoff[formatted_address]=1%20Telegraph%20Hill%20Blvd%2C%20San%20Francisco%2C%20CA%2094133&product_id=a1111c8c-c720-46c3-8534-2fcdd730040d";
    
    
    NSString * uber_url = [NSString stringWithFormat:@"uber://?client_id=%@&action=setPickup&pickup[latitude]=%@&pickup[longitude]=%@&pickup[nickname]=%@&dropoff[latitude]=%@&dropoff[longitude]=%@dropoff[nickname]=%@", client_id, pickup_lat, pickup_long, pickup_nickname, dropoff_lat, dropoff_long, dropoff_nickname];
    
    NSString * test_uber_url = [NSString stringWithFormat:@"uber://?client_id=%@&action=setPickup&pickup[latitude]=%@&pickup[longitude]=%@&pickup[nickname]=%@", client_id, pickup_lat, pickup_long, pickup_nickname];
    
    
    
    NSString * uber_api_url = [NSString stringWithFormat:@"https://api.uber.com/v1/products?latitude=%@&longitude=%@", pickup_lat, pickup_long];
    
    //NSString * uber_api_url = @"https://api.uber.com/v1/products?latitude=30&longitude=-97";
    
    
    
    NSString *pswd = @"XXXXXXXXX";
    
    NSURL *url = [NSURL URLWithString:uber_api_url];
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    NSString *authValue = [NSString stringWithFormat:@"Bearer %@", pswd];
    NSString * uber_authValue = [NSString stringWithFormat:@"Token %@", server_toker];
    [request setValue:uber_authValue forHTTPHeaderField:@"Authorization"];
    
    
    
    NSLog(@"********************************");
    NSLog(@"Request = %@", request);
    
    
    
    //create a mutable HTTP request
    //NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:myUrl];
    //sets the receiver’s timeout interval, in seconds
    [request setTimeoutInterval:30.0f];
    //sets the receiver’s HTTP request method
    [request setHTTPMethod:@"GET"];
    //sets the request body of the receiver to the specified data.
    //[request setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    
    //allocate a new operation queue
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    //Loads the data for a URL request and executes a handler block on an
    //operation queue when the request completes or fails.
//    [NSURLConnection
//     sendAsynchronousRequest:request
//     queue:queue
//     completionHandler:^(NSURLResponse *response,
//                         NSData *data,
//                         NSError *error) {
//         if ([data length] >0 && error == nil){
//             //process the JSON response
//             //use the main queue so that we can interact with the screen
//             dispatch_async(dispatch_get_main_queue(), ^{
//                 [self parseResponse:data];
//             });
//         }
//         else if ([data length] == 0 && error == nil){
//             NSLog(@"Empty Response, not sure why?");
//         }
//         else if (error != nil){
//             NSLog(@"Not again, what is the error = %@", error);
//         }
//     }];
    
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"uber://"]]) {
        // The app is installed! Launch app.
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:test_uber_url]];
    }
    else {
        // No Uber app! Open mobile website.
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.uber.com"]];
    }
    
    
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:test_uber_url]];
    
}

-(void)reload{
    
    [self.editButton setHidden:YES];
    
    NSNumber * score = [NSNumber new];
    score = self.user[@"score"];
    if (score == nil) {
        score = 0;
    }
    NSString * points = [NSString stringWithFormat:@"%@ points", score];
    self.NameLabel.text = self.user[@"firstName"];
    //self.headline.text = self.user[@"headline"];
    self.points.text = points;
    
    
    UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
    CALayer * imageLayer = self.profilePartyPic.layer;
    [imageLayer setCornerRadius:5];
    [imageLayer setBorderWidth:0];
    [imageLayer setMasksToBounds:YES];
    
    [self.profilePartyPic.layer setCornerRadius:self.profilePartyPic.frame.size.width/2];
    [self.profilePartyPic.layer setMasksToBounds:YES];
    
    [self.profilePartyPic setImage:defaultimg];
    
//    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(zoomer)];
//    [singleTap setNumberOfTapsRequired:1];
//    [self.profileIcon setUserInteractionEnabled:YES];
//    [self.profileIcon addGestureRecognizer:singleTap];
    
    
//    if (self.user[@"profilePic"] != nil) {
//
//        PFFile * pic = [self.user objectForKey:@"profilePic"];
//        
//        
//        [pic getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
//            if (!error) {
//                UIImage *image = [UIImage imageWithData:data];
//                CALayer *imageLayer = self.profilePartyPic.layer;
//                [imageLayer setCornerRadius:5];
//                [imageLayer setBorderWidth:1];
//                [imageLayer setMasksToBounds:YES];
//                [self.profilePartyPic.layer setCornerRadius:self.profilePartyPic.frame.size.width/2];
//                [self.profilePartyPic.layer setMasksToBounds:YES];
//                [self.profilePartyPic setImage:image];
//            }
//            else {
//                
//                UIImage * defaultimg = [UIImage imageNamed:@"58.png"];
//                
//                CALayer *imageLayer = self.profilePartyPic.layer;
//                [imageLayer setCornerRadius:5];
//                [imageLayer setBorderWidth:1];
//                [imageLayer setMasksToBounds:YES];
//                [self.profilePartyPic.layer setCornerRadius:self.profilePartyPic.frame.size.width/2];
//                [self.profilePartyPic.layer setMasksToBounds:YES];
//                [self.profilePartyPic setImage:defaultimg];
//            }
//        }];
//        
//    }
//    else {
//        UIImage * defaultimg = [UIImage imageNamed:@"PFIcon"];
//        
//        CALayer *imageLayer = self.profilePartyPic.layer;
//        [imageLayer setCornerRadius:5];
//        [imageLayer setBorderWidth:1];
//        [imageLayer setMasksToBounds:YES];
//        [self.profilePartyPic.layer setCornerRadius:self.profilePartyPic.frame.size.width/2];
//        [self.profilePartyPic.layer setMasksToBounds:YES];
//        [self.profilePartyPic setImage:defaultimg];
//    }
    
    PFObject * photo = [self.user objectForKey:@"profilePicture"];
    
    if (photo != nil) {
        [photo fetchInBackgroundWithBlock:^(PFObject * photoObject, NSError * error){
        
            if (!error) {
                PFFile * pic =[photoObject objectForKey:@"photo"];
                PFImageView * userImage =[PFImageView new];
                userImage.image = [UIImage imageNamed:@"userPic"];
                userImage.file = (PFFile *) pic;
                
                [userImage loadInBackground:^(UIImage * image, NSError * error){
                    if (!error) {
                        image = [self squareImageFromImage:image scaledToSize:image.size.height];
                        [self.profilePartyPic setImage:image];
                        
                    }
                    
                }];
                
            }
            
            
        
        }];
    }
    
    
    
    
    
}



-(void)reloadForCurrentUser{
    
    [self.editButton setHidden:NO];
    
    NSNumber * score = [NSNumber new];
    score = self.user[@"score"];
    if (score == nil) {
        score = 0;
    }
    NSString * points = [NSString stringWithFormat:@"%@ points", score];
    self.NameLabel.text = self.user[@"firstName"];
    //self.headline.text = self.user[@"headline"];
    self.points.text = points;
    
    
    UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
    CALayer * imageLayer = self.profilePartyPic.layer;
    [imageLayer setCornerRadius:5];
    [imageLayer setBorderWidth:0];
    [imageLayer setMasksToBounds:YES];
    
    [self.profilePartyPic.layer setCornerRadius:self.profilePartyPic.frame.size.width/2];
    [self.profilePartyPic.layer setMasksToBounds:YES];
    
    [self.profilePartyPic setImage:defaultimg];
    
    
    
    PFObject * photo = [self.user objectForKey:@"profilePicture"];
    
    if (photo != nil) {
        [photo fetchInBackgroundWithBlock:^(PFObject * photoObject, NSError * error){
            
            if (!error) {
                PFFile * pic =[photoObject objectForKey:@"photo"];
                PFImageView * userImage =[PFImageView new];
                userImage.image = [UIImage imageNamed:@"userPic"];
                userImage.file = (PFFile *) pic;
                
                [userImage loadInBackground:^(UIImage * image, NSError * error){
                    if (!error) {
                        [self.profilePartyPic setImage:image];
                    }
                    
                }];
                
            }
            
            
            
        }];
    }
    
    
//    if (self.user[@"profilePic"] != nil) {
//        
//        PFFile * pic = [self.user objectForKey:@"profilePic"];
//        
//        
//        [pic getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
//            if (!error) {
//                UIImage *image = [UIImage imageWithData:data];
//                CALayer *imageLayer = self.profilePartyPic.layer;
//                [imageLayer setCornerRadius:5];
//                [imageLayer setBorderWidth:1];
//                [imageLayer setMasksToBounds:YES];
//                [self.profilePartyPic.layer setCornerRadius:self.profilePartyPic.frame.size.width/2];
//                [self.profilePartyPic.layer setMasksToBounds:YES];
//                [self.profilePartyPic setImage:image];
//            }
//            else {
//                
//                UIImage * defaultimg = [UIImage imageNamed:@"58.png"];
//                
//                CALayer *imageLayer = self.profilePartyPic.layer;
//                [imageLayer setCornerRadius:5];
//                [imageLayer setBorderWidth:1];
//                [imageLayer setMasksToBounds:YES];
//                [self.profilePartyPic.layer setCornerRadius:self.profilePartyPic.frame.size.width/2];
//                [self.profilePartyPic.layer setMasksToBounds:YES];
//                [self.profilePartyPic setImage:defaultimg];
//            }
//        }];
//        
//    }
//    else {
//        UIImage * defaultimg = [UIImage imageNamed:@"PFIcon"];
//        
//        CALayer *imageLayer = self.profilePartyPic.layer;
//        [imageLayer setCornerRadius:5];
//        [imageLayer setBorderWidth:1];
//        [imageLayer setMasksToBounds:YES];
//        [self.profilePartyPic.layer setCornerRadius:self.profilePartyPic.frame.size.width/2];
//        [self.profilePartyPic.layer setMasksToBounds:YES];
//        [self.profilePartyPic setImage:defaultimg];
//    }
    
    
    
    
}

-(void)changeColor
{
    self.view.backgroundColor = [UIColor redColor];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    
    
    if ([segue.identifier isEqualToString:@"comm"]) {
        
        //PFAddCommentViewController * commentVC = [segue destinationViewController];
        
        //PFCommentsTableViewController * commentsTVC = [segue destinationViewController];
        
        UINavigationController * nav = [segue destinationViewController];
        PFCommentHistoryVC * commentsVC = (PFCommentHistoryVC *)nav.topViewController;
        
        //PFPartyCommentsVC * commentsVC = [segue destinationViewController];
        
        //commentsVC.pfParty = self.selectedMessage;
        commentsVC.otherUser = self.user;
        
       // commentsVC.pfPost = self.userParty;
        
        
    }
    
}
- (IBAction)toMaps:(id)sender {

    PFGeoPoint * nameLocation = self.userParty[@"location"];
    
    CLLocationCoordinate2D partySpot = CLLocationCoordinate2DMake(nameLocation.latitude, nameLocation.longitude);
    
    MKPlacemark * placemark = [[MKPlacemark alloc] initWithCoordinate:partySpot addressDictionary:nil];
    
    MKMapItem * mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    NSString * nameSpot = [NSString stringWithFormat:@"%@", self.user[@"firstName"]];
    mapItem.name = nameSpot;
    [mapItem openInMapsWithLaunchOptions:nil];
    
//    CLLocation * location = [[CLLocation alloc] initWithLatitude:nameLocation.latitude longitude:nameLocation.longitude];
//    
//    NSString *addressString = [NSString stringWithFormat:@"http://maps.apple.com/?ll=50.894967,4.341626"];
//                               //,location.coordinate.latitude, location.coordinate.longitude];
//    
//    
//    NSURL *url = [NSURL URLWithString:addressString];
//    [[UIApplication sharedApplication] openURL:url];

    

    
    
}

- (UIImage *)squareImageFromImage:(UIImage *)image scaledToSize:(CGFloat)newSize {
    CGAffineTransform scaleTransform;
    CGPoint origin;
    
    if (image.size.width > image.size.height) {
        CGFloat scaleRatio = newSize / image.size.height;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(-(image.size.width - image.size.height) / 2.0f, 0);
    } else {
        CGFloat scaleRatio = newSize / image.size.width;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(0, -(image.size.height - image.size.width) / 2.0f);
    }
    
    CGSize size = CGSizeMake(newSize, newSize);
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(size, YES, 0);
    } else {
        UIGraphicsBeginImageContext(size);
    }
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextConcatCTM(context, scaleTransform);
    
    [image drawAtPoint:origin];
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}


@end
