//
//  AccountView.h
//  PartyFriends
//
//  Created by Joshua Silva  on 8/14/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Parse/Parse.h"

@interface AccountView : UIViewController < UIPickerViewDelegate,UIPickerViewDataSource, UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *firstName;
@property (strong, nonatomic) IBOutlet UITextField *lastName;
@property (strong, nonatomic) IBOutlet UITextField *phoneNumber;
@property (strong, nonatomic) IBOutlet UIDatePicker *birthPicker;

@property (strong, nonatomic) NSObject * waffles;


@property (strong, nonatomic) IBOutlet UITextField *address;
@property (strong, nonatomic) IBOutlet UITextField *city;

@property (strong, nonatomic) IBOutlet UITextField *zipcode;
@property (strong, nonatomic) IBOutlet UIPickerView *state;

@property (strong, nonatomic) IBOutlet UITextField *email;

- (IBAction)theChange:(id)sender;


@property (strong, nonatomic) IBOutlet UITextField *oldPassword;

@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UITextField *comfirmPassword;

- (IBAction)passwordChange:(id)sender;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) PFUser * currentUser;

@property (strong, nonatomic) NSArray * theStates;

@property (strong, nonatomic) PFObject * accountInfo;
@property (strong, nonatomic) NSString * userState;

@end
