//
//  TableViewController.h
//  party2
//
//  Created by Gilberto Silva on 4/25/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface RequestTableViewController : UITableViewController
@property (strong, nonatomic) IBOutlet UITableView *friendRequests;
@property (strong, nonatomic) NSMutableArray *requests;
@property (strong, nonatomic) NSMutableArray *requeste;
@property (strong, nonatomic) PFUser *currentUser;
@property (strong, nonatomic) NSIndexPath* index;
@property (strong, nonatomic) NSMutableArray * updatedRequest;
@property (strong, nonatomic) PFRole * role;
@property (strong, nonatomic) PFRelation * roleRelation;
@property (strong, nonatomic) PFRelation * exclusiveRelation;
@property (strong, nonatomic) NSMutableArray * exclusiveArray;

- (IBAction)accept:(id)sender;
- (IBAction)deny:(id)sender;
@end
