//
//  ActionView.m
//  PartyFriends
//
//  Created by Joshua Silva  on 8/4/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import "ActionView.h"
#import "LocationManagerSingleton.h"
#import "tabBarController.h"
#import "UIView+Toast.h"
#import <sys/utsname.h>
#import "PFUpsidedownAddViewController.h"
#import "PFCommentHistoryVC.h"
#import <ParseUI/ParseUI.h>
#import "PFPartyCommentsVC.h"
#import "SettingsView.h"
@import GoogleMaps;





@interface ActionView ()

@property (weak, nonatomic) IBOutlet UIImageView *profileIcon;

@end

//CLLocationCoordinate2D cood;

@implementation ActionView {
    tabBarController * embededTabbar;
    GMSPlacesClient * _placesClient;
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    
    self.pushUser = @"";
    [self.motion stopAccelerometerUpdates];
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view sendSubviewToBack:self.statusView];
    
    [self backgroundCheck];
    
    [self.statusButton setTintColor:[UIColor purpleColor]];
    
    [self.lowerBar setBackgroundColor:[UIColor purpleColor]];
    
    _placesClient = [[GMSPlacesClient alloc] init];
    
    
    
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString * deviceModel = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    if ([deviceModel containsString:@"iPhone8"] || [deviceModel containsString:@"iPhone7"]){
        
        
        
    }
    
    //[[self.lowerBar layer] setBorderColor:[[UIColor purpleColor] CGColor]];
    //[[self.lowerBar layer] setBorderWidth:2.3];
    
    //  UIImage * PFButton = [[UIImage imageNamed:@"PIcon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    
    
    UIImage * PFButton = [[UIImage imageNamed:@"NewGoldIcon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [self.PFButton setImage:PFButton forState:UIControlStateNormal];
    
    self.tapGesture.cancelsTouchesInView = NO;
    self.tapGesture.enabled = YES;
    
    self.editLocation.autocapitalizationType = UITextAutocapitalizationTypeWords;
    //[self.lowerBar setOpaque:YES];
    self.lowerBar.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
    //[self.lowerBar setBackgroundColor:[UIColor blueColor]];
    
    self.currentUser = [PFUser currentUser];
    [self.currentUser fetchInBackground];
    
    self.allFriendsRelation = [self.currentUser relationForKey:@"friends"];
    NSLog(@"Current user: %@", self.currentUser);
    
    PFFile * pic = [self.currentUser objectForKey:@"profilePic"];
    
    [self.usersName setHidden:YES];
    //CALayer * imageLayer = self.profileIcon.layer;
    //[imageLayer setCornerRadius:5];
    //[imageLayer setBorderWidth:0];
    //[imageLayer setMasksToBounds:YES];
    
    
    
    if (self.currentUser[@"profilePic"] == nil) {
        UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
        CALayer * imageLayer = self.profileIcon.layer;
        [imageLayer setCornerRadius:5];
        [imageLayer setBorderWidth:0];
        [imageLayer setMasksToBounds:YES];
        
        [self.profileIcon.layer setCornerRadius:self.profileIcon.frame.size.width/2];
        [self.profileIcon.layer setMasksToBounds:YES];
        
        [self.profileIcon setImage:defaultimg];
        
        UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(zoomer)];
        [singleTap setNumberOfTapsRequired:1];
        [self.profileIcon setUserInteractionEnabled:YES];
        [self.profileIcon addGestureRecognizer:singleTap];
    }
    
    
    if (self.profileIcon != nil) {
        self.usersName.text = @"";
    }
    
//    PFObject * photo = self.currentUser[@"profilePicture"];
//    
//   // NSLog(@"loggin the PICObject.id %@", photo.objectId);
//    if (photo !=nil) {
//        [photo fetchInBackgroundWithBlock:^(PFObject * photoObject, NSError * error){
//            if (!error) {
//                PFFile * pic =[photoObject objectForKey:@"photo"];
//                PFImageView * userImage =[PFImageView new];
//                userImage.image = [UIImage imageNamed:@"userPic"];
//                userImage.file = (PFFile *) pic;
//                
//                [userImage loadInBackground:^(UIImage * image, NSError * error){
//                    if (!error) {
//                        [self.profileIcon setImage:image];
//                    }
//                }];
//            }
//        }];
//    }else{
//        
//        
//    }
    
    
    //moved to viewDidLoad
    /* PFQuery * getFriends = [self.allFriendsRelation query];
     [getFriends findObjectsInBackgroundWithBlock:^(NSArray *friends, NSError *error){
     if(error){
     NSLog(@"gh%@",error);
     }
     else{
     NSLog(@"hjasfjhaskjdhfkjashdfkjhaskfhkjashfafholla %lu",(unsigned long)friends.count);
     }
     
     }];
     */
    
    
    
    
    self.editLocation.delegate= self;
    
    //    if ([self.currentUser[@"status"]  isEqual: @0]) {
    //        self.userStatus = @"WH";
    //    }if ([self.currentUser[@"status"] isEqual:@1]) {
    //        self.userStatus = @"PM";
    //
    //    }if ([self.currentUser[@"status"] isEqual:@2]) {
    //        self.userStatus = @"GO";
    //    }
    
    
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(toRequestView:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionUp)];
    
    
    
    self.usersName.userInteractionEnabled = YES;
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(zoom:)];
    [self.usersName addGestureRecognizer:tapGesture];
    
    NSString * roleName = [NSString stringWithFormat:@"Role-%@", self.currentUser.objectId];
    
    
    //    if ([self.userStatus  isEqual: @"PM"]) {
    //        [self partyMode];
    //    }
    //    if ([self.userStatus isEqual: @"GO"]) {
    //
    //
    //
    //    }
    //    if ([self.userStatus isEqual: @"WH"]) {
    //
    //        //[self offine];
    //
    //    }
    
    
    /*              NSString * roleName = [NSString stringWithFormat:@"Role-%@", self.currentUser.objectId];
     //  querying the role of the current user
     NSLog(@"this log is checking the string for the role name before it goes into the role query %@", roleName);
     PFQuery * queryRole = [PFRole query];
     [queryRole whereKey:@"name" equalTo:roleName];
     [queryRole getFirstObjectInBackgroundWithBlock:^(PFObject * object, NSError * error){
     if (error || object== nil){
     NSLog(@"this error belongs to the role assigning block -- %@", error);
     }else {
     self.rolling = (PFRole *)object;
     NSLog(@"checking to see if the role went through and the size of objects %@", self.rolling);
     
     
     }
     
     
     }];*/
    
    
    /* [queryRole findObjectsInBackgroundWithBlock:^(NSArray * objects, NSError * error){
     if (error || objects.count == 0 ) {
     NSLog(@"this error belongs to the role assigning block -- %@", error);
     }else if (objects.count > 0){
     
     NSLog(@"checking to see if the role went through and the size of objects %lu", (unsigned long)objects.count);
     self.rolling = objects[0];
     
     }
     }];
     
     */
    
    // Listen for keyboard appearances and disappearances
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
    // embededTabbar = (tabBarController  *) [self childViewControllers];
    
    //    NSString * deviceType = [UIDevice currentDevice].model;
    //
    //    NSLog(@"the device model %@", deviceType);
    
    //[self updateCheck];
    
    if (self.sessionToken == nil) {
        [PFSession getCurrentSessionInBackgroundWithBlock:^(PFSession * session, NSError * error){
            if (!error) {
                self.sessionToken = session;
                
                
            }else{
                
                
            }
            
        }];
    }
    
    
    self.statusButton.layer.cornerRadius = 10;
    self.statusButton.clipsToBounds = YES;
    [self.statusButton setBackgroundColor:[UIColor colorWithRed:(127/255.0) green:(127/255.0) blue:(127/255.0) alpha:.2]];
    
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:NO];
    
    
//    PFObject * photo = self.currentUser[@"profilePicture"];
//    
//    // NSLog(@"loggin the PICObject.id %@", photo.objectId);
//    if (photo !=nil) {
//        [photo fetchInBackgroundWithBlock:^(PFObject * photoObject, NSError * error){
//            if (!error) {
//                PFFile * pic =[photoObject objectForKey:@"photo"];
//                PFImageView * userImage =[PFImageView new];
//                userImage.image = [UIImage imageNamed:@"userPic"];
//                userImage.file = (PFFile *) pic;
//                
//                [userImage loadInBackground:^(UIImage * image, NSError * error){
//                    if (!error) {
//                        [self.profileIcon setImage:image];
//                    }
//                }];
//            }
//        }];
//    }else{
//        
//        
//    }
//    
//    
//    [photo fetchInBackgroundWithBlock:^(PFObject * photoObject, NSError * error){
//        if (!error) {
//            PFFile * pic =[photoObject objectForKey:@"photo"];
//            PFImageView * userImage =[PFImageView new];
//            userImage.image = [UIImage imageNamed:@"userPic"];
//            userImage.file = (PFFile *) pic;
//            
//            [userImage loadInBackground:^(UIImage * image, NSError * error){
//                if (!error) {
//                    [self.profileIcon setImage:image];
//                }
//            }];
//        }
//    }];
    //// this blocked out code is to the new post
    
    
    //    if (self.currentUser[@"status"] == nil) {
    //        [self.currentUser fetchInBackgroundWithBlock:^(PFObject * user, NSError * error){
    //            if (!error) {
    //                self.currentUser = (PFUser *) user;
    //
    //
    //
    //            }else{
    //
    //
    //
    //            }
    //
    //        }];
    //    }
    
    
    
    [self.currentUser fetchInBackgroundWithBlock:^(PFObject * _Nullable user, NSError * _Nullable error) {
        if (!error) {
            
            NSLog(@"cake");
            self.currentUser = (PFUser *) user;
            
            PFObject * profilePhoto = self.currentUser[@"profilePicture"];
            
            [profilePhoto fetchInBackgroundWithBlock:^(PFObject * photoObject, NSError * error){
                if (!error) {
                    NSLog(@"cake1");
                    PFFile * pic =[photoObject objectForKey:@"photo"];
                    PFImageView * userImage =[PFImageView new];
                    userImage.image = [UIImage imageNamed:@"userPic"];
                    userImage.file = (PFFile *) pic;
                    
                    [userImage loadInBackground:^(UIImage * image, NSError * error){
                        if (!error) {
                            image = [self squareImageFromImage:image scaledToSize:image.size.height];
                            
                            [self.profileIcon setImage:image];
                        }
                    }];
                }
            }];
            
            
            if ([self.currentUser[@"status"]  isEqual: @0]) {
                
                [self.statusButton setTitle:@"Offline" forState:UIControlStateNormal];
            }if ([self.currentUser[@"status"] isEqual:@1]) {
                
                [self.statusButton setTitle:@"PartyMode" forState:UIControlStateNormal];
            }if ([self.currentUser[@"status"] isEqual:@2]) {
                
                [self.statusButton setTitle:@"Exclusive" forState:UIControlStateNormal];
                

                NSLog(@"cake2");
                
                
                __block PFObject * postObject = self.currentUser[@"lastPost"];
                [postObject fetchInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
                    if(!error){
                        postObject = object;
                        
                        if (postObject[@"headline"] == nil || postObject[@"headline"]== [NSNull null]) {
                            self.editLocation.placeholder = @"whats happening?";
                        }
                        self.editLocation.placeholder = postObject[@"headline"];
                        
                    }else {self.editLocation.placeholder = @"Whats happening?";
                    }
                }];
                
            }

            
            
        }else{
            self.editLocation.placeholder  = @"Whats happening?";
            
            
            
        }
    }];
    
    if([self.pushType isEqualToString:@"comment"]){
        [self performSegueWithIdentifier:@"history" sender:self];
        NSTimer * pushTime = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(toCommentHistoryView) userInfo:nil repeats:NO];
        
    }
    else if([self.pushType isEqualToString:@"tug"]){
        
        
    }
    
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString * deviceModel = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    if ([deviceModel containsString:@"iPhone8"] || [deviceModel containsString:@"iPhone7"]){
        
        self.motion = [[CMMotionManager alloc] init];
        self.motion.accelerometerUpdateInterval = 1;
        
        if ([self.motion isAccelerometerAvailable])
        {
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            [self.motion startAccelerometerUpdatesToQueue:queue withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    NSLog(@"x-axis= %f || y-axis= %f || z-axis= %f",accelerometerData.acceleration.x,accelerometerData.acceleration.y, accelerometerData.acceleration.z);
                    
                    if (accelerometerData.acceleration.y >= 0.5){
                        NSLog(@"do this, whatever i want to change and check if this can actually change anything?");
                        
                        [self.motion stopAccelerometerUpdates];
                        [self performSegueWithIdentifier:@"actionToAdd" sender:self];
                        
                        
                        
                    }
                    if (error) {
                        NSLog(@"there was an erro here because other montior was stoped!");
                    }
                    
                    //                self.xAxis.text = [NSString stringWithFormat:@"%.2f",accelerometerData.acceleration.x];
                    //                self.yAxis.text = [NSString stringWithFormat:@"%.2f",accelerometerData.acceleration.y];
                    //                self.zAxis.text = [NSString stringWithFormat:@"%.2f",accelerometerData.acceleration.z];
                });
            }];
        }
        
        
    }
    
    
    
    
    
    NSString * roleName = [NSString stringWithFormat:@"Role-%@", self.currentUser.objectId];
    
    
    
    PFQuery * query = [self.allFriendsRelation query];
    [query orderByAscending:@"firstName"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error){
            
            NSLog(@"error %@ %@", error, [error userInfo]);
            
            
        }
        else{
            self.allFriends = objects;
            
            //            NSLog(@"this checking to see all the current friends on parse of the user NUMBER 2, %lu",(unsigned long)[self.allFriends count]);
            //            // self.pickFriends = [NSMutableArray arrayWithArray:self.allFriends];
            //            for (int i = 0; i < self.allFriends.count; i++) {
            //                PFUser * user = [self.allFriends objectAtIndex:i];
            //                [self.pickFriends addObject:user];
            //            }
            
            //[self.tableView reloadData];
            
            //NSLog(@"looking to see if the mutable array has been popluated!! %lu", (unsigned long)self.pickFriends.count);
            
            
            
            //            PFQuery * queryRole = [PFRole query];
            //            [queryRole whereKey:@"name" equalTo:roleName];
            //            [queryRole getFirstObjectInBackgroundWithBlock:^(PFObject * object, NSError * error){
            //                if (error || object== nil){
            //                    NSLog(@"this error belongs to the role assigning block -- %@", error);
            //                }else {
            //                    self.rolling = (PFRole *)object;
            //
            //
            //                    NSLog(@"checking to see if the role went through and the size of objects %@", self.rolling);
            //                    self.pickedFriendsRelation = self.rolling.users;
            //
            //                    for (int i = 0 ; i < self.allFriends.count; i++) {
            //                        [self.pickedFriendsRelation addObject:self.allFriends[i]];
            //
            //                        [self.tableView reloadData];
            //                    }
            //
            //
            //                }
            //
            //
            //            }];
            
            
        }
        
    }];
    
    //    PFQuery * queryRole = [PFRole query];
    //    [queryRole whereKey:@"name" equalTo:roleName];
    //    [queryRole getFirstObjectInBackgroundWithBlock:^(PFObject * object, NSError * error){
    //        if (error || object== nil){
    //            NSLog(@"this error belongs to the role assigning block -- %@", error);
    //        }else {
    //            self.rolling = (PFRole *)object;
    //
    //
    //            NSLog(@"checking to see if the role went through and the size of objects %@", self.rolling);
    //
    //        }
    //
    //
    //    }];
    
    
    
    //    NSDictionary * dictionary = [NSDictionary new];
    //
    //        [PFCloud callFunctionInBackground:@"Refresh" withParameters:dictionary block:^(NSArray * objects, NSError * error) {
    //            if (error){
    //
    //                NSLog(@"theres an error %@", error);
    //
    //            }else{
    //                self.refreshArray = objects;
    //                NSLog(@"returning the objects - %@", objects);
    //                embededTabbar.refreshArray = [NSArray arrayWithArray:self.refreshArray];
    //            }
    //
    //        }];
    
    
    //    PFQuery * getFriends = [self.allFriendsRelation query];
    //    [getFriends findObjectsInBackgroundWithBlock:^(NSArray *friends, NSError *error){
    //        if(error){
    //            NSLog(@"gh%@",error);
    //
    //        }
    //        else{
    //
    //            self.allFriends = friends;
    //            NSLog(@"hjasfjhaskjdhfkjashdfkjhaskfhkjashfafholla %lu",(unsigned long)friends.count);
    //
    //
    //        }
    //
    //    }];
    
    //[self.rolling.users addObject:self.allFriends[i]];
    
    if ([self.currentUser[@"status"] isEqual:@1] || [self.currentUser[@"status"] isEqual:@2]) {
        
    [self updatingUserLocation];
    }
}

-(void)OrientationDidChange:(NSNotification*)notification
{
    UIDeviceOrientation Orientation=[[UIDevice currentDevice]orientation];
    
    // NSLog(@"checking if this ran mutple times?!?!?!?!?!?");
    
    if(Orientation == UIInterfaceOrientationPortrait)
    {
        //[self backToBlack:nil];
        //[self performSegueWithIdentifier:@"backBack" sender:self];
    }
    
    else if(Orientation==UIDeviceOrientationPortraitUpsideDown)
    {
        [self performSegueWithIdentifier:@"seg" sender:self];
        
        NSLog(@"checking if this ran mutple times?!?!?!?!?!?");
        
        
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"toComments"]) {
        UINavigationController * nav = [segue destinationViewController];
        PFPartyCommentsVC * commentsTVC = (PFPartyCommentsVC *)nav.topViewController;
        commentsTVC.pfPost = self.commentObject;
        
        
    }
    
    if ([segue.identifier isEqualToString:@"history"]) {
        UINavigationController * nav = [segue destinationViewController];
        PFCommentHistoryVC * commentsTVC = (PFCommentHistoryVC *)nav.topViewController;
        NSLog(@"loggion\n  the string from\n  the appD %@ lets se\n e what is comes back \n with!!!!", self.pushType);
        commentsTVC.postObejct = self.commentObject;
        commentsTVC.userPush = self.pushUser;
        
        
        
        if ([self.pushType isEqualToString:@"comment"]) {
            
            //            UINavigationController * nav = [segue destinationViewController];
            //            PFCommentHistoryVC * commentsTVC = (PFCommentHistoryVC *)nav.topViewController;
            commentsTVC.userPush = self.pushUser;
            // commentsTVC.userPush = self.pushUser;
            self.pushType = @"";
            //self.pushUser = @"";
            NSLog(@"loggion\n  the string from\n  the appD %@ lets se\n e what is comes back \n with %@!!!!", self.pushType, self.pushUser);
        }
        
        
        
    }
    
    if ([segue.identifier isEqualToString:@"toTabbar"]) {
        embededTabbar = segue.destinationViewController;
        //UINavigationController * nav = [segue destinationViewController];
        
    }if ([segue.identifier isEqualToString:@"actionToAdd"]){
        PFUpsidedownAddViewController * addFriend = segue.destinationViewController;
        addFriend.statusString = @"1";
        
    }
}

- (IBAction)toRequestView:(id)sender {
    
    // [self performSegueWithIdentifier:@"toRequest" sender:self];
    
}


- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y+220);
    [self.scrollView setContentOffset:scrollPoint animated:YES];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self.scrollView setContentOffset:CGPointZero animated:YES];
    
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    //self.currentUser[@"headline"] = self.editLocation.text;
    NSString * grayOut = self.editLocation.text;
    
    [PFCloud callFunctionInBackground:@"Post" withParameters:@{@"headline":self.editLocation.text} block:^(NSString * results, NSError * error) {
        if(!error) {
            NSLog(@" the HEADLINE function went through!!!!!!!!!!!");
            
            [embededTabbar refreshAll];
            [embededTabbar toTable];
            self.editLocation.text = @"";
            self.editLocation.placeholder = grayOut;
            
        }else{
            NSLog(@"ERROR = %@ !@!@@!@! ERROR NUMBER: %ld",error, (long)error.code);
        }
    }];
    
    //[self.currentUser saveInBackground];
    self.personalText = self.editLocation.text;
    
    return YES;
}

-(IBAction)backToActionComments:(UIStoryboardSegue *) segue{
    [embededTabbar refresh];
    
}


-(IBAction)backToAction:(UIStoryboardSegue *) segue{
    
    [embededTabbar refresh];
    //    if ([self.userStatus  isEqual: @"PM"]) {
    //        [self partyMode];
    //    }
    //    if ([self.userStatus isEqual: @"GO"]) {
    //        [self.statusButton setTitle:@"Exclusive" forState:UIControlStateNormal];
    //        self.userStatus = @"GO";
    //    }
    //    if ([self.userStatus isEqual: @"WH"]) {
    //        [self offine];
    //    }
    
    
}
-(IBAction)backToActionAddFriend:(UIStoryboardSegue *) segue{
    
    
}

-(IBAction)backToActionRequest:(UIStoryboardSegue *) segue{
    
    
    
}


-(IBAction)backToActionBegin:(UIStoryboardSegue *) segue{
    
    
    
    [self updatingUserLocation];
    [self.statusButton setAttributedTitle:[[NSAttributedString alloc]  initWithString:@"Exclusive" attributes:nil] forState:UIControlStateNormal];
    
    
    
    //    self.currentUser[@"status"]=@2;
    //    [self.currentUser saveInBackgroundWithBlock:^(BOOL nice, NSError * error){
    //        if (error) {
    //            NSLog(@"error in saving status 2");
    //
    //        }else{
    //            self.userStatus = @"GO";
    //            [self.statusButton setAttributedTitle:[[NSAttributedString alloc]  initWithString:@"Exclusive" attributes:nil] forState:UIControlStateNormal];
    //            [self.statusButton setEnabled:YES];
    //
    //            NSLog(@"qwer  %@", self.exclusiveHeadline);
    //
    //            if (self.exclusiveHeadline != nil || ![self.exclusiveHeadline  isEqual: @""]) {
    //
    //
    //            [PFCloud callFunctionInBackground:@"Headline" withParameters:@{@"headline":self.exclusiveHeadline} block:^(NSString * results, NSError * error) {
    //                if(!error) {
    //                    NSLog(@" the HEADLINE function went through!!!!!!!!!!!");
    //
    //                    [embededTabbar refreshAll];
    //
    //                }else{
    //                    NSLog(@"ERROR = %@ !@!@@!@! ERROR NUMBER: %ld",error, (long)error.code);
    //                }
    //            }];
    //            }else {
    //            [embededTabbar refreshAll];
    //
    //            }
    //
    //
    //        }
    //
    //
    //
    //    }];
    
    
    
    
    
    
    
    
    
    //    [self.updatingLocation invalidate];
    //    self.updatingLocation =nil;
    //
    //
    //    [self.statusButton setAttributedTitle:[[NSAttributedString alloc]  initWithString:@"Going Out" attributes:nil] forState:UIControlStateNormal];
    //    [self.statusButton setEnabled:YES];
    
    
    
}

-(IBAction)backToActionSet:(UIStoryboardSegue *) segue{
    
    
    
}

-(void)goingOut{
    
    
    //    [self.statusButton setAttributedTitle:[[NSAttributedString alloc]  initWithString:@"Exclusive" attributes:nil] forState:UIControlStateNormal];
    [self.statusButton setEnabled:YES];
    
    [self performSegueWithIdentifier:@"setGoingOut" sender:self];
    
}



-(void)offine{
    //self.currentUser[@"status"]=@0;
    //[self.currentUser saveInBackground];
    NSLog(@"LOGGING the user IN THE OFFLINE %@", self.currentUser);
    
    NSLog(@" checking to see if theres anything in the relation %lu", (unsigned long)self.allFriends.count);
    [[LocationManagerSingleton sharedSingleton].locationManager stopMonitoringVisits];
    [[LocationManagerSingleton sharedSingleton].locationManager stopUpdatingLocation];
    
    
    
    NSLog(@"the role was saved!!!");
    
    PFObject * log = [PFObject objectWithClassName:@"log"];
    if (self.sessionToken == nil) {
        [PFSession getCurrentSessionInBackgroundWithBlock:^(PFSession * session, NSError * error){
            if (!error) {
                self.sessionToken = session;
                log[@"status"] = @0;
                log[@"deviceInstallation"] = [PFInstallation currentInstallation];
                log[@"deviceSession"] = session;
                
                [log saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                    if (!error) {
                        [self.currentUser fetchInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
                            if (!error) {
                                [[LocationManagerSingleton sharedSingleton].locationManager startMonitoringVisits];
                                
                                [self.statusButton setAttributedTitle:[[NSAttributedString alloc]  initWithString:@"Offline" attributes:nil] forState:UIControlStateNormal];
                                [self.statusButton setEnabled:YES];
                                [embededTabbar refreshAll];
                            }else{
                                
                                
                                
                            }
                            
                        }];
                        
                    }else{
                        
                    }
                }];
            }else{
                
                
            }
            
        }];
    }else{
        
        log[@"status"] = @0;
        log[@"deviceInstallation"] = [PFInstallation currentInstallation];
        log[@"deviceSession"] = self.sessionToken;
        
        [log saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
            if (!error) {
                [self.currentUser fetchInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
                    if (!error) {
                        
                        [self.statusButton setAttributedTitle:[[NSAttributedString alloc]  initWithString:@"Offline" attributes:nil] forState:UIControlStateNormal];
                        [self.statusButton setEnabled:YES];
                        [embededTabbar refreshAll];
                    }else{
                        
                        
                        
                    }
                    
                }];
                
            }else{
                
            }
        }];
    }
    
    
    
    
    
    //    self.userStatus = @"WH";
    //
    //
    //
    //    [self.statusButton setAttributedTitle:[[NSAttributedString alloc]  initWithString:@"Offine" attributes:nil] forState:UIControlStateNormal];
    //    [self.statusButton setEnabled:YES];
    //
    //    [self.currentUser saveInBackgroundWithBlock:^(BOOL good, NSError * error){
    //        if(!error){
    //
    //            self.userStatus = @"WH";
    //
    //
    //            [self.statusButton setAttributedTitle:[[NSAttributedString alloc]  initWithString:@"Offine" attributes:nil] forState:UIControlStateNormal];
    //            [self.statusButton setEnabled:YES];
    //
    //            [embededTabbar refreshAll];
    //
    //        }else{
    //            NSLog(@"error on the offline button %ld", (long)error.code);
    //
    //        }
    //
    //    }];
    
}



-(void)partyMode{
    
    NSString * roleName = [NSString stringWithFormat:@"Role-%@", self.currentUser.objectId];
    
    
    NSLog(@"LOGGING the user IN THE PARTY MODE %@", self.currentUser);
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) {
        
        [[LocationManagerSingleton sharedSingleton].locationManager startMonitoringVisits];
        
        //// this block is to the new post system!!!
        
        //        PFObject * log = [PFObject objectWithClassName:@"log"];
        //    if (self.sessionToken == nil) {
        //        [PFSession getCurrentSessionInBackgroundWithBlock:^(PFSession * session, NSError * error){
        //            if (!error) {
        //                self.sessionToken = session;
        //                log[@"status"] = @1;
        //                log[@"deviceInstallation"] = [PFInstallation currentInstallation];
        //                log[@"deviceSession"] = session;
        //
        //                [log saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        //                    if (!error) {
        //
        //                    }else{
        //
        //                    }
        //                }];
        //            }else{
        //
        //
        //            }
        //
        //        }];
        //    }else{
        //
        //    log[@"status"] = @1;
        //    log[@"deviceInstallation"] = [PFInstallation currentInstallation];
        //    log[@"deviceSession"] = self.sessionToken;
        //
        //        [log saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        //            if (!error) {
        //
        //            }else{
        //
        //            }
        //        }];
        //    }
        
        
        
        
        
        //self.currentUser[@"status"]=@1;
        
        //NSString * roleName = [NSString stringWithFormat:@"Role-%@", self.currentUser.objectId];
        
        
        
        PFQuery * queryRole = [PFRole query];
        [queryRole whereKey:@"name" equalTo:roleName];
        [queryRole getFirstObjectInBackgroundWithBlock:^(PFObject * object, NSError * error){
            if (error || object== nil){
                NSLog(@"this error belongs to the role assigning block -- %@", error);
            }else {
                self.rolling = (PFRole *)object;
                
                
                
                NSLog(@"looking for the amount that the array has %lu", (unsigned long)self.allFriends.count);
                PFRelation * friends = self.rolling.users;
                
                for (int i=0; i < [self.allFriends count] ; i++) {
                    [friends addObject:self.allFriends[i]];
                    //[self.rolling.users addObject:self.allFriends[i]];
                    //NSLog(@"kasdjfashdfjashdfjhaslkjdfhalskjfh52364587263756238475623847658234765823658234658923465827645 %@", self.allFriends[i]);
                    
                    //i++;
                    
                    
                }
                
                [self.rolling saveInBackgroundWithBlock:^(BOOL status, NSError * error){
                    
                    if (!error) {
                        NSLog(@"the role was saved!!!");
                        NSLog(@"CEREAL CAT the relation ACTION VIEW!! %@", friends);
                        PFObject * log = [PFObject objectWithClassName:@"log"];
                        if (self.sessionToken == nil) {
                            [PFSession getCurrentSessionInBackgroundWithBlock:^(PFSession * session, NSError * error){
                                if (!error) {
                                    self.sessionToken = session;
                                    log[@"status"] = @1;
                                    log[@"deviceInstallation"] = [PFInstallation currentInstallation];
                                    log[@"deviceSession"] = session;
                                    
                                    [log saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                                        if (!error) {
                                            
                                            
                                            [self.currentUser fetchInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
                                                if (!error) {
                                                    [[LocationManagerSingleton sharedSingleton].locationManager startMonitoringVisits];
                                                    [self updatingUserLocation];
                                                    
                                                    [self.statusButton setAttributedTitle:[[NSAttributedString alloc]  initWithString:@"PartyMode" attributes:nil] forState:UIControlStateNormal];
                                                    [self.statusButton setEnabled:YES];
                                                    [embededTabbar refreshAll];
                                                }else{
                                                    
                                                    
                                                    
                                                }
                                                
                                            }];
                                            
                                        }else{
                                            
                                            if ([[error.userInfo objectForKey:@"error"] isEqualToString:@"Already in PartyMode!" ]) {
                                               
                                                [self updatingUserLocation];
                                                [embededTabbar refreshAll];
                                                
                                            }else{
                                            
                                            
                                            }
                                            
                                            
                                            
                                            
                                            
                                        }
                                    }];
                                }else{
                                    
                                    
                                }
                                
                            }];
                        }else{
                            
                            log[@"status"] = @1;
                            log[@"deviceInstallation"] = [PFInstallation currentInstallation];
                            log[@"deviceSession"] = self.sessionToken;
                            
                            [log saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                                if (!error) {
                                    [self.currentUser fetchInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
                                        if (!error) {
                                            
                                            [self.statusButton setAttributedTitle:[[NSAttributedString alloc]  initWithString:@"PartyMode" attributes:nil] forState:UIControlStateNormal];
                                            [self.statusButton setEnabled:YES];
                                            [embededTabbar refreshAll];
                                        }else{
                                            
                                            if ([[error.userInfo objectForKey:@"error"] isEqualToString:@"Already in PartyMode!" ]) {
                                                
                                                [self updatingUserLocation];
                                                [embededTabbar refreshAll];
                                                
                                            }else{
                                                
                                                
                                            }
                                            
                                            
                                            
                                        }
                                        
                                    }];
                                    
                                }else{
                                    
                                }
                            }];
                        }
                        
                        
                    }else{
                        NSLog(@"this error is in the role save %@", error);
                    }
                    
                    
                }];
                
            }
            
            
        }];
        
    } else{
        
        [self deniedPermissionView];
        
    }
    
    /* PFQuery * queryRole = [PFRole query];
     
     
     
     
     [queryRole whereKey:@"name" equalTo:roleName];
     [queryRole findObjectsInBackgroundWithBlock:^(NSArray * objects, NSError * error){
     if (error || objects.count == 0 ) {
     NSLog(@"this error belongs to the role assigning block -- %@", error);
     }else if (objects.count > 0){
     
     self.rolling = objects[0];
     
     
     
     }
     
     
     
     }];     */
    // [queryRole wh]
    
    //[queryRole whereKey: equalTo:]
    
    /*for (int i = 0; i < [self.allFriends count] ; i++ ) {
     [self.rolling addObjectsFromArray:self.allFriends];
     } */
    
    //[self.rolling addObjectsFromArray:self.allFriends forKey:user];
    
    
    
    
    /* [self.statusButton setAttributedTitle:[[NSAttributedString alloc]  initWithString:@"Party Mode" attributes:nil] forState:UIControlStateNormal];
     [self.statusButton setEnabled:YES];
     */
    
    
    
    
}

-(IBAction)zoom:(id)sender{
    
    NSLog(@"zoom button pressed");
    
    [embededTabbar toZoom];
    
    
    
    
    //    if (cood.longitude == 0 && cood.latitude == 0){
    //
    //        MKCoordinateRegion region;
    //        region.center.latitude = me_HughesLat;
    //        region.center.longitude = me_HughesLong;
    //        region.span = MKCoordinateSpanMake(spanX, spanY);
    //        [self.mapView setRegion:region animated:YES];
    //    }else{
    //
    //        MKCoordinateRegion region;
    //        region.center.latitude = cood.latitude;
    //        region.center.longitude = cood.longitude;
    //        region.span = MKCoordinateSpanMake(spanX, spanY);
    //        [self.mapView setRegion:region animated:YES];
    //
    //    }
    
    
    /*[PFCloud callFunctionInBackground:@"Add"
     withParameters:@{@"parameterKey": @"parameterValue"}
     block:^(NSArray *results, NSError *error) {
     if (!error) {
     // this is where you handle the results and change the UI.
     NSLog(@"Add function Cloud");
     }
     else {
     NSLog(@"Add function error from Cloud");
     }
     }];
     
     [PFCloud callFunctionInBackground:@"Delete"
     withParameters:@{@"parameterKey": @"parameterValue"}
     block:^(NSArray *results, NSError *error) {
     if (!error) {
     // this is where you handle the results and change the UI.
     NSLog(@"Delete function from cloud");
     
     }
     else {
     NSLog(@"Delete function error");
     }
     }];*/
    
    
    
    
    //
    //
    //    PFQuery * freinds = [PFQuery queryWithClassName:@"friendrequest"];
    //
    //    [freinds whereKey:@"user1" equalTo:self.currentUser];
    //    [freinds whereKey:@"status" equalTo:@1];
    //    [freinds includeKey:@"user2"];
    
    //    [freinds findObjectsInBackgroundWithBlock:^(NSArray * objects, NSError * error){
    //
    //
    //        if (error) {
    //        }else if([objects count] > 0){
    //
    //
    //
    //            for (int i = 0; [objects count] > i; i++) {
    //
    //
    //
    //
    //                PFObject * request = objects[i];
    //                PFUser *user = [request objectForKey:@"user2"];
    //                NSLog(@"friend1%@",user);
    //                [self.allFriendsRelation addObject:user];
    //
    //
    //            }
    //            NSLog(@"relations   %@",self.allFriendsRelation);
    //
    //            [self.currentUser saveInBackgroundWithBlock:^(BOOL suc, NSError * error){
    //
    //                if(error){
    //
    //
    //
    //                }else{
    //
    //
    //                    for (int m = 0; [objects count] >m ; m++) {
    //                        PFObject * object = objects[m];
    //                        object[@"status"] = @3;
    //                        NSLog(@"friend2%@",object);
    //
    //                        [object saveInBackground];
    //
    //                    }}
    //
    //
    //            }
    //
    //
    //             ];
    //
    //        }
    //
    //
    //
    //    }];
    
    
    
    
}


- (IBAction)statusButton:(id)sender {
    
    
    UIAlertController * actionSheet = [UIAlertController alertControllerWithTitle:@"PartyFriends" message:@"Select Mode" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction * offlineMode = [UIAlertAction actionWithTitle:@"Offline" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self offine];
    }];
    
    UIAlertAction * partyMode = [UIAlertAction actionWithTitle:@"PartyMode" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self partyMode];
    }];
    
    UIAlertAction * goingoutMode = [UIAlertAction actionWithTitle:@"Exclusive" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self goingOut];
    }];
    
    UIAlertAction * cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [actionSheet dismissViewControllerAnimated:YES completion:nil];
    }];
    
    // [[[actionSheet valueForKey:@"_buttons"] objectAtIndex:0] setImage:[UIImage imageNamed:@"whiteBallon"] forState:UIControlStateNormal];
    
    [actionSheet addAction:offlineMode];
    [actionSheet addAction:partyMode];
    [actionSheet addAction:goingoutMode];
    
    [actionSheet addAction:cancel];
    [offlineMode setValue:[[UIImage imageNamed:@"whiteBallon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
    [partyMode setValue:[[UIImage imageNamed:@"RedBallon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
    [goingoutMode setValue:[[UIImage imageNamed:@"BlueBallon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
    //        UIButton * button = (UIButton *) sender;
    //button.tag = button.tag+1;
    //
    //        NSLog(@"%lu",(unsigned long) button.tag);
    //
    //        if (button.tag % 2 ==0) {
    //            [self.view sendSubviewToBack:self.statusView];
    //
    //        }
    //        if (button.tag % 2 == 1) {
    //            [self.view bringSubviewToFront:self.statusView];
    //
    //        }
    
    
}


- (void) zoomer {
    
    [embededTabbar toZoom];
    
}

-(void)byeKeyboard{
    [self tapBackgound:self];
    NSLog(@"lk");
    
}


- (IBAction)tapBackgound:(id)sender {
    
    NSLog(@"checking the tapping in the bakground");
    [self.view endEditing:YES];
    
}


//setting the textField to limit of characters

#define MAXLength 50

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= MAXLength || returnKey;
}

-(void)checkingTimer{
    
    
    
}

-(void) creatParty{
    
    
    
}




-(void)updatingUserLocation{
    
    self.currentUser =[PFUser currentUser];
    //[self.currentUser fetch];
    
    
    self.userLocation = [LocationManagerSingleton sharedSingleton].locationManager.location;
    
    PFGeoPoint * currentLocation  = [PFGeoPoint geoPointWithLocation:self.userLocation];
    NSString * roleName = [NSString stringWithFormat:@"Role-%@", self.currentUser.objectId];
    // PFObject  *lat = [currentLocation latitude];
    // PFObject * Long = [LocationManagerSingleton sharedSingleton].locationManager.location.coordinate.longitude;
    
    NSNumber * Long = [NSNumber numberWithDouble:[LocationManagerSingleton sharedSingleton].locationManager.location.coordinate.longitude];
    NSNumber * lat = [NSNumber numberWithDouble:[LocationManagerSingleton sharedSingleton].locationManager.location.coordinate.latitude];
    CLLocation * location = [[CLLocation alloc] initWithLatitude:self.userLocation.coordinate.latitude longitude:self.userLocation.coordinate.longitude];
    int batteryLevel = [[UIDevice currentDevice] batteryLevel] * 100;
    NSNumber * battery = [NSNumber numberWithInt:batteryLevel];
    
    
    NSMutableDictionary * params = [NSMutableDictionary new];
    params[@"headline"] = @"";
    params[@"lat"] = lat;
    params[@"long"] = Long;
    params[@"batteryPercentage"] = battery;
    
    
//    CLGeocoder * waffles = [CLGeocoder new];
//    [waffles reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
//        if (error) {
//            NSLog(@"Error %@", error.description);
//            NSLog(@"syrup!!! error %@", error);
//            [PFCloud callFunctionInBackground:@"CreateParty" withParameters:params block:^(NSString * results, NSError * error){
//                
//                if (!error) {
//                    NSLog(@"the results ARE %@: ", results);
//                }else{
//                    NSLog(@"create  ERROR: %@", error);
//                    NSLog(@"theres an erRor %@", error);
//                }
//                
//            }];
//            
//        } else {
//            CLPlacemark * placemark = [placemarks lastObject];
//            
//            NSString * applePlacemark = placemark.areasOfInterest[0];
//            if (applePlacemark != nil) {
//                params[@"applePlaceMark"] = applePlacemark;
//            }else{
//                params[@"applePlaceMark"] = placemark.name;
//            }
//            
//            
//            
//          
//            
//            
//            
//            
//        }
//    }];
    
    
                [_placesClient currentPlaceWithCallback:^(GMSPlaceLikelihoodList *likelihoodList, NSError *error) {
                    if (error != nil) {
                        NSLog(@"Current Place error %@", [error localizedDescription]);
                        [PFCloud callFunctionInBackground:@"CreateParty" withParameters:params block:^(NSString * results, NSError * error){
                            
                            if (!error) {
                                NSLog(@"the results ARE %@: ", results);
                            }else{
                                NSLog(@"create  ERROR: %@", error);
                                NSLog(@"theres an erRor %@", error);
                            }
                            
                        }];
                        
                        
                        
                        return;
                    }
                   
                    
                    for (GMSPlaceLikelihood *likelihood in likelihoodList.likelihoods) {
                        
                        if (likelihood.likelihood >= .70){
                            GMSPlace * myPlace = likelihood.place;
                            
                            params[@"googlePlacesName"] = myPlace.name;
                            params[@"googlePlacesID"] = myPlace.placeID;
                            params[@"address"] = myPlace.addressComponents;
                            params[@"googlePlaces"] = @YES;
                            
                            [PFCloud callFunctionInBackground:@"CreateParty" withParameters:params block:^(NSString * results, NSError * error){
                                
                                if (!error) {
                                   
                                    
                                }else{
                                    NSLog(@"create  ERROR: %@", error);
                                    
                                    UILocalNotification *o= [[UILocalNotification alloc] init];
                                    o.alertBody = @"CHECK! AGAIN!";
                                    o.soundName = UILocalNotificationDefaultSoundName;
                                    o.fireDate = [NSDate date];
                                    //  [[UIApplication sharedApplication] scheduleLocalNotification:o];
                                    NSLog(@"create  ERROR: %@", error);
                                    
                                    
                                }
                                
                            }];
                            break;
                        }
                        GMSPlace* place = likelihood.place;
                        NSLog(@"Current Place name %@ at likelihood %g", place.name, likelihood.likelihood);
                        NSLog(@"Current Place address %@", place.formattedAddress);
                        NSLog(@"Current Place attributions %@", place.attributions);
                        NSLog(@"Current PlaceID %@", place.placeID);
                    
                        if ([params objectForKey:@"googlePlacesName"] == nil) {
                            CLGeocoder * waffles = [CLGeocoder new];
                            [waffles reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
                                if (error) {
                                    NSLog(@"Error %@", error.description);
                                    NSLog(@"syrup!!! error %@", error);
                                    [PFCloud callFunctionInBackground:@"CreateParty" withParameters:params block:^(NSString * results, NSError * error){
                                        
                                        if (!error) {
                                            NSLog(@"the results ARE %@: ", results);
                                        }else{
                                            NSLog(@"create  ERROR: %@", error);
                                            NSLog(@"theres an erRor %@", error);
                                        }
                                        
                                    }];
                                    
                                } else {
                                    CLPlacemark * placemark = [placemarks lastObject];
                                    
                                    NSString * applePlacemark = placemark.areasOfInterest[0];
                                    if (applePlacemark != nil) {
                                        params[@"applePlaceMark"] = applePlacemark;
                                    
                                    }else{
                                        params[@"applePlaceMark"] = placemark.name;
                                    }
        
                                    [PFCloud callFunctionInBackground:@"CreateParty" withParameters:params block:^(NSString * results, NSError * error){
                                        
                                        if (!error) {
                                            NSLog(@"the results ARE %@: ", results);
                                        }else{
                                            NSLog(@"create  ERROR: %@", error);
                                            NSLog(@"theres an erRor %@", error);
                                        }
                                        
                                    }];

                                    
                                    
                                    
                                }
                            }];

                            
                        }

                        
                        
                    }
                    
                }];
    
    
//    CLGeocoder * waffles = [CLGeocoder new];
//    [waffles reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
//        if (error) {
//            NSLog(@"Error %@", error.description);
//            NSLog(@"syrup!!! error %@", error);
//            [PFCloud callFunctionInBackground:@"CreateParty" withParameters:params block:^(NSString * results, NSError * error){
//                
//                if (!error) {
//                    NSLog(@"the results ARE %@: ", results);
//                }else{
//                    NSLog(@"create  ERROR: %@", error);
//                    NSLog(@"theres an erRor %@", error);
//                }
//                
//            }];
//            
//        } else {
//            CLPlacemark * placemark = [placemarks lastObject];
//            
//            NSString * applePlacemark = placemark.areasOfInterest[0];
//            if (applePlacemark != nil) {
//                params[@"applePlaceMark"] = applePlacemark;
//            }else{
//                params[@"applePlaceMark"] = placemark.name;
//            }
//            
//            
//            
//            NSLog(@"syrup!!! %@ and %@", placemark.name, params[@"applePlaceMark"]);
//            [PFCloud callFunctionInBackground:@"CreateParty" withParameters:params block:^(NSString * results, NSError * error){
//                
//                if (!error) {
//                    NSLog(@"the results ARE %@: ", results);
//                }else{
//                    NSLog(@"create  ERROR: %@", error);
//                    
//                }
//                
//            }];
//            
//            
//        }
//    }];
    
    
    
    
    
//    
//    [PFCloud callFunctionInBackground:@"CreateParty" withParameters:params block:^(NSString * results, NSError * error){
//        
//        if (!error) {
//            NSLog(@"the results ARE %@: ", results);
//        }else{
//            NSLog(@"create  ERROR: %@", error);
//            
//        }
//        
//    }];

    
    /* PFQuery *query = [PFRole query];
     [query whereKey:@"name" equalTo:roleName];
     [query findObjectsInBackgroundWithBlock:^(NSArray * objects, NSError * error){
     if (error.code == 101 || objects.count==0) {
     
     
     PFACL *acl = [PFACL ACL];
     [acl setPublicReadAccess:false];
     [acl setPublicWriteAccess:false];
     [acl setReadAccess:true forUser:[PFUser currentUser]];
     [acl setWriteAccess:true forUser:[PFUser currentUser]];
     
     PFRole *role = [PFRole roleWithName:roleName];
     [role setACL:acl];
     [role saveInBackgroundWithBlock:^(BOOL succeeded, NSError * error){
     
     if (!error) {
     PFQuery *query = [PFRole query];
     [query whereKey:@"name" equalTo:roleName];
     [query findObjectsInBackgroundWithBlock:^(NSArray * objects, NSError * error){
     NSLog(@"double checking");
     if (error) {
     NSLog(@"double checking");
     }else if(objects.count>0){
     NSLog(@"double checking2");
     
     
     self.rolling = objects[0];
     PFACL *myAcl = [PFACL ACL];
     [myAcl setWriteAccess:NO forRole:self.rolling];
     [myAcl setReadAccess:YES forRole:self.rolling];
     [myAcl setPublicReadAccess:NO];
     [myAcl setPublicReadAccess:NO];
     [myAcl setReadAccess:YES forUser:self.currentUser];
     [myAcl setWriteAccess:YES forUser:self.currentUser];
     PFRelation * relation=[PFRelation new];
     relation= self.rolling.users;
     
     
     
     PFObject * party = [PFObject objectWithClassName:@"party"];
     [party setObject:self.currentUser  forKey:@"createdBy"];
     [party setObject:currentLocation forKey:@"location"];
     party.ACL = myAcl;
     [party saveInBackgroundWithBlock:^(BOOL suedfbj, NSError * error) {
     if (error){
     
     NSLog(@"45%@", error);
     
     }
     else{
     PFQuery * myParty = [PFQuery queryWithClassName:@"party"];
     [myParty whereKey:@"createdBy" equalTo:[PFUser currentUser]];
     [myParty orderByDescending:@"createdAt"];
     [myParty setLimit:100];
     
     [myParty findObjectsInBackgroundWithBlock:^(NSArray * objects, NSError * error){if(!error){
     
     PFObject * party = objects[0];
     NSLog(@"%@",party);
     [self.currentUser setObject:party forKey:@"lastParty"];
     [self.currentUser saveInBackgroundWithBlock:^(BOOL succeed, NSError * error){
     if(succeed){
     NSLog(@"Success");
     }
     else{
     NSLog(@"123%@",error);
     }
     }];
     
     
     
     }
     
     
     
     }];
     
     
     }
     
     } ];
     
     }else{
     
     }
     
     
     
     
     
     
     
     
     }];
     
     
     
     
     
     
     }
     
     }];
     }else{
     self.rolling = objects[0];
     PFACL *myAcl = [PFACL ACL];
     [myAcl setWriteAccess:NO forRole:self.rolling];
     [myAcl setReadAccess:YES forRole:self.rolling];
     [myAcl setPublicReadAccess:NO];
     [myAcl setPublicReadAccess:NO];
     [myAcl setReadAccess:YES forUser:self.currentUser];
     [myAcl setWriteAccess:YES forUser:self.currentUser];
     
     
     
     PFObject * party = [PFObject objectWithClassName:@"party"];
     [party setObject:self.currentUser  forKey:@"createdBy"];
     [party setObject:currentLocation forKey:@"location"];
     party.ACL = myAcl;
     [party saveInBackgroundWithBlock:^(BOOL suedfbj, NSError * error) {
     if (error){
     
     NSLog(@"45%@", error);
     
     }
     else{
     PFQuery * myParty = [PFQuery queryWithClassName:@"party"];
     [myParty whereKey:@"createdBy" equalTo:[PFUser currentUser]];
     [myParty orderByDescending:@"createdAt"];
     [myParty setLimit:100];
     
     [myParty findObjectsInBackgroundWithBlock:^(NSArray * objects, NSError * error){if(!error){
     
     PFObject * party = objects[0];
     NSLog(@"%@",party);
     [self.currentUser setObject:party forKey:@"lastParty"];
     [self.currentUser saveInBackgroundWithBlock:^(BOOL succeed, NSError * error){
     if(succeed){
     NSLog(@"Success");
     }
     else{
     NSLog(@"123%@",error);
     }
     }];
     
     
     
     }
     
     
     
     }];
     
     
     }
     
     } ];        }
     
     
     
     
     
     }];
     
     
     
     */
    
    
    
}

-(void)refreshAll{
    
    //    NSDictionary * dictionary = [NSDictionary new];
    //
    //    [PFCloud callFunctionInBackground:@"Refresh" withParameters:dictionary block:^(NSArray * objects, NSError * error) {
    //        if (error){
    //
    //            NSLog(@"theres an error %@", error);
    //
    //        }else{
    //            self.refreshArray = objects;
    //           // NSLog(@"returning the objects - %@", objects);
    //            embededTabbar.refreshArray = [NSMutableArray arrayWithArray:self.refreshArray];
    //
    //            [embededTabbar refreshBothTabs];
    //
    //        }
    //
    //    }];
    
    
}

-(void)toGoingOut {
    [self performSegueWithIdentifier:@"setGoingOut" sender:self];
}

-(void)toCommentsView{
    NSLog(@"the comment object should be showing here! %@", self.commentObject);
    [self performSegueWithIdentifier:@"toComments" sender:self];
}

- (void)toCommentHistoryView
{
    
    
    [self performSegueWithIdentifier:@"history" sender:self];
}

-(void) toPrivateParty{
    
    [self performSegueWithIdentifier:@"party" sender:self];
    
}

- (void)keyboardDidShow: (NSNotification *) notif{
    // Do something here
    NSLog(@"the keyboard did show!");
    self.tapGesture.cancelsTouchesInView = YES;
    self.tapGesture.enabled = YES;
}

- (void)keyboardDidHide: (NSNotification *) notif{
    // Do something here
    NSLog(@"the keyboard did hide");
    self.tapGesture.cancelsTouchesInView  = NO;
    self.tapGesture.enabled = NO;
    
}

-(void)backgroundCheck{
    NSLog(@"\n \n \n \n \n chekcing the state of the APP %ld",(long)[[UIApplication sharedApplication] applicationState]);
    NSLog(@"\n %f \n \n \n \n \n the time left on the background thead",  [[UIApplication sharedApplication] backgroundTimeRemaining]);
    
    
}

-(void)clearedUpdate{
    UIImage * PIcon =[[UIImage imageNamed:@"NewGoldIcon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    [self.PFButton setImage:PIcon forState:UIControlStateNormal];
    
}

-(void)updateCheck{
    UIImage * redIcon =[[UIImage imageNamed:@"NewRed"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    [self.PFButton setImage:redIcon forState:UIControlStateNormal];
    
}

-(void) deniedPermissionView{
    
    //    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied){
    //        NSLog(@"location services are blocked by the user");
    //        UIAlertController * locationDenied = [UIAlertController alertControllerWithTitle:@"Background Location Access Denied" message:@"Background location updates are needed to share your location with friends only if you are in Party or Exclusive" preferredStyle:UIAlertControllerStyleAlert];
    //
    //        UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    //        [locationDenied addAction:cancelAction];
    //
    //
    //        UIAlertAction * settingAction = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
    //            NSLog(@"asdhfgjk");
    //            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    //
    //
    //        }];;
    //        [locationDenied addAction:settingAction];
    //        [locationDenied addAction:cancelAction];
    
    
    
}

- (IBAction)toSettingsAction:(id)sender {
    
    
    
}

- (UIImage *)squareImageFromImage:(UIImage *)image scaledToSize:(CGFloat)newSize {
    CGAffineTransform scaleTransform;
    CGPoint origin;
    
    if (image.size.width > image.size.height) {
        CGFloat scaleRatio = newSize / image.size.height;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(-(image.size.width - image.size.height) / 2.0f, 0);
    } else {
        CGFloat scaleRatio = newSize / image.size.width;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(0, -(image.size.height - image.size.width) / 2.0f);
    }
    
    CGSize size = CGSizeMake(newSize, newSize);
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(size, YES, 0);
    } else {
        UIGraphicsBeginImageContext(size);
    }
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextConcatCTM(context, scaleTransform);
    
    [image drawAtPoint:origin];
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}

-(BOOL)shouldAutorotate
{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
    //return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}









@end
