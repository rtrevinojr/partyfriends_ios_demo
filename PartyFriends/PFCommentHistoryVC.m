//
//  PFCommentHistoryVC.m
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 1/14/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import "PFCommentHistoryVC.h"

#import "PFCommentsTableViewCell.h"
#import "PFAddCommentViewController.h"
#import "Gradient.h"
#import "UIView+Toast.h"
#import <ParseUI/ParseUI.h>
#import "NSDate+NVTimeAgo.h"

@implementation PFCommentHistoryVC


- (IBAction)reload:(id)sender {
    
    self.refreshControl = [[UIRefreshControl alloc]init];
    [self.tableView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    CGPoint scrollPoint = CGPointMake(0, textView.frame.origin.y+220
                                      );
    [self.scrollView setContentOffset:scrollPoint animated:YES];
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    
    [self.scrollView setContentOffset:CGPointZero animated:YES];
}

-(void) refreshComments{
    
    if(self.postObejct != nil){
    PFRelation * comments = [[self postObejct] objectForKey:@"comments"];
    
    
    PFQuery * query = [comments query];
    [query includeKey:@"createdBy"];
    [query orderByAscending:@"createdAt"];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if (!error) {
            
        
        self.commentList = [NSMutableArray arrayWithArray:objects];
            [self.tableView reloadData];
        }else if (objects.count == 0) {
            [self.commentList addObject:@"No Comments"];
            [self.tableView reloadData];
        }else{
            [self.view makeToast:@"Unable to load page" duration:3 position:CSToastPositionCenter];
        
        }
        
    }];
        
    }else{
        PFObject * post = [self.otherUser objectForKey:@"lastPost"];
        
        [post fetchInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
            if (!error) {
                self.postObejct = object;
                
                PFRelation * comments = [[self postObejct] objectForKey:@"comments"];
                
                
                PFQuery * query = [comments query];
                [query includeKey:@"createdBy"];
                [query orderByAscending:@"createdAt"];
                
                [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
                    if (!error) {
                        
                        
                        self.commentList = [NSMutableArray arrayWithArray:objects];
                        [self.tableView reloadData];
                    }else if (objects.count == 0) {
                        [self.commentList addObject:@"No Comments"];
                        [self.tableView reloadData];
                    }else{
                        [self.view makeToast:@"Unable to load page" duration:3 position:CSToastPositionCenter];
                        
                    }
                    
                }];
                
                
                
            }else{
            
            
            }
        }];
        
    
    }
    
    [self.tableView reloadData];
}

- (void)refreshTable {
    //TODO: refresh your data
    
    NSLog(@"refreshTable.........................");
    
    [self.refreshControl endRefreshing];
    
//    [self requeryTable];
    //[self fetchHeadline];
    [self refreshComments];
    [self.tableView reloadData];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.tableView reloadData];
}



- (void) fetchHeadline
{
    
    
    
    self.headlineList = [NSMutableArray new];
    self.allCommentsList = [NSMutableArray new];

    
    self.head0 = [NSMutableArray new];
    self.head1 = [NSMutableArray new];
    self.head2 = [NSMutableArray new];
    self.head3 = [NSMutableArray new];
    self.head4 = [NSMutableArray new];
    self.head5 = [NSMutableArray new];
    self.head6 = [NSMutableArray new];
    self.head7 = [NSMutableArray new];
    self.head8 = [NSMutableArray new];
    self.head9 = [NSMutableArray new];
    
//    PFUser * thisUser = [self.pfParty objectForKey:@"createdBy"];
//    PFQuery * pushUserQuery = [PFUser query];
//    [pushUserQuery whereKey:@"objectID" equalTo:self.userPush];
//    [pushUserQuery getFirstObjectInBackgroundWithBlock:^(PFObject * user, NSError* error){
//        if (!error) {
//            
//        }else{
//        
//        }
//    
//    }];
    if (self.partyUser == nil) {
                    PFQuery * pushUserQuery = [PFUser query];
                    [pushUserQuery whereKey:@"objectId" equalTo:self.userPush];
                    [pushUserQuery getFirstObjectInBackgroundWithBlock:^(PFObject * user, NSError* error){
                        if (!error) {
                            self.partyUser = (PFUser *)user;

        

        //
        //        
        //        self.partyUser = self.pushUser;
        //        self.userPush = nil;
        
        
            PFUser * thisUser = self.partyUser;
       PFRelation * headlineRel = [thisUser objectForKey:@"recentParties"];
        
        PFQuery *query1 = [headlineRel query];
        [query1 orderByDescending:@"createdAt"];
        [query1 findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
            if (!error) {
                self.headlineList = objects;
                
                NSLog(@"............................ Headline List Count = %lu", self.headlineList.count);
                
                PFQuery * query2 =  [PFQuery queryWithClassName:@"comment"];
                [query2 whereKey:@"party" containedIn:self.headlineList];
                [query2 includeKey:@"party"];
                [query2 orderByDescending:@"createdAt"];
                [query2 findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
                    
                    if (!error) {
                        self.allCommentsList = objects;
                        
                        NSLog(@"................................ HEADLINE LIST COUNT = %lu", self.headlineList.count);
                        NSLog(@"................................ All Comments List = %lu", self.allCommentsList.count);
                        
                        for (int i = 0; i < self.headlineList.count; i++) {
                            
                            PFObject * thisHeadline = [self.headlineList objectAtIndex:i];
                            
                            for (int j = 0; j < self.allCommentsList.count; j++) {
                                
                                PFObject * thisComment = [self.allCommentsList objectAtIndex:j];
                                
                                NSLog(@"This Headline. objId = %@", [thisHeadline objectId]);
                                NSLog(@"This Comment. objId = %@", [thisComment objectId]);
                                
                                PFObject * party = [thisComment objectForKey:@"party"];
                                
                                if ([[thisHeadline objectId] isEqualToString:[party objectId]]) {
                                    
                                    NSLog(@"...EQUAL OBJECT IDS");
                                    
                                    if (i == 0) {
                                        [self.head0 addObject:thisComment];
                                        NSLog(@"... ADD COMMENT OBJECT = %@ AT SECTION = %lu", thisComment, i );
                                    }
                                    else if (i == 1) {
                                        [self.head1 addObject:thisComment];
                                        NSLog(@"... ADD COMMENT OBJECT = %@ AT SECTION = %lu", thisComment, i );
                                    }
                                    else if (i == 2) {
                                        [self.head2 addObject:thisComment];
                                        NSLog(@"... ADD COMMENT OBJECT = %@ AT SECTION = %lu", thisComment, i );
                                    }
                                    else if (i == 3) {
                                        [self.head3 addObject:thisComment];
                                        NSLog(@"... ADD COMMENT OBJECT = %@ AT SECTION = %lu", thisComment, i );
                                    }
                                    else if (i == 4) {
                                        [self.head4 addObject:thisComment];
                                        NSLog(@"... ADD COMMENT OBJECT = %@ AT SECTION = %lu", thisComment, i );
                                    }
                                    else if (i == 5) {
                                        [self.head5 addObject:thisComment];
                                        NSLog(@"... ADD COMMENT OBJECT = %@ AT SECTION = %lu", thisComment, i );
                                    }
                                    else if (i == 6) {
                                        [self.head6 addObject:thisComment];
                                        NSLog(@"... ADD COMMENT OBJECT = %@ AT SECTION = %lu", thisComment, i );
                                    }
                                    else if (i == 7) {
                                        [self.head7 addObject:thisComment];
                                        NSLog(@"... ADD COMMENT OBJECT = %@ AT SECTION = %lu", thisComment, i );
                                    }
                                    else if (i == 8) {
                                        [self.head8 addObject:thisComment];
                                        NSLog(@"... ADD COMMENT OBJECT = %@ AT SECTION = %lu", thisComment, i );
                                    }
                                    else if (i == 9) {
                                        [self.head9 addObject:thisComment];
                                        NSLog(@"... ADD COMMENT OBJECT = %@ AT SECTION = %lu", thisComment, i );
                                    }
                                    
                                }
                            }
                            
                        }
                        
                        
                        
                        NSLog(@"headline 0 = %lu", self.head0.count);
                        NSLog(@"headline 1 = %lu", self.head1.count);
                        NSLog(@"headline 2 = %lu", self.head2.count);
                        NSLog(@"headline 3 = %lu", self.head3.count);
                        NSLog(@"headline 4 = %lu", self.head4.count);
                        NSLog(@"headline 5 = %lu", self.head5.count);
                        NSLog(@"headline 6 = %lu", self.head6.count);
                        NSLog(@"headline 7 = %lu", self.head7.count);
                        NSLog(@"headline 8 = %lu", self.head8.count);
                        NSLog(@"headline 9 = %lu", self.head9.count);
                        
                        NSLog(@"headline 0 = %@", self.head0);
                        NSLog(@"headline 1 = %@", self.head1);
                        NSLog(@"headline 2 = %@", self.head2);
                        NSLog(@"headline 3 = %@", self.head3);
                        NSLog(@"headline 4 = %@", self.head4);
                        NSLog(@"headline 5 = %@", self.head5);
                        NSLog(@"headline 6 = %@", self.head6);
                        NSLog(@"headline 7 = %@", self.head7);
                        NSLog(@"headline 8 = %@", self.head8);
                        NSLog(@"headline 9 = %@", self.head9);
                    }
                    else {
                        NSLog(@"Query2 error = %@",error);
                        
                        
                    }
                    
                    
                    [self.tableView reloadData];
                    
                    NSLog(@"headline 0 = %lu", self.head0.count);
                    NSLog(@"headline 1 = %lu", self.head1.count);
                    NSLog(@"headline 2 = %lu", self.head2.count);
                    NSLog(@"headline 3 = %lu", self.head3.count);
                    NSLog(@"headline 4 = %lu", self.head4.count);
                    NSLog(@"headline 5 = %lu", self.head5.count);
                    NSLog(@"headline 6 = %lu", self.head6.count);
                    NSLog(@"headline 7 = %lu", self.head7.count);
                    NSLog(@"headline 8 = %lu", self.head8.count);
                    NSLog(@"headline 9 = %lu", self.head9.count);
                    [self.tableView reloadData];
                    
                }
                 
                 ];
                
                [self.tableView reloadData];
                
                NSLog(@"headline 0 = %@", self.head0);
                NSLog(@"headline 1 = %@", self.head1);
                NSLog(@"headline 2 = %@", self.head2);
                NSLog(@"headline 3 = %@", self.head3);
                NSLog(@"headline 4 = %@", self.head4);
                NSLog(@"headline 5 = %@", self.head5);
                NSLog(@"headline 6 = %@", self.head6);
                NSLog(@"headline 7 = %@", self.head7);
                NSLog(@"headline 8 = %@", self.head8);
                NSLog(@"headline 9 = %@", self.head9);
                
                
            }
        }];
        
        
        
        
                        }else{
                            NSLog(@"JOTO = %@", self.userPush);
        
                        }
        
                    }];
        
        
        
        
        
        
        
        
        
        
        
        
    }
    else {
    PFUser * thisUser = self.partyUser;
    
    
    PFRelation * headlineRel = [thisUser objectForKey:@"recentParties"];
    
    PFQuery *query1 = [headlineRel query];
    [query1 orderByDescending:@"createdAt"];
    [query1 findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if (!error) {
            self.headlineList = objects;
            
            NSLog(@"............................ Headline List Count = %lu", self.headlineList.count);
            
            PFQuery * query2 =  [PFQuery queryWithClassName:@"comment"];
            [query2 whereKey:@"party" containedIn:self.headlineList];
            [query2 includeKey:@"party"];
            [query2 orderByDescending:@"createdAt"];
            [query2 findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
                
                if (!error) {
                    self.allCommentsList = objects;
                    
                    NSLog(@"................................ HEADLINE LIST COUNT = %lu", self.headlineList.count);
                    NSLog(@"................................ All Comments List = %lu", self.allCommentsList.count);
                    
                    for (int i = 0; i < self.headlineList.count; i++) {
                        
                        PFObject * thisHeadline = [self.headlineList objectAtIndex:i];
                        
                        for (int j = 0; j < self.allCommentsList.count; j++) {
                            
                            PFObject * thisComment = [self.allCommentsList objectAtIndex:j];
                            
                            NSLog(@"This Headline. objId = %@", [thisHeadline objectId]);
                            NSLog(@"This Comment. objId = %@", [thisComment objectId]);
                            
                            PFObject * party = [thisComment objectForKey:@"party"];
                            
                            if ([[thisHeadline objectId] isEqualToString:[party objectId]]) {
                                
                                NSLog(@"...EQUAL OBJECT IDS");
                     
                                if (i == 0) {
                                    [self.head0 addObject:thisComment];
                                    NSLog(@"... ADD COMMENT OBJECT = %@ AT SECTION = %lu", thisComment, i );
                                }
                                else if (i == 1) {
                                    [self.head1 addObject:thisComment];
                                    NSLog(@"... ADD COMMENT OBJECT = %@ AT SECTION = %lu", thisComment, i );
                                }
                                else if (i == 2) {
                                    [self.head2 addObject:thisComment];
                                    NSLog(@"... ADD COMMENT OBJECT = %@ AT SECTION = %lu", thisComment, i );
                                }
                                else if (i == 3) {
                                    [self.head3 addObject:thisComment];
                                    NSLog(@"... ADD COMMENT OBJECT = %@ AT SECTION = %lu", thisComment, i );
                                }
                                else if (i == 4) {
                                    [self.head4 addObject:thisComment];
                                    NSLog(@"... ADD COMMENT OBJECT = %@ AT SECTION = %lu", thisComment, i );
                                }
                                else if (i == 5) {
                                    [self.head5 addObject:thisComment];
                                    NSLog(@"... ADD COMMENT OBJECT = %@ AT SECTION = %lu", thisComment, i );
                                }
                                else if (i == 6) {
                                    [self.head6 addObject:thisComment];
                                    NSLog(@"... ADD COMMENT OBJECT = %@ AT SECTION = %lu", thisComment, i );
                                }
                                else if (i == 7) {
                                    [self.head7 addObject:thisComment];
                                    NSLog(@"... ADD COMMENT OBJECT = %@ AT SECTION = %lu", thisComment, i );
                                }
                                else if (i == 8) {
                                    [self.head8 addObject:thisComment];
                                    NSLog(@"... ADD COMMENT OBJECT = %@ AT SECTION = %lu", thisComment, i );
                                }
                                else if (i == 9) {
                                    [self.head9 addObject:thisComment];
                                    NSLog(@"... ADD COMMENT OBJECT = %@ AT SECTION = %lu", thisComment, i );
                                }
                                
                            }
                        }
                        
                    }
                    
 
                    
                    NSLog(@"headline 0 = %lu", self.head0.count);
                    NSLog(@"headline 1 = %lu", self.head1.count);
                    NSLog(@"headline 2 = %lu", self.head2.count);
                    NSLog(@"headline 3 = %lu", self.head3.count);
                    NSLog(@"headline 4 = %lu", self.head4.count);
                    NSLog(@"headline 5 = %lu", self.head5.count);
                    NSLog(@"headline 6 = %lu", self.head6.count);
                    NSLog(@"headline 7 = %lu", self.head7.count);
                    NSLog(@"headline 8 = %lu", self.head8.count);
                    NSLog(@"headline 9 = %lu", self.head9.count);
                    
                    NSLog(@"headline 0 = %@", self.head0);
                    NSLog(@"headline 1 = %@", self.head1);
                    NSLog(@"headline 2 = %@", self.head2);
                    NSLog(@"headline 3 = %@", self.head3);
                    NSLog(@"headline 4 = %@", self.head4);
                    NSLog(@"headline 5 = %@", self.head5);
                    NSLog(@"headline 6 = %@", self.head6);
                    NSLog(@"headline 7 = %@", self.head7);
                    NSLog(@"headline 8 = %@", self.head8);
                    NSLog(@"headline 9 = %@", self.head9);
                }
                else {
                    NSLog(@"Query2 error = %@",error);
                    

                }
    
                
                [self.tableView reloadData];
                
                NSLog(@"headline 0 = %lu", self.head0.count);
                NSLog(@"headline 1 = %lu", self.head1.count);
                NSLog(@"headline 2 = %lu", self.head2.count);
                NSLog(@"headline 3 = %lu", self.head3.count);
                NSLog(@"headline 4 = %lu", self.head4.count);
                NSLog(@"headline 5 = %lu", self.head5.count);
                NSLog(@"headline 6 = %lu", self.head6.count);
                NSLog(@"headline 7 = %lu", self.head7.count);
                NSLog(@"headline 8 = %lu", self.head8.count);
                NSLog(@"headline 9 = %lu", self.head9.count);
                [self.tableView reloadData];

            }
             
             ];
            
            [self.tableView reloadData];
            
            NSLog(@"headline 0 = %@", self.head0);
            NSLog(@"headline 1 = %@", self.head1);
            NSLog(@"headline 2 = %@", self.head2);
            NSLog(@"headline 3 = %@", self.head3);
            NSLog(@"headline 4 = %@", self.head4);
            NSLog(@"headline 5 = %@", self.head5);
            NSLog(@"headline 6 = %@", self.head6);
            NSLog(@"headline 7 = %@", self.head7);
            NSLog(@"headline 8 = %@", self.head8);
            NSLog(@"headline 9 = %@", self.head9);
            
            
        }
    }];
    

    }
}




- (void) requeryTable
{
    
    NSLog(@"requeryTable.........................");
    
    PFRelation * comments = [[self pfParty] objectForKey:@"comments"];
    
    
    self.commentsList = [NSMutableArray new];
    
    //self.allCommentsList = [NSMutableArray new];
    
    PFQuery * query = [comments query];
    [query includeKey:@"createdBy"];
    [query orderByAscending:@"createdAt"];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        
        self.commentsList = objects;
        
//      self.allCommentsList = objects;
        
        if (objects.count == 0){
            [self.commentsList addObject:@"Zero"];
        }
        
        NSLog(@"*** comments Array in background count = %luu \n and the Array that comes back %lu uhuhuhuh !!!!", self.commentsList.count, objects.count);
        
        
        NSLog(@"the array after the finding nothing%@", self.commentsList);
        
        [self.tableView reloadData];
    }];
    
    [self.tableView reloadData];
    
    [self.tableView setContentOffset:CGPointMake(0, CGFLOAT_MAX + 100)];
    
}

- (void) refreshTouch
{
    NSLog(@"........refreshTouch");
    [self.tableView reloadData];
}


- (void) viewDidLoad
{
    [super viewDidLoad];
    self.commentTextView2.delegate = self;
    self.tableView.delegate = self;
    self.currentUser = [PFUser currentUser];
    
    NSLog(@"for the \n push \n user object!\n  %@ \n %@", self.userPush, self.pfParty);
    
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    self.refreshControl = [[UIRefreshControl alloc]init];
    [self.tableView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    CGSize barSize = CGSizeMake(self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height +20);
    
    // CAGradientLayer * whiteLayer = [Gradient white];
    
    CAGradientLayer * randomLayer = [Gradient gold];
    
    //  whiteLayer.frame = CGRectMake(0, -20, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height + 20);
    randomLayer.frame = CGRectMake(0, -20, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height + 20);
    
    //[whiteLayer setStartPoint:CGPointMake(0.0, 0.5)];
    // [whiteLayer setEndPoint:CGPointMake(1.0, 0.5)];
    //    [randomLayer setStartPoint:CGPointMake(0.0, 0.5)];
    //    [randomLayer setEndPoint:CGPointMake(1.0, 0.5)];
    
    [randomLayer setStartPoint:CGPointMake(-1.5, 0.0)];
    [randomLayer setEndPoint:CGPointMake(2.5, 0.0)];
    UIGraphicsBeginImageContext(barSize);
    [randomLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * anotherImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //[self.navigationController.navigationBar setBackgroundImage:anotherImage forBarMetrics:UIBarMetricsDefault];
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//
//    });
    
 
    
    UIButton * backButtonView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [backButtonView addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [backButtonView setBackgroundImage:[UIImage imageNamed:@"GoldBackButton"] forState:UIControlStateNormal];
    
    UIBarButtonItem * backBUTTON = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
    self.navigationItem.leftBarButtonItem = backBUTTON;
    
    
    
//    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
//                                   initWithImage:image
//                                   style:UIBarButtonItemStylePlain
//                                   target:self action:@selector(goBack)];
    

    
    UIButton * refreshButtonView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [refreshButtonView addTarget:self action:@selector(refreshComments) forControlEvents:UIControlEventTouchUpInside];
    [refreshButtonView setBackgroundImage:[UIImage imageNamed:@"refreshGold"] forState:UIControlStateNormal];
    
    
    UIBarButtonItem *refreshButton =[[UIBarButtonItem alloc] initWithCustomView:refreshButtonView];
    
    //[[UIBarButtonItem alloc]
//                                  initWithImage:[UIImage imageNamed:@"refreshGold"]
//                                  style:UIBarButtonItemStylePlain
//                                  target:self action:@selector(refreshTouch)];
    
    self.navigationItem.rightBarButtonItem = refreshButton;
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    
    NSLog(@"PFCommentHistoryVC viewDidLoad. PFObject = %@", self.postObejct);
    

//    if(self.userPush != nil){
//        
//            PFQuery * pushUserQuery = [PFUser query];
//            [pushUserQuery whereKey:@"objectId" equalTo:self.userPush];
//            [pushUserQuery getFirstObjectInBackgroundWithBlock:^(PFObject * user, NSError* error){
//                if (!error) {
//                    self.partyUser = (PFUser *)user;
//                }else{
//                    NSLog(@"JOTO = %@", self.userPush);
//                
//                }
//            
//            }];
//        
//        
//        self.partyUser = self.pushUser;
//        self.userPush = nil;
//        
//        
//        
//    }
//    else{
        //self.partyUser = [self.pfParty objectForKey:@"createdBy"];
    //self.partyUser = [self.pfParty objectForKey:@"createdBy"];
    
    
    
    if (self.postObejct !=nil) {
        self.partyUser = [self.postObejct objectForKey:@"createdBy"];
    
        self.navigationItem.title = self.partyUser[@"firstName"];}else{
            self.navigationItem.title = self.otherUser[@"firstName"];
        }
    
//        
//        PFRelation * comments = [[self pfParty] objectForKey:@"comment"];
//        
//        PFQuery * query = [comments query];
//        [query includeKey:@"createdBy"];
//        [query orderByAscending:@"createdAt"];
//        [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
//            
//            self.commentsList = objects;
//            //self.allCommentsList = objects;
//            
//            if (objects.count == 0){
//                [self.commentsList addObject:@"Zero"];
//            }
//            
//            NSLog(@"*** comments Array in background count = %luu", self.commentsList.count);
//            NSLog(@"%@", self.commentsList);
//            
//            [self.tableView reloadData];
//        }];
    
    
    
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:(142/255.0) green:(83/255.0) blue:(249/255.0) alpha:1.0];

    
}

- (void) viewWillAppear:(BOOL)animated
{
    NSLog(@"viewDidAppear PFPartyCommentsVC");
    
    //[self fetchHeadline];
    [self refreshComments];
//    [self requeryTable];
}

-(void)dismissKeyboard {
    [self.commentTextView2 resignFirstResponder];
    //[self.tableView setContentOffset:CGPointMake(0, CGFLOAT_MAX + 100)];
}

-(BOOL) textViewShouldReturn:(UITextView *)textView
{
    [self.commentTextView2 resignFirstResponder];
    //[self.tableView setContentOffset:CGPointMake(0, CGFLOAT_MAX + 100)];
    return YES;
}

- (IBAction)postCommentButton:(id)sender
{
    [self addCommentTouch];
}


- (void) addCommentTouch
{
    NSLog(@".........................postComment ");
    
    NSString * message_string = [NSString stringWithFormat:@"%@", self.commentTextView2.text];
    
    self.commentTextView2.text = @"";
    
    if ([message_string isEqualToString:@""]) {
        
        NSLog(@"comment is nil");
        
        
        UIAlertController * alert = [UIAlertController
                                      alertControllerWithTitle:@"PartyFriends"
                                      message:@"Your comment is empty"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                   }];
        
        [alert addAction:okButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        return;
    }

    PFObject * message = [PFObject objectWithClassName:@"comment"];
    
//    PFRelation * friendsRel = [PFRelation alloc];
    
    //[message setObject:self.pfParty forKey:@"party"];
    
    //friendsRel = [message relationForKey:@"party"];

    message[@"comment"] = message_string;
    message[@"post"] = self.postObejct;
    
    if (self.pfParty != nil) {
        message[@"party"] =  self.pfParty;
    }
    else {
        NSLog(@"************* self.partyObject is nil");
    }
    
    
    
    [message saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        if (!error) {
            NSLog(@"message save in background");
            
//            [self requeryTable];
            //[self fetchHeadline];
            [self refreshComments];
        } else {
//            PFRelation * comments = [[self pfParty] objectForKey:@"comments"];
//            
//            PFQuery * query = [comments query];
//            [query includeKey:@"createdBy"];
//            
//            [query orderByAscending:@"createdAt"];
//            [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
            
//                self.commentsList = objects;
//                
//                self.allCommentsList = objects;
//                
//
//                
//                NSLog(@"*** comments /n  Array in background /n count = %luu /n error = %@", self.commentsList.count,error);
//                
//                NSLog(@"%@", self.commentsList);
            
           // [self fetchHeadline];
            [self refreshComments];
//                [self.tableView reloadData];
//            }];
        }
        
    }];
    
    //[self requeryTable];
    
    
    //    if (self.tableView.contentSize.height > self.tableView.frame.size.height) {
    //        CGPoint offset = CGPointMake(0, self.tableView.contentSize.height - self.tableView.frame.size.height);
    //        [self.tableView setContentOffset:offset animated:YES];
    //    }
    
    [self.commentTextView2 resignFirstResponder];
    
    //    PFRelation * comments = [[self pfParty] objectForKey:@"comments"];
    //
    //    PFQuery * query = [comments query];
    //    [query includeKey:@"createdBy"];
    //
    //    [query orderByAscending:@"createdAt"];
    //
    //
    //    //self.commentsList = [query findObjects];
    //
    //
    //    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
    //
    //        self.commentsList = objects;
    //
    //        NSLog(@"*** comments Array in background count = %luu", self.commentsList.count);
    //
    //        NSLog(@"%@", self.commentsList);
    //
    //        [self.tableView reloadData];
    //    }];
    //
    //    [self.tableView reloadData];
    
}



- (void) animateTextView:(BOOL) up
{
    // Get the size of the keyboard.
    //    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    // Given size may not account for screen rotation
    //    int height = MIN(keyboardSize.height,keyboardSize.width);
    //    int width = MAX(keyboardSize.height,keyboardSize.width);
    
    const int movementDistance = 10; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    int movement= movement = (up ? -movementDistance : movementDistance);
    NSLog(@"%d",movement);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.commentActionView.frame, 0, movement);
    [UIView commitAnimations];
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return textView.text.length + (text.length - range.length) <= 140;
}

- (void) goBack {
    
    NSLog(@"Back");
    //[self performSegueWithIdentifier:@"unwindToAction" sender:self];
    [self performSegueWithIdentifier:@"backAction" sender:self];
    //[self.navigationController popViewControllerAnimated:YES];
}


- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
//    NSInteger sectionCount = self.headlineList.count;
//    if (sectionCount > 10)
//        return 10;
//    else
//        return sectionCount;
    return 1;
}

//- (NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    
//    NSString * postText = self.postObejct[@"headline"];
//    
////    NSString * result = [[self.headlineList objectAtIndex:section] objectForKey:@"headline"];
////    
////    __block NSString * title = @"";
////    
////    PFGeoPoint * userLocation = [self.pfParty objectForKey:@"location"];
////    
////    CLGeocoder *ceo = [[CLGeocoder alloc]init];
////    CLLocation *loc = [[CLLocation alloc]initWithLatitude:userLocation.latitude longitude:userLocation.longitude];
////    [ceo reverseGeocodeLocation:loc
////              completionHandler:^(NSArray *placemarks, NSError *error) {
////                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
////                  NSLog(@"placemark %@",placemark);
////                  //String to hold address
////                  NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
////                  
////                  title = locatedAt;
////                  
////                  NSLog(@"addressDictionary %@", placemark.addressDictionary);
////                  
////                  NSLog(@"placemark %@",placemark.region);
////                  NSLog(@"placemark %@",placemark.country);  // Give Country Name
////                  NSLog(@"placemark %@",placemark.locality); // Extract the city name
////                  NSLog(@"location %@",placemark.name);
////                  NSLog(@"location %@",placemark.ocean);
////                  NSLog(@"location %@",placemark.postalCode);
////                  NSLog(@"location %@",placemark.subLocality);
////                  
////                  NSLog(@"location %@",placemark.location);
////                  //Print the location to console
////                  NSLog(@"I am currently at %@",locatedAt);
////                  
////                  
//////                  if (result)
//////                      return result;
//////                  else if ([result isEqualToString:@""])
//////                      return locatedAt;
//////                  else
//////                      return @"";
////              }
//////     else {
//////         NSLog(@"Could not locate");
//////     }
////     ];
////    
////    if ([result isEqualToString:@""] || result == nil)
////        return title;
////    else if (result)
////        return result;
////    else
////        return @"";
//    
//    return postText;
//
//}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 90;
}

- (NSString *) getHeadlineTimeStamp: (NSInteger) section
{
    NSLog(@"PFCommentHistoryVC.m - getHeadlineTimeStamp");
    
    PFObject * headlineObject = [self.headlineList objectAtIndex:section];
    
    NSLog(@"PFObject headline Object = %@", headlineObject);
    
    //NSDate * timeStamp = [headlineObject objectForKey:@"createdAt"];
    
    NSDate * timeStamp = [headlineObject createdAt];
    
    NSLog(@"Comment time stamp = %@", timeStamp);
    
    NSDate * currentTimeStamp = [NSDate date];
    
    NSLog(@"Current Time Stamp = %@", currentTimeStamp);
    
   NSTimeInterval diff = [currentTimeStamp timeIntervalSinceDate:timeStamp];
    
    NSLog(@"NSTimeInterval = %f", diff);
    
    //CGFloat * d = diff / 60.0;
    
    //long seconds = lroundf(diff); // Since modulo operator (%) below needs int or long
    
    //int hour = seconds / 3600;
    
    NSInteger hours = floor(diff/(60*60));
    
    NSInteger days = hours / 24;
    
    NSString * result = [NSString stringWithFormat:@"%ld", hours];
    
    if (hours > 24)
        return [[NSString stringWithFormat:@"%ld", days] stringByAppendingString:@" days ago... "];
    else
        return [result stringByAppendingString:@" hours ago... "];
    
    //return result;
    
}

                                  
    
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    PFObject * post = self.postObejct;
    
    NSString * headline = [[self.headlineList objectAtIndex:section] objectForKey:@"headline"];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 1000)];
    //Create custom view to display section header
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(80, 10, tableView.frame.size.width - 10, 18)];
    [label1 setFont:[UIFont fontWithName:@"Helvetica Neue" size:16]];
    [label1 setText:post[@"headline"]];
    
    //[view addSubview:label1];
    
//    if (headline == nil || [headline isEqualToString:@""]) {
//        
//        PFGeoPoint * userLocation = [self.pfParty objectForKey:@"location"];
//        
//        CLGeocoder *ceo = [[CLGeocoder alloc]init];
//        CLLocation *loc = [[CLLocation alloc]initWithLatitude:userLocation.latitude longitude:userLocation.longitude];
//        [ceo reverseGeocodeLocation:loc
//                  completionHandler:^(NSArray *placemarks, NSError *error) {
//                      CLPlacemark *placemark = [placemarks objectAtIndex:0];
//                      NSLog(@"placemark %@",placemark);
//                      //String to hold address
//                      NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
//                      
//                      NSString * title = locatedAt;
//                      
//                      NSLog(@"addressDictionary %@", placemark.addressDictionary);
//                      
//                      NSLog(@"placemark %@",placemark.region);
//                      NSLog(@"placemark %@",placemark.country);  // Give Country Name
//                      NSLog(@"placemark %@",placemark.locality); // Extract the city name
//                      NSLog(@"location %@",placemark.name);
//                      NSLog(@"location %@",placemark.ocean);
//                      NSLog(@"location %@",placemark.postalCode);
//                      NSLog(@"location %@",placemark.subLocality);
//                      
//                      NSLog(@"location areas of interest %@", placemark.areasOfInterest);
//                      
//                      NSLog(@"location %@",placemark.location);
//                      //Print the location to console
//                      NSLog(@"I am currently at %@",locatedAt);
//                      
//                      UILabel * label2 = [[UILabel alloc] initWithFrame:CGRectMake(80, 30, tableView.frame.size.width - 10, 18)];
//                      
//                      [label2 setFont:[UIFont fontWithName:@"Helvetica Neue" size:16]];
//                      [label2 setText:placemark.subLocality];
//                      
//                      UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(80, 10, tableView.frame.size.width - 10, 18)];
//                      [label setFont:[UIFont fontWithName:@"Helvetica Neue" size:16]];
//                      [label setText:placemark.name];
//                      
//                      [view addSubview:label];
//                      [view addSubview:label2];
//                  }
//         
//         ];
//    }
    NSDate * timePost = [post createdAt];
    
    
    NSString * timeAgo = [timePost formattedAsTimeAgo];
    
    UILabel * postLabel = [[UILabel alloc] initWithFrame:CGRectMake(80, 10, view.frame.size.width-85, 150)];
        [postLabel setFont:[UIFont fontWithName:@"Helvetica Neue" size:13]];
    [postLabel setText:post[@"headline"]];
    postLabel.numberOfLines = 0;

    postLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [postLabel sizeToFit];
    
    UIImageView * headerImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 72, 72)];
    [view addSubview:postLabel];
    
    
//    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, tableView.frame.size.width - 10, 18)];
//    [label setFont:[UIFont fontWithName:@"Helvetica Neue" size:16]];
//    [label setText:headline];
    
    UILabel * timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(80, 60, 100, 20)];

    
    
    [timeLabel setFont:[UIFont fontWithName:@"Helvetica Neue" size:11]];
    [timeLabel setText:timeAgo];
    
//    label.textAlignment = NSTextAlignmentCenter;
   //timeLabel.textAlignment = NSTextAlignmentRight;
//    
//    [view addSubview:label];
    [view addSubview:timeLabel];
    
    [view addSubview:headerImage];
    
    
    UIColor * color1 = [UIColor colorWithRed:0.0f green:0.34f blue:0.62f alpha:1.0f];
    
    UIColor *color2 = [UIColor colorWithWhite:0 alpha:0.3];
    
    
    
    [view setBackgroundColor:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.01]];
    
    //[view setBackgroundColor:color2];
    
    //[label setCenter:view.center];
    
    //temp button attributes
    CALayer *sectionLayer = [view layer];
    //[sectionLayer setMasksToBounds:YES];
    //[sectionLayer setCornerRadius:10.0f];
    // Apply a 1 pixel, black border
    [sectionLayer setBorderWidth:0.5f];
    [sectionLayer setBorderColor:[[UIColor grayColor] CGColor]];
    CALayer *imageLayer = headerImage.layer;
    [imageLayer setCornerRadius:5];
    [imageLayer setBorderWidth:0];
    [imageLayer setMasksToBounds:YES];
    [headerImage.layer setCornerRadius:headerImage.frame.size.width/2];
    [headerImage.layer setMasksToBounds:YES];
    
    
    PFUser * user = [self.postObejct objectForKey:@"createdBy"];
    PFObject * photo = user[@"profilePicture"];
    
    if (photo != nil) {
        [photo fetchInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
            if (!error) {
                PFFile * pic = [object objectForKey:@"photo"];
                PFImageView * friendImage = [PFImageView new];
                friendImage.image = [UIImage imageNamed:@"nigger"];
                friendImage.file = (PFFile *) pic;
                
                [friendImage loadInBackground:^(UIImage * image, NSError * error){
                    if (!error) {
                        image = [self squareImageFromImage:image scaledToSize:image.size.height];
                        
                        
                        [headerImage setImage:image];
                        
                    }else{
                        
                        UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
                        [headerImage setImage:defaultimg];
                        
                    }
                    
                    
                }];
                
                
            }else {
                
                UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
                [headerImage setImage:defaultimg];
                
            }
        }];
    }else{
        
        UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
        [headerImage setImage:defaultimg];
        
    }
    
    
    
//    if (user[@"profilePic"] != nil) {
//        
//        PFFile * pic = [user objectForKey:@"profilePic"];
//        
//        
//        [pic getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
//            if (!error) {
//                UIImage *image = [UIImage imageWithData:data];
//                
//                CALayer *imageLayer = headerImage.layer;
//                [imageLayer setCornerRadius:5];
//                [imageLayer setBorderWidth:1];
//                [imageLayer setMasksToBounds:YES];
//                [headerImage.layer setCornerRadius:headerImage.frame.size.width/2];
//                [headerImage.layer setMasksToBounds:YES];
//                [headerImage setImage:image];
//            }
//            else {
//                
//                UIImage * defaultimg = [UIImage imageNamed:@"58.png"];
//                
//                CALayer *imageLayer = headerImage.layer;
//                [imageLayer setCornerRadius:5];
//                [imageLayer setBorderWidth:1];
//                [imageLayer setMasksToBounds:YES];
//                [headerImage.layer setCornerRadius:headerImage.frame.size.width/2];
//                [headerImage.layer setMasksToBounds:YES];
//                [headerImage setImage:defaultimg];
//            }
//        }];
//        
//    }
//    else {
//        UIImage * defaultimg = [UIImage imageNamed:@"PFIcon"];
//        
//        CALayer *imageLayer = headerImage.layer;
//        [imageLayer setCornerRadius:5];
//        [imageLayer setBorderWidth:1];
//        [imageLayer setMasksToBounds:YES];
//        [headerImage.layer setCornerRadius:headerImage.frame.size.width/2];
//        [headerImage.layer setMasksToBounds:YES];
//        [headerImage setImage:defaultimg];
//    }
    



    return view;
}

- (UIImage *)squareImageFromImage:(UIImage *)image scaledToSize:(CGFloat)newSize {
    CGAffineTransform scaleTransform;
    CGPoint origin;
    
    if (image.size.width > image.size.height) {
        CGFloat scaleRatio = newSize / image.size.height;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(-(image.size.width - image.size.height) / 2.0f, 0);
    } else {
        CGFloat scaleRatio = newSize / image.size.width;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(0, -(image.size.height - image.size.width) / 2.0f);
    }
    
    CGSize size = CGSizeMake(newSize, newSize);
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(size, YES, 0);
    } else {
        UIGraphicsBeginImageContext(size);
    }
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextConcatCTM(context, scaleTransform);
    
    [image drawAtPoint:origin];
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}


- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    NSLog(@"number of rows in section  %lu = %lu", section, self.commentsList.count);
//    
//    if (section == 0) {
//        NSLog(@"section %lu has a row count = %lu", section, self.head0.count);
//        if (self.head0.count == 0)
//            return 1;
//        else
//            return self.head0.count;
//    }
//    if (section == 1) {
//        NSLog(@"section %lu has a row count = %lu", section, self.head1.count);
//        if (self.head1.count == 0)
//            return 1;
//        else
//            return self.head1.count;
//    }
//    if (section == 2) {
//        NSLog(@"section %lu has a row count = %lu", section, self.head2.count);
//        if (self.head2.count == 0)
//            return 1;
//        else
//        return self.head2.count;
//    }
//    if (section == 3) {
//        NSLog(@"section %lu has a row count = %lu", section, self.head3.count);
//        if (self.head3.count == 0)
//            return 1;
//        else
//        return self.head3.count;
//    }
//    if (section == 4) {
//        NSLog(@"section %lu has a row count = %lu", section, self.head4.count);
//        if (self.head4.count == 0)
//            return 1;
//        else
//        return self.head4.count;
//    }
//    if (section == 5) {
//        NSLog(@"section %lu has a row count = %lu", section, self.head5.count);
//        if (self.head5.count == 0)
//            return 1;
//        else
//        return self.head5.count;
//    }
//    if (section == 6) {
//        NSLog(@"section %lu has a row count = %lu", section, self.head6.count);
//        if (self.head6.count == 0)
//            return 1;
//        else
//        return self.head6.count;
//    }
//    if (section == 7) {
//        NSLog(@"section %lu has a row count = %lu", section, self.head7.count);
//        if (self.head7.count == 0)
//            return 1;
//        else
//        return self.head7.count;
//    }
//    if (section == 8) {
//        NSLog(@"section %lu has a row count = %lu", section, self.head8.count);
//        if (self.head8.count == 0)
//            return 1;
//        else
//        return self.head8.count;
//    }
//    if (section == 9) {
//        NSLog(@"section %lu has a row count = %lu", section, self.head9.count);
//        if (self.head9.count == 0)
//            return 1;
//        else
//        return self.head9.count;
//    }
    
    return [self.commentList count];
    
}

- (PFCommentsTableViewCell *) tableView: (UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"..............................................indexPath = %lu", indexPath.row);

    static NSString * cellIdentifier = @"com";
    
    PFCommentsTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
  
    //    PFCommentsTableViewCell * cell = [[PFCommentsTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    if ([[self.commentList objectAtIndex:0] isEqual:@"No Comments"]) {
        cell.commentAuthor.text = @"";
        cell.commentTextView.text = [self.commentList objectAtIndex:0];
        [cell.editCommentButton setHidden:YES];
    }else{
    
        PFObject * comments = [self.commentList objectAtIndex:indexPath.row];
        
        PFUser * user = [comments objectForKey:@"createdBy"];
        
        if ([user.objectId isEqualToString:self.currentUser.objectId]) {
            [cell.editCommentButton setHidden:NO];
            [cell.editCommentButton setEnabled:YES];
        }else{
            [cell.editCommentButton setHidden:YES];
            [cell.editCommentButton setEnabled:NO];
        }
        
        
        cell.commentObject = comments;
        
        
        
        
        NSString * commentString = comments[@"comment"];
        
        CGSize stringSize = [commentString sizeWithFont:[UIFont boldSystemFontOfSize:15] constrainedToSize:CGSizeMake(320, 9999) lineBreakMode:UILineBreakModeWordWrap];
        
        
        
        cell.commentTextView.font = [UIFont systemFontOfSize:11.0];
        cell.commentTextView.font = [UIFont systemFontOfSize:11.0];
        cell.commentTextView.text = commentString;
        cell.commentTextView.editable = NO;
        [cell.commentTextView setScrollEnabled:NO];
        
        cell.commentAuthor.text = [user objectForKey:@"firstName"];
        
        
        NSDate * postDate = [comments createdAt];
        cell.timeStamp.text = [postDate formattedAsTimeAgo];
        
        
        
        //[postDate formattedAsTimeAgo];
        NSString * random = [postDate formattedAsTimeAgo];
        
        NSLog(@"fuck %@ \n %@", random, cell.commentObject);
        PFObject * photo = user[@"profilePicture"];
        
        CALayer *imageLayer = cell.commentSenderProfilePic.layer;
        [imageLayer setCornerRadius:5];
        [imageLayer setBorderWidth:0];
        [imageLayer setMasksToBounds:YES];
        [cell.commentSenderProfilePic.layer setCornerRadius:cell.commentSenderProfilePic.frame.size.width/2];
        [cell.commentSenderProfilePic.layer setMasksToBounds:YES];
        
        if (photo != nil) {
            [photo fetchInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
                if (!error) {
                    PFFile * pic = [object objectForKey:@"photo"];
                    PFImageView * friendImage = [PFImageView new];
                    friendImage.image = [UIImage imageNamed:@"nigger"];
                    friendImage.file = (PFFile *) pic;
                    
                    [friendImage loadInBackground:^(UIImage * image, NSError * error){
                        if (!error) {
                            
                            
                        
                            [cell.commentSenderProfilePic   setImage:image];
                            
                        }else{
                            
                            UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
                            [cell.commentSenderProfilePic setImage:defaultimg];
                            
                        }
                        
                        
                    }];
                    
                    
                }else {
                
                    UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
                    [cell.commentSenderProfilePic setImage:defaultimg];
                
                }
            }];
        }else{
        
            UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
            [cell.commentSenderProfilePic setImage:defaultimg];
        
        }
 
        
        
    }
    
//    if (indexPath.section == 0 && self.head0.count < 1) {
//    //if ([[self.head0 objectAtIndex:0] isEqual:@"Zero"]) {
//        cell.commentAuthor.text = @"";
//        cell.commentTextView.text = @"No Comments yet";
//        [cell.editCommentButton setHidden:YES];
//        [cell.commentSenderProfilePic setHidden:YES];
//        //h0 = true;
//        return cell;
//    //}
//    }
//    else if (indexPath.section == 1 && self.head1.count < 1) {
//    //if ([[self.head1 objectAtIndex:0] isEqual:@"Zero"]) {
//        cell.commentAuthor.text = @"";
//        cell.commentTextView.text = @"No Comments yet";
//        [cell.editCommentButton setHidden:YES];
//        [cell.commentSenderProfilePic setHidden:YES];
//        //h1 = true;
//        return cell;
//    //}
//    }
//    else if (indexPath.section == 2 && self.head2.count < 1) {
//    //if ([[self.head2 objectAtIndex:0] isEqual:@"Zero"]) {
//        cell.commentAuthor.text = @"";
//        cell.commentTextView.text = @"No Comments yet";
//        [cell.editCommentButton setHidden:YES];
//        [cell.commentSenderProfilePic setHidden:YES];
//        //h2 = true;
//        return cell;
//    //}
//    }
//    else if (indexPath.section == 3 && self.head3.count < 1) {
//    //if ([[self.head3 objectAtIndex:0] isEqual:@"Zero"]) {
//        cell.commentAuthor.text = @"";
//        cell.commentTextView.text = @"No Comments yet";
//        [cell.editCommentButton setHidden:YES];
//        [cell.commentSenderProfilePic setHidden:YES];
//        //h3 = true;
//        return cell;
//    //}
//    }
//    else if (indexPath.section == 4 && self.head4.count < 1) {
//    //if ([[self.head4 objectAtIndex:0] isEqual:@"Zero"]) {
//        cell.commentAuthor.text = @"";
//        cell.commentTextView.text = @"No Comments yet";
//        [cell.editCommentButton setHidden:YES];
//        [cell.commentSenderProfilePic setHidden:YES];
//        //h4 = true;
//        return cell;
//    //}
//    }
//    else if (indexPath.section == 5 && self.head5.count < 1) {
//    //if ([[self.head5 objectAtIndex:0] isEqual:@"Zero"]) {
//        cell.commentAuthor.text = @"";
//        cell.commentTextView.text = @"No Comments yet";
//        [cell.editCommentButton setHidden:YES];
//        [cell.commentSenderProfilePic setHidden:YES];
//        //h5 = true;
//        return cell;
//    //}
//    }
//    else if (indexPath.section == 6 && self.head6.count < 1) {
//    //if ([[self.head6 objectAtIndex:0] isEqual:@"Zero"]) {
//        cell.commentAuthor.text = @"";
//        cell.commentTextView.text = @"No Comments yet";
//        [cell.editCommentButton setHidden:YES];
//        [cell.commentSenderProfilePic setHidden:YES];
//        //h6 = true;
//        return cell;
//    //}
//    }
//    else if (indexPath.section == 7 && self.head7.count < 1) {
//    //if ([[self.head7 objectAtIndex:0] isEqual:@"Zero"]) {
//        cell.commentAuthor.text = @"";
//        cell.commentTextView.text = @"No Comments yet";
//        [cell.editCommentButton setHidden:YES];
//        [cell.commentSenderProfilePic setHidden:YES];
//        //h7 = true;
//        return cell;
//    //}
//    }
//    else if (indexPath.section == 8 && self.head8.count < 1) {
//    //if ([[self.head8 objectAtIndex:0] isEqual:@"Zero"]) {
//        cell.commentAuthor.text = @"";
//        cell.commentTextView.text = @"No Comments yet";
//        [cell.editCommentButton setHidden:YES];
//        [cell.commentSenderProfilePic setHidden:YES];
//        //h8 = true;
//        return cell;
//    //}
//    }
//    else if (indexPath.section == 9 && self.head9.count < 1) {
//    //if ([[self.head9 objectAtIndex:0] isEqual:@"Zero"]) {
//        cell.commentAuthor.text = @"";
//        cell.commentTextView.text = @"No Comments yet";
//        [cell.editCommentButton setHidden:YES];
//        [cell.commentSenderProfilePic setHidden:YES];
//        //h9 = true;
//        return cell;
//    //}
//    }
//    else {
//        
//        if (cell == nil) {
//            cell = [[PFCommentsTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
//        }
//        
//        [cell.commentSenderProfilePic setHidden:NO];
//        
//        PFObject * comments = [self.commentsList objectAtIndex:indexPath.row];
//        
//        if (indexPath.section == 0) {
//            comments = [self.head0 objectAtIndex:indexPath.row];
//        }
//        else if (indexPath.section == 1) {
//            comments = [self.head1 objectAtIndex:indexPath.row];
//        }
//        else if (indexPath.section == 2) {
//            comments = [self.head2 objectAtIndex:indexPath.row];
//        }
//        else if (indexPath.section == 3) {
//            comments = [self.head3 objectAtIndex:indexPath.row];
//        }
//        else if (indexPath.section == 4) {
//            comments = [self.head4 objectAtIndex:indexPath.row];
//        }
//        else if (indexPath.section == 5) {
//            comments = [self.head5 objectAtIndex:indexPath.row];
//        }
//        else if (indexPath.section == 6) {
//            comments = [self.head6 objectAtIndex:indexPath.row];
//        }
//        else if (indexPath.section == 7) {
//            comments = [self.head7 objectAtIndex:indexPath.row];
//        }
//        else if (indexPath.section == 8) {
//            comments = [self.head8 objectAtIndex:indexPath.row];
//        }
//        else if (indexPath.section == 9) {
//            comments = [self.head9 objectAtIndex:indexPath.row];
//        }
//        
//        cell.commentObject = comments;
//        
//        PFUser * user = [comments objectForKey:@"createdBy"];
//        
//        [user fetchIfNeeded];
//        
//        
//        
//        PFUser * currentUser = [PFUser currentUser];
//        
//        if ([user.objectId isEqualToString:currentUser.objectId]) {
//            [cell.editCommentButton setHidden:NO];
//            [cell.editCommentButton setEnabled:YES];
//        }
//        else {
//            [cell.editCommentButton setHidden:YES];
//            [cell.editCommentButton setEnabled:NO];
//        }
//        
//        
//        
//        cell.commentObject = comments;
//        
//        NSLog(@"createdBy = %@", comments);
//        
//        
//        NSString * comment_str = comments[@"comment"];
//        
//        CGSize stringSize = [comment_str sizeWithFont:[UIFont boldSystemFontOfSize:15] constrainedToSize:CGSizeMake(320, 9999) lineBreakMode:UILineBreakModeWordWrap];
//        
//        NSLog(@"******************");
//        NSLog(@"cell string size = %f", stringSize.height);
//        
//        cell.commentTextView.font = [UIFont systemFontOfSize:11.0];
//        cell.commentTextView.text = comment_str;
//        cell.commentTextView.editable = NO;
//        [cell.commentTextView setScrollEnabled:NO];
//        
//        
//        cell.commentAuthor.text = [user objectForKey:@"firstName"];
//        
//        
//        if (user[@"profilePic"] != nil) {
//            
//            PFFile * pic = [user objectForKey:@"profilePic"];
//            
//            
//            [pic getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
//                if (!error) {
//                    UIImage *image = [UIImage imageWithData:data];
//                    
//                    CALayer *imageLayer = cell.commentSenderProfilePic.layer;
//                    [imageLayer setCornerRadius:5];
//                    [imageLayer setBorderWidth:1];
//                    [imageLayer setMasksToBounds:YES];
//                    [cell.commentSenderProfilePic.layer setCornerRadius:cell.commentSenderProfilePic.frame.size.width/2];
//                    [cell.commentSenderProfilePic.layer setMasksToBounds:YES];
//                    [cell.commentSenderProfilePic setImage:image];
//                }
//                else {
//                    
//                    UIImage * defaultimg = [UIImage imageNamed:@"58.png"];
//                    
//                    CALayer *imageLayer = cell.commentSenderProfilePic.layer;
//                    [imageLayer setCornerRadius:5];
//                    [imageLayer setBorderWidth:1];
//                    [imageLayer setMasksToBounds:YES];
//                    [cell.commentSenderProfilePic.layer setCornerRadius:cell.commentSenderProfilePic.frame.size.width/2];
//                    [cell.commentSenderProfilePic.layer setMasksToBounds:YES];
//                    [cell.commentSenderProfilePic setImage:defaultimg];
//                }
//            }];
//            
//        }
//        else {
//            UIImage * defaultimg = [UIImage imageNamed:@"PFIcon"];
//            
//            CALayer *imageLayer = cell.commentSenderProfilePic.layer;
//            [imageLayer setCornerRadius:5];
//            [imageLayer setBorderWidth:1];
//            [imageLayer setMasksToBounds:YES];
//            [cell.commentSenderProfilePic.layer setCornerRadius:cell.commentSenderProfilePic.frame.size.width/2];
//            [cell.commentSenderProfilePic.layer setMasksToBounds:YES];
//            [cell.commentSenderProfilePic setImage:defaultimg];
//        }
//        
//    }
    return cell;
    
    
}

#define MAXLength 50



- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    return newLength <= MAXLength || returnKey;
}

/*- (BOOL)textView:(UITextView *) textView shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString * )string{
 
 NSUInteger oldLength = [textView.text length];
 NSUInteger replaceLenght = [string length];
 NSUInteger rangeLenght = range.length;
 
 NSUInteger newLenght = oldLength - rangeLenght + replaceLenght;
 
 BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
 
 
 return newLenght <= MAXLength || returnKey;
 }*/


/*
 - (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
 {
 return textView.text.length + (text.length - range.length) <= 300
 ;
 }
 */

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    PFCommentsTableViewCell * cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    PFObject * object = cell.commentObject;
    
    PFUser * user = [object objectForKey:@"createdBy"];
    
    PFUser * currentUser = [PFUser currentUser];
    
    
    
    if ([cell.commentTextView.text isEqualToString:@"No Comments yet"]) {
        return NO;
    }
    
    if ([currentUser.objectId isEqualToString:self.partyUser.objectId]) {
        return YES;
    }
    
    if ([user.objectId isEqualToString:[currentUser objectId]])
        return YES;
    else
        return NO;
    
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"........................................ Delete comment.");
    
    PFCommentsTableViewCell * cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        //PFObject * comment = [self.commentsList objectAtIndex:indexPath.row];
        
        PFObject * comment = cell.commentObject;
        
        NSLog(@"comment object = %@", comment);
        
        
        NSInteger section = indexPath.section;
        
        switch (section) {
            case 0:
                [self.head0 removeObjectAtIndex:indexPath.row];
                break;
                
            case 1:
                [self.head1 removeObjectAtIndex:indexPath.row];
                break;
            case 2:
                [self.head2 removeObjectAtIndex:indexPath.row];
                break;
            case 3:
                [self.head3 removeObjectAtIndex:indexPath.row];
                break;
            case 4:
                [self.head4 removeObjectAtIndex:indexPath.row];
                break;
            case 5:
                [self.head5 removeObjectAtIndex:indexPath.row];
                break;
            case 6:
                [self.head6 removeObjectAtIndex:indexPath.row];
                break;
            case 7:
                [self.head7 removeObjectAtIndex:indexPath.row];
                break;
            case 8:
                [self.head8 removeObjectAtIndex:indexPath.row];
                break;
            case 9:
                [self.head9 removeObjectAtIndex:indexPath.row];
                break;
            default:
                break;
        }
        [self.commentsList removeObjectAtIndex:indexPath.row];
        [tableView reloadData]; // tell table to refresh now
        
        PFQuery *query = [PFQuery queryWithClassName:@"comment"];
        [query whereKey:@"objectId" equalTo:[comment objectId]];
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (!object) {
                NSLog(@"The getFirstObject request failed.");
            } else {
                NSLog(@"Successfully retrieved the object.");
                [object deleteInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    if (succeeded && !error) {
                        NSLog(@"Image deleted from Parse");
                    } else {
                        NSLog(@"error: %@", error);
                    }
                }];
            }
        }];
    }
}

- (IBAction)editCommentTouch:(id)sender
{
    PFCommentsTableViewCell * cell = (PFCommentsTableViewCell *) [[sender superview] superview];
    self.commentObject = cell.commentObject;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"editcomment"]) {
        PFAddCommentViewController * commentVC = [segue destinationViewController];
        commentVC.commentObject = self.commentObject;
        
        NSLog(@"***************** SEGUE. self.commentObject = %@", self.commentObject);
    }
}




@end
