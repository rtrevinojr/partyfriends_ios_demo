//
//  BeginningView.h
//  PartyFriends
//
//  Created by Joshua Silva  on 6/24/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <Parse/Parse.h>

@interface BeginningView : UIViewController <CLLocationManagerDelegate>


@property (strong, nonatomic) CLLocationManager * locationManager;
@property (strong, nonatomic) CLLocation * location;
@property (strong, nonatomic) PFUser * currentUser;
@property (strong, nonatomic) PFObject * placeObject;
@property (strong, nonatomic) PFRole * rolling;
@property (strong, nonatomic) PFRelation * allFriendsRelation;
@property (strong, nonatomic) NSArray * allFriends;
@property (strong, nonatomic) IBOutlet UIButton *Turorial;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;


@property (strong, nonatomic) IBOutlet UIView *noteView;

@property (strong, nonatomic) NSString * userStatus;

- (IBAction)whatsHappening:(id)sender;
- (IBAction)partyMode:(id)sender;

- (IBAction)goingOut:(id)sender;




@end
