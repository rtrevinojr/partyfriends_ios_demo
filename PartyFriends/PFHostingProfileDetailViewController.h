//
//  PFHostingProfileDetailViewController.h
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 11/19/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MapKit/MapKit.h>

#import <Parse/Parse.h>


@interface PFHostingProfileDetailViewController : UIViewController


@property (weak, nonatomic) IBOutlet MKMapView *hostingMapView;

@property (weak, nonatomic) IBOutlet UILabel *hostingHeadline;

@property (weak, nonatomic) IBOutlet UILabel *hostingHost;

@property (weak, nonatomic) IBOutlet UILabel *hostingTime;

@property (weak, nonatomic) IBOutlet UILabel *hostingDate;

@property (strong, nonatomic) PFObject * pfObject;

@property (weak, nonatomic) IBOutlet UITextView *hostingDescriptionTextView;

@end
