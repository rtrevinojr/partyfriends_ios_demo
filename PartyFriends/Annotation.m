//
//  Annotation.m
//  PartyFriends
//
//  Created by Joshua Silva  on 7/21/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import "Annotation.h"

@implementation Annotation

@synthesize coordinate, title, subtitle, annObjectId;


-(id) initWithTitle: (NSString * ) ctitle Subtitle: (NSString *) csubtitle Location:(CLLocationCoordinate2D )location{
    self = [super init];
    
    if (self) {
        self.title = ctitle;
        self.subtitle = csubtitle;
        self.coordinate = location;

        
        /*
         ctitle = self.customtitle;
         csubtitle = self.customsubtitle;
         location = self.coordinate;*/
    }
    
    return self;
}

/*- (id) initWithTitle: (NSString *) ctitle AndSubtile: (NSString *) csubtitle andObjectId: (NSString *) objId
 {
 self = [super init];
 
 if (self) {
 self.customtitle = ctitle;
 self.customsubtitle = csubtitle;
 self.annObjectId = objId;
 }
 return self;
 }
 */
/*-(MKAnnotationView *)annotationView{
    MKAnnotationView * annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:self reuseIdentifier:@"MyCustomAnnotation"];
    
   //waffles90
    annotationView.image = [UIImage imageNamed:@"PIcon"];
    annotationView.enabled = YES;
    annotationView.canShowCallout = YES;
    annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    
    return annotationView;
    
}*/

+ (MKAnnotationView *)createViewAnnotationForMapView:(MKMapView *)mapView annotation:(id <MKAnnotation>)annotation
{
    MKAnnotationView *returnedAnnotationView =
    [mapView dequeueReusableAnnotationViewWithIdentifier:NSStringFromClass([Annotation class])];
    if (returnedAnnotationView == nil)
    {
        returnedAnnotationView =
        [[MKAnnotationView alloc] initWithAnnotation:annotation
                                     reuseIdentifier:NSStringFromClass([Annotation class])];
        
        returnedAnnotationView.canShowCallout = YES;
        returnedAnnotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        //returnedAnnotationView.centerOffset = CGPointMake(1, 1);
        returnedAnnotationView.layer.anchorPoint = CGPointMake(.5f, 1.0f);
        
        // offset the flag annotation so that the flag pole rests on the map coordinate
       // returnedAnnotationView.centerOffset = CGPointMake( returnedAnnotationView.centerOffset.x + returnedAnnotationView.image.size.width, returnedAnnotationView.centerOffset.y );
    }
    else
    {
        returnedAnnotationView.annotation = annotation;
    }
    return returnedAnnotationView;
}



@end