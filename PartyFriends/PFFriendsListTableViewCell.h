//
//  PFFriendsListTableViewCell.h
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 12/4/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PFFriendsListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *friendNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *friendProfileImage;
@property (weak, nonatomic) IBOutlet UIButton *friendDeleteButtton;

@end
