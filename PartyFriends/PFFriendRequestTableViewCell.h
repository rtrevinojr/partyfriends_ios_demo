//
//  PFFriendRequestTableViewCell.h
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 12/4/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PFFriendRequestTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *friendRequestName;

@property (weak, nonatomic) IBOutlet UIImageView *friendRequestProfileImage;

@property (weak, nonatomic) IBOutlet UIButton *confirmRequestButton;

@property (weak, nonatomic) IBOutlet UIButton *deleteRequestButton;



@end
