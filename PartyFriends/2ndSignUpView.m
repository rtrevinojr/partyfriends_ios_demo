//
//  2ndSignUpView.m
//  PartyFriends
//
//  Created by Joshua Silva  on 6/24/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import "2ndSignUpView.h"
#import "Parse/Parse.h"
#import "UIView+Toast.h"
#import "Reach.h"
#import "LocationManagerSingleton.h"

@interface _ndSignUpView ()

@end

@implementation _ndSignUpView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.address.delegate = self;
    self.city.delegate = self;
    
    self.zipCode.delegate = self;
    
    
    self.currentUser = [PFUser currentUser];
    self.statePicker.delegate = self;
    
   _theStates = [[NSArray alloc]initWithObjects: @"State", @"AL",@"AK",@"AZ",@"AR",@"CA",@"CO",@"CT",@"DE",@"DC",@"FL",@"GA",@"HI",@"ID",@"IL",@"IN",@"IA",@"KS",@"KY",@"LA",@"ME",@"MD",@"MA",@"MI",@"MN",@"MS",@"MO",@"MT",@"NE",@"NV",@"NH",@"NJ",@"NM",@"NY",@"NC",@"ND",@"OH",@"OK",@"OR",@"PA",@"RI",@"SC",@"SD",@"TN",@"TX",@"UT",@"VT",@"VA",@"WA",@"WV",@"WI",@"WY",nil];
    
    self.privacyStatemant.hidden = YES;
    self.gotitButton.hidden = YES;
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
        [self.view setTintColor:[UIColor colorWithRed:(142/255.0) green:(83/255.0) blue:(249/255.0) alpha:1.0]];
    
    self.activityIndicator.color = [UIColor colorWithRed:(142/255.0) green:(83/255.0) blue:(249/255.0) alpha:1.0];
    self.activityIndicator.hidden = YES;
    
    //[PFUser logOut];
    
}

-(void) activityOffAction{
    self.activityIndicator.hidden = YES;
    [self.activityIndicator stopAnimating];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {

    return 1;

}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {

    return [self.theStates count];
}


-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {

    self.userState = [[NSString alloc] initWithFormat:@"%@", [self.theStates objectAtIndex:row]];


}



-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{

    return [self.theStates objectAtIndex:row];

}
- (IBAction)whyAction:(id)sender {
    UIAlertView * whyView = [[UIAlertView alloc]initWithTitle:@"Why your address?" message:@"Your privacy and safety are important to us. Submitting your home address will help protect your privacy at home." delegate:nil cancelButtonTitle:@"Okay!" otherButtonTitles: nil];
    [whyView show];
    
//    self.privacyStatemant.hidden = NO;
//    self.gotitButton.hidden = NO;
//    self.homeLabel.hidden = YES;
//    self.whyButton.hidden = YES;
    
}


- (IBAction)gotitAction:(id)sender {
    self.privacyStatemant.hidden = YES;
    self.gotitButton.hidden = YES;
    self.homeLabel.hidden = NO;
    self.whyButton.hidden = NO;
    
}

- (IBAction)backgroundTap:(id)sender {
    
    [self.view endEditing:YES];
    
}
- (IBAction)skip:(id)sender {
    
    
//    [self performSegueWithIdentifier:@"toBegins" sender:self];
    
    NSLog(@"still works");
    
}
- (IBAction)moreSignUp:(id)sender {
    
    
    
    if ([Reachability internetCheck]) {
        [sender setEnabled:NO];
        self.activityIndicator.hidden = NO;
        [self.activityIndicator startAnimating];
    
    
    
    NSDate * birthDay = [self.birthday date];
   __block NSString * address = self.address.text;
    __block NSString * zipString = [self.zipCode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSNumberFormatter * zipCode = [NSNumberFormatter new];
    zipCode.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber * zip = [zipCode numberFromString:self.zipCode.text];
    NSString * city = self.city.text;
   __block NSString * state = self.userState;
        
    
    
    self.accountInfo = [PFObject objectWithClassName:@"accountinfo"];
    
    NSRegularExpression * capitalRegex = [NSRegularExpression regularExpressionWithPattern:@"[A-Z]" options:0 error:nil];
   // NSRegularExpression * numberRegex = [NSRegularExpression regularExpressionWithPattern:@"[0-9]" options:0 error:nil];
    NSRegularExpression * letterRegex = [NSRegularExpression regularExpressionWithPattern:@"[a-z]" options:0 error:nil];
    
    
       NSUInteger zipCodeLetter = (unsigned long)[letterRegex matchesInString:zipString options:0 range: NSMakeRange(0, zipString.length)].count;
    NSUInteger zipCodeCapital = (unsigned long)[capitalRegex matchesInString:zipString options:0 range:NSMakeRange(0, zipString.length)].count;
    
    if (zipCodeCapital != 0 || zipCodeLetter != 0 ) {
        UIAlertView *error = [[UIAlertView alloc] initWithTitle:@"Zipcode Error" message:@"Numbers only in the zipcoed field " delegate:nil cancelButtonTitle:@"Thanks :)" otherButtonTitles: nil];
        [error show];
        [sender setEnabled:YES];
        [self activityOffAction];
        
    }else {
        if (zipString.length != 5 ) {
            UIAlertView *error = [[UIAlertView alloc] initWithTitle:@"Zipcode Error" message:@"Zipcode must contain only 5 digits" delegate:nil cancelButtonTitle:@"Thanks :)" otherButtonTitles: nil];
            [error show];
            [sender setEnabled:YES];
            [self activityOffAction];
        }else {if([state  isEqual:@"State"]){
        
            UIAlertView * stateError = [[UIAlertView alloc] initWithTitle:@"State Picker" message:@"Please select the state" delegate:nil cancelButtonTitle:@"Thank you" otherButtonTitles: nil];
            [stateError show];
            [sender setEnabled:YES];
            [self activityOffAction];
        
        
        }else{
        
            self.accountInfo[@"birthday"] = birthDay;
            self.accountInfo[@"state"] = state;
            self.accountInfo[@"city"] = city;
            self.accountInfo[@"address"] = address;
            self.accountInfo[@"zip"]= zip;
            self.accountInfo[@"createdBy"] = self.currentUser;
            
            
            
            
            self.accountInfo.ACL =[PFACL ACLWithUser:self.currentUser];
            
            
            [self.accountInfo  saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (error) {
                    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"sorry" message:[error.userInfo objectForKey:@"error"] delegate:nil cancelButtonTitle:@"Please try again" otherButtonTitles: nil];
                    
                    [alertview show];
                    [sender setEnabled:YES];
                    [self activityOffAction];
                }
                else{
                    
                    // self.currentUser[@"accountinfo"] = self.accountInfo;
                    
                    //[self.currentUser saveInBackground];
                    [sender setEnabled:YES];
                    [self activityOffAction];
                    NSString * addressString = [NSString stringWithFormat:@"%@ %@ %@", address, zipString , state];
                    
                    
                    CLGeocoder * geocoder = [CLGeocoder new];
                    [geocoder geocodeAddressString:addressString completionHandler:^(NSArray *placemarks, NSError *error) {
                        if (error) {
                            NSLog(@"%@", error);
                            
                        }else {
                            CLPlacemark * placemark = [placemarks lastObject];
                            CLLocationCoordinate2D homeCord = CLLocationCoordinate2DMake(placemark.location.coordinate.latitude, placemark.location.coordinate.longitude);
                            
                            CLRegion * homeRegion = [[CLCircularRegion alloc] initWithCenter:homeCord radius:200 identifier:@"home"];
                            
                            
    [[LocationManagerSingleton sharedSingleton].locationManager startMonitoringForRegion:homeRegion];
                            
                            
                            
//                            MKCoordinateRegion region;
//                            region.center.latitude = placemark.location.coordinate.latitude;
//                            region.center.longitude = placemark.location.coordinate.longitude;
//                            region.span = MKCoordinateSpanMake(spanX, spanY);
                            
                        }
                    }];
                    
                    
                    [self performSegueWithIdentifier:@"toAdd" sender:self];
                
                
                }
            }];
        }}
    
    }
    
    
}else if (![Reachability internetCheck]) {
        
        [self.view makeToast:@"Try again: No internet connection" duration:3 position:CSToastPositionCenter];
    [sender setEnabled:YES];
    }
    
//    self.accountInfo[@"birthday"] = birthDay;
//    self.accountInfo[@"state"] = state;
//    self.accountInfo[@"city"] = city;
//    self.accountInfo[@"address"] = address;
//    self.accountInfo[@"zip"]= zip;
//    self.accountInfo[@"createdBy"] = self.currentUser;
//
//    
//    
//    
//    self.accountInfo.ACL =[PFACL ACLWithUser:self.currentUser];
//    
//    
//    [self.accountInfo  saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//        if (error) {
//            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"sorry" message:[error.userInfo objectForKey:@"error"] delegate:nil cancelButtonTitle:@"I dont party enough!!" otherButtonTitles: nil];
//            [alertview show];
//        }
//        else{
//            
//           // self.currentUser[@"accountinfo"] = self.accountInfo;
//            
//            //[self.currentUser saveInBackground];
//            
//            [self performSegueWithIdentifier:@"toBegins" sender:self];}
//    }];
    
    
    
}


- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y-200);
    [self.scrollView setContentOffset:scrollPoint animated:YES];
    [self.birthday setUserInteractionEnabled:NO];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self.scrollView setContentOffset:CGPointZero animated:YES];
    [self.birthday setUserInteractionEnabled:YES];
    
    
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    
    [textField resignFirstResponder];
    [self.view endEditing:YES];
    return YES;
}
- (IBAction)skipButton:(id)sender {
    UIAlertView * skipAlert = [[UIAlertView alloc] initWithTitle:@"Are you sure?" message:@"Your privacy and safety are important to us. Submitting your home address will help protect your privacy at home." delegate:self cancelButtonTitle:@"Skip" otherButtonTitles:@"Enter address", nil];
    
    [skipAlert show];
    
//   [self performSegueWithIdentifier:@"toBegins" sender:self];
    
    
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if( 0 == buttonIndex ){ //cancel button
        
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        // NSLog(@"this shows this is a female ekkkk!!");
        [self performSegueWithIdentifier:@"toAdd" sender:self];
        
    } else if ( 1 == buttonIndex ){
        
        
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
        
        
    }
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
