//
//  SignUpView.h
//  PartyFriends
//
//  Created by Joshua Silva  on 6/24/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Parse/Parse.h"

@interface SignUpView : UIViewController
<UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>


@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UITextField *lastName;
@property (strong, nonatomic) IBOutlet UITextField *firstName;
@property (strong, nonatomic) IBOutlet UITextField *phoneNumber;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UITextField *passwordConfirm;
@property (strong, nonatomic) IBOutlet UITextField *eMail;
@property (strong, nonatomic) IBOutlet UITextField *confirmEmail;
@property (weak, nonatomic) IBOutlet UIPickerView *genderPicker;
@property (strong, nonatomic) IBOutlet UITextField *invitePin;

@property (weak, nonatomic) IBOutlet UITextField *confirmPhoneNumber;

@property (weak, nonatomic) NSString * membersNumber;
@property (strong, nonatomic) NSArray * genderString;
@property (strong, nonatomic) NSString * gender;
@property (strong, nonatomic) PFUser * firstFriend;

- (IBAction)newPF:(id)sender;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;


@end
