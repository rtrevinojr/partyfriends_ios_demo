//
//  AddFriend.m
//  PartyFriends
//
//  Created by Joshua Silva  on 6/25/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import "AddFriend.h"

#import <AddressBook/AddressBook.h>
#import "UIView+Toast.h"
#import <AddressBookUI/AddressBookUI.h>
#import <ParseUI/ParseUI.h>
#import "Reach.h"


@interface AddFriend ()

@end

@implementation AddFriend


- (void) loadContactsActivityIndicator
{
    NSLog(@"*** loadContactsActivityIndicator");
    
    [self.addContactsActivityIndicator startAnimating];
    
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.topLabel.layer.cornerRadius = 5;
    self.friendsName.layer.cornerRadius = 5;
    
    self.current = [PFUser currentUser];
    self.friendPhone.delegate = self;
     [self.view setTintColor:[UIColor colorWithRed:(142/255.0) green:(83/255.0) blue:(249/255.0) alpha:1.0]];
    
    [self.addFriendsFromContacts addTarget:self
                 action:@selector(loadContactsActivityIndicator)
       forControlEvents:UIControlEventTouchUpInside];
    
    [self.friendImage setHidden:YES];
    [self.friendsName setHidden:YES];
    //[self.search setHidden:YES];
    [self.activityIndicator setHidden:YES];
    [self.add setHidden:YES];
    [self.secondActivityIndicator setHidden:YES];
    
    CFErrorRef myError = NULL;
    ABAddressBookRef myAddressBook = ABAddressBookCreateWithOptions(NULL, &myError);
    
    
    
    
    //    ABAddressBookRef myAddressBook = ABAddressBookCreateWithOptions(nil, nil);
    //
    //    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined){
    //        ABAddressBookRequestAccessWithCompletion(myAddressBook, ^(bool granted, CFErrorRef error){
    //            if(granted){
    //                //AddressRequest was authorized
    //
    //                [self performSegueWithIdentifier:@"toContacts" sender:self];
    //            }else {
    //
    //                UIAlertController * deinedAccess = [UIAlertController alertControllerWithTitle:@"Contact Access Denied" message:@"If you decide to allow PartyFriends access to your contacts, go to your phones settings > PartyFriends" preferredStyle:UIAlertControllerStyleAlert];
    //
    //                [self presentViewController:deinedAccess animated:YES completion:nil];
    //            }
    //
    //        });
    //
    //
    //    }else if (ABAddressBookGetAuthorizationStatus()==kABAuthorizationStatusAuthorized){
    //        [self performSegueWithIdentifier:@"toContacts" sender:self];
    //
    //    }else {
    //        NSLog(@"no access!!!!syrup");
    //        //disply alert showing the contact access must be changed in app settings
    //        
    //    }
    
    
           if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined){
    
    ABAddressBookRequestAccessWithCompletion(myAddressBook, ^(bool granted, CFErrorRef error) {
        if (granted) {
            //NSArray * clist = CFBridgingRelease(ABAddressBookCopyPeopleWithName(myAddressBook, CFSTR("Trevino")));
            
            //_ContactsLst = [clist copy];
        }
        else {
            // Handle the case of being denied access and/or the error.
        }
        CFRelease(myAddressBook);
        
    });
           }else{
               //[self.friendsName setHidden:NO];
               if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
                   //self.friendsName.text = @"Authorized";
               }if (ABAddressBookGetAuthorizationStatus()==kABAuthorizationStatusDenied){
                   
                  // self.friendsName.text = @"Denied";
                   
               }
               
           
           
           }
    
    
    CALayer * imageLayer = self.friendImage.layer;
    [imageLayer setCornerRadius:5];
    [imageLayer setBorderWidth:0];
    [imageLayer setMasksToBounds:YES];
    
    [self.friendImage.layer setCornerRadius:self.friendImage.frame.size.width/2];
    [self.friendImage.layer setMasksToBounds:YES];
           
    
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    [self.addContactsActivityIndicator stopAnimating];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self.addContactsActivityIndicator stopAnimating];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)search:(id)sender {
    //    [self.activityIndicator setHidden:NO];
    //    [self.activityIndicator startAnimating];
    
    NSString *friendNumber = [self.friendPhone.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    //Find friends PFUser account object
    
    if ([Reachability internetCheck]) {
        if (friendNumber.length == 10){
            
            
            
            [self.activityIndicator setHidden:NO];
            [self.activityIndicator startAnimating];
            
            
            
            PFQuery *findFriend= [PFUser query];
            [findFriend whereKey:@"username" equalTo:friendNumber];
            // [findFriend includeKey:@"profilePic"];
            
            
            
            [findFriend findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                NSLog(@"shitdog %@",objects);
                if (!error){
                    
                    
                    
                    self.addedFriend =objects[0];
                    //PFUser * theAddedFriend = (PFUser *)objects[0];
                    PFObject * photo = self.addedFriend[@"profilePicture"];
                    
                    if (photo !=nil) {
                        [photo fetchInBackgroundWithBlock:^(PFObject * photoObject, NSError * error){
                            if (!error) {
                                PFFile * pic =[photoObject objectForKey:@"photo"];
                                PFImageView * userImage =[PFImageView new];
                                userImage.image = [UIImage imageNamed:@"userPic"];
                                userImage.file = (PFFile *) pic;
                                
                                [userImage loadInBackground:^(UIImage * image, NSError * error){
                                    if (!error) {
                                        image = [self squareImageFromImage:image scaledToSize:image.size.height];
                                        
                                        [self.friendImage setImage:image];
                                        [self.friendImage setHidden:NO];
                                        self.friendsName.text = self.addedFriend[@"firstName"];
                                        [self.friendsName setHidden:NO];
                                        [self.activityIndicator stopAnimating];
                                        [self.activityIndicator setHidden:YES];
                                        [self.add setHidden:NO];
                                        [self.add setEnabled:YES];
                                        [self.view endEditing:YES];
                                        
                                    }else{
                                        UIImage * image = [UIImage imageNamed:@"NoImage"];
                                        
                                        [self.friendImage setImage:image];
                                        [self.friendImage setHidden:NO];
                                        self.friendsName.text = self.addedFriend[@"firstName"];
                                        [self.friendsName setHidden:NO];
                                        [self.activityIndicator stopAnimating];
                                        [self.activityIndicator setHidden:YES];
                                        [self.add setHidden:NO];
                                        [self.add setEnabled:YES];
                                        [self.view endEditing:YES];
                                        
                                        
                                    }
                                    
                                }];
                                
                            }
                            
                            
                        }];
                    }else{
                        
                        UIImage * image = [UIImage imageNamed:@"NoImage"];
                        
                        [self.friendImage setImage:image];
                        [self.friendImage setHidden:NO];
                        self.friendsName.text = self.addedFriend[@"firstName"];
                        [self.friendsName setHidden:NO];
                        [self.activityIndicator stopAnimating];
                        [self.activityIndicator setHidden:YES];
                        [self.add setHidden:NO];
                        [self.add setEnabled:YES];
                        [self.view endEditing:YES];
                    }
                    
                    //PFFile * pic = [self.addedFriend objectForKey:@"profilePicture"];
                    //            [pic getDataInBackgroundWithBlock:^(NSData *data, NSError *error){
                    //                if (!error) {
                    //                    UIImage * image = [UIImage imageWithData:data];
                    //
                    //                    [self.friendImage setImage:image];
                    //                    [self.friendImage setHidden:NO];
                    //                    self.friendsName.text = self.addedFriend[@"firstName"];
                    //                    [self.friendsName setHidden:NO];
                    //                    [self.activityIndicator stopAnimating];
                    //                    [self.activityIndicator setHidden:YES];
                    //                    [self.add setHidden:NO];
                    //                    [self.add setEnabled:YES];
                    //                    [self.view endEditing:YES];
                    //                }else if (data==nil || error){
                    //                    UIImage * image = [UIImage imageNamed:@"NoImage"];
                    //
                    //                    [self.friendImage setImage:image];
                    //                    [self.friendImage setHidden:NO];
                    //                    self.friendsName.text = self.addedFriend[@"firstName"];
                    //                    [self.friendsName setHidden:NO];
                    //                    [self.activityIndicator stopAnimating];
                    //                    [self.activityIndicator setHidden:YES];
                    //                    [self.add setHidden:NO];
                    //                    [self.add setEnabled:YES];
                    //                    [self.view endEditing:YES];
                    //
                    //                }
                    ////                [self.activityIndicator setHidden:YES ];
                    ////                [self.activityIndicator startAnimating];
                    //
                    //            }];
                    
                    
                    
                    //checking if there is no previous pending request
                    //            PFQuery *friendRequest =[PFQuery queryWithClassName:@"friendrequest"];
                    //            [friendRequest whereKey:@"user1" equalTo:self.current];
                    //            [friendRequest whereKey:@"user2" equalTo:self.addedFriend];
                    //            [friendRequest whereKey:@"status" equalTo:@0];
                    //
                    //            [friendRequest findObjectsInBackgroundWithBlock:^(NSArray *objects1, NSError *error) {
                    //                NSLog(@"%@tncycftyntcfntufnu",objects1);
                    //                if (objects1.count==0){
                    //                    NSLog(@"%@",error);
                    //                    //sending new friendrequest
                    //                    PFObject *sendRequest=[PFObject objectWithClassName:@"friendrequest"];
                    //                    sendRequest[@"user1"]=self.current;
                    //                    sendRequest[@"user2"]=self.addedFriend;
                    //                    sendRequest[@"status"]=@0;
                    //                    [sendRequest saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                    //
                    //                        if(!error){
                    //
                    //                            NSLog(@"Request Sent!");
                    //                            [self.view makeToast:@"PF Added" duration:2.5 position:CSToastPositionCenter];
                    //
                    //                        }
                    //                        else if (error){
                    //
                    //                            NSLog(@"3%@%@",error,error.userInfo);
                    //                            NSString * message = [NSString stringWithFormat:(@"Error %@ -Try Again", error.userInfo)];
                    //
                    //                            [self.view makeToast:message duration:2.5 position:CSToastPositionCenter];
                    //                        }
                    //                    }];
                    //
                    //                }
                    //                else if (!error){
                    //
                    //                    NSLog(@"Request Still Pending:");
                    //
                    //
                    //                }
                    //                else {
                    //
                    //                    NSLog(@"2%@%@",error,error.userInfo);
                    //
                    //                }
                    //            }];
                    //
                    //
                }
                else if (error.code==101){
                    
                    NSLog(@"User Not in Party Friends");
                    [self.view makeToast:@"Not a PartyFriend" duration:2 position:CSToastPositionCenter];
                    [self.activityIndicator stopAnimating];
                    
                }
                else if (error){
                    
                    NSLog(@"%@%@",error,error.userInfo);
                    
                    [self.activityIndicator stopAnimating];
                    
                    
                }else{
                    
                    NSLog(@"waffles!!!!!");
                    [self.activityIndicator stopAnimating];
                    
                }
            }
             
             
             ];
            
        }else{
            [self.view makeToast:@"Phone number must be 10 digits" duration:3.0 position:CSToastPositionCenter];
            
        }
        
        
    }else if (![Reachability internetCheck]){
        [self.view makeToast:@"No internet connection" duration:2.5 position:CSToastPositionCenter];
        [self.activityIndicator stopAnimating];
        
        
    }
}


- (IBAction)add:(id)sender{
    
    
    [self.secondActivityIndicator setHidden:NO];
    [self.secondActivityIndicator startAnimating];
    //PFUser * theAddedFriend = (PFUser *)objects[0];
    
    //checking if there is no previous pending request
    if ([self.addedFriend.objectId isEqualToString:self.current.objectId]) {
        [self.view makeToast:@"Not friends with yourself?" duration:2.5 position:CSToastPositionCenter];
        
        [self.secondActivityIndicator  stopAnimating];
        [self.secondActivityIndicator setHidden:YES];
        
        
    }else{
    
    PFQuery *friendRequest =[PFQuery queryWithClassName:@"friendrequest"];
    [friendRequest whereKey:@"user1" equalTo:self.current];
    [friendRequest whereKey:@"user2" equalTo:self.addedFriend];
    [friendRequest whereKey:@"status" equalTo:@0];
    
    [friendRequest findObjectsInBackgroundWithBlock:^(NSArray *objects1, NSError *error) {
        NSLog(@"%@tncycftyntcfntufnu",objects1);
        if (objects1.count==0){
            NSLog(@"%@",error);
            //sending new friendrequest
            PFObject *sendRequest=[PFObject objectWithClassName:@"friendrequest"];
            sendRequest[@"user1"]=self.current;
            sendRequest[@"user2"]=self.addedFriend;
            sendRequest[@"status"]=@0;
            [sendRequest saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                
                if(!error){
                    NSString * addedfriend = [NSString stringWithFormat:@"%@ is now your PF!", self.addedFriend[@"firstName"]];
                    NSLog(@"Request Sent!");
                    [self.add setHidden:YES];
                    [self.friendsName setHidden:YES];
                    [self.friendImage setHidden:YES];
                    [self.view makeToast:addedfriend duration:2.5 position:CSToastPositionCenter];
                    [self.secondActivityIndicator  stopAnimating];
                    [self.secondActivityIndicator setHidden:YES];
                    
                }
                else if (error){
                    
                    NSLog(@"3%@%@",error,error.userInfo);
                    NSString * message = [NSString stringWithFormat:(@"Error %@ -Try Again", [[error userInfo] objectForKey:NSLocalizedDescriptionKey])];
                    
                    [self.view makeToast:message duration:2.5 position:CSToastPositionCenter];
                    [self.secondActivityIndicator  stopAnimating];
                    [self.secondActivityIndicator setHidden:YES];
                }
            }];
            
        }
        else if (!error){
            
            NSLog(@"Request Still Pending:");
            [self.view makeToast:@"Request still pending" duration:2 position:CSToastPositionCenter];
            [self.secondActivityIndicator  stopAnimating];
            [self.secondActivityIndicator setHidden:YES];
            
            
        }
        else {
            
            NSLog(@"2%@%@",error,error.userInfo);
            [self.secondActivityIndicator  stopAnimating];
            [self.secondActivityIndicator setHidden:YES];
            
        }
    }];
    
    }
//    NSString *friendNumber = [self.friendPhone.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//
//    //Find friends PFUser account object
//
//    PFQuery *findFriend= [PFUser query];
//    [findFriend whereKey:@"username" equalTo:friendNumber];
//    [findFriend findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
//        NSLog(@"%@",objects);
//        if (!error){
//         
//            self.addedFriend =objects[0];
//            //PFUser * theAddedFriend = (PFUser *)objects[0];
//            
//            //checking if there is no previous pending request
//            PFQuery *friendRequest =[PFQuery queryWithClassName:@"friendrequest"];
//            [friendRequest whereKey:@"user1" equalTo:self.current];
//            [friendRequest whereKey:@"user2" equalTo:self.addedFriend];
//            [friendRequest whereKey:@"status" equalTo:@0];
//            
//            [friendRequest findObjectsInBackgroundWithBlock:^(NSArray *objects1, NSError *error) {
//                NSLog(@"%@tncycftyntcfntufnu",objects1);
//                if (objects1.count==0){
//                    NSLog(@"%@",error);
//                    //sending new friendrequest
//                    PFObject *sendRequest=[PFObject objectWithClassName:@"friendrequest"];
//                    sendRequest[@"user1"]=self.current;
//                    sendRequest[@"user2"]=self.addedFriend;
//                    sendRequest[@"status"]=@0;
//                    [sendRequest saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
//                        
//                            if(!error){
//                            
//                                NSLog(@"Request Sent!");
//                                [self.view makeToast:@"PF Added" duration:2.5 position:CSToastPositionCenter];
//                                
//                            }
//                            else if (error){
//                                
//                                NSLog(@"3%@%@",error,error.userInfo);
//                                NSString * message = [NSString stringWithFormat:(@"Error %@ -Try Again", error.userInfo)];
//                                
//                                [self.view makeToast:message duration:2.5 position:CSToastPositionCenter];
//                            }
//                    }];
//                    
//                }
//                else if (!error){
//                    
//                    NSLog(@"Request Still Pending:");
//                    
//                    
//                }
//                else {
//                    
//                    NSLog(@"2%@%@",error,error.userInfo);
//                    
//                }
//            }];
//         
//     
//             }
//     else if (error.code==101){
//     
//         NSLog(@"User Not in Party Friends");
//         
//     }
//     else if (error){
//         
//         NSLog(@"%@%@",error,error.userInfo);
//         
//     }else{
//     
//     NSLog(@"waffles!!!!!");
//     
//     }
//    }
//     
//    
//     ];

    
}





- (IBAction)AddFriendFromContacts:(id)sender {
    
    
    NSLog(@"********** AddFriendFromContacts");
    
    ABAddressBookRef myAddressBook = ABAddressBookCreateWithOptions(nil, nil);
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined){
        
        ABAddressBookRequestAccessWithCompletion(myAddressBook, ^(bool granted, CFErrorRef error) {
            if (granted) {
                //NSArray * clist = CFBridgingRelease(ABAddressBookCopyPeopleWithName(myAddressBook, CFSTR("Trevino")));
                
                //_ContactsLst = [clist copy];
                
                
                [self performSegueWithIdentifier:@"invite" sender:self];
            }
            else {
                // Handle the case of being denied access and/or the error.
                [self performSegueWithIdentifier:@"invite" sender:self];
            }
            CFRelease(myAddressBook);
            
        });
    }else{
       // [self.friendsName setHidden:NO];
        if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
            [self performSegueWithIdentifier:@"invite" sender:self];
            //self.friendsName.text = @"Authorized";
        }if (ABAddressBookGetAuthorizationStatus()==kABAuthorizationStatusDenied){
            [self performSegueWithIdentifier:@"invite" sender:self];
            
            //self.friendsName.text = @"Denied";
            
        }
        
    }
    
    
    [self.addContactsActivityIndicator startAnimating];
    
 
    
    
}

- (IBAction)backgroundTap:(id)sender {
    
    [self.view endEditing:YES];
    
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    [self.view endEditing:YES];
    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textView
{
    
    [self.friendImage setHidden:YES];
    [self.friendsName setHidden:YES];
    [self.activityIndicator setHidden:YES];
    [self.activityIndicator stopAnimating];
    [self.secondActivityIndicator setHidden:YES];
    [self.secondActivityIndicator stopAnimating];
    [self.add setHidden:YES];
    [self.add setEnabled:NO];
    textView.text = @"";
    
}

// for later use: get the rotation in place

/*- (NSUInteger)supportedInterfaceOrientations {
    
    return (UIInterfaceOrientationMaskPortraitUpsideDown);
    
    
    
}*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"invite"]){
        //[[segue destinationViewController] setBarSelection:BarName];
        //[[segue destinationViewController] setBarFKeySelection:BarKey];
        //SLog(@"Bar Name Selection: %@", BarName);
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (UIImage *)squareImageFromImage:(UIImage *)image scaledToSize:(CGFloat)newSize {
    CGAffineTransform scaleTransform;
    CGPoint origin;
    
    if (image.size.width > image.size.height) {
        CGFloat scaleRatio = newSize / image.size.height;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(-(image.size.width - image.size.height) / 2.0f, 0);
    } else {
        CGFloat scaleRatio = newSize / image.size.width;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(0, -(image.size.height - image.size.width) / 2.0f);
    }
    
    CGSize size = CGSizeMake(newSize, newSize);
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(size, YES, 0);
    } else {
        UIGraphicsBeginImageContext(size);
    }
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextConcatCTM(context, scaleTransform);
    
    [image drawAtPoint:origin];
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}
@end
