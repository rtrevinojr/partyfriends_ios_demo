//
//  TableViewController.m
//  party2
//
//  Created by Gilberto Silva on 4/25/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import "RequestTableViewController.h"

#import "PFFriendRequestTableViewCell.h"
#import <ParseUI/ParseUI.h>
#import "UIView+Toast.h"

@interface RequestTableViewController ()

@end

@implementation RequestTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.currentUser=[PFUser currentUser];
    PFQuery *query = [PFQuery queryWithClassName:@"friendrequest"];
    
    
    [query whereKey:@"user2" equalTo:self.currentUser];
    [query whereKey:@"status" equalTo:@0];
    [query includeKey:@"user1"];
    [query orderByDescending:@"createdAt"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error)
        {
            
        }
        else
        {
            NSLog(@"/n %@",objects);
            self.requests= [[NSMutableArray alloc] initWithArray: objects];
            self.updatedRequest = [NSMutableArray new];
            for(int i = 0; i < self.requests.count; i++){
                PFObject *request = [self.requests objectAtIndex:i];
                if(request[@"viewed"] == nil || request[@"viewed"] == false){
                    request[@"viewed"] = @NO ;
                    [self.updatedRequest addObject:request];
                }
                
            }
            
            if (self.updatedRequest.count > 0) {
                [PFObject saveAllInBackground:self.updatedRequest block:^(BOOL good, NSError * error){
                
                    if (error) {
                        
                    }else {
                    
                    
                    }
                
                }];
            }
            self.requeste=[_requests valueForKey:@"user1"];
            NSLog(@"%@",_requeste);
            [self.tableView reloadData];
            
        }
    }];
    
    
        NSString * roleName = [NSString stringWithFormat:@"Role-%@", self.currentUser.objectId];
    
    
    PFQuery * queryRole = [PFRole query];
    [queryRole whereKey:@"name" equalTo:roleName];
    [queryRole getFirstObjectInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
        if (error || object == nil) {
            NSLog(@"this error belongs to the role assigning block -- %@", error);
        }else{
            self.role = (PFRole * )object;
            
            self.roleRelation = self.role.users;
            
            
        
        }
    }];
    
        self.exclusiveRelation = [[PFUser currentUser] relationForKey:@"exclusive"];
    
    
    
    
    // Uncomment the following line to preserve selection between presentations.
     //self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
     //self.navigationItem.rightBarButtonItem = self.editButtonItem;


}

-(void)viewWillAppear:(BOOL)animated  {
    [super viewWillAppear:YES];
    
    PFQuery * queryExclusive = [self.exclusiveRelation query];
    [queryExclusive orderByAscending:@"firstName"];
    [queryExclusive findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if (!error) {
            self.exclusiveArray = [NSMutableArray arrayWithArray:objects];
        }else{
        
        
        }
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.requeste count];
}


- (PFFriendRequestTableViewCell *) tableView: (UITableView *) tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    static NSString * cellIdentifier = @"requestcell";
    
    PFFriendRequestTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[PFFriendRequestTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    PFUser * friend = [self.requeste objectAtIndex:indexPath.row];
    
    NSString * firstName = [friend objectForKey:@"firstName"];
    NSString * lastName = [friend objectForKey:@"lastName"];
    NSString * fullName = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
    
    CALayer *imageLayer = cell.friendRequestProfileImage.layer;
    [imageLayer setCornerRadius:5];
    [imageLayer setBorderWidth:0];
    [imageLayer setMasksToBounds:YES];
    [cell.friendRequestProfileImage.layer setCornerRadius:cell.friendRequestProfileImage.frame.size.width/2];
    [cell.friendRequestProfileImage.layer setMasksToBounds:YES];
    
    
    PFObject * photo = friend[@"profilePicture"];
    
    if (photo !=nil) {
        [photo fetchInBackgroundWithBlock:^(PFObject * photoObject, NSError * error){
            if (!error) {
                PFFile * pic =[photoObject objectForKey:@"photo"];
                PFImageView * userImage =[PFImageView new];
                userImage.image = [UIImage imageNamed:@"userPic"];
                userImage.file = (PFFile *) pic;
                
                [userImage loadInBackground:^(UIImage * image, NSError * error){
                    if (!error) {
                        image = [self squareImageFromImage:image scaledToSize:image.size.height];
                        [cell.friendRequestProfileImage setImage:image];
                    }else {
                        
                        UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
                        [cell.friendRequestProfileImage setImage:defaultimg];
                        
                    }
                    
                }];
            }
            
            
        }]; }else{
            UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
            [cell.friendRequestProfileImage setImage:defaultimg];
        
        }
    
//    if (friend[@"profilePic"] != nil) {
//        PFFile * pic = [friend objectForKey:@"profilePic"];
//        [pic getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
//            if (!error) {
//                UIImage *image = [UIImage imageWithData:data];
//                [cell.friendRequestProfileImage setImage:image];
//            }
//            else {
//                UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
//                [cell.friendRequestProfileImage setImage:defaultimg];
//            }
//        }];
//    }
//    else {
//        UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
//        [cell.friendRequestProfileImage setImage:defaultimg];
//    }
    
    cell.friendRequestName.text = fullName;
    
    
    return cell;
    
}


/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellid=@"cell";
    UITableViewCell *cell= [tableView dequeueReusableCellWithIdentifier:cellid forIndexPath:indexPath];
   
    // Configure the cell...
    PFUser *friend=[self.requeste objectAtIndex:indexPath.row];
    NSString *name=[friend objectForKey:@"firstName"];
    cell.textLabel.text=name;
    
    UIButton * acecept = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    acecept.frame = CGRectMake(cell.bounds.origin.x + 200, cell.bounds.origin.y + 20 , 50, 30);
    [acecept setTitle: @"Accept" forState:UIControlStateNormal];
    [acecept addTarget:self action:@selector(accept:) forControlEvents:UIControlEventTouchUpInside];
    acecept.backgroundColor = [UIColor whiteColor];
    acecept.tag = 1;
    [cell.contentView addSubview:acecept];
    
    
    UIButton * deny = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    deny.frame = CGRectMake(cell.bounds.origin.x + 250, cell.bounds.origin.y + 20 , 50, 30);
    [deny setTitle: @"Deny" forState:UIControlStateNormal];
    [deny addTarget:self action:@selector(deny:) forControlEvents:UIControlEventTouchUpInside];
    deny.backgroundColor = [UIColor whiteColor];
    deny.tag = 2;
    [cell.contentView addSubview:deny];
    
    return cell;
    
    
}
 */

-(void)addToExclusive{


    PFRelation *addFriend=[self.currentUser relationForKey:@"friends"];
    PFUser *newFriend= [self.requeste objectAtIndex:_index.row];
    [addFriend addObject:newFriend];
    NSString * messageString = [NSString stringWithFormat:@"You are currently in exclusive mode, would you like %@ to see your location?", newFriend[@"firstName"]];
    
    UIAlertController * exclusive = [UIAlertController alertControllerWithTitle:@"New" message:messageString preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * accepted= [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
        [self.currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            
            if(error)
            {
                NSLog(@"%@%@ 1",error,error.userInfo);
            }
            else
            {
                PFObject *requestId=[self.requests objectAtIndex:_index.row];
                NSLog(@"%@",requestId.objectId);
                PFQuery *updateRequest=[PFQuery queryWithClassName:@"friendrequest"];
                [updateRequest getObjectInBackgroundWithId:requestId.objectId block:^(PFObject *object, NSError *error) {
                    if (error) {
                        
                        NSLog(@"%@%@ 2",error,error.userInfo);
                    }
                    
                    //object[@"update"]= [NSNumber numberWithBool:YES];
                    object[@"status"]=@1;
                    NSLog(@"Friend Added!");
                    
                    
                    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                        if (!error) {
                            
                            [self.requeste removeObjectAtIndex:_index.row];
                            [self.requests removeObjectAtIndex:self.index.row];
                            [self.tableView reloadData];
                            [self.roleRelation addObject:newFriend];
                            
                            [self.role saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                                if(!error){
                                    
                                    
                                }else{
                                    
                                    
                                }
                            }];
                            
                        }else{
                            
                            
                        }
                    }];
                    
                    
                    
                }];        }
        }];

        
        
        
    }];
    
    NSLog(@"is this being called?!?!?!?*****&^#");
    
    [self.currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        if(error)
        {
            NSLog(@"%@%@ 1",error,error.userInfo);
        }
        else
        {
            PFObject *requestId=[self.requests objectAtIndex:_index.row];
            NSLog(@"%@",requestId.objectId);
            PFQuery *updateRequest=[PFQuery queryWithClassName:@"friendrequest"];
            [updateRequest getObjectInBackgroundWithId:requestId.objectId block:^(PFObject *object, NSError *error) {
                if (error) {
                    
                    NSLog(@"%@%@ 2",error,error.userInfo);
                }
                
                //object[@"update"]= [NSNumber numberWithBool:YES];
                object[@"status"]=@1;
                NSLog(@"Friend Added!");
                
                
                [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                    if (!error) {
                        
                        [self.requeste removeObjectAtIndex:_index.row];
                        [self.requests removeObjectAtIndex:self.index.row];
                        [self.tableView reloadData];
                        [self.roleRelation addObject:newFriend];
                        
                        [self.role saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                            if(!error){
                                
                                
                            }else{
                                
                                
                            }
                        }];
                        
                    }else{
                        
                        
                    }
                }];
                
                
                
            }];        }
    }];

}

-(void)notOnExclusive{

    PFRelation *addFriend=[self.currentUser relationForKey:@"friends"];
    PFUser *newFriend= [self.requeste objectAtIndex:_index.row];
    [addFriend addObject:newFriend];
    
    
    NSLog(@"is this being called?!?!?!?*****&^#");
    
    
    
    
//    [self.currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//        
//        if(error)
//        {
//            NSLog(@"%@%@ 1",error,error.userInfo);
//        }
//        else
//        {
//            PFObject *requestId=[self.requests objectAtIndex:_index.row];
//            NSLog(@"%@",requestId.objectId);
//            PFQuery *updateRequest=[PFQuery queryWithClassName:@"friendrequest"];
//            [updateRequest getObjectInBackgroundWithId:requestId.objectId block:^(PFObject *object, NSError *error) {
//                if (error) {
//                    
//                    NSLog(@"%@%@ 2",error,error.userInfo);
//                }
//                
//                //object[@"update"]= [NSNumber numberWithBool:YES];
//                object[@"status"]=@1;
//                NSLog(@"Friend Added!");
//                
//                
//                [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
//                    if (!error) {
//                        
//                        [self.requeste removeObjectAtIndex:_index.row];
//                        [self.requests removeObjectAtIndex:self.index.row];
//                        [self.tableView reloadData];
//                        [self.roleRelation addObject:newFriend];
//                        
//                        [self.role saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
//                            if(!error){
//                                
//                                
//                            }else{
//                                
//                                
//                            }
//                        }];
//                        
//                    }else{
//                        
//                        
//                    }
//                }];
//                
//                
//                
//            }];        }
//    }];
}


-(IBAction)accept:(id)sender {
    NSLog(@"%ld",(long)_index);
    PFRelation *addFriend=[self.currentUser relationForKey:@"friends"];
    PFUser *newFriend= [self.requeste objectAtIndex:_index.row];
    [addFriend addObject:newFriend];
    
    
    NSLog(@"is this being called?!?!?!?*****&^#");
    
    if ([self.currentUser[@"status"] isEqual:@2]) {
        
    
    NSString * messageString = [NSString stringWithFormat:@"You are currently in Exclusive mode, would you like %@ to see your location?", newFriend[@"firstName"]];
    
    UIAlertController * exclusive = [UIAlertController alertControllerWithTitle:@"Share location" message:messageString preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * noExclusive = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
       
        
        [self.currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            
            if(error)
            {
                NSLog(@"%@%@ 1",error,error.userInfo);
            }
            else
            {
                PFObject *requestId=[self.requests objectAtIndex:_index.row];
                NSLog(@"%@",requestId.objectId);
                PFQuery *updateRequest=[PFQuery queryWithClassName:@"friendrequest"];
                [updateRequest getObjectInBackgroundWithId:requestId.objectId block:^(PFObject *object, NSError *error) {
                    if (error) {
                        
                        NSLog(@"%@%@ 2",error,error.userInfo);
                    }
                    
                    //object[@"update"]= [NSNumber numberWithBool:YES];
                    object[@"status"]=@1;
                    NSLog(@"Friend Added!");
                    
                    
                    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                        if (!error) {
                            
                            [self.requeste removeObject:newFriend];
                            [self.requests removeObjectAtIndex:self.index.row];
                            [self.tableView reloadData];
                            [self.roleRelation addObject:newFriend];
                            
                            [self.role saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                                if(!error){
                                    NSString * added = [NSString stringWithFormat:@"%@ add",newFriend[@"firstName"] ];
                                    [self.view makeToast:added duration:2.5 position:CSToastPositionCenter];
                                    
                                }else{
                                    
                                    
                                }
                            }];
                            
                        }else{
                            
                            
                        }
                    }];
                    
                    
                    
                }];        }
        }];

        
    }];
    
    
    UIAlertAction * accepted= [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self.exclusiveRelation addObject:newFriend];
        
        [self.currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            
            if(error)
            {
                NSLog(@"%@%@ 1",error,error.userInfo);
            }
            else
            {
                PFObject *requestId=[self.requests objectAtIndex:_index.row];
                NSLog(@"%@",requestId.objectId);
                PFQuery *updateRequest=[PFQuery queryWithClassName:@"friendrequest"];
                [updateRequest getObjectInBackgroundWithId:requestId.objectId block:^(PFObject *object, NSError *error) {
                    if (error) {
                        
                        NSLog(@"%@%@ 2",error,error.userInfo);
                    }
                    
                    //object[@"update"]= [NSNumber numberWithBool:YES];
                    object[@"status"]=@1;
                    NSLog(@"Friend Added!");
                    
                    
                    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                        if (!error) {
                            
                            [self.requeste removeObjectAtIndex:_index.row];
                            [self.requests removeObjectAtIndex:self.index.row];
                            [self.tableView reloadData];
                            [self.roleRelation addObject:newFriend];
                            
                            [self.role saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                                if(!error){
                                    
                                    NSString * added = [NSString stringWithFormat:@"%@ add",newFriend[@"firstName"] ];
                                    [self.view makeToast:added duration:2.5 position:CSToastPositionCenter];
                                    
                                }else{
                                    
                                    
                                }
                            }];
                            
                        }else{
                            
                            
                        }
                    }];
                    
                    
                    
                }];        }
        }];
        
        
        
        
    }];

    
    [exclusive addAction:accepted];
    [exclusive addAction:noExclusive];
    
    [self presentViewController:exclusive animated:YES completion:nil];
    
    
    }else{
    
        [self.currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            
            if(error)
            {
                NSLog(@"%@%@ 1",error,error.userInfo);
            }
            else
            {
                PFObject *requestId=[self.requests objectAtIndex:_index.row];
                NSLog(@"%@",requestId.objectId);
                PFQuery *updateRequest=[PFQuery queryWithClassName:@"friendrequest"];
                [updateRequest getObjectInBackgroundWithId:requestId.objectId block:^(PFObject *object, NSError *error) {
                    if (error) {
                        
                        NSLog(@"%@%@ 2",error,error.userInfo);
                    }
                    
                    //object[@"update"]= [NSNumber numberWithBool:YES];
                    object[@"status"]=@1;
                    NSLog(@"Friend Added!");
                    
                    
                    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                        if (!error) {
                            
                            [self.requeste removeObjectAtIndex:_index.row];
                            [self.requests removeObjectAtIndex:self.index.row];
                            [self.tableView reloadData];
                            [self.roleRelation addObject:newFriend];
                            
                            [self.role saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                                if(!error){
                                    
                                    NSString * added = [NSString stringWithFormat:@"%@ add",newFriend[@"firstName"] ];
                                    [self.view makeToast:added duration:2.5 position:CSToastPositionCenter];
                                }else{
                                    
                                    
                                }
                            }];
                            
                        }else{
                            
                            
                        }
                    }];
                    
                    
                    
                }];        }
        }];
    
    
    
    }

//    [self.currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//        
//        if(error)
//        {
//            NSLog(@"%@%@ 1",error,error.userInfo);
//        }
//        else
//        {
//            PFObject *requestId=[self.requests objectAtIndex:_index.row];
//            NSLog(@"%@",requestId.objectId);
//            PFQuery *updateRequest=[PFQuery queryWithClassName:@"friendrequest"];
//            [updateRequest getObjectInBackgroundWithId:requestId.objectId block:^(PFObject *object, NSError *error) {
//                if (error) {
//                    
//                    NSLog(@"%@%@ 2",error,error.userInfo);
//                }
//                
//                //object[@"update"]= [NSNumber numberWithBool:YES];
//                object[@"status"]=@1;
//                NSLog(@"Friend Added!");
//                
//                
//                [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
//                    if (!error) {
//                     
//                    [self.requeste removeObjectAtIndex:_index.row];
//                    [self.requests removeObjectAtIndex:self.index.row];
//                    [self.tableView reloadData];
//                        [self.roleRelation addObject:newFriend];
//                        
//                        [self.role saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
//                            if(!error){
//                            
//                            
//                            }else{
//                            
//                            
//                            }
//                        }];
//                        
//                    }else{
//                    
//                    
//                    }
//                }];
//                
//                
//                
//            }];        }
//    }];
    
    
    UIButton *aceceptBtn = (UIButton *) sender;
    [aceceptBtn setHidden:YES];
    
    UIButton *denyBtn = [aceceptBtn.superview viewWithTag:2];
    [denyBtn setHidden:YES];
    

    
    
 }

- (UIImage *)squareImageFromImage:(UIImage *)image scaledToSize:(CGFloat)newSize {
    CGAffineTransform scaleTransform;
    CGPoint origin;
    
    if (image.size.width > image.size.height) {
        CGFloat scaleRatio = newSize / image.size.height;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(-(image.size.width - image.size.height) / 2.0f, 0);
    } else {
        CGFloat scaleRatio = newSize / image.size.width;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(0, -(image.size.height - image.size.width) / 2.0f);
    }
    
    CGSize size = CGSizeMake(newSize, newSize);
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(size, YES, 0);
    } else {
        UIGraphicsBeginImageContext(size);
    }
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextConcatCTM(context, scaleTransform);
    
    [image drawAtPoint:origin];
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}
 
     
 
- (IBAction)deny:(id)sender {
    
    NSLog(@"OR is this being called?!?!?!");
    
    PFObject *deniedReq =[self.requests objectAtIndex:_index.row];
    PFQuery *denied =[PFQuery queryWithClassName:@"friendrequest"];
    [denied getObjectInBackgroundWithId:deniedReq.objectId block:^(PFObject *object, NSError *error) {
        if (error) {
            NSLog(@"%@%@3",error,error.userInfo);
            
        }
        else{
            
            //object[@"update"]= [NSNumber numberWithBool:YES];
            object[@"status"]=@2;
            [object saveInBackground];
            
            [self.requeste removeObjectAtIndex:_index.row];
            [self.requests removeObjectAtIndex:self.index.row];
          [self.tableView reloadData];
            
        }
    }
    
    
     ];
    
    
    UIButton *denyBtn = (UIButton *) sender;
    [denyBtn setHidden:YES];
    
    UIButton *aceceptBtn = [denyBtn.superview viewWithTag:1];
    [aceceptBtn setHidden:YES];
    
    
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

   


}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/




@end
