//
//  Gradient.h
//  PartyFriends
//
//  Created by Joshua Silva  on 2/26/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface Gradient : NSObject

+(CAGradientLayer *) random;

+(CAGradientLayer *) gold;

+(CAGradientLayer *) goldTwo;

+(CAGradientLayer  *) white;

+(CAGradientLayer  *) black;



@end
