//
//  AppDelegate.m
//  PartyFriends
//
//  Created by Joshua Silva  on 6/23/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import "AppDelegate.h"
#import "PFCommentHistoryVC.h"
#import "ActionView.h"
@import GoogleMaps;


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
 
    [NSThread sleepForTimeInterval:1];

    // [Optional] Power your app with Local Datastore. For more info, go to
    // https://parse.com/docs/ios_guide#localdatastore/iOS
    [Parse enableLocalDatastore];
    
    // Initialize Parse.
    [Parse setApplicationId:@"SQn4MaZU2X4VIUxzHCXW3Ez7jPwjZ7QAaoDngQIH"
        clientKey:@"4cjwOolEAbUy8XcPTtYlrKuKvxZdK4eQeAxfcXCZ"];
    
    [PFUser enableRevocableSessionInBackground];
    
    [GMSServices provideAPIKey:@"AIzaSyCm0CNN1356H4u_a2mxacgY1fCcTUAjAoc"];
    
    // [Optional] Track statistics around application opens.
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    if (![[UIDevice currentDevice] isBatteryMonitoringEnabled]){
        [[UIDevice currentDevice] setBatteryMonitoringEnabled:YES];
    
    }
    
    // ...
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
   // NSDictionary * notificationPayload = launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey];

    
    
    
    
    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                    UIUserNotificationTypeBadge |
                                                    UIUserNotificationTypeSound);
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                             categories:nil];
    [application registerUserNotificationSettings:settings];
    [application registerForRemoteNotifications];
    
   // NSDictionary * notificationPayload = launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber=0;
    
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    

    
    UIViewController *loginController = [storyboard instantiateViewControllerWithIdentifier:@"Login"];
    UIViewController * actionController = [storyboard instantiateViewControllerWithIdentifier:@"Action"];

    NSDictionary * notificationPayload = [launchOptions objectForKey: UIApplicationLaunchOptionsRemoteNotificationKey];
    self.currentUser = [PFUser currentUser];

    NSLog(@"sajfhadfjhajf \n \n \n waffles - %@ ", self.currentUser.username);
    

    if(self.currentUser != nil){
       // NSLog(@"loggin the \n NSDictionary with the payload \n %@", notificationPayload);
        self.window.rootViewController = actionController;
        
        ActionView * actionPush = (ActionView *) self.window.rootViewController;
        if (launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey ] || notificationPayload != nil) {

  //  NSDictionary * notificationPayload = [launchOptions objectForKey: UIApplicationLaunchOptionsRemoteNotificationKey];
            
            NSString * pushType = [notificationPayload objectForKey:@"type"];
            NSString * pushUser = [notificationPayload objectForKey:@"user"];
            
//            PFUser * userPush = [PFUser objectWithoutDataWithClassName:[PFObject ] objectId:payload];
//            [userPush fetchIfNeededInBackgroundWithBlock:^(PFObject * user, NSError * error){
//                if (error){[self.window makeKeyAndVisible];}else{
//
//                    
            
            
                    
            actionPush.pushUser = pushUser;
            actionPush.pushType = pushType;
               [self.window makeKeyAndVisible];
//
//
//                }
//                
//            }];
        }else {
         //actionPush.pushType = @"comment";
       //  actionPush.pushUser = @"random";
        
            [self.window makeKeyAndVisible];}
    
    }else{
    self.window.rootViewController = loginController;
    [self.window makeKeyAndVisible];
        
    }
    
    [application setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    
    return YES;
}

-(void) application:(UIApplication *)application performFetchWithCompletionHandler:(nonnull void (^)(UIBackgroundFetchResult))completionHandler{
    
//    if([self.currentUser[@"status"] isEqual:@2]){
//    
//        NSLog(@"current user status == %@", self.currentUser[@"status"]);
//        self.currentUser[@"status"] = @1;
//        
//        
//        [self.currentUser save];
//        NSLog(@"current user status ==  %@", self.currentUser[@"status"]);
//        completionHandler(UIBackgroundFetchResultNewData);
//    }else if ([self.currentUser[@"status"] isEqual:@1]){
//    
//        
//        NSLog(@"current user status == %@", self.currentUser[@"status"]);
//        self.currentUser[@"status"] = @0;
//        
//        
//        [self.currentUser save];
//        NSLog(@"current user status ==  %@", self.currentUser[@"status"]);
//    completionHandler(UIBackgroundFetchResultNewData);
//    
//    }else{
//        NSLog(@"current user status == %@", self.currentUser[@"status"]);
//        self.currentUser[@"status"] = @4;
//        
//        
//        [self.currentUser save];
//        NSLog(@"current user status ==  %@", self.currentUser[@"status"]);
//    completionHandler(UIBackgroundFetchResultNewData);
//    
//    }
    
    
    
//    NSLog(@"current user status == %@", self.currentUser[@"status"]);
//    self.currentUser[@"status"] = @2;
//  
//    
//    [self.currentUser save];
//  NSLog(@"current user status ==  %@", self.currentUser[@"status"]);
//   // completionHandler(UIBackgroundFetchResultNewData);
    
}



- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    currentInstallation.channels = @[ @"global" ];
    [currentInstallation saveInBackground];
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [PFPush handlePush:userInfo];
    
    
    
    
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
   // NSTimer * backCheck = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(cereal) userInfo:nil repeats:YES];
    NSLog(@"app is now in the background! %@", self.currentUser[@"status"]);
    
////    self.currentUser[@"status"] = @0;
////    NSLog(@"\n \n \n \n \n has has entered the background?!?!?");
////    [self.currentUser saveInBackgroundWithBlock:^(BOOL good, NSError * error){
////        if (good) {
////            NSLog(@"seems to have gone through!!!! check the backend!!!");
////            [self cereal];
////        }else {
////        
////            NSLog(@"didnt go through but why, %ld", (long)error.code);
////        
////        }
//    
//    }];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    NSLog(@"this is the log for the installation %@", currentInstallation);
    if (currentInstallation.badge != 0) {
        currentInstallation.badge = 0;
        [currentInstallation saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (error) {
                [currentInstallation saveEventually];
            }
        }];
    }
    // ...
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}



- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "PartyFriends.PartyFriends" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"PartyFriends" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"PartyFriends.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

-(void)cereal{

    NSLog(@"checking the time remaining for the background updates %f", [[UIApplication sharedApplication] backgroundTimeRemaining]);

}

@end
