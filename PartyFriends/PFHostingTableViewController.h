//
//  PFHostingTableViewController.h
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 11/16/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PFHostingTableViewController : UITableViewController <UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource>


@property (weak, nonatomic) IBOutlet UISegmentedControl *partyTypeSelection;

@property (assign, nonatomic) Boolean invitationBool;

@property (strong, nonatomic) NSString * invitationMode;


@property (strong, nonatomic) NSArray * inviteList;

@property (strong, nonatomic) NSArray * hostingList;

@property (strong, nonatomic) UIRefreshControl * resfreshControl;


@end
