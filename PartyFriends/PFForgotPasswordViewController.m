//
//  PFForgotPasswordViewController.m
//  PartyFriends
//
//  Created by Joshua Silva  on 1/14/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import "PFForgotPasswordViewController.h"
#import "UIView+Toast.h"
#import "Reach.h"
#import "PFContactUsView.h"

@interface PFForgotPasswordViewController ()

@end

@implementation PFForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.emailAddress.delegate = self;
    self.emailTextView.clipsToBounds = YES;
    self.emailTextView.layer.cornerRadius = 5.0f;
    
        [self.view setTintColor:[UIColor colorWithRed:(142/255.0) green:(83/255.0) blue:(249/255.0) alpha:1.0]];
    self.activityIndicator.color = [UIColor colorWithRed:(142/255.0) green:(83/255.0) blue:(249/255.0) alpha:1.0];
    self.activityIndicator.hidden = YES;
    
    
    
}

-(void) activityOffAction{
    self.activityIndicator.hidden = YES;
    [self.activityIndicator stopAnimating];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)send:(id)sender {
    
    
    if ([Reachability internetCheck]) {
        [sender setEnabled:NO];
        self.activityIndicator.hidden = NO;
        [self.activityIndicator startAnimating];
        
    
    
    NSString * email = self.emailAddress.text;
    NSString * emailSent = [NSString stringWithFormat:@"Email sent to %@", email];
    
    [PFUser requestPasswordResetForEmailInBackground:email block:^(BOOL success, NSError * error){
    
        if (error){
            if (error.code == 204){
                [self.view makeToast:@"Email not Found" duration:2 position:CSToastPositionCenter];
            }
           // [self.view makeToast:@"Check email" duration:2 position:CSToastPositionCenter];
            [sender setEnabled:YES];
            [self activityOffAction];
            
        
        }else{
            [self.view makeToast:emailSent duration:2 position:CSToastPositionCenter];
            [sender setEnabled:YES];
            [self activityOffAction];
        }
    
        
    
    }];
    }else if (![Reachability internetCheck]){
    
        [self.view makeToast:@"No Internet Connection" duration:2 position:CSToastPositionCenter];
        
    
    }
    
    
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (IBAction)backgroundTap:(id)sender {
    
    [self.view endEditing:YES];
    
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    [self.view endEditing:YES];
    
    return YES;
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"fromForgot"]) {
        PFContactUsView * nextView = (PFContactUsView * )segue.destinationViewController;
        nextView.whereFrom = @"forgot";
        
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
