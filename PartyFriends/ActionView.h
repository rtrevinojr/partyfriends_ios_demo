//
//  ActionView.h
//  PartyFriends
//
//  Created by Joshua Silva  on 8/4/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Parse/Parse.h"
#import "PFCommentsTableViewController.h"
#import <CoreMotion/CoreMotion.h>

@interface ActionView : UIViewController <UITextFieldDelegate>


-(void)offine;

-(void)partyMode;

-(void)goingOut;

-(void)zoomer;

-(void)toCommentsView;

-(void)toCommentHistoryView;

-(void)updateCheck;

-(void)toPrivateParty;

-(void)clearedUpdate;
-(void)byeKeyboard;




@property (strong, nonatomic) PFUser * currentUser;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapGesture;
@property (strong, nonatomic) IBOutlet UIButton *PFButton;
@property (strong, nonatomic) NSString * exclusiveHeadline;

@property (strong, nonatomic) PFObject * commentObject;
@property (strong, nonatomic) PFRole * rolling;
@property (strong, nonatomic) PFRelation * allFriendsRelation;
@property (strong, nonatomic) IBOutlet UITextField *editLocation;
@property (strong, nonatomic) NSString * userStatus;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *lowerBar;
@property (strong, nonatomic) IBOutlet UILabel *usersName;
@property (strong, nonatomic) IBOutlet UIView *statusView;
- (IBAction)statusButton:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *statusButton;
@property (strong, nonatomic) NSTimer * updatingLocation;
@property (strong, nonatomic) CLLocation * userLocation;
@property (strong, nonatomic) NSArray * allFriends;
@property (strong, nonatomic) NSString * personalText;
@property (strong, nonatomic) CMMotionManager * motion;
@property (strong, nonatomic) NSString * friendRequest;

@property (strong, nonatomic) NSString * pushUser;
@property (strong, nonatomic) NSString * pushType;
@property (strong, nonatomic) PFSession * sessionToken;






@end
