//
//  PFPartiesTabBarViewController.m
//  PartyFriends
//
//  Created by Joshua Silva  on 4/21/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import "PFPartiesTabBarViewController.h"

@interface PFPartiesTabBarViewController ()

@end

@implementation PFPartiesTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.tabBar setHidden:YES];
    
    }


-(void)indexOne{
    
    [self setSelectedIndex:0];
}

-(void)indexTwo{
    
    
    [self setSelectedIndex:1];
    
}

-(void)indexThree{
    [self setSelectedIndex:2];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
