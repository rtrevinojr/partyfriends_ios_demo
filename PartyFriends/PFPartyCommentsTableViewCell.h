//
//  PFPartyCommentsTableViewCell.h
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 1/8/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PFPartyCommentsTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UITextField *comment;

@property (weak, nonatomic) IBOutlet UIImageView *commentSenderProfilePic;

@property (weak, nonatomic) IBOutlet UITextView *commentTextView;

@property (weak, nonatomic) IBOutlet UILabel *commentAuthor;

@end
