//
//  PFPartyCommentsViewController.h
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 12/15/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Parse/Parse.h>


@interface PFPartyCommentsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UITextViewDelegate>


@property (strong, nonatomic) PFObject * pfParty;

@property (strong, nonatomic) NSMutableArray * commentsList;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *commentActionView;

//@property (weak, nonatomic) IBOutlet UITextView *commentTextView;

@property (weak, nonatomic) IBOutlet UIButton *commentPostButton;

@property (weak, nonatomic) IBOutlet UITextView *commentTextView2;

@property (strong, nonatomic) UIRefreshControl * refreshControl;

@end
