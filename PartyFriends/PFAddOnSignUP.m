//
//  PFAddOnSignUP.m
//  PartyFriends
//
//  Created by Joshua Silva  on 4/5/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import "PFAddOnSignUP.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "PFInviteAddressBookTVC.h"

@implementation PFAddOnSignUP

-(void)viewDidLoad{
    [super viewDidLoad];
    
[self.navigationItem setHidesBackButton:YES animated:YES];
    
     [self.view setTintColor:[UIColor colorWithRed:(142/255.0) green:(83/255.0) blue:(249/255.0) alpha:1.0]];
    
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    //CFErrorRef myError = NULL;
    

    
    




}
-(IBAction)skipButton:(id)sender {
    
    [self performSegueWithIdentifier:@"toBegins" sender:self];
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"toContacts"]) {
        PFInviteAddressBookTVC * contacts = (PFInviteAddressBookTVC *) segue.destinationViewController;
        
        contacts.check = @"YES";
    
    }


}

- (IBAction)contactAccess:(id)sender {
    
    
////    
////                    UIAlertController * deinedAccess = [UIAlertController alertControllerWithTitle:@"Contact Access Denied" message:@"If you decide to allow PartyFriends access to your contacts, go to your phones settings > PartyFriends" preferredStyle:UIAlertControllerStyleAlert];
////    
////    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action") style:UIAlertActionStyleCancel handler:nil];
////    UIAlertAction * okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"ok", @"okay action") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
////    
////        NSLog(@"okay action");
////        
////        
////        
////    }];
//    
//    [deinedAccess addAction:cancelAction];
//    [deinedAccess addAction:okAction];
//    
//    [self presentViewController:deinedAccess animated:YES completion:nil];
    
    
    ABAddressBookRef myAddressBook = ABAddressBookCreateWithOptions(nil, nil);
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined){
        ABAddressBookRequestAccessWithCompletion(myAddressBook, ^(bool granted, CFErrorRef error){
            if(granted){
                //AddressRequest was authorized
                
                [self performSegueWithIdentifier:@"toContacts" sender:self];
            }else {
                
                UIAlertController * deinedAccess = [UIAlertController alertControllerWithTitle:@"Contact Access Denied" message:@"If you decide to allow PartyFriends access to your contacts, go to your phones settings > PartyFriends then enable contacts" preferredStyle:UIAlertControllerStyleAlert];
                
                 UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Got it!", @"Cancel action") style:UIAlertActionStyleCancel handler:nil];
                [deinedAccess addAction:cancelAction];
                
                [self presentViewController:deinedAccess animated:YES completion:nil];
                [self performSegueWithIdentifier:@"toBegins" sender:self];
            }
            
        });
        
        
    }else if (ABAddressBookGetAuthorizationStatus()==kABAuthorizationStatusAuthorized){
        [self performSegueWithIdentifier:@"toContacts" sender:self];
        
    }else {
        
        UIAlertController * deinedAccess = [UIAlertController alertControllerWithTitle:@"Contact Access Denied" message:@"If you decide to allow PartyFriends access to your contacts, go to your phones settings > PartyFriends then enable contacts" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Got it!", @"Cancel action") style:UIAlertActionStyleCancel handler:nil];
        [deinedAccess addAction:cancelAction];
        
        [self presentViewController:deinedAccess animated:YES completion:nil];
        [self performSegueWithIdentifier:@"toBegins" sender:self];
       
        
    }
    
    
}


@end
