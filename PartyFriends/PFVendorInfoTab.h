//
//  PFVendorInfoTab.h
//  VendorProfile
//
//  Created by Gilberto Silva on 11/30/15.
//  Copyright © 2015 Gilberto Silva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "tabbarViewController.h"
#import <Parse/Parse.h>

@interface PFVendorInfoTab : UIViewController<MKMapViewDelegate, UIScrollViewDelegate, UITextFieldDelegate, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *vendorMap;

@property (strong, nonatomic) IBOutlet UIScrollView *vendorInfoScrollView;

@property (strong, nonatomic) PFObject * vendorPFObject;

@property (strong, nonatomic) IBOutlet UITextField *businessName;
@property (strong, nonatomic) IBOutlet UITextField *address;
@property (strong, nonatomic) IBOutlet UITextField *city;
@property (strong, nonatomic) IBOutlet UITextField *zip;
@property (strong, nonatomic) IBOutlet UITextView *descriptionView;
@property (strong, nonatomic) IBOutlet UITextField *businessPhone;
@property (strong, nonatomic) IBOutlet UITextField *phone;
@property (strong, nonatomic) IBOutlet UITextField *businessEmail;
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *businessFax;
@property (strong, nonatomic) IBOutlet UITextField *fax;
@property (strong, nonatomic) IBOutlet UITextField *website;
- (IBAction)save:(id)sender;

@end
