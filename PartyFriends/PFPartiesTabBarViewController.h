//
//  PFPartiesTabBarViewController.h
//  PartyFriends
//
//  Created by Joshua Silva  on 4/21/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PFPartiesTabBarViewController : UITabBarController

-(void)indexOne;
-(void)indexTwo;
-(void)indexThree;


@end
