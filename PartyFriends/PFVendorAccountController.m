//
//  PFVendorAccountController.m
//  PartyFriends
//
//  Created by Joshua Silva  on 12/26/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import "PFVendorAccountController.h"

@interface PFVendorAccountController ()

@end

@implementation PFVendorAccountController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
