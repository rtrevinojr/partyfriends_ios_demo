//
//  PFVendorEditPromoterTableViewController.m
//  PartyFriends
//
//  Created by Gilberto Silva on 12/2/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import "PFVendorEditPromoterTableViewController.h"

#import "PFVendorEditPromoterTableViewCell.h"

#import "PFVendorContainerViewController.h"

#import "PFVendorTabBarController.h"

#import "PFVendorSelectPromoTableViewController.h"

#import <Parse/Parse.h>


@interface PFVendorEditPromoterTableViewController ()

@property (strong, nonatomic) PFObject * vendorPFObject;

@property (strong, nonatomic) NSMutableArray * promoterList;

@end

@implementation PFVendorEditPromoterTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc]
                                  initWithImage:[UIImage imageNamed:@"add-party"]
                                  style:UIBarButtonItemStylePlain
                                  target:self action:@selector(addPromoTouch)];
    
    self.navigationItem.rightBarButtonItem = addButton;
    
    self.navigationController.navigationItem.rightBarButtonItem = addButton;
    
    //[self.navigationItem.rightBarButtonItem setEnabled:NO];
    //[self.navigationItem.rightBarButtonItem setImage:nil];
    
    //PFVendorTabBarController * container = self.parentViewController;
    
    PFVendorTabBarController * container = (PFVendorTabBarController *) self.navigationController.parentViewController;
    
    
    NSLog(@"********* Parent VC = %@", container);
    
    self.vendorPFObject = container.vendorPromoPFObject;
    
    NSLog(@"********* %@", self.vendorPFObject);
    

    //PFQuery * query = [PFQuery queryWithClassName:@"vendor"];
    
    PFRelation * promoters = [self.vendorPFObject relationForKey:@"promoters"];
    PFQuery *query = [promoters query];
    
    self.promoterList = [NSMutableArray new];
    
    
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if (!error) {
            self.promoterList = objects;
        }
        
        NSLog(@"*********** Promo List");
        NSLog(@"%@", self.promoterList);
        
        [self.tableView reloadData];
    }];
    
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:NO];
    
    [self viewDidLoad];
}

- (void) addPromoTouch
{
    NSLog(@"addPromoTouch");
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.promoterList.count;
}


- (PFVendorEditPromoterTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PFVendorEditPromoterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"promocell" forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[PFVendorEditPromoterTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"promocell"];
    }
    
    
    PFObject * promo = [self.promoterList objectAtIndex:indexPath.row];
    
    
    
    
    CALayer *imageLayer = cell.promoterProfileImage.layer;
    [imageLayer setCornerRadius:5];
    [imageLayer setBorderWidth:1];
    [imageLayer setMasksToBounds:YES];
    [cell.promoterProfileImage.layer setCornerRadius:cell.promoterProfileImage.frame.size.width/2];
    [cell.promoterProfileImage.layer setMasksToBounds:YES];
    
    
    if (promo[@"profilePic"] != nil) {
        
        PFFile * pic = [promo objectForKey:@"profilePic"];
        [pic getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
            if (!error) {
                UIImage *image = [UIImage imageWithData:data];
                [cell.promoterProfileImage setImage:image];
            }
            else {
                UIImage * defaultimg = [UIImage imageNamed:@"58.png"];
                [cell.promoterProfileImage setImage:defaultimg];
            }
        }];
    }
    else {
        UIImage * defaultimg = [UIImage imageNamed:@"PFIcon"];
        [cell.promoterProfileImage setImage:defaultimg];
    }
    
    
    cell.promoterNameLabel.text = [promo objectForKey:@"firstName"];
    
    return cell;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
 */


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"selectpromofriends"]) {
        PFVendorSelectPromoTableViewController * destVC = (PFVendorSelectPromoTableViewController *) [segue destinationViewController];
        destVC.vendorList = self.promoterList;
    }
    
}


@end
