//
//  SettingsView.m
//  PartyFriends
//
//  Created by Joshua Silva  on 7/27/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import "SettingsView.h"
#import "FriendsView.h"
#import "AccountView.h"
#import "Gradient.h"
#import "PFUpsidedownAddViewController.h"
#import <sys/utsname.h>

#import "LocationManagerSingleton.h"

#import "Parse/Parse.h"


@interface SettingsView ()

@end

@implementation SettingsView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    self.currentUser = [PFUser currentUser];
    
    CGSize barSize = CGSizeMake(self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height +20);
    
    // CAGradientLayer * whiteLayer = [Gradient white];
    
    CAGradientLayer * randomLayer = [Gradient gold];
    
    UIButton * backButtonView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [backButtonView addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [backButtonView setBackgroundImage:[UIImage imageNamed:@"GoldBackButton"] forState:UIControlStateNormal];
    
    UIBarButtonItem * backBUTTON = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
    self.navigationItem.leftBarButtonItem = backBUTTON;
    
    
    //  whiteLayer.frame = CGRectMake(0, -20, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height + 20);
    
    randomLayer.frame = CGRectMake(0, -20, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height + 20);
    
    //[whiteLayer setStartPoint:CGPointMake(0.0, 0.5)];
    // [whiteLayer setEndPoint:CGPointMake(1.0, 0.5)];
    //    [randomLayer setStartPoint:CGPointMake(0.0, 0.5)];
    //    [randomLayer setEndPoint:CGPointMake(1.0, 0.5)];
    
    [randomLayer setStartPoint:CGPointMake(-1.5, 0.0)];
    [randomLayer setEndPoint:CGPointMake(2.5, 0.0)];
    UIGraphicsBeginImageContext(barSize);
    [randomLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * anotherImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // [self.navigationController.navigationBar setBackgroundImage:anotherImage forBarMetrics:UIBarMetricsDefault];
    
    // [self.navigationController.navigationBar.layerinsertSublayer:randomLayer atIndex:1];
    //[self.navigationController.navigationBar.layer insertSublayer:whiteLayer atIndex:1];
    // [self.navigationController.navigationBar.layer insertSublayer:randomLayer below:whiteLayer];
    
    [self.accountButton sendSubviewToBack:self.view];
    
    //[self.accountButton.layer insertSublayer:randomLayer atIndex:1];
    
    //self.navigationController.navigationBar.titleTextAttributes.
    
    self.pinNumber.text = self.currentUser[@"invitePin"];
    
    
    //self.missingInfo = @"missing";
    //    CAGradientLayer * randomLayer = [Gradient gold];
    //    randomLayer.frame = CGRectMake(0, -20, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height + 20);
    //    [randomLayer setStartPoint:CGPointMake(0.0, 0.5)];
    //    [randomLayer setEndPoint:CGPointMake(1.0, 0.5)];
    //    CGSize goldSize = CGSizeMake(randomLayer.bounds.size.width, randomLayer.bounds.size.height);
    //
    //   UIGraphicsBeginImageContextWithOptions(goldSize, NO, 0);
    //
    //    [randomLayer renderInContext:UIGraphicsGetCurrentContext()];
    //    UIImage *gradientImage =UIGraphicsGetImageFromCurrentImageContext();
    //    UIGraphicsEndImageContext();
    //
    //    [self.navigationController.navigationBar setBackgroundImage:gradientImage forBarMetrics:UIBarMetricsDefault];
    //
    //    [self.accountButton setBackgroundImage:gradientImage forState:UIControlStateNormal];
    
    //[self.colorView ];
    
    //[self.colorView.layer insertSublayer:randomLayer atIndex:0];
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:(142/255.0) green:(83/255.0) blue:(249/255.0) alpha:1.0];

    
    
}

-(void)goBack{
    
    [self performSegueWithIdentifier:@"backAction" sender:self];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:NO];
    
    NSLog(@"the subviews for the bar %@",self.navigationController.navigationBar.subviews);
    self.friends = [[PFUser currentUser] objectForKey:@"friends"];
    
    NSLog(@"the number of sublayers, %@",self.navigationController.navigationBar.layer.sublayers);
    self.countView.layer.cornerRadius = 10;
    self.numberCount.layer.masksToBounds = YES;
    self.numberCount.layer.cornerRadius = 10;
    self.countView.hidden = YES;
    
    [self.currentUser fetchInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
        
        if (!error){
            self.currentUser = (PFUser *) object;
            NSNumber * requestCount = [self.currentUser objectForKey:@"request"];
            int number = [requestCount intValue];
            
            if (number > 0) {
                
            
            NSString * zipString = [NSString stringWithFormat:@"%@", requestCount];
                self.countView.hidden = NO;
                self.numberCount.text = zipString;
            }
        }
    
    }];
    
    
    
//    PFQuery * query = [self.friends query];
//    [query orderByAscending:@"firstName"];
//    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
//        if(error) {
//            
//            NSLog(@" error %@  %@  ", error, [error userInfo]);
//            
//        }
//        else {
//            
//            self.allFriends =  [[NSMutableArray alloc] initWithArray:objects];
//            
//        }
//        
//    }];
    
    /*   PFQuery * queryAccount = [PFQuery queryWithClassName:@"accountinfo"];
     [queryAccount whereKey:@"createdBy" equalTo:self.currentUser];
     
     [queryAccount getFirstObjectInBackgroundWithBlock:^(PFObject * object, NSError * error){
     if (error){
     NSLog(@"error %@ %@", error, [error userInfo
     ]);
     
     if (error.code == 101) {
     
     NSDate * defaut = [NSDate date];
     NSNumber * noZip = [NSNumber numberWithInt:1];
     self.accountInfo = [PFObject objectWithClassName:@"accountinfo"];
     
     self.accountInfo[@"birthday"] = defaut;
     self.accountInfo[@"state"] = self.missingInfo;
     self.accountInfo[@"city"] = self.missingInfo;
     self.accountInfo[@"address"] = self.missingInfo;
     self.accountInfo[@"zip"]= noZip;
     self.accountInfo[@"createdBy"]=self.currentUser;
     
     self.accountInfo.ACL =[PFACL ACLWithUser:self.currentUser];
     
     [self.accountInfo saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
     if (error) {
     UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"sorry" message:[error.userInfo objectForKey:@"error"] delegate:nil cancelButtonTitle:@"I dont party enough!!" otherButtonTitles: nil];
     [alertview show];
     }
     else{
     
     self.accountInfo = object;
     
     }
     }];
     
     }
     }else{
     self.accountInfo = object;
     
     NSLog(@"this is the address to the user %@", self.accountInfo[@"address"]);
     }}];*/
    
    
    NSLog(@" address to the user %@", self.accountInfo[@"address"]);
    NSDictionary * dictionary = [[NSDictionary alloc] init];
    //[dictionary setObject:t forKey:@"feed"];
    
    [PFCloud callFunctionInBackground:@"UpdateFriends" withParameters:dictionary block:^(NSArray * objects, NSError * error) {
        if (error){
            NSLog(@"theres an error %@", error);
            
        }else{
            
            NSLog(@"returning the objects - %@", objects);
        }
        
    }];
    
    
    //  Refresh fucntion for the Party/Map feed
    
    //    [PFCloud callFunctionInBackground:@"Refresh" withParameters:dictionary block:^(NSArray * objects, NSError * error) {
    //        if (error){
    //            NSLog(@"theres an error %@", error);
    //
    //        }else{
    //
    //            NSLog(@"returning the objects - %@", objects);
    //        }
    //
    //    }];
    
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString * deviceModel = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    if ([deviceModel containsString:@"iPhone8"] || [deviceModel containsString:@"iPhone7"]){
        
        self.motionCheck = [[CMMotionManager alloc] init];
        self.motionCheck.accelerometerUpdateInterval = 1;
        if ([self.motionCheck isAccelerometerAvailable])
        {
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            [self.motionCheck startAccelerometerUpdatesToQueue:queue withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    NSLog(@"x-axis= %f || y-axis= %f || z-axis= %f",accelerometerData.acceleration.x,accelerometerData.acceleration.y, accelerometerData.acceleration.z);
                    
                    if (accelerometerData.acceleration.y >= 0.5){
                        NSLog(@"do this, whatever i want to change and check if this can actually change anything?");
                        
                        [self.motionCheck stopAccelerometerUpdates];
                        
                        
                        [self performSegueWithIdentifier:@"actionToAdd" sender: self];
                        
                        
                        
                    }
                    if (error) {
                        NSLog(@"there was an erro here because other montior was stoped!");
                    }
                    
                    //                self.xAxis.text = [NSString stringWithFormat:@"%.2f",accelerometerData.acceleration.x];
                    //                self.yAxis.text = [NSString stringWithFormat:@"%.2f",accelerometerData.acceleration.y];
                    //                self.zAxis.text = [NSString stringWithFormat:@"%.2f",accelerometerData.acceleration.z];
                });
            }];
        }
        
        
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    /* if([segue.identifier isEqualToString:@"toFriendsView"]){
     FriendsView * friendsView = (FriendsView *) segue.destinationViewController;
     
     friendsView.currentUser = self.currentUser;
     friendsView.currentFriends = [[NSMutableArray alloc] initWithArray: self.allFriends];
     //  friendsView.friends = self.friends;
     
     if ([segue.identifier isEqualToString:@"toAccountInfo"]) {
     
     NSLog(@" address to the user %@", self.accountInfo[@"address"]);
     
     AccountView * accountView = (AccountView *) segue.destinationViewController;
     accountView.currentUser = self.currentUser;
     accountView.accountInfo = self.accountInfo;
     
     
     
     }
     
     
     
     
     //}
     
     */
    if ([segue.identifier isEqualToString:@"toRequest"]) {
        [self.countView setHidden:YES];
    }
    
    if ([segue.identifier isEqualToString:@"actionToAdd"]) {
        PFUpsidedownAddViewController * addFriend = segue.destinationViewController;
        addFriend.statusString = @"2";
    }
    
}


- (IBAction)editPrivatePartiesTouch:(id)sender {
    
    NSLog(@"private parties touch 2");
    
    //[self performSegueWithIdentifier:@"hostings2" sender:self];
}


- (IBAction)privatePartiesBtnTouch:(id)sender {
    
    NSLog(@"private parties touch");
    [self performSegueWithIdentifier:@"hosting2" sender:self];
    
}





- (IBAction)logout:(id)sender {
    
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    NSLog(@"this at the log on to check if the current user is shoeing on time %@", [PFUser currentUser]);
    [currentInstallation removeObjectForKey:@"createdBy"];
    //currentInstallation[@"createdBy"] = [PFUser currentUser];
    //  [currentInstallation setDeviceTokenFromData:deviceToken];
    
    
    
    PFObject * log = [PFObject objectWithClassName:@"log"];
    
    [PFSession getCurrentSessionInBackgroundWithBlock:^(PFSession * session, NSError * error){
        if (!error) {
            
            log[@"status"] = @0;
            log[@"deviceInstallation"] = [PFInstallation currentInstallation];
            log[@"deviceSession"] = session;
            
            [log saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                if (!error) {
                    [self.currentUser fetchInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
                        if (!error) {
                            [[LocationManagerSingleton sharedSingleton].locationManager stopMonitoringVisits];
                            
                            [currentInstallation saveInBackgroundWithBlock:^(BOOL secuess, NSError * error){
                                if (!error) {
                                    NSLog(@" the installation was a good");
                                    [PFUser logOutInBackground];
                                    
                                    [self performSegueWithIdentifier:@"logout" sender:self];
                                    
                                }else{
                                    //[currentInstallation saveEventually];
                                    
                                    NSLog(@"ERROROROROROROOROROROROROROOROROROROOR");
                                    
                                }
                                
                            }];
                            
                        }else{
                            
                            
                            if ([[error.userInfo objectForKey:@"error"] isEqualToString:@"Already offline."]) {
                                
                                
                                [currentInstallation saveInBackgroundWithBlock:^(BOOL secuess, NSError * error){
                                    if (!error) {
                                        NSLog(@" the installation was a good");
                                        [PFUser logOutInBackground];
                                        
                                        [self performSegueWithIdentifier:@"logout" sender:self];
                                        
                                    }else{
                                        //[currentInstallation saveEventually];
                                        
                                        NSLog(@"ERROROROROROROOROROROROROROOROROROROOR");
                                        
                                    }
                                    
                                }];
                            }else{
                                UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Sorry" message:[error.userInfo objectForKey:@"error"] delegate:nil cancelButtonTitle:@"Try again!!" otherButtonTitles:nil];
                                [alertview show];
                                [sender setEnabled:YES];
                                
                            }
                            
                            
                            
                        }
                        
                    }];
                    
                }else{
                    
                    
                    if ([[error.userInfo objectForKey:@"error"] isEqualToString:@"Already offline."]) {
                        
                        
                        [currentInstallation saveInBackgroundWithBlock:^(BOOL secuess, NSError * error){
                            if (!error) {
                                NSLog(@" the installation was a good");
                                [PFUser logOutInBackground];
                                
                                [self performSegueWithIdentifier:@"logout" sender:self];
                                
                            }else{
                                //[currentInstallation saveEventually];
                                
                                NSLog(@"ERROROROROROROOROROROROROROOROROROROOR");
                                
                            }
                            
                        }];
                    }else{
                        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Sorry" message:[error.userInfo objectForKey:@"error"] delegate:nil cancelButtonTitle:@"Try again!!" otherButtonTitles:nil];
                        [alertview show];
                        [sender setEnabled:YES];
                        
                    }
                    
                    
                    
                }
            }];
        }else{
            
            
        }
        
    }];
    
    
    
    //    [PFUser logOutInBackground];
    //
    //    [self performSegueWithIdentifier:@"logout" sender:self];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    [self.motionCheck stopAccelerometerUpdates];
    
}

-(IBAction)backToSettings:(UIStoryboardSegue *) segue{
    
    
    
}
- (IBAction)uber:(id)sender {
    
    
    CLLocationCoordinate2D cood;
    
    
    cood = [LocationManagerSingleton sharedSingleton].locationManager.location.coordinate;
    
    
    
    
    NSString * client_id = @"L58ExalNrphCN8FtEvGEOP2MRVM3QvXc&action";
    
    NSString * server_toker = @"jlnT1TewykCf5US51fU9-BD8CBlGr4jzPN0oM479";
    
    NSString * pickup_lat = [NSString stringWithFormat:@"%f", cood.latitude];
    
    NSString * pickup_long = [NSString stringWithFormat:@"%f", cood.longitude];
    
    NSLog(@"pick up latitude = %@", pickup_lat);
    NSLog(@"pick up longitude = %@", pickup_long);
    
    NSString * pickup_nickname = self.currentUser[@"firstName"];
    
    NSString * pickup_address;
    
    NSString * dropoff_lat;
    
    NSString * dropoff_long;
    
    NSString * dropoff_nickname;
    
    NSString * dropoff_address;
    
    NSString * product_id = @"";
    
    
    NSString * basic_url = @"uber://?action=setPickup&pickup=my_location";
    
    
    NSString * test_url = @"uber://?client_id=L58ExalNrphCN8FtEvGEOP2MRVM3QvXc&action=setPickup&pickup[latitude]=37.775818&pickup[longitude]=-122.418028&pickup[nickname]=UberHQ&pickup[formatted_address]=1455%20Market%20St%2C%20San%20Francisco%2C%20CA%2094103&dropoff[latitude]=37.802374&dropoff[longitude]=-122.405818&dropoff[nickname]=Coit%20Tower&dropoff[formatted_address]=1%20Telegraph%20Hill%20Blvd%2C%20San%20Francisco%2C%20CA%2094133&product_id=a1111c8c-c720-46c3-8534-2fcdd730040d";
    
    
    NSString * uber_url = [NSString stringWithFormat:@"uber://?client_id=%@&action=setPickup&pickup[latitude]=%@&pickup[longitude]=%@&pickup[nickname]=%@&dropoff[latitude]=%@&dropoff[longitude]=%@dropoff[nickname]=%@", client_id, pickup_lat, pickup_long, pickup_nickname, dropoff_lat, dropoff_long, dropoff_nickname];
    
    NSString * test_uber_url = [NSString stringWithFormat:@"uber://?client_id=%@&action=setPickup&pickup[latitude]=%@&pickup[longitude]=%@&pickup[nickname]=%@", client_id, pickup_lat, pickup_long, pickup_nickname];
    
    
    
    NSString * uber_api_url = [NSString stringWithFormat:@"https://api.uber.com/v1/products?latitude=%@&longitude=%@", pickup_lat, pickup_long];
    
    //NSString * uber_api_url = @"https://api.uber.com/v1/products?latitude=30&longitude=-97";
    
    
    
    NSString *pswd = @"XXXXXXXXX";
    
    NSURL *url = [NSURL URLWithString:uber_api_url];
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    NSString *authValue = [NSString stringWithFormat:@"Bearer %@", pswd];
    NSString * uber_authValue = [NSString stringWithFormat:@"Token %@", server_toker];
    [request setValue:uber_authValue forHTTPHeaderField:@"Authorization"];
    
    
    
    NSLog(@"********************************");
    NSLog(@"Request = %@", request);
    
    
    
    //create a mutable HTTP request
    //NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:myUrl];
    //sets the receiver’s timeout interval, in seconds
    [request setTimeoutInterval:30.0f];
    //sets the receiver’s HTTP request method
    [request setHTTPMethod:@"GET"];
    //sets the request body of the receiver to the specified data.
    //[request setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    
    //allocate a new operation queue
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    //Loads the data for a URL request and executes a handler block on an
    //operation queue when the request completes or fails.
    [NSURLConnection
     sendAsynchronousRequest:request
     queue:queue
     completionHandler:^(NSURLResponse *response,
                         NSData *data,
                         NSError *error) {
         if ([data length] >0 && error == nil){
             //process the JSON response
             //use the main queue so that we can interact with the screen
             dispatch_async(dispatch_get_main_queue(), ^{
                 [self parseResponse:data];
             });
         }
         else if ([data length] == 0 && error == nil){
             NSLog(@"Empty Response, not sure why?");
         }
         else if (error != nil){
             NSLog(@"Not again, what is the error = %@", error);
         }
     }];
    
    
    
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:test_uber_url]];
    
    
    
    
    
}




- (void) parseResponse:(NSData *) data {
    
    NSString *myData = [[NSString alloc] initWithData:data
                                             encoding:NSUTF8StringEncoding];
    NSLog(@"JSON data = %@", myData);
    NSError *error = nil;
    
    // Construct a Array around the Data from the response
    NSArray* object = [NSJSONSerialization
                       JSONObjectWithData:data
                       options:0
                       error:&error];
    
    NSLog(@"*********************************");
    
    for (int i=0; i < [object count]; i++) {
        //NSLog(@"Item %.2i - Title  - %@", i+1, [object[i] objectForKey:@"product_id"] );
        NSLog(@"%@", object[i]);
        //NSLog(@"        - Artist - %@ \n\n", [[object[i] objectForKey:@"artist"] objectForKey:@"name"] );
    }
    
    //    for (id obj in object) {
    //
    //        NSLog(@"%@", [obj objectForKey:@"product_id"]);
    //    }
    
    //parsing the JSON response
    //    id jsonObject = [NSJSONSerialization
    //                     JSONObjectWithData:data
    //                     options:NSJSONReadingAllowFragments
    //                     error:&error];
    
    //if (jsonObject != nil && error == nil){
    //  NSLog(@"Successfully deserialized...");
    
    //check if the country code was valid
    //NSNumber *success = [jsonObject objectForKey:@"success"];
    
    
    //NSString * product_id = [jsonObject objectForKey:@"product_id"];
    
    //NSLog(@"Product id = %@", product_id);
    
    
    //}
    
    
    // Prepare the variables for the JSON response
    //NSData *urlData;
    //NSURLResponse *response;
    //NSError *error;
    
    // Make synchronous request
    //urlData = [NSURLConnection sendSynchronousRequest:urlRequest
    //                                returningResponse:&response
    //                                            error:&error];
    
    // Construct a Array around the Data from the response
    //NSArray* object = [NSJSONSerialization
    //                   JSONObjectWithData:urlData
    //                   options:0
    //                   error:&error];
    
    // Iterate through the object and print desired results
    //    for (int i=0; i < [object count]; i++) {
    //        NSLog(@"Item %.2i - Title  - %@", i+1, [object[i] objectForKey:@"title"] );
    //        NSLog(@"        - Artist - %@ \n\n", [[object[i] objectForKey:@"artist"] objectForKey:@"name"] );
    //    }
    
}
- (IBAction)pinInfo:(id)sender {
    
    
    UIAlertController * pinView = [UIAlertController alertControllerWithTitle:@"Invite Pin" message:@"A 4 digit pin is used to invite your friends to PartyFriends. PartyFriends is an invite-only network, your friends must be invited to sign up." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Got it!", @"Cancel action") style:UIAlertActionStyleCancel handler:nil];
    [pinView addAction:cancelAction];
    
    [self presentViewController:pinView animated:YES completion:nil];
    
}


- (IBAction)Cloud:(id)sender {
    
    NSDictionary * dictionary = [[NSDictionary alloc] init];
    
    NSLog(@"checking cloud code");
    [PFCloud callFunction:@"CheckRole" withParameters:dictionary];
}


@end