//
//  PFPartyCommentsViewController.m
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 12/15/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import "PFPartyCommentsViewController.h"

#import "PFCommentsTableViewCell.h"


#import <Parse/Parse.h>



@implementation PFPartyCommentsViewController


- (IBAction)reload:(id)sender {
    
    //[self.tableView reloadData];
    NSLog(@"refresh Table");
    
    //[self requeryTable];
    
    self.refreshControl = [[UIRefreshControl alloc]init];
    [self.tableView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
}

- (void)refreshTable {
    //TODO: refresh your data
    [self.refreshControl endRefreshing];
    
    [self requeryTable];
    
    [self.tableView reloadData];
}

- (void) requeryTable
{
    PFRelation * comments = [[self pfParty] objectForKey:@"comments"];
    
    
    self.commentsList = [NSMutableArray new];
    
    
    // self.tableView.delegate = self;
    
    //self.commentTextView.delegate = self;
    
    
    PFQuery * query = [comments query];
    [query includeKey:@"createdBy"];
    
    [query orderByAscending:@"createdAt"];
    
    
    //self.commentsList = [query findObjects];
    
    
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        
        self.commentsList = objects;
        
        NSLog(@"*** comments Array in background count = %luu", self.commentsList.count);
        
        NSLog(@"%@", self.commentsList);
        
        [self.tableView reloadData];
    }];
    
}


- (void) viewDidLoad
{
    [super viewDidLoad];
    
    self.refreshControl = [[UIRefreshControl alloc]init];
    [self.tableView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                       initWithImage:[UIImage imageNamed:@"arrow-left"]
                                       style:UIBarButtonItemStylePlain
                                       target:self action:@selector(goBack)];
    
    
    
    self.navigationItem.leftBarButtonItem = backButton;
    
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc]
                                      initWithImage:[UIImage imageNamed:@"add-party"]
                                      style:UIBarButtonItemStylePlain
                                      target:self action:@selector(addCommentTouch)];
    
    self.navigationItem.rightBarButtonItem = addButton;
    
    NSLog(@"PFPartyCommentsViewController viewDidLoad. PFObject = %@", self.pfParty);
    
    
    PFRelation * comments = [[self pfParty] objectForKey:@"comments"];
    
    
   // self.tableView.delegate = self;
    
    //self.commentTextView.delegate = self;
    
    
    PFQuery * query = [comments query];
    [query includeKey:@"createdBy"];
    
    [query orderByAscending:@"createdAt"];
    
    
    //self.commentsList = [query findObjects];
    
    
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        
        self.commentsList = objects;
        
        NSLog(@"*** comments Array in background count = %luu", self.commentsList.count);
        
        NSLog(@"%@", self.commentsList);
        
        [self.tableView reloadData];
    }];
    
}


- (void) addCommentTouch
{
    NSLog(@"postComment ");
    
    PFObject * message = [PFObject objectWithClassName:@"comment"];
    
    PFRelation * friendsRel = [PFRelation alloc];
    
    friendsRel = [message relationForKey:@"party"];
    
    NSLog(@"*** comment = %@", self.commentTextView2);
    
    NSString * message_string = [NSString stringWithFormat:@"%@", self.commentTextView2.text];
    
    message[@"comment"] = message_string;
    
    if (self.pfParty != nil) {
        message[@"party"] =  self.pfParty;
    }
    else {
        NSLog(@"************* self.partyObject is nil");
    }
    

    
    [message saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        if (!error) {
            NSLog(@"message save in background");
        }else {
            PFRelation * comments = [[self pfParty] objectForKey:@"comments"];
            
            PFQuery * query = [comments query];
            [query includeKey:@"createdBy"];
            
            [query orderByAscending:@"createdAt"];
            [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
            
            self.commentsList = objects;
            
            NSLog(@"*** comments /n  Array in background /n count = %luu", self.commentsList.count);
            
            NSLog(@"%@", self.commentsList);
            
            [self.tableView reloadData];
              //  [self viewDidLoad];
        }];}
        
    }];
    
    //[self requeryTable];
    
    
//    if (self.tableView.contentSize.height > self.tableView.frame.size.height) {
//        CGPoint offset = CGPointMake(0, self.tableView.contentSize.height - self.tableView.frame.size.height);
//        [self.tableView setContentOffset:offset animated:YES];
//    }
    
    [self.commentTextView2 resignFirstResponder];
    
//    PFRelation * comments = [[self pfParty] objectForKey:@"comments"];
//    
//    PFQuery * query = [comments query];
//    [query includeKey:@"createdBy"];
//    
//    [query orderByAscending:@"createdAt"];
//    
//    
//    //self.commentsList = [query findObjects];
//    
//    
//    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
//        
//        self.commentsList = objects;
//        
//        NSLog(@"*** comments Array in background count = %luu", self.commentsList.count);
//        
//        NSLog(@"%@", self.commentsList);
//        
//        [self.tableView reloadData];
//    }];
//    
//    [self.tableView reloadData];
    
}

//- (void) viewDidAppear:(BOOL)animated
//{
//    [super viewDidAppear:animated];
//    
//    [self.tableView reloadData];
//}

/*
- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.tableView.contentSize.height > self.tableView.frame.size.height) {
        CGPoint offset = CGPointMake(0, self.tableView.contentSize.height - self.tableView.frame.size.height);
        [self.tableView setContentOffset:offset animated:YES];
    }
}
*/
 
/*
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    //if([text isEqualToString:@"\n"])
        [textView resignFirstResponder];
    return YES;
}
 */

/*
-(void)textViewDidBeginEditing:(UITextView *)textView{
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:self.commentActionView cache:YES];
    self.commentActionView.frame = CGRectMake(10, 50, 300, 200);
    [UIView commitAnimations];
    
    NSLog(@"Started editing target!");
    
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:self.commentActionView cache:YES];
    self.commentActionView.frame = CGRectMake(10, 150, 300, 200);
    [UIView commitAnimations];
}
 */


/*
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self animateTextView: YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self animateTextView:NO];
}
 */

/*
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    if (textView == self.commentTextView)
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.view.frame = CGRectMake(self.view.frame.origin.x , (self.view.frame.origin.y - 80), self.view.frame.size.width, self.view.frame.size.height);
        
//        self.commentActionView.frame = CGRectMake(self.commentActionView.frame.origin.x, (self.commentActionView.frame.origin.y - 80), self.commentActionView.frame.size.width, self.commentActionView.frame.size.height);
        
        [UIView commitAnimations];
    }
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView == self.commentTextView)
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.view.frame = CGRectMake(self.view.frame.origin.x , (self.view.frame.origin.y + 80), self.view.frame.size.width, self.view.frame.size.height);
        
//        self.commentActionView.frame = CGRectMake(self.commentActionView.frame.origin.x, (self.commentActionView.frame.origin.y + 80), self.commentActionView.frame.size.width, self.commentActionView.frame.size.height);
        
        [UIView commitAnimations];
    }
}
 */

- (void) animateTextView:(BOOL) up
{
    
    // Get the size of the keyboard.
//    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //Given size may not account for screen rotation
//    int height = MIN(keyboardSize.height,keyboardSize.width);
//    int width = MAX(keyboardSize.height,keyboardSize.width);
    
    const int movementDistance = 10; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    int movement= movement = (up ? -movementDistance : movementDistance);
    NSLog(@"%d",movement);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.commentActionView.frame, 0, movement);
    [UIView commitAnimations];
}
 

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
 

- (void) goBack {
    
    NSLog(@"Back");
    
    [self performSegueWithIdentifier:@"unwindToAction" sender:self];
    //[self.navigationController popViewControllerAnimated:YES];
}


- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"number of rows in section = %lu", self.commentsList.count);
    return self.commentsList.count;
}

- (PFCommentsTableViewCell *) tableView: (UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"indexPath = %lu", indexPath.row);
    
    static NSString * cellIdentifier = @"com";
    
    PFCommentsTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    //PFCommentsTableViewCell * cell = [[PFCommentsTableViewCell alloc] init];
    
//    PFCommentsTableViewCell * cell = [[PFCommentsTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[PFCommentsTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    PFObject * comments = [self.commentsList objectAtIndex:indexPath.row];
    
    NSLog(@"createdBy = %@", comments);
    
    PFUser *user = comments[@"createdBy"];
    
    NSString * comment_str = comments[@"comment"];
    
    CGSize stringSize = [comment_str sizeWithFont:[UIFont boldSystemFontOfSize:15] constrainedToSize:CGSizeMake(320, 9999) lineBreakMode:UILineBreakModeWordWrap];
    
    NSLog(@"******************");
    NSLog(@"cell string size = %f", stringSize.height);
    
    UITextView *textV = [[UITextView alloc] initWithFrame:CGRectMake(100, 5, 290, stringSize.height+10)];
    textV.font = [UIFont systemFontOfSize:12.0];
    textV.text = comment_str;
    textV.textColor = [UIColor blackColor];
    textV.editable = NO;
    [textV setScrollEnabled:NO];
    
//        textV.keepTopInset.equal = KeepRequired(7);
//        textV.keepBottomInset.equal = KeepRequired(7);
//        textV.keepRightInset.equal = KeepRequired(10);
//        textV.keepLeftInset.equal = KeepRequired(70);
    
    [cell.contentView addSubview:textV];
    //[textV release];
    
    //cell.comment.text = comment_str;
    
    //cell.commentTextView.text = [comments objectForKey:@"comment"];
    
    cell.commentAuthor.text = [user objectForKey:@"firstName"];
    
    
    if (user[@"profilePic"] != nil) {
        
        PFFile * pic = [user objectForKey:@"profilePic"];
        
        
        [pic getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
            if (!error) {
                UIImage *image = [UIImage imageWithData:data];
                
                CALayer *imageLayer = cell.commentSenderProfilePic.layer;
                [imageLayer setCornerRadius:5];
                [imageLayer setBorderWidth:1];
                [imageLayer setMasksToBounds:YES];
                [cell.commentSenderProfilePic.layer setCornerRadius:cell.commentSenderProfilePic.frame.size.width/2];
                [cell.commentSenderProfilePic.layer setMasksToBounds:YES];
                [cell.commentSenderProfilePic setImage:image];
            }
            else {
                
                UIImage * defaultimg = [UIImage imageNamed:@"58.png"];
                
                CALayer *imageLayer = cell.commentSenderProfilePic.layer;
                [imageLayer setCornerRadius:5];
                [imageLayer setBorderWidth:1];
                [imageLayer setMasksToBounds:YES];
                [cell.commentSenderProfilePic.layer setCornerRadius:cell.commentSenderProfilePic.frame.size.width/2];
                [cell.commentSenderProfilePic.layer setMasksToBounds:YES];
                [cell.commentSenderProfilePic setImage:defaultimg];
            }
        }];
        
    }
    else {
        UIImage * defaultimg = [UIImage imageNamed:@"PFIcon"];
        
        CALayer *imageLayer = cell.commentSenderProfilePic.layer;
        [imageLayer setCornerRadius:5];
        [imageLayer setBorderWidth:1];
        [imageLayer setMasksToBounds:YES];
        [cell.commentSenderProfilePic.layer setCornerRadius:cell.commentSenderProfilePic.frame.size.width/2];
        [cell.commentSenderProfilePic.layer setMasksToBounds:YES];
        [cell.commentSenderProfilePic setImage:defaultimg];
    }
    
    
    return cell;
    
    
}



@end
