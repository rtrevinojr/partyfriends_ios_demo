//
//  PFCreateVendorViewController.h
//  PartyFriends
//
//  Created by Joshua Silva  on 1/10/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface PFCreateVendorViewController : UIViewController<UITextFieldDelegate, UITextViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate>
@property (strong, nonatomic) IBOutlet UITextField *businessName;
@property (strong, nonatomic) IBOutlet UITextView *descriptionView;
@property (strong, nonatomic) IBOutlet UITextField *address;
@property (strong, nonatomic) IBOutlet UITextField *businessPhone;
@property (strong, nonatomic) IBOutlet UITextField *phone;
@property (strong, nonatomic) IBOutlet UITextField *businessEmail;
@property (strong, nonatomic) IBOutlet UITextField *confirmEmail;
@property (strong, nonatomic) IBOutlet UITextField *businessFax;
@property (strong, nonatomic) IBOutlet UITextField *faxNumber;
@property (strong, nonatomic) IBOutlet UITextField *website;
@property (strong, nonatomic) IBOutlet UITextField *zip;
@property (strong, nonatomic) IBOutlet UIPickerView *statePicker;
@property (strong, nonatomic) IBOutlet UITextField *city;

@property (strong, nonatomic) NSArray * theStates;
@property (strong, nonatomic) NSString * userState;
@property (strong, nonatomic) PFUser * currentUser;



@end
