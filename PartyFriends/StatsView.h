//
//  StatsView.h
//  PartyFriends
//
//  Created by Joshua Silva  on 8/13/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatsView : UIViewController
- (IBAction)offine:(id)sender;
- (IBAction)partyMode:(id)sender;
- (IBAction)goingOut:(id)sender;


@end
