//
//  PFHostingTableViewCell.h
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 11/18/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Parse/Parse.h>


@interface PFHostingTableViewCell : UITableViewCell


@property (strong, nonatomic) PFObject * hostingPartyObject;


@property (weak, nonatomic) IBOutlet UIImageView *hostingCellProfileImage;

@property (weak, nonatomic) IBOutlet UILabel *hostingCellHeadlineLabel;
@property (weak, nonatomic) IBOutlet UILabel *hostingCellCreatedByLabel;


@end
