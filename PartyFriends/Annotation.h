//
//  Annotation.h
//  PartyFriends
//
//  Created by Joshua Silva  on 7/21/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <Parse/Parse.h>

@interface Annotation : NSObject <MKAnnotation>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString * title;
@property (nonatomic, copy) NSString * subtitle;
@property (nonatomic, copy) UIImage * friendsImage;
@property (nonatomic, strong) PFObject * partyObject;
@property (nonatomic, strong) PFUser * theUserObject;

@property (nonatomic, copy) UIImageView * annImage;

@property (nonatomic, strong) NSString * annObjectId;

-(id) initWithTitle: (NSString * ) ctitle Subtitle: (NSString *) csubtitle Location:(CLLocationCoordinate2D )location;
+ (MKAnnotationView *)createViewAnnotationForMapView:(MKMapView *)mapView annotation:(id <MKAnnotation>)annotation;

//-(MKAnnotationView *) annotationView;




@end