//
//  NewsFeed.h
//  PartyFriends
//
//  Created by Joshua Silva  on 6/27/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <AddressBookUI/AddressBookUI.h>
#import "Parse/Parse.h"
#import "PFRefresh.h"

#import "tabBarController.h"


@interface NewsFeed : UIViewController <UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, UIScrollViewDelegate>

@property (strong, nonatomic) UIRefreshControl * refreshControl;


@property (strong, nonatomic) IBOutlet UIView *leftEdgeView;

@property (strong, nonatomic) NSMutableArray * postObjectArray;
@property (strong, nonatomic) NSMutableArray * partyObjectArray;
@property (strong, nonatomic) NSMutableArray * userObjectArray;


@property (strong, nonatomic) PFUser *current;
@property (strong, nonatomic) NSArray * onlineUsers;

@property (strong, nonatomic) PFObject *selectedMessage;

@property (strong, nonatomic) PFRelation * currentPFs;
@property (strong, nonatomic) PFGeoPoint *myGeo;
@property (strong, nonatomic) CLLocation * yourCord;
@property (strong, nonatomic) CLLocationManager * theCords;

@property (strong, nonatomic) PFRelation * friendsRelation;

@property (strong, nonatomic) NSArray * waffles;
@property (strong, nonatomic) NSArray * badFriends;
@property (strong, nonatomic) NSArray * whoElse;

@property (strong, nonatomic) IBOutlet UILabel *yourName;
@property (strong, nonatomic) IBOutlet UILabel *yourLocation;

//@property (strong, nonatomic) IBOutlet UIView *SettingsWindow;

@property (strong, nonatomic) IBOutlet UITableView * tableView;

@property (strong, nonatomic) NSMutableArray * who;

@property (strong, nonatomic) NSMutableArray * allArray;

@property (strong, nonatomic) NSMutableArray * organizedArray;

@property (strong, nonatomic) IBOutlet UIView *lowBar;

-(void)refreshTable;
-(void)newData;
-(void)failedRefresh;


- (IBAction)logout:(id)sender;




@end
