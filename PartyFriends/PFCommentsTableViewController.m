//
//  PFCommentsTableViewController.m
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 11/23/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import "PFCommentsTableViewController.h"

#import "PFAddCommentViewController.h"

#import "PFCommentsTableViewCell.h"


@implementation PFCommentsTableViewController

- (IBAction)postComment:(id)sender {
    
    NSLog(@"postComment ");
    
    PFObject * message = [PFObject objectWithClassName:@"comment"];
    
    PFRelation * friendsRel = [PFRelation alloc];
    
    friendsRel = [message relationForKey:@"party"];
    
    // NSString * message_string = [NSString stringWithFormat:@"%@", self.commentTextView.text];
    
    //message[@"comment"] = message_string;
    
//    if (self.partyObject != nil) {
//        message[@"party"] =  self.partyObject;
//    }
//    else {
//        NSLog(@"************* self.partyObject is nil");
//    }
    
    
//    [message saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
//        if (!error) {
//            NSLog(@"message save in background");
//        }
//    }];
    
    //[self dismissViewControllerAnimated: YES completion: nil];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    

    
    
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationDelegate:self];
//    [UIView setAnimationDuration:0.5];
//    [UIView setAnimationBeginsFromCurrentState:YES];
    

    //self.commentTextFieldTest.frame = CGRectMake(0, 0, self.commentTextFieldTest.frame.size.width, self.commentTextFieldTest.frame.size.height);
    
//    self.commentTextField.frame = CGRectMake(self.commentTextField.frame.origin.x, (self.commentTextField.frame.origin.y - 100.0), self.commentTextField.frame.size.width, self.commentTextField.frame.size.height);
//    [UIView commitAnimations];
    
    
    
//    UIBarButtonItem *addButton = [[UIBarButtonItem alloc]
//                                  initWithImage:[UIImage imageNamed:@"add-party"]
//                                  style:UIBarButtonItemStylePlain
//                                  target:self action:@selector(addCommentTouch)];
//    
//    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
//                                   initWithImage:[UIImage imageNamed:@"arrow-left"]
//                                   style:UIBarButtonItemStylePlain
//                                   target:self action:@selector(goBack)];
    
    
//    self.navigationItem.leftBarButtonItem = backButton;
//    
//    
//    self.navigationItem.rightBarButtonItem = addButton;
    
    NSLog(@"viewWillAppear");
    NSLog(@"self.pfParty = %@", self.pfParty);
    
    
    PFRelation * comments = [[self pfParty] objectForKey:@"comments"];
    
    
    
    
    PFQuery * query = [comments query];
    [query includeKey:@"createdBy"];
    
    
    //self.commentsArray = [query findObjects];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        
        self.commentsArray = objects;
        
        NSLog(@"*** comments Array in background count = %luu", self.commentsArray.count);
        
        [self.tableView reloadData];
    }];
    
    
    //NSLog(@"comments Array size = %luu",(unsigned long) self.commentsArray.count);
    
    
}


- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.tableView.contentSize.height > self.tableView.frame.size.height) {
        CGPoint offset = CGPointMake(0, self.tableView.contentSize.height - self.tableView.frame.size.height);
        [self.tableView setContentOffset:offset animated:YES];
    }
}


- (void) textFieldDidBeginEditing:(UITextField *)textField
{

    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationBeginsFromCurrentState:YES];
    self.commentTextField.frame = CGRectMake(self.commentTextField.frame.origin.x, (self.commentTextField.frame.origin.y - 100.0), self.commentTextField.frame.size.width, self.commentTextField.frame.size.height);
    [UIView commitAnimations];
    
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationBeginsFromCurrentState:YES];
    self.commentTextField.frame = CGRectMake(self.commentTextField.frame.origin.x, (self.commentTextField.frame.origin.y + 100.0), self.commentTextField.frame.size.width, self.commentTextField.frame.size.height);
    [UIView commitAnimations];
}


- (void) goBack {
    
    NSLog(@"Back");
    
    [self performSegueWithIdentifier:@"unwindToAction" sender:self];
    //[self.navigationController popViewControllerAnimated:YES];
}


- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:NO];
    
    NSLog(@"viewWillAppear");
    NSLog(@"self.pfParty = %@", self.pfParty);
    
    /*
    PFRelation * comments = [[self pfParty] objectForKey:@"comments"];
 
    
    PFQuery * query = [comments query];
    
    
    //self.commentsArray = [query findObjects];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        
        self.commentsArray = objects;
        
        NSLog(@"*** comments Array in background count = %luu", self.commentsArray.count);
        
        [self.tableView reloadData];
    }];
    
    
    NSLog(@"comments Array size = %luu",(unsigned long) self.commentsArray.count);
 */
    
}
 
 

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.commentsArray.count;
}

/*
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath  {
    
    //NSDictionary *d=(NSDictionary *)[self.menuArray objectAtIndex:indexPath.section];
    //NSString *label =  [d valueForKey:@"Description"];
    
    NSString * label = [self.commentsArray objectAtIndex:indexPath.row];
    
    CGSize stringSize = [label sizeWithFont:[UIFont boldSystemFontOfSize:15]
                          constrainedToSize:CGSizeMake(320, 9999)
                              lineBreakMode:UILineBreakModeWordWrap];
    
    
    if (stringSize.height + 25 > 50)
        return stringSize.height+25;
    else
    
} */

- (PFCommentsTableViewCell *) tableView: (UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
    static NSString * cellIdentifier = @"commentcell2";
    
    PFCommentsTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[PFCommentsTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    PFObject * comments = [self.commentsArray objectAtIndex:indexPath.row];
    
    PFUser *user = comments[@"createdBy"];
    
    NSString * comment_str = comments[@"comment"];
    
    CGSize stringSize = [comment_str sizeWithFont:[UIFont boldSystemFontOfSize:15] constrainedToSize:CGSizeMake(320, 9999) lineBreakMode:UILineBreakModeWordWrap];
    
    NSLog(@"******************");
    NSLog(@"cell string size = %f", stringSize.height);
    
    UITextView *textV = [[UITextView alloc] initWithFrame:CGRectMake(100, 5, 290, stringSize.height+10)];
    textV.font = [UIFont systemFontOfSize:12.0];
    textV.text = comment_str;
    textV.textColor = [UIColor blackColor];
    textV.editable = NO;
    [textV setScrollEnabled:NO];
//    
//    textV.keepTopInset.equal = KeepRequired(7);
//    textV.keepBottomInset.equal = KeepRequired(7);
//    textV.keepRightInset.equal = KeepRequired(10);
//    textV.keepLeftInset.equal = KeepRequired(70);
    
    [cell.contentView addSubview:textV];
    //[textV release];
    
    cell.comment.text = comment_str;
    
    cell.commentTextView.text = [comments objectForKey:@"comment"];
    
    cell.commentAuthor.text = [user objectForKey:@"firstName"];
    
    
    if (user[@"profilePic"] != nil) {
        
        PFFile * pic = [user objectForKey:@"profilePic"];
        
        
        [pic getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
            if (!error) {
                UIImage *image = [UIImage imageWithData:data];
                
                CALayer *imageLayer = cell.commentSenderProfilePic.layer;
                [imageLayer setCornerRadius:5];
                [imageLayer setBorderWidth:1];
                [imageLayer setMasksToBounds:YES];
                [cell.commentSenderProfilePic.layer setCornerRadius:cell.commentSenderProfilePic.frame.size.width/2];
                [cell.commentSenderProfilePic.layer setMasksToBounds:YES];
                [cell.commentSenderProfilePic setImage:image];
            }
            else {
                
                UIImage * defaultimg = [UIImage imageNamed:@"58.png"];
                
                CALayer *imageLayer = cell.commentSenderProfilePic.layer;
                [imageLayer setCornerRadius:5];
                [imageLayer setBorderWidth:1];
                [imageLayer setMasksToBounds:YES];
                [cell.commentSenderProfilePic.layer setCornerRadius:cell.commentSenderProfilePic.frame.size.width/2];
                [cell.commentSenderProfilePic.layer setMasksToBounds:YES];
                [cell.commentSenderProfilePic setImage:defaultimg];
            }
        }];
        
    }
    else {
        UIImage * defaultimg = [UIImage imageNamed:@"PFIcon"];
        
        CALayer *imageLayer = cell.commentSenderProfilePic.layer;
        [imageLayer setCornerRadius:5];
        [imageLayer setBorderWidth:1];
        [imageLayer setMasksToBounds:YES];
        [cell.commentSenderProfilePic.layer setCornerRadius:cell.commentSenderProfilePic.frame.size.width/2];
        [cell.commentSenderProfilePic.layer setMasksToBounds:YES];
        [cell.commentSenderProfilePic setImage:defaultimg];
    }
    
    
    return cell;
    
    
}



- (void) addCommentTouch
{
    [self performSegueWithIdentifier:@"addcoment" sender:self];
}


- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"addcoment"]) {
        PFAddCommentViewController * commentVC = [segue destinationViewController];
        commentVC.partyObject = self.pfParty;
    }
}


@end
