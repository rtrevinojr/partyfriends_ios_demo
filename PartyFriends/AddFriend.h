//
//  AddFriend.h
//  PartyFriends
//
//  Created by Joshua Silva  on 6/25/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface AddFriend : UIViewController <UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *friendPhone;
@property (strong, nonatomic) IBOutlet UIButton *add;
@property (strong, nonatomic) PFUser *current;
@property (strong, nonatomic) PFUser *addedFriend;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *addContactsActivityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *addFriendsFromContacts;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *secondActivityIndicator;

@property (strong, nonatomic) IBOutlet UIImageView *friendImage;
@property (strong, nonatomic) IBOutlet UIButton *search;
@property (strong, nonatomic) IBOutlet UILabel *friendsName;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UILabel *topLabel;


@end
