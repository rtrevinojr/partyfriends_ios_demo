//
//  PFAddCommentViewController.h
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 11/23/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Parse/Parse.h>


@interface PFAddCommentViewController : UIViewController <UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *commentTextView;


@property (strong, nonatomic) PFObject * partyObject;

@property (strong, nonatomic) PFObject * commentObject;


-(IBAction) textFieldDoneEditing : (id) sender;





@end
