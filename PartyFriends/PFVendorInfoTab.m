//
//  PFVendorInfoTab.m
//  VendorProfile
//
//  Created by Gilberto Silva on 11/30/15.
//  Copyright © 2015 Gilberto Silva. All rights reserved.
//

#import "PFVendorInfoTab.h"
#import "UIView+Toast.h"

@interface PFVendorInfoTab ()

@end

float panx = 0.1;
float pany = 0.1;

@implementation PFVendorInfoTab{
    tabbarViewController  * tabbar;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.vendorMap.delegate = self;
    self.vendorInfoScrollView.delegate = self;
    
    self.businessName.delegate = self;
    self.address.delegate = self;
    self.city.delegate = self;
    self.zip.delegate = self;
    self.descriptionView.delegate = self;
    self.businessPhone.delegate = self;
    self.phone.delegate = self;
    self.businessEmail.delegate = self;
    self.email.delegate = self;
    self.businessFax.delegate = self;
    self.fax.delegate = self;
    self.website.delegate = self;
    
    
    // Do any additional setup after loading the view.
    
   // self.vendorMap.userInteractionEnabled = NO;
    self.vendorInfoScrollView.userInteractionEnabled = YES;
    NSLog(@"this is viewDIDLOAD FOR THE FIRST ITEM IN THE TABBARCONTROLLER");
    tabbar = (tabbarViewController * ) self.tabBarController;
    
    
}

-(void)viewWillAppear:(BOOL)animated{

    self.vendorPFObject = tabbar.vendorObject;
    
    NSLog(@"the vendor file allllllLLLLLLLLLLL the way to %@", self.vendorPFObject);
    
    self.businessName.text = self.vendorPFObject[@"businessName"];
    self.address.text = self.vendorPFObject[@"address"];
    
    self.city.text = self.vendorPFObject[@"city"];
    NSNumber * zipcodeNumber = self.vendorPFObject[@"zip"];
    NSString * zipPlace = [NSString stringWithFormat:@"%@",zipcodeNumber];
    self.zip.text = zipPlace;
   // self.zip.text = self.vendorPFObject[@"zip"];
    NSString * address = [NSString stringWithFormat:@"%@ , %@", self.vendorPFObject[@"address"], zipPlace];
    CLGeocoder * busniessPlace = [CLGeocoder new];
    [busniessPlace geocodeAddressString:address completionHandler:^(NSArray * placemarks, NSError * error){
        if (error) {
            
        }else{
            CLPlacemark * placemark = [placemarks objectAtIndex:0];
            
            CLLocation * spot = placemark.location;
            
            
            MKCoordinateRegion region;
            region.center.latitude = spot.coordinate.latitude;
            region.center.longitude = spot.coordinate.longitude;
            region.span = MKCoordinateSpanMake(panx, pany);
            [self.vendorMap setRegion:region animated:YES];
        
        }
        
    }];
    
    self.descriptionView.text = self.vendorPFObject[@"description"];
    if (self.descriptionView.text.length == 0) {
        self.descriptionView.text = @"description";
    }
    self.businessPhone.text = self.vendorPFObject[@"businessPhone"];
    if (self.businessPhone.text.length == 0) {
        self.businessPhone.text = @"business phone";
    }
    self.phone.text = self.vendorPFObject[@"phone"];
    if (self.phone.text.length == 0){
    self.phone.text = @"phone";
    }
    self.businessEmail.text = self.vendorPFObject[@"businessEmail"];
    if (self.businessPhone.text.length == 0) {
        self.businessPhone.text = @"business Email";
    }
    self.email.text = self.vendorPFObject[@"email"];
    if (self.email.text.length == 0){
    self.email.text = @"email";
    }
    self.businessFax.text = self.vendorPFObject[@"businessFax"];
    if (self.businessFax.text.length == 0) {
        self.businessFax.text = @"business Fax";
    }
    self.fax.text = self.vendorPFObject[@"fax"];
    if (self.fax.text.length == 0) {
        self.fax.text = @"fax";
    }
    self.website.text = self.vendorPFObject[@"website"];
    if (self.website.text.length == 0) {
        self.website.text = @"website";
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)save:(id)sender {
    
    NSString * zipString = [self.zip.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString * phoneString = [self.businessPhone.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSRegularExpression * capitalRegex = [NSRegularExpression regularExpressionWithPattern:@"[A-Z]" options:0 error:nil];
    NSRegularExpression * numberRegex = [NSRegularExpression regularExpressionWithPattern:@"[0-9]" options:0 error:nil];
    NSRegularExpression * letterRegex = [NSRegularExpression regularExpressionWithPattern:@"[a-z]" options:0 error:nil];
    
    NSUInteger phoneNumberletter = (unsigned long)[letterRegex matchesInString:phoneString options:0 range:NSMakeRange(0, phoneString.length)].count;
    NSUInteger phoneNumberCap = (unsigned long)[capitalRegex matchesInString:phoneString options:0 range:NSMakeRange(0, phoneString.length)].count;
    
    NSUInteger zipCheckLetter = (unsigned long)[letterRegex matchesInString:zipString options:0 range:NSMakeRange(0, zipString.length)].count;
    NSUInteger zipCheckCap = (unsigned long)[capitalRegex matchesInString:zipString options:0 range:NSMakeRange(0, zipString.length)].count;
    
            if ([zipString length] == 5 && zipCheckLetter == 0 && zipCheckCap == 0) {
                if (phoneString.length == 10 && phoneNumberCap == 0 && phoneNumberletter == 0) {
    
    
    self.vendorPFObject[@"businessName"] = self.businessName.text;
    self.vendorPFObject[@"address"] = self.address.text;
    self.vendorPFObject[@"city"] = self.city.text;
    NSNumberFormatter * zipCode = [NSNumberFormatter new];
    zipCode.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber * zip = [zipCode numberFromString:self.zip.text];
    self.vendorPFObject[@"zip"] = zip;
    self.vendorPFObject[@"description"] = self.descriptionView.text;
    self.vendorPFObject[@"businessPhone"] = self.businessPhone.text;
    self.vendorPFObject[@"phone"] = self.phone.text;
    self.vendorPFObject[@"businessEmail"] = self.businessEmail.text;
    self.vendorPFObject[@"email"] = self.email.text;
    self.vendorPFObject[@"businessFax"] = self.businessFax.text;
    self.vendorPFObject[@"fax"] = self.fax.text;
    self.vendorPFObject[@"website"] = self.website.text;
                    
                    [self.vendorPFObject saveInBackgroundWithBlock:^(BOOL good, NSError * error){
                        
                        if(!error)
                        {
                            
                            [self.view makeToast:@"Info Updated" duration:2 position:CSToastPositionCenter ];
                            
                        }
                        else{
                            
                            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error" message:[error.userInfo objectForKey:@"error"] delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
                            [alertview show];
                            
                        }
                    
                        
                        
                    }];
                
                }else{
                    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Business Phone number should be 10 digits only. No characters." delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
                    [alertview show];
                }
            
            }else{
                UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Zip should be 5 digits." delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
                [alertview show];
                }
            
    
    
}

- (IBAction)backgroundTap:(id)sender {
    
    [self.view endEditing:YES];
    
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{

    [textField resignFirstResponder];
    
    [self.view endEditing:YES];
    
    return YES;
}

@end
