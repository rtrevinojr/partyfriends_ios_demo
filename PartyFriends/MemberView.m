//
//  MemberView.m
//  PartyFriends
//
//  Created by Joshua Silva  on 6/24/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import "MemberView.h"
#import "Reach.h"
#import "UIView+Toast.h"
#import "SignUpView.h"


@interface MemberView ()

@end

@implementation MemberView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    PFUser * currentUser = [PFUser currentUser];
    
    NSLog(@"%@", currentUser);
    
    
    
    self.membersNumber.delegate = self;
    self.membersPassword.delegate = self;
    self.membersPin.delegate = self;
    //self.membersNumber.delegate = self;
    self.activityIndicator.color = [UIColor colorWithRed:(142/255.0) green:(83/255.0) blue:(249/255.0) alpha:1.0];
    self.activityIndicator.hidden = YES;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) activityOffAction{
    self.activityIndicator.hidden = YES;
    [self.activityIndicator stopAnimating];
    
    
}



- (void)textFieldDidBeginEditing:(UITextField *)textField {
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y-200);
    [self.scrollView setContentOffset:scrollPoint animated:YES];
    
    
    if (textField.tag == 1) {
        self.membersPin.text = @"";
    }
    if (textField.tag ==2) {
        self.membersPassword.text = @"";
    }
    
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self.scrollView setContentOffset:CGPointZero animated:YES];
    
    
}

-(void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    
    self.membersPassword.text = @"";
    self.membersNumber.text = @"";
    self.membersPin.text = @"";
    
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"SignUp"]) {
        if (self.firstFriendMember != nil) {
             SignUpView * viewController = (SignUpView *)segue.destinationViewController;
            
            viewController.firstFriend = self.firstFriendMember;
        }
    }
}




- (IBAction)backgroundTap:(id)sender {
    
    [self.view endEditing:YES];
    
}
- (IBAction)backToLogin:(id)sender {
    
    [PFUser logOut];
    
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    
    [self.view endEditing:YES];
    
    
    return YES;
}


- (IBAction)newPF:(id)sender {
    
    
    
    if ([Reachability internetCheck]) {
        [sender setEnabled:NO];
        self.activityIndicator.hidden = NO;
        [self.activityIndicator startAnimating];
    
        
    _userNumber = [self.membersNumber.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString * password = [self.membersPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString * pin = [self.membersPin.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (password.length > 0) {
            
    
    if ([_userNumber length] == 0 || [password length]==0) {
        UIAlertView * error = [[UIAlertView alloc] initWithTitle:@"Missing field(s)." message:@"Please check if the phone number and password field are correct" delegate: nil cancelButtonTitle:@"okay" otherButtonTitles: nil];
        [error show];
        [sender setEnabled:YES];
        [self activityOffAction];
        
    }else{
        if (self.userNumber.length != 10) {
            
            UIAlertView * error = [[UIAlertView alloc] initWithTitle:@"Incorrect Number" message:@"Phone number must be 10 digits." delegate: nil cancelButtonTitle:@"okay" otherButtonTitles: nil];
            [error show];
            [sender setEnabled:YES];
            [self activityOffAction];
            
        }else{
        [PFUser logInWithUsernameInBackground:_userNumber password:password block:^(PFUser *user, NSError *error) {
            if (error) {
                UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Oops!" message:@"Must already be a PartyFriend" delegate:nil cancelButtonTitle:@"Try again!" otherButtonTitles:nil];
                [alertview show];
                
                [sender setEnabled:YES];
                [self activityOffAction];
                // [error.userInfo objectForKey:@"error"]
                
            }
            else {
                /* PFUser *currentUser = [PFUser currentUser];
                 NSLog(@"user that just signed in: %@", currentUser.username); */
                [sender setEnabled:YES];
                [self activityOffAction];
                
                [self performSegueWithIdentifier:@"SignUp" sender:sender ];
                
            }
            
            
        }];
        
        
        }}
        
    }else if (pin.length > 0){
        
        if (self.userNumber.length != 10) {
            
            UIAlertView * error = [[UIAlertView alloc] initWithTitle:@"Incorrect Number" message:@"Phone number must be 10 digits." delegate: nil cancelButtonTitle:@"okay" otherButtonTitles: nil];
            [error show];
            [sender setEnabled:YES];
            [self activityOffAction];
            
        }else{
    
        PFQuery * userQuery = [PFUser query];
        [userQuery whereKey:@"invitePin" equalTo:pin];
        [userQuery whereKey:@"username" equalTo:self.membersNumber.text];
        [userQuery getFirstObjectInBackgroundWithBlock:^(PFObject * member, NSError * error){
            if (!error) {
                
                PFUser * friendPin = (PFUser *)member;
                self.firstFriendMember = friendPin;
                
                NSLog(@"errorPin %@  %@  %@", error, member, self.firstFriendMember);
                [self performSegueWithIdentifier:@"SignUp" sender:sender];
                [sender setEnabled:YES];
                [self activityOffAction];
                
            }else {
            
                UIAlertView * pinAlert = [[UIAlertView alloc] initWithTitle:@"Oops!" message:@"Unable to find user with that pin code" delegate:nil cancelButtonTitle:@"Try again!" otherButtonTitles:nil];
                [pinAlert show];
                
                 NSLog(@"errorPin %@ %ld", error, (long)error.code);
                [sender setEnabled:YES];
                [self activityOffAction];
                
            }
        
        
        }];
        }
    
    }else{
    
        UIAlertView * error = [[UIAlertView alloc] initWithTitle:@"Missing field(s)." message:@"Please check if the phone number and password/pin field are correct" delegate: nil cancelButtonTitle:@"okay" otherButtonTitles: nil];
        [error show];
        [sender setEnabled:YES];
        [self activityOffAction];
    
    }
        [sender setEnabled:YES];
        [self activityOffAction];
    }else if (![Reachability internetCheck]) {
    
        [self.view makeToast:@"Try again: No internet connection" duration:3 position:CSToastPositionCenter];
    }

    
    
}


@end
