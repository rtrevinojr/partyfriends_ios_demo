//
//  PFCommentHistoryVC.h
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 1/14/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Parse/Parse.h>


@interface PFCommentHistoryVC : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UITextViewDelegate>
@property (strong, nonatomic) PFUser * currentUser;
@property (strong, nonatomic) PFObject * pfParty;
@property (strong, nonatomic) PFObject * postObejct;

@property (strong, nonatomic) PFUser * partyUser;
@property (strong, nonatomic) PFUser * pushUser;
@property (strong, nonatomic) PFUser * otherUser;

@property (strong, nonatomic) NSString * userPush;

@property (strong, nonatomic) PFObject * commentObject;

@property (weak, nonatomic) IBOutlet UITextView *cellCommentTextView;

@property (strong, nonatomic) NSMutableArray * commentsList;
@property (strong, nonatomic) NSMutableArray * commentList;

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *commentActionView;

//@property (weak, nonatomic) IBOutlet UITextView *commentTextView;

@property (weak, nonatomic) IBOutlet UIButton *commentPostButton;

@property (weak, nonatomic) IBOutlet UITextView *commentTextView2;

@property (strong, nonatomic) UIRefreshControl * refreshControl;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;



@property (strong, nonatomic) NSMutableArray * headlineList;

@property (strong, nonatomic) NSMutableArray * allCommentsList;

@property (strong, nonatomic) NSMutableArray * head0;
@property (strong, nonatomic) NSMutableArray * head1;
@property (strong, nonatomic) NSMutableArray * head2;
@property (strong, nonatomic) NSMutableArray * head3;
@property (strong, nonatomic) NSMutableArray * head4;
@property (strong, nonatomic) NSMutableArray * head5;
@property (strong, nonatomic) NSMutableArray * head6;
@property (strong, nonatomic) NSMutableArray * head7;
@property (strong, nonatomic) NSMutableArray * head8;
@property (strong, nonatomic) NSMutableArray * head9;




@end
