//
//  StatsView.m
//  PartyFriends
//
//  Created by Joshua Silva  on 8/13/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import "StatsView.h"
#import "ActionView.h"

@interface StatsView ()

@end

@implementation StatsView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.5];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)offine:(id)sender {
    
    ActionView * parent = (ActionView * )[self parentViewController];
    [parent offine];
    
    
}

- (IBAction)partyMode:(id)sender {
    
    
    ActionView * parent = (ActionView * )[self parentViewController];
    [parent partyMode];
}

- (IBAction)goingOut:(id)sender {
    
    ActionView * parent = (ActionView * )[self parentViewController];
    [parent goingOut];
    
    
}
@end
