//
//  tabBarController.h
//  PartyFriends
//
//  Created by Joshua Silva  on 8/4/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActionView.h"
#import <Parse/Parse.h>

@interface tabBarController : UITabBarController

@property (strong, nonatomic) PFGeoPoint * friendsLocation;
@property (strong, nonatomic) PFObject * commentObject;
@property (strong, nonatomic) NSMutableArray * refreshArray;
@property (strong, nonatomic) NSMutableArray * pinsArray;
@property (strong, nonatomic) NSMutableArray * cellsArray;
@property (strong, nonatomic) NSMutableArray * postObjectArray;
@property (strong, nonatomic) NSMutableArray * partyObjectArray;
@property (strong, nonatomic) NSMutableArray * userObjectArray;

@property (strong, nonatomic) PFUser * pinUser;
@property NSUInteger numberPin;

-(void)toZoom;

-(void)segueToComments;

- (void) segueToCommentHistory;

-(void)refresh;

-(void)refreshBothTabs;

-(void)refreshAction;

-(void)refreshAll;

-(void)toTable;

-(void)privateParty;

-(void)removeKeyboard;

@end
