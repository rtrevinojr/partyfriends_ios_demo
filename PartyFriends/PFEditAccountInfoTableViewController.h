//
//  PFEditAccountInfoTableViewController.h
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 1/23/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Parse/Parse.h>


@interface PFEditAccountInfoTableViewController : UITableViewController < UIPickerViewDelegate,UIPickerViewDataSource, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (strong, nonatomic) NSArray * genderArray;
@property (weak, nonatomic) IBOutlet UILabel *bdayLabel;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UITextField *zipcodeTextField;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UITextField *invitePin;
@property (strong, nonatomic) IBOutlet UILabel *genderLabel;

@property (weak, nonatomic) IBOutlet UIView *accountContainerView;

@property (weak, nonatomic) IBOutlet UIDatePicker *bdayDatePicker;

@property (weak, nonatomic) IBOutlet UIPickerView *statePicker;
@property (strong, nonatomic) IBOutlet UIPickerView *genderPicker;

@property (assign, nonatomic) BOOL datePickerIsShowing;

@property (assign, nonatomic) BOOL statePickerIsShowing;
@property (assign, nonatomic) BOOL genderPickerIsShowing;

@property (strong, nonatomic) PFUser * currentUser;
@property (strong, nonatomic) PFObject * userAccountInfo;

@property (strong, nonatomic) NSArray * theStates;
@property (strong, nonatomic) NSString * userState;
@property (strong, nonatomic) NSString * userGender;

@end
