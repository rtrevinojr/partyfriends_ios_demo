//
//  PFPromoViewController.h
//  PartyFriends
//
//  Created by Joshua Silva  on 2/22/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface PFPromoViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) PFObject * vendorPFObject;

@property (strong, nonatomic) NSMutableArray * promoterList;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
