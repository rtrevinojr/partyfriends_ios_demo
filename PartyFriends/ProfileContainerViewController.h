//
//  ProfileContainerViewController.h
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 10/19/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Parse/Parse.h"
#import "PFProfileInfomation.h"


@interface ProfileContainerViewController : UIViewController
{
    
    PFProfileInfomation * fetchName;
    
}

@property (strong, nonatomic) IBOutlet UIButton *toAppleMaps;
@property(strong, nonatomic) NSString * labelString;

@property (strong, nonatomic) NSString * objectId;


@property (strong, nonatomic) IBOutlet UILabel *NameLabel;
@property (strong, nonatomic) IBOutlet UILabel *headline;
@property (strong, nonatomic) IBOutlet UILabel *points;

@property (strong, nonatomic) PFUser * currentUser;
@property (strong, nonatomic) PFUser * user;
@property (strong, nonatomic) PFObject * userParty;

@property (strong, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIImageView * profilePartyPic;

-(void)reload;
-(void)reloadForCurrentUser;

-(void)reloadForCurrentUserAgain;
-(void)reloadUser;

-(void)changeColor;

@end
