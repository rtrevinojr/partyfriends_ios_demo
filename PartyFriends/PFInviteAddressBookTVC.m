//
//  PFInviteAddressBookTVC.m
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 11/5/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import "PFInviteAddressBookTVC.h"

#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>

#import "PFInviteAddressBookCell.h"

#import <MessageUI/MessageUI.h>

//#import "Parse/Parse.h"


@interface PFInviteAddressBookTVC()

@property (strong, nonatomic) NSMutableArray * nameList;

@property (strong, nonatomic) NSMutableArray * phoneList;

@property (strong, nonatomic) NSMutableArray * matchList;

@property (strong, nonatomic) NSMutableArray * addList;

@property (strong, nonatomic) NSMutableArray * recentAddList;


@end


@implementation PFInviteAddressBookTVC: UITableViewController


- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    _nameList = [[NSMutableArray alloc] init];
    _phoneList = [[NSMutableArray alloc] init];
    _matchList = [[NSMutableArray alloc] init];
    
    _recentAddList = [NSMutableArray new];
    
    self.currentUser = [PFUser currentUser];
    
    
    //CFErrorRef myError = NULL;
    //ABAddressBookRef myAddressBook = ABAddressBookCreateWithOptions(NULL, &myError);
    //APLViewController * __weak weakSelf = self;  // avoid capturing self in the block
    //    ABAddressBookRequestAccessWithCompletion(myAddressBook, ^(bool granted, CFErrorRef error) {
    //        if (granted) {
    //        }
    //        else {
    //            // Handle the case of being denied access and/or the error.
    //        }
    //        CFRelease(myAddressBook);
    //    });
    
    //    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    //    [self.view addSubview:spinner];
    //
    //    [spinner startAnimating];
    
    
    
    //
    //
    //    UIActivityIndicatorView* activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleWhiteLarge];
    //
    //    activityIndicator.center = self.tableView.center;
    //
    //    [activityIndicator startAnimating];
    //
    //    [self.tableView addSubview: activityIndicator];
    //
    //
    //
    //    dispatch_async(dispatch_get_main_queue(), ^{
    //        [self fetchAllContacts];
    //
    //        [self fetchAddList];
    //
    //    });
    //
    //
    //
    //    [self.tableView reloadData];
    
    // [self queryContacts];
    
    //    dispatch_async(dispatch_get_main_queue(), ^{
    //        [self fetchAddList];
    //    });
    
    //[self fetchAddList];
    
    
}

-(void)next{
    [self performSegueWithIdentifier:@"fromContacts" sender:self];
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"contactsPAGEdog");
    
    if ([self.check isEqualToString:@"YES"]){
    
        UIBarButtonItem * nextButton = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarStyleDefault target:self action:@selector(next)];\
        self.navigationItem.rightBarButtonItem = nextButton;
        
    }
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
        
        UIActivityIndicatorView* activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleWhiteLarge];
        
        activityIndicator.center = self.tableView.center;
        
        [activityIndicator startAnimating];
        
        [self.tableView addSubview: activityIndicator];
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self fetchAllContacts];
            
            [self fetchAddList];
            
        });
        
        [self.tableView reloadData];
        
    }else{
        self.steps = @[@"Granting permission", @"1. Go to phone Settings", @"2. Go to Privacy", @"3. Go to Contacts", @"4. Enable Contacts"];
        
        UIAlertController * contactsDenied = [UIAlertController alertControllerWithTitle:@"Contact Access Denied" message:@"PartyFriends was denied permission to access your contacts. Please Authorize in the app settings to help serach/invite friends to the network." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
        [contactsDenied addAction:cancelAction];
        
        
        UIAlertAction * settingAction = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
            NSLog(@"asdhfgjk");
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }];;
        
        [self.tableView reloadData];
        [self.tableView setScrollEnabled:NO];
        [contactsDenied addAction:settingAction];
        [self presentViewController:contactsDenied animated:YES completion:nil];
        
    }
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    self.check = @"NO";

}

- (void) queryContacts:(NSArray *) array {
    
    NSMutableArray * contacts = [NSMutableArray arrayWithArray:array];
    PFQuery *query = [PFUser query];
    [query whereKey:@"username" containedIn:contacts];
    
    NSArray * users = [query findObjects];
    
    NSLog(@"****** query contacts **********");
    
    NSLog(@"%@", users);
    
    NSLog(@"***** EQUAL NUMBERS *****");
    
    NSLog(@"Users.size = %lu", [users count]);
    NSLog(@"phonelist.size = %lu", [_phoneList count]);
    
    for (PFObject * object in users) {
        
        NSString * parseNum = [object objectForKey:@"username"];
        
        for (NSString * number in _phoneList) {
            
            if ([parseNum isEqualToString:number]) {
                NSLog(@"number match = %@", parseNum);
                NSLog(@"object = %@", [object objectForKey:@"firstName"]);
                [_matchList addObject:object];
            }
            
        }
        
    }
    NSLog(@"%@", _matchList);
    
    
}

- (void) fetchAddList {
    
    NSMutableArray * matches = _matchList;
    
    
    PFQuery * friendsQuery = [PFUser query];
    
    PFRelation * friendsRel = [[PFUser currentUser] relationForKey:@"friends"];
    
    PFQuery * query = [friendsRel query];
    
    NSArray * adds = [query findObjects];
    self.friends = [NSMutableArray arrayWithArray:adds];
    [matches removeObjectsInArray:adds];
    [matches removeObject:self.currentUser];
    
    // _addList = matches;
    
    [self pendingRequests:matches];
    [self removeMyFriends:self.phoneList :self.nameList :self.friends];
    
}

- (void) pendingRequests:(NSMutableArray *)array
{
    
    NSMutableArray * users = array;
    
    PFQuery * query1 = [PFQuery queryWithClassName:@"friendrequest"];
    [query1 whereKey:@"user1" equalTo:self.currentUser];
    [query1 whereKey:@"user2" containedIn:users];
    [query1 whereKey:@"status" equalTo:@0];
    
    PFQuery * query2 = [PFQuery queryWithClassName:@"friendrequest"];
    [query2 whereKey:@"user1" containedIn:users];
    [query2 whereKey:@"user2" equalTo:self.currentUser];
    [query2 whereKey:@"status" equalTo:@0];
    
    PFQuery * query3 = [PFQuery queryWithClassName:@"friendrequest"];
    [query3 whereKey:@"user1" equalTo:self.currentUser];
    [query3 whereKey:@"user2" containedIn:users];
    [query3 whereKey:@"status" equalTo:@1];
    
    PFQuery * query4 = [PFQuery queryWithClassName:@"friendrequest"];
    [query4 whereKey:@"user1" containedIn:users];
    [query4 whereKey:@"user2" equalTo:self.currentUser];
    [query4 whereKey:@"status" equalTo:@1];
    
    PFQuery * query5 = [PFQuery queryWithClassName:@"friendrequest"];
    [query5 whereKey:@"user1" equalTo:self.currentUser];
    [query5 whereKey:@"user2" containedIn:users];
    [query5 whereKey:@"status" equalTo:@3];
    
    PFQuery * query6 = [PFQuery queryWithClassName:@"friendrequest"];
    [query6 whereKey:@"user1" containedIn:users];
    [query6 whereKey:@"user2" equalTo:self.currentUser];
    [query6 whereKey:@"status" equalTo:@3];
    
    PFQuery * query7 = [PFQuery queryWithClassName:@"friendrequest"];
    [query7 whereKey:@"user1" equalTo:self.currentUser];
    [query7 whereKey:@"user2" containedIn:users];
    [query7 whereKey:@"status" equalTo:@4];
    
    PFQuery * query8 = [PFQuery queryWithClassName:@"friendrequest"];
    [query8 whereKey:@"user1" containedIn:users];
    [query8 whereKey:@"user2" equalTo:self.currentUser];
    [query8 whereKey:@"status" equalTo:@4];
    
    PFQuery * query = [PFQuery orQueryWithSubqueries:@[query1, query2, query3, query4, query5, query6, query7, query8]];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray * objects, NSError * error){
        if (!error) {
            NSLog(@"Pe\  \n ndin\n g requ \n est \n query\n  cou\n nt = %lu", (unsigned long)objects.count);
            NSMutableArray * pendingFriends = [NSMutableArray new];
            for(int i = 0; objects.count>i;i++){
                PFObject *request = objects[i];
                PFUser *user2 = [request objectForKey:@"user1"];
                if(user2 == self.currentUser){
                    user2 = [request objectForKey:@"user2"];
                }
                NSLog(@"Pending friend = %@",user2.objectId);
                [pendingFriends addObject:user2];
            }
            [users removeObjectsInArray:pendingFriends];
            self.addList = users;
            
            [self.tableView reloadData];
        }
        else{
            NSLog(@"Pending request query error = %@", error);
            self.addList = users;
            [self.tableView reloadData];
        }
        
    }];
    
    
}

- (void) removeMyFriends: (NSMutableArray *) phoneArray:(NSMutableArray *) nameArray :(NSMutableArray *) friendArray{
    NSMutableArray * phoneList2 = [NSMutableArray new];
    NSMutableArray * finalPhoneList = [NSMutableArray new];
    NSMutableArray * nameList2 = [NSMutableArray new];
    NSMutableArray * friendsPhoneList = [NSMutableArray new];
    NSLog(@"removeMyFriends phoneList count = %lu", phoneArray.count);
    NSLog(@"removeMyFriends nameList count = %lu", nameArray.count);
    NSLog(@"removeMyFriends friendList count = %lu", friendArray.count);
    
    for(int i = 0; friendArray.count > i; i++){
        PFUser * user = friendArray[i];
        NSString *friendsNumber = user[@"username"];
        [friendsPhoneList addObject:friendsNumber];
    }
    NSLog(@"removeMyFriends friendPhoneList count = %lu", friendsPhoneList.count);
    int friendCount = 0;
    for(int i = 0; friendsPhoneList.count > i; i++){
        NSString * number = friendsPhoneList[i];
        Boolean isFriend = false;
        for(int n = 0; phoneArray.count > n; n++){
            //            NSLog(@"Check 2 %d",n);
            NSString * friendsNumber = phoneArray[n];
            if([friendsNumber isEqualToString:number]){
                isFriend = true;
                friendCount++;
                //                NSLog(@"friend phoneNumber = %@ / %@", friendsNumber,number);
            }
        }
        if(isFriend){
            for(int n = 0; phoneArray.count > n; n++){
                //                NSLog(@"Check %d",n);
                NSString * finalPhone = phoneArray[n];
                if([finalPhone isEqualToString:number]){
                    [phoneList2 addObject:finalPhone];
                    //[nameList2 addObject:nameArray[n]];
                    NSLog(@"friend phoneNumber = %@ / %@", finalPhone,number);
                    
                }
            }
        }
        //        NSLog(@"%d",i);
    }
    
    for(int i = 0; phoneArray.count > i; i++){
        NSString * finalNumber = phoneArray[i];
        Boolean shouldAdd = true;
        for (int n = 0; phoneList2.count > n; n++) {
            NSString * finalNumber2 = phoneList2[n];
            if([finalNumber2 isEqualToString:finalNumber]){
                shouldAdd = false;
            }
        }
        if(shouldAdd){
            [finalPhoneList addObject:finalNumber];
            [nameList2 addObject:nameArray[i]];
        }
    }
    //    NSLog(@"Friend count = %lu", friendCount);
    self.nameList = nameList2;
    self.phoneList = finalPhoneList;
    [self.tableView reloadData];
    
}


- (NSString *) parsePhoneNumber: (NSString *) num {
    
    NSString * reverse = [self reverseString:num];
    NSMutableString * result = [[NSMutableString alloc] init];
    
    NSLog(@"reverse string = %@", reverse);
    
    for (int i = 0; i < [reverse length]; i++) {
        
        if ([reverse characterAtIndex:i] >= '0' && [reverse characterAtIndex:i] <= '9') {
            //[result appendString:[NSString stringWithFormat:@"%c", [reverse characterAtIndex:i]]];
            
            NSString * character = [NSString stringWithFormat:@"%c", [reverse characterAtIndex:i]];
            
            result = [character stringByAppendingString:result];
        }
    }
    
    if (([result hasPrefix:@"0"] || [result hasPrefix:@"1"]) && [result length] > 1) {
        result = [result substringFromIndex:1];
    }
    
    NSLog(@"result string = %@", result);
    
    return result;
    
}

- (NSString *)reverseString:(NSString *)input {
    NSUInteger len = [input length];
    NSMutableString *result = [[NSMutableString alloc] initWithCapacity:len];
    for (int i = len - 1; i >= 0; i--) {
        [result appendFormat:@"%c", [input characterAtIndex:i]];
    }
    return result;
}



- (void) fetchAllContacts {
    
    ABAddressBookRef allPeople = ABAddressBookCreate();
    CFArrayRef allContacts = ABAddressBookCopyArrayOfAllPeople(allPeople);
    CFIndex numberOfContacts  = ABAddressBookGetPersonCount(allPeople);
    
    NSLog(@"numberOfContacts------------------------------------%ld",numberOfContacts);
    
    
    for (int i = 0; i < numberOfContacts; i++) {
        NSString* name = @"";
        NSString* phone = @"";
        NSString* email = @"";
        
        ABRecordRef aPerson = CFArrayGetValueAtIndex(allContacts, i);
        ABMultiValueRef fnameProperty = ABRecordCopyValue(aPerson, kABPersonFirstNameProperty);
        ABMultiValueRef lnameProperty = ABRecordCopyValue(aPerson, kABPersonLastNameProperty);
        
        ABMultiValueRef phoneProperty = ABRecordCopyValue(aPerson, kABPersonPhoneProperty);
        ABMultiValueRef emailProperty = ABRecordCopyValue(aPerson, kABPersonEmailProperty);
        
        NSArray *emailArray = (__bridge NSArray *)ABMultiValueCopyArrayOfAllValues(emailProperty);
        NSArray *phoneArray = (__bridge NSArray *)ABMultiValueCopyArrayOfAllValues(phoneProperty);
        
        
        if (fnameProperty != nil) {
            name = [NSString stringWithFormat:@"%@", fnameProperty];
        }
        if (lnameProperty != nil) {
            name = [name stringByAppendingString:[NSString stringWithFormat:@" %@", lnameProperty]];
        }
        
        if ([phoneArray count] > 0) {
            if ([phoneArray count] > 1) {
                for (int i = 0; i < [phoneArray count]; i++) {
                    phone = [phone stringByAppendingString:[NSString stringWithFormat:@"%@\n", [phoneArray objectAtIndex:i]]];
                }
            }else {
                phone = [NSString stringWithFormat:@"%@", [phoneArray objectAtIndex:0]];
            }
        }
        
        if ([emailArray count] > 0) {
            if ([emailArray count] > 1) {
                for (int i = 0; i < [emailArray count]; i++) {
                    email = [email stringByAppendingString:[NSString stringWithFormat:@"%@\n", [emailArray objectAtIndex:i]]];
                }
            }else {
                email = [NSString stringWithFormat:@"%@", [emailArray objectAtIndex:0]];
            }
        }
        
        NSLog(@"NAME : %@",name);
        NSLog(@"PHONE: %@",phone);
        NSLog(@"EMAIL: %@",email);
        NSLog(@"\n");
        
        [_nameList addObject:name];
        
        NSLog(@"_phoneList PHONE = %@", [self parsePhoneNumber:phone]);
        
        [_phoneList addObject:[self parsePhoneNumber:phone]];
        
        
    }
    
    NSLog(@"name list count = %lu", [_nameList count]);
    NSLog(@"phone list count = %lu", [_phoneList count]);
    [self queryContacts:self.phoneList];
}


- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *CellIdentifier = @"inviteCell";
    PFInviteAddressBookCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[PFInviteAddressBookCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    if (ABAddressBookGetAuthorizationStatus()==kABAuthorizationStatusAuthorized) {
        
        
        
        if (indexPath.section == 1) {
            NSString * name = [_nameList objectAtIndex:indexPath.row];
            NSString * phone = [_phoneList objectAtIndex:indexPath.row];
            
            NSLog(@"cell name = %@", name);
            NSLog(@"cell phone = %@", phone);
            
            cell.ContactNameLabel.text = name;
            cell.ContactPhoneLabel.text = phone;
            
            //        CALayer *addbtnLayer = [cell.ContactInviteBtn layer];
            //        [addbtnLayer setMasksToBounds:YES];
            //        [addbtnLayer setCornerRadius:10.0f];
            //
            //        [addbtnLayer setBorderWidth:1.0f];
            //        [addbtnLayer setBorderColor:[[UIColor blackColor] CGColor]];
            //        [cell.ContactInviteBtn setTitle:@"Invite" forState:UIControlStateNormal];
            
        }
        else if (indexPath.section == 0) {
            
            //PFObject * obj = [_matchList objectAtIndex:indexPath.row ];
            PFObject *obj = [_addList objectAtIndex:indexPath.row];
            
            NSString * name = [obj objectForKey:@"firstName"];
            NSString * phone = [obj objectForKey:@"username"];
            
            cell.ContactNameLabel.text = name;
            cell.ContactPhoneLabel.text = phone;
            
            cell.cellObjectId = [obj objectId];
            
            //        CALayer *addbtnLayer = [cell.ContactInviteBtn layer];
            //        [addbtnLayer setMasksToBounds:YES];
            //        [addbtnLayer setCornerRadius:10.0f];
            //
            //        [addbtnLayer setBorderWidth:1.0f];
            //        [addbtnLayer setBorderColor:[[UIColor blackColor] CGColor]];
            
            
            //cell.ContactInviteBtn.titleLabel.text = @"+";
            [cell.ContactInviteBtn setTitle:@"+" forState:UIControlStateNormal];
        }}else{
            cell.ContactPhoneLabel.text = [self.steps objectAtIndex:indexPath.row];
            
            //[cell.ContactInviteBtn setHidden:YES];
            [cell.ContactNameLabel setHidden:YES];
            
        }
    
    return cell;
    
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    if (ABAddressBookGetAuthorizationStatus()==kABAuthorizationStatusAuthorized) {
        
        return 2;
    }else{
        return 1;
        
    }
}

- (NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (ABAddressBookGetAuthorizationStatus()==kABAuthorizationStatusAuthorized) {
        
        
        if (section == 0)
            return @"ADD FRIENDS";
        else
            return @"INVITE FRIENDS";
    }else{
        return @"Contacts Access Denied";
    }
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (ABAddressBookGetAuthorizationStatus()==kABAuthorizationStatusAuthorized) {
        
        
        if (section == 0)
            return _addList.count;
        else
            return _nameList.count;
    }else{
        return self.steps.count;
    }
}


- (void)add:(id)sender withNumber:(NSString *)num {
    
    //NSString *friendNumber = [self.friendPhone.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    UIButton *btn = (UIButton *)sender;
    
    PFInviteAddressBookCell * cell = (PFInviteAddressBookCell*)[[sender superview] superview];
    
    //CGPoint pointInSuperview = [btn.superview convertPoint:btn.center toView:self.tableView];
    // Infer Index Path
    //NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:pointInSuperview];
    
    NSString * cellstr = cell.cellObjectId;
    
    if ([_recentAddList containsObject:cellstr]) {
        
        [btn setBackgroundColor:[UIColor whiteColor]];
        [btn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        
        [_recentAddList removeObject:cellstr];
    }
    else {
        
        [btn setBackgroundColor:[UIColor blueColor]];
        
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [_recentAddList addObject:cellstr];
        
        NSString * friendNumber = num;
        
        PFQuery *findFriend= [PFUser query];
        [findFriend whereKey:@"username" equalTo:friendNumber];
        [findFriend findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            NSLog(@"%@",objects);
            if (!error){
                
                PFUser * addedFriend = objects[0];
                
                PFUser * current = [PFUser currentUser];
                
                //checking if there is no previous pending request
                PFQuery *friendRequest =[PFQuery queryWithClassName:@"friendrequest"];
                [friendRequest whereKey:@"user1" equalTo:current];
                [friendRequest whereKey:@"user2" equalTo:addedFriend];
                [friendRequest whereKey:@"status" equalTo:@0];
                
                [friendRequest findObjectsInBackgroundWithBlock:^(NSArray *objects1, NSError *error) {
                    NSLog(@"%@tncycftyntcfntufnu",objects1);
                    if (objects1.count==0){
                        NSLog(@"%@",error);
                        //sending new friendrequest
                        PFObject *sendRequest=[PFObject objectWithClassName:@"friendrequest"];
                        sendRequest[@"user1"]=current;
                        sendRequest[@"user2"]=addedFriend;
                        sendRequest[@"status"]=@0;
                        [sendRequest saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
                            
                            if(!error){
                                NSLog(@"Request Sent!");
                            }
                            else if (error){
                                NSLog(@"3%@%@",error,error.userInfo);
                            }
                        }];
                        
                    }
                    else if (!error){
                        
                        NSLog(@"Request Still Pending:");
                        
                    }
                    else {
                        
                        NSLog(@"2%@%@",error,error.userInfo);
                        
                    }
                }];
                
                
            }
            else if (error.code==101){
                
                NSLog(@"User Not in Party Friends");
                
            }
            else if (error){
                
                NSLog(@"%@%@",error,error.userInfo);
                
            }else{
                
                NSLog(@"waffles!!!!!");
                
            }
        }
         
         
         ];
        
    }
    
}




- (IBAction)sendInvite:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    CGPoint pointInSuperview = [btn.superview convertPoint:btn.center toView:self.tableView];
    // Infer Index Path
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:pointInSuperview];
    
    PFInviteAddressBookCell * cell = (PFInviteAddressBookCell *)[[sender superview] superview];
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
        
        
        if (indexPath.section == 1) {
            NSString * name = cell.ContactNameLabel.text;
            NSString * phone = cell.ContactPhoneLabel.text;
            
            MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init] ;
            if([MFMessageComposeViewController canSendText])
            {
                NSString * textMessage = [NSString stringWithFormat:@"%@ would like for you to party with them on PartyFriends! Use this pin to accept \n invitePin:%@ \n www.partyfriendsapp.com", self.currentUser[@"firstName"], self.currentUser[@"invitePin"]];
                
                controller.body = textMessage;
                controller.recipients = [NSArray arrayWithObjects:phone, nil];
                controller.messageComposeDelegate = self;
                [self presentModalViewController:controller animated:YES];
            }
            
        }
        else {
            
            NSString * phone = cell.ContactPhoneLabel.text;
            
            [self add:sender withNumber:phone];
        }
    }else{
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        
    }
    
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end