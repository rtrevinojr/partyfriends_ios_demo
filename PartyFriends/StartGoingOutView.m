//
//  TESTING.m
//  PartyFriends
//
//  Created by Joshua Silva  on 8/28/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import "StartGoingOutView.h"
#import "ActionView.h"
#import <ParseUI/ParseUI.h>
#import "PFGoingOutTableViewCell.h"

@interface StartGoingOutView ()

@end

@implementation StartGoingOutView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //[self.tapGesture cancelsTouchesInView];
    self.tapGesture.cancelsTouchesInView = NO;
    
    self.editLocation.delegate = self;
    
    self.currentUser = [PFUser currentUser];
    
    self.waffles = [[NSArray alloc] initWithObjects:@"waffles", @"pancakes", @"flapjacks", @"french toast",@"flapjaks2 2", @"cereal", @"bacon", @"toast", nil];
    
    self.allFriendsRelation = [[PFUser currentUser]
                       objectForKey:@"friends"];
    self.exclusiveRelation = [[PFUser currentUser] relationForKey:@"exclusive"];
    self.beforePicked = [NSArray new];
    
    self.tableView.delegate = self;
    
    self.cancelX = [[UIImageView alloc] initWithFrame:CGRectMake(20, 20, 20, 20)];
    [self.cancelX setImage:[UIImage imageNamed:@"CancelViewX.png"]];
    self.pickedFriendsRelation = [PFRelation new];
    
    self.unpickedFriends = [[NSMutableArray alloc]init];
    self.wantedFriends = [NSMutableArray new];
    
    if (self.sessionToken == nil) {
        [PFSession getCurrentSessionInBackgroundWithBlock:^(PFSession * session, NSError * error){
            if (!error) {
                self.sessionToken = session;
                
                
            }else{
                
                
            }
            
        }];
    }
    
    
    UIButton * backButtonView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [backButtonView addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    [backButtonView setBackgroundImage:[UIImage imageNamed:@"GoldBackButton"] forState:UIControlStateNormal];
    
    UIBarButtonItem * backBUTTON = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
    self.navigationItem.leftBarButtonItem = backBUTTON;
    
    
    [self.allSwitch setOn:NO];
    
}
- (IBAction)switchAction:(id)sender {
    self.unpickedFriends = nil;
    self.pickFriends = nil;
                NSLog(@"BEFORE number count of all three arrays all=%lu , picked = %lu , unpicked = %lu", (unsigned long)self.allFriends.count, (unsigned long)self.pickFriends.count, (unsigned long)self.unpickedFriends.count);
    
    UISwitch *switchObject = (UISwitch *)sender;
    if(switchObject.isOn){
        NSLog(@"switchON");
        self.pickFriends = [[NSMutableArray alloc] initWithArray:self.allFriends];
        self.unpickedFriends = [[NSMutableArray alloc] init];
//        for (int i = 0; self.allFriends.count > i ; i++) {
//            
//            PFUser * user = self.allFriends[i];
//            //[self.unpickedFriends removeObject:user];
//            [self.pickFriends addObject:user];
//            //[self.pickFriends addObject:self.unpickedFriends[i]];
//            
//            
//            NSLog(@"INSIDE number count of all three arrays all=%lu , picked = %lu , unpicked = %lu", (unsigned long)self.allFriends.count, (unsigned long)self.pickFriends.count, (unsigned long)self.unpickedFriends.count);
//        }
        NSLog(@"number count of all three arrays all=%lu , picked = %lu , unpicked = %lu", (unsigned long)self.allFriends.count, (unsigned long)self.pickFriends.count, (unsigned long)self.unpickedFriends.count);
        [self.tableView reloadData];
        
    }else{
        NSLog(@"switchOFF");
        self.pickFriends = [[NSMutableArray alloc] init];
        self.unpickedFriends = [[NSMutableArray alloc] initWithArray:self.allFriends];
//        for (int i = 0; self.allFriends.count > i; i ++) {
//            PFUser * user = self.allFriends[i];
//            [self.unpickedFriends addObject:user];
//            //[self.pickFriends removeObject:user];
//            //[self.unpickedFriends addObject:self.pickFriends[i]];
//            
//            NSLog(@"OUTSIDE number count of all three arrays all=%lu , picked = %lu , unpicked = %lu", (unsigned long)self.allFriends.count, (unsigned long)self.pickFriends.count, (unsigned long)self.unpickedFriends.count);
//            
//        }
        
        
        NSLog(@"number count of all three arrays all=%lu , picked = %lu , unpicked = %lu", (unsigned long)self.allFriends.count, (unsigned long)self.pickFriends.count, (unsigned long)self.unpickedFriends.count);
        [self.tableView reloadData];
    }
    
}
- (IBAction)random:(id)sender {
    [self.allSwitch setOn:NO];
    
    
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:NO];
    
   /* PFQuery * query7 = [PFUser query];
    [query7 includeKey:@"myRole"];
    [query7 findObjectsInBackgroundWithBlock:^(NSArray * objects, NSError * error){
    
        self.currentUser = objects[0];
        self.rolling = [self.currentUser objectForKey:@"myRole"];
        if(self.rolling == NULL){
        
            [PFCloud callFunctionInBackground:@"CheckRole" withParameters:@{@"":@""} block:^(NSString * results, NSError * error){
            
                if (!error) {
                    PFQuery * query5 = [PFUser query];
                    [query5 includeKey:@"myRole"];
                    [query5 findObjectsInBackgroundWithBlock:^(NSArray * objects, NSError * error){
                    
                        self.currentUser = objects[0];
                        self.rolling = [self.currentUser objectForKey:@"myRole"];
                        
                    
                    }];
                
                
                }
            
            }];
        }
            
    
    
    }
     ];*/
    NSString * roleName = [NSString stringWithFormat:@"Role-%@", self.currentUser.objectId];
    

    

    PFQuery * query = [self.allFriendsRelation query];
    [query orderByAscending:@"firstName"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error){
            NSLog(@"error %@ %@", error, [error userInfo]);
            
        }
        else{
            
            self.allFriends = objects;
            NSLog(@"this checking to see all the current friends on parse of the user NUMBER 2, %lu",(unsigned long)[self.allFriends count]);
            
            PFQuery * queryExclusive = [self.exclusiveRelation query];
            [queryExclusive orderByAscending:@"firstName"];
            [queryExclusive findObjectsInBackgroundWithBlock:^(NSArray *pickedObjects, NSError *error){
                if (error) {
                    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Sorry" message:[error.userInfo objectForKey:@"error"] delegate:nil cancelButtonTitle:@"Try again!!" otherButtonTitles:nil];
                    [alertview show];
                }else{
                    self.beforePicked = pickedObjects;
                    
            [self.unpickedFriends addObjectsFromArray:self.allFriends];
                    self.pickFriends = [NSMutableArray arrayWithArray:self.beforePicked];
            //self.pickFriends = pickedObjects;
                    
                     NSLog(@"unpicked friends5 %lu %lu", (unsigned long)self.pickFriends.count, (unsigned long)self.beforePicked.count);
                    
                    for (int i = 0; i < self.pickFriends.count ; i++) {
                       // PFUser * user = [self.pickFriends objectAtIndex:i];
                                                [self.unpickedFriends removeObject:self.pickFriends[i]];
                         NSLog(@"unpicked friends4 %lu", (unsigned long)self.unpickedFriends.count);
                        
                    }
                    
                 
                    NSLog(@"unpicked friends %lu", (unsigned long)self.unpickedFriends.count);
//                    for (int k = 0; k < self.pickFriends.count; k++){
//  NSLog(@"unpicked friends3 %lu", (unsigned long)self.unpickedFriends.count);
//                    }
                                        NSLog(@"unpicked friends 2 %lu", (unsigned long)self.unpickedFriends.count);
                    
                    [self.tableView reloadData];
                }
                
                
            }];
            
//            for (int i = 0; i < self.allFriends.count; i++) {
//                PFUser * user = [self.allFriends objectAtIndex:i];
//                [self.pickFriends addObject:user];
//            }
            
            //[self.tableView reloadData];
            
 
            
            
            
            PFQuery * queryRole = [PFRole query];
            [queryRole whereKey:@"name" equalTo:roleName];
            [queryRole getFirstObjectInBackgroundWithBlock:^(PFObject * object, NSError * error){
                if (error || object== nil){
                    NSLog(@"this error belongs to the role assigning block -- %@", error);
                }else {
                    self.rolling = (PFRole *)object;
                    NSLog(@"checking to see if the role went through and the size of objects %@", self.rolling);
                    self.pickedFriendsRelation = self.rolling.users;
                    
                    for (int i = 0 ; i < self.allFriends.count; i++) {
                        [self.pickedFriendsRelation addObject:self.allFriends[i]];
                        
                     //   [self.tableView reloadData];
                    }
                    
                    
                }
                
                
            }];
            
            
        }
        
    }];

    
    NSCalendar * day = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate * today = [NSDate date];
    NSDateComponents * comps = [NSDateComponents new];
    [comps setHour:18];
    //[comps setDay:1];
    NSDate * max = [day dateByAddingComponents:comps toDate:today options:0];
    //[comps setHour:-3];

    
    
    [self.setWhen setMinimumDate:today];
    [self.setWhen setMaximumDate:max];
    
    
    
   /* NSString * roleName = [NSString stringWithFormat:@"Role-%@", self.currentUser.objectId];
    PFQuery *qquery = [PFRole query];
    [qquery whereKey:@"name" equalTo:roleName];
    [qquery findObjectsInBackgroundWithBlock:^(NSArray * objects, NSError * error){
        NSLog(@"here");
        if (error.code == 101 || objects.count==0) {
            NSLog(@"here2");
            
            PFACL *acl = [PFACL ACL];
            [acl setPublicReadAccess:false];
            [acl setPublicWriteAccess:false];
            [acl setReadAccess:true forUser:[PFUser currentUser]];
            [acl setWriteAccess:true forUser:[PFUser currentUser]];
          
            PFRole *role = [PFRole roleWithName:roleName];
            [role setACL:acl];
            [role saveInBackgroundWithBlock:^(BOOL succeeded, NSError * error){
                
                if (!error) {
                    PFQuery *query = [PFRole query];
                    [query whereKey:@"name" equalTo:roleName];
                    [query findObjectsInBackgroundWithBlock:^(NSArray * objects, NSError * error){
                        NSLog(@"double checking");
                        if (error) {
                            NSLog(@"double checking");
                        }else if(objects.count>0){
                            NSLog(@"double checking2");
                            
                            
                            self.rolling = objects[0];
                            
                            [self.tableView reloadData];
                        }else{

                        }

                    }];
                    }
                
            }];
        }else{
            self.rolling = objects[0];

        }
        
        
        
    }];
*/
    
    
    NSLog(@"this checking to see all the current friends on parse of the user, %lu",(unsigned long)[self.allFriends count]);
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
    self.scrollView.contentSize = self.contain.bounds.size;
 
}


-(void)localNotificationGoingOut:(NSDate *)firetime {
    UILocalNotification * notification = [UILocalNotification  new];
    notification.applicationIconBadgeNumber = 1;
    
    notification.fireDate = firetime;
    notification.alertBody = @"Party Time!";
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.allFriends count];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return NO;
    
}

- (PFGoingOutTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * cellId = @"cell";
    //    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
    PFGoingOutTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    
    if (cell == nil) {
        cell = [[PFGoingOutTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        
        UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
        [cell.imageView setImage:defaultimg];
        CALayer *imageLayer = cell.imageView.layer;
        [imageLayer setCornerRadius:5];
        [imageLayer setBorderWidth:0];
        [imageLayer setMasksToBounds:YES];
        [cell.imageView.layer setCornerRadius:cell.imageView.frame.size.width/2];
        [cell.imageView.layer setMasksToBounds:YES];
        
    }
    else{
        
//        UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
//        [cell.imageView setImage:defaultimg];
//        CALayer *imageLayer = cell.profileView.layer;
//        [imageLayer setCornerRadius:5];
//        [imageLayer setBorderWidth:0];
//        [imageLayer setMasksToBounds:YES];
//        [cell.profileView.layer setCornerRadius:cell.profileView.frame.size.width/2];
//        [cell.profileView.layer setMasksToBounds:YES];
        
    }
    PFUser * user = [self.allFriends objectAtIndex:indexPath.row];
    
    CALayer *imageLayer = cell.profileView.layer;
    [imageLayer setCornerRadius:5];
    [imageLayer setBorderWidth:0];
    [imageLayer setMasksToBounds:YES];
    [cell.profileView.layer setCornerRadius:cell.profileView.frame.size.width/2];
    [cell.profileView.layer setMasksToBounds:YES];
    
           UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
         [cell.profileView setImage:defaultimg];
    
//    cell.textLabel.textColor = [UIColor blackColor];
//    cell.textLabel.text = user[@"firstName"];
    cell.userName.text = user[@"firstName"];
    
    
    
    
    
    
    
    
    //    UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
    //    [cell.imageView setImage:defaultimg];
    
    
    //cell.imageView.image =
    
    /*if ([self.pickFriends containsObject:user.objectId]){
     
     cell.accessoryType = UITableViewCellAccessoryCheckmark;
     
     //[self.rolling.users addObject:user];
     
     }else{
     cell.accessoryType = UITableViewCellAccessoryNone ;
     [self.rolling.users removeObject:user];
     
     }*/
    //[self.allFriends containsObject:user.objectId]  ||
    
    if ([self.pickFriends containsObject:user]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    
    
    
    PFObject * photo = user[@"profilePicture"];
    
        if (photo !=nil) {
            [photo fetchInBackgroundWithBlock:^(PFObject * photoObject, NSError * error){
                if (!error) {
                    PFFile * pic =[photoObject objectForKey:@"photo"];
                    PFImageView * userImage =[PFImageView new];
                    userImage.image = [UIImage imageNamed:@"userPic"];
                    userImage.file = (PFFile *) pic;
    
                    [userImage loadInBackground:^(UIImage * image, NSError * error){
                        if (!error) {
 
                            [cell.profileView setImage:image];
                        }else {
 
                            UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
                            [cell.profileView setImage:defaultimg];
    
                        }
    
                    }];
                }
    
    
            }]; }
    //    else{
    //            CALayer *imageLayer = cell.imageView.layer;
    //            [imageLayer setCornerRadius:5];
    //            [imageLayer setBorderWidth:0];
    //            [imageLayer setMasksToBounds:YES];
    //            [cell.imageView.layer setCornerRadius:cell.imageView.frame.size.width/2];
    //            [cell.imageView.layer setMasksToBounds:YES];
    //            UIImage * defaulting = [UIImage imageNamed:@"NoImage"];
    //            [cell.imageView setImage:defaulting];
    //            UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
    //            [cell.imageView setImage:defaultimg];
    //        
    //        }
    
    return cell;
    
    
    
}


//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    
//    static NSString * cellId = @"cell";
////    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
//    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellId];
//    
//    
//    if (cell == nil) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
//        
//        UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
//        [cell.imageView setImage:defaultimg];
//        CALayer *imageLayer = cell.imageView.layer;
//        [imageLayer setCornerRadius:5];
//        [imageLayer setBorderWidth:0];
//        [imageLayer setMasksToBounds:YES];
//        [cell.imageView.layer setCornerRadius:cell.imageView.frame.size.width/2];
//        [cell.imageView.layer setMasksToBounds:YES];
//        
//    }
//    else{
//        
//        UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
//        [cell.imageView setImage:defaultimg];
//        CALayer *imageLayer = cell.imageView.layer;
//        [imageLayer setCornerRadius:5];
//        [imageLayer setBorderWidth:0];
//        [imageLayer setMasksToBounds:YES];
//        [cell.imageView.layer setCornerRadius:cell.imageView.frame.size.width/2];
//        [cell.imageView.layer setMasksToBounds:YES];
//        
//    }
//    PFUser * user = [self.allFriends objectAtIndex:indexPath.row];
//
//    
//   cell.textLabel.textColor = [UIColor blackColor];
//    cell.textLabel.text = user[@"firstName"];
//
//    
//
//    
//    
//    
//
//
////    UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
////    [cell.imageView setImage:defaultimg];
//    
//    
//    //cell.imageView.image =
//    
//    /*if ([self.pickFriends containsObject:user.objectId]){
//        
//        cell.accessoryType = UITableViewCellAccessoryCheckmark;
//        
//               //[self.rolling.users addObject:user];
//        
//    }else{
//        cell.accessoryType = UITableViewCellAccessoryNone ;
//   [self.rolling.users removeObject:user];
//        
//    }*/
//    //[self.allFriends containsObject:user.objectId]  ||
//    
//    if ([self.pickFriends containsObject:user]) {
//        cell.accessoryType = UITableViewCellAccessoryCheckmark;
//    }else{
//        cell.accessoryType = UITableViewCellAccessoryNone;
//    }
//    
//    
//
//    
//    PFObject * photo = user[@"profilePicture"];
//    
////    if (photo !=nil) {
////        [photo fetchInBackgroundWithBlock:^(PFObject * photoObject, NSError * error){
////            if (!error) {
////                PFFile * pic =[photoObject objectForKey:@"photo"];
////                PFImageView * userImage =[PFImageView new];
////                userImage.image = [UIImage imageNamed:@"userPic"];
////                userImage.file = (PFFile *) pic;
////                
////                [userImage loadInBackground:^(UIImage * image, NSError * error){
////                    if (!error) {
////                        CALayer *imageLayer = cell.imageView.layer;
////                        [imageLayer setCornerRadius:5];
////                        [imageLayer setBorderWidth:0];
////                        [imageLayer setMasksToBounds:YES];
////                        [cell.imageView.layer setCornerRadius:cell.imageView.frame.size.width/2];
////                        [cell.imageView.layer setMasksToBounds:YES];
////                        [cell.imageView setImage:image];
////                    }else {
////                        CALayer *imageLayer = cell.imageView.layer;
////                        [imageLayer setCornerRadius:5];
////                        [imageLayer setBorderWidth:0];
////                        [imageLayer setMasksToBounds:YES];
////                        [cell.imageView.layer setCornerRadius:cell.imageView.frame.size.width/2];
////                        [cell.imageView.layer setMasksToBounds:YES];
////                        UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
////                        [cell.imageView setImage:defaultimg];
////                        
////                    }
////                    
////                }];
////            }
////            
////            
////        }]; }
////    else{
////            CALayer *imageLayer = cell.imageView.layer;
////            [imageLayer setCornerRadius:5];
////            [imageLayer setBorderWidth:0];
////            [imageLayer setMasksToBounds:YES];
////            [cell.imageView.layer setCornerRadius:cell.imageView.frame.size.width/2];
////            [cell.imageView.layer setMasksToBounds:YES];
////            UIImage * defaulting = [UIImage imageNamed:@"NoImage"];
////            [cell.imageView setImage:defaulting];
////            UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
////            [cell.imageView setImage:defaultimg];
////        
////        }
//   
//    return cell;
//   
//    
//    
//}

/*- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cellCheck = [tableView
                                  cellForRowAtIndexPath:indexPath];
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    cellCheck.accessoryType = UITableViewCellAccessoryCheckmark;
}*/

/*- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* uncheckCell = [tableView
                                    cellForRowAtIndexPath:indexPath];
    uncheckCell.accessoryType = UITableViewCellAccessoryNone;
}
*/


-(void)tableView: (UITableView *) tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    //[tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSLog(@"CHECKING IF ALL THIS IS WORKING?!?!?!??!?!?!??!?!?!??!?!");

   [self.tableView deselectRowAtIndexPath:indexPath animated:NO];

    
    UITableViewCell * cell = [self.tableView cellForRowAtIndexPath:indexPath];
    //cell.accessoryView = self.cancelX;
    
        PFUser * user = [self.allFriends objectAtIndex:indexPath.row];
    
    
    if (cell.accessoryType == UITableViewCellAccessoryNone) {
        
        
    
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [self.unpickedFriends removeObject:user];
         [self.pickFriends addObject:user];
        NSLog(@"wafflesIOP %lu then %lu", (unsigned long)self.pickFriends.count, (unsigned long)self.unpickedFriends.count);
    }else {
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        if (self.allFriends.count == self.pickFriends.count) {
            [self.allSwitch setOn:NO];
        }
        
        [self.unpickedFriends addObject:user];
        [self.pickFriends removeObject:user];
     NSLog(@"wafflesIOP %lu then %lu", (unsigned long)self.pickFriends.count, (unsigned long)self.unpickedFriends.count);
    }
    //cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    
 /*
   if (cell.accessoryType == UITableViewCellAccessoryNone) {
        cell.accessoryType =  UITableViewCellAccessoryCheckmark;
        ///[self.rolling.users addObject:user];
        NSLog(@"%@",user);
        NSLog(@"%@",self.rolling.users);

        //[self.pickFriends addObject:user.objectId];
        
        
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
        [self.pickFriends removeObject:user.objectId];
        NSLog(@"%@",user);
        NSLog(@"%@",self.rolling.users);

      // [self.rolling.users removeObject:user];
        
    
    }
 */
   
 

    NSLog(@"%@", self.unpickedFriends);

}


/*
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath  {




}
*/


#pragma mark - Navigation

 //In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
  //   Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"beginExclusive"]) {
        ActionView * backToAction = (ActionView * ) segue.destinationViewController;
        backToAction.exclusiveHeadline = self.editLocation.text;
        
    }
}


- (IBAction)startGoingOut:(id)sender {
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.timeZone = [NSTimeZone defaultTimeZone];
    dateFormatter.timeStyle = NSDateFormatterShortStyle;
    dateFormatter.dateStyle = NSDateFormatterShortStyle;
    
    NSString * dataString = [dateFormatter stringFromDate:self.setWhen.date];
    
    NSLog(@"the time you'd like to go out %@", dataString);
    NSLog(@"waffles %@ %@ \n %lu \n %lu", self.rolling.users, self.exclusiveRelation, (unsigned long)self.pickFriends.count, (unsigned long)self.unpickedFriends.count);
    NSLog(@"if everyhting goes through!1!");
    for (int e = 0 ; e < self.pickFriends.count; e ++) {
        NSLog(@"if everyhting goes through!0!");
        PFUser * user = self.pickFriends[e];
        NSLog(@"if everyhting goes through!09!");

        [self.exclusiveRelation addObject:user];
    }NSLog(@"if everyhting goes through!2!");
    
    for (int w = 0; w <self.unpickedFriends.count; w++) {NSLog(@"if everyhting goes through!3!");
        PFUser * user = self.unpickedFriends[w];
        [self.exclusiveRelation removeObject:user];
        //[self.pickedFriendsRelation removeObject:self.unpickedFriends[i]];
    }
   /* PFRelation * guestList = [PFRelation new];
    guestList = self.rolling.users;

    for (int i = 0 ; i < [self.pickFriends count]; i++) {
        [guestList addObject:[self.pickFriends objectAtIndex:i]];
    }*/
    PFObject * log = [PFObject objectWithClassName:@"log"];
    
    
    if(self.sessionToken == nil){
    [PFSession getCurrentSessionInBackgroundWithBlock:^(PFSession * _Nullable session, NSError * _Nullable error) {
        if (!error) {
           [self.currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
               if (!error) {
                   self.sessionToken = session;
                   log[@"status"] = @2;
                   log[@"deviceInstallation"] = [PFInstallation currentInstallation];
                   log[@"deviceSession"] = session;
                   [log saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                       if(!error){
                           
                           [self performSegueWithIdentifier:@"beginExclusive" sender:self];
                           
                           NSLog(@"syrup5");
                           
                           
                       }else{
                       
                           if ([[error.userInfo objectForKey:@"error"] isEqualToString:@"Exclusive mode updated." ]) {
                               [self performSegueWithIdentifier:@"beginExclusive" sender:self];
                               
                           }else{
                           
                           UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Sorry" message:[error.userInfo objectForKey:@"error"] delegate:nil cancelButtonTitle:@"Try again!!" otherButtonTitles:nil];
                           [alertview show];
                           }
                       
                       }
                   }];
               
               }else{
                   
                   
            
                   UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Sorry" message:[error.userInfo objectForKey:@"error"] delegate:nil cancelButtonTitle:@"Try again!!" otherButtonTitles:nil];
                   [alertview show];
            
               }}];
        
        
        
        }else{
            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Sorry" message:[error.userInfo objectForKey:@"error"] delegate:nil cancelButtonTitle:@"Try again!!" otherButtonTitles:nil];
            [alertview show];}}];
    
    }else{
    
    
        [self.currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
            if (!error) {
                
                log[@"status"] = @2;
                log[@"deviceInstallation"] = [PFInstallation currentInstallation];
                log[@"deviceSession"] = self.sessionToken;
                [log saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                    if(!error){
                        
                        [self performSegueWithIdentifier:@"beginExclusive" sender:self];
                        
                        NSLog(@"syrup7");
                        
                        
                    }else{
                        
                        if ([[error.userInfo objectForKey:@"error"] isEqualToString:@"Exclusive mode updated." ]) {
                            [self performSegueWithIdentifier:@"beginExclusive" sender:self];
                            
                        }else{
                        
                        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Sorry" message:[error.userInfo objectForKey:@"error"] delegate:nil cancelButtonTitle:@"Try again!!" otherButtonTitles:nil];
                        [alertview show];
                        
                        }
                    }
                }];
                
            }else{
                
                UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Sorry" message:[error.userInfo objectForKey:@"error"] delegate:nil cancelButtonTitle:@"Try again!!" otherButtonTitles:nil];
                [alertview show];
                
            }}];
    
    
    
    
    
    
    
    }
    
//    [self.currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
//        if (!error) {
//            
//            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Sorry" message:[error.userInfo objectForKey:@"error"] delegate:nil cancelButtonTitle:@"Try again!!" otherButtonTitles:nil];
//            [alertview show];
//            
//        }else{
//            
//
//        }
//    }];
    
    
    
//    [self.rolling saveInBackgroundWithBlock:^(BOOL sdfjdf, NSError * error){
//    
//        if (error) {
//            NSLog(@"there was an error in saving");
//        }
//    
//        else{
////            self.currentUser[@"headline"] = self.editLocation.text;
////            [self.currentUser saveInBackground];
//        }
//        
//        
//    }
//     ];
    
    
//    self.currentUser[@"headline"] = self.editLocation.text;
//    [self.currentUser saveInBackground];
    
    
}

// later use

/* - (IBAction)asjhfjksdf:(id)sender {
    
    
    PFUser * unwanted = [self.allFriends objectAtIndex:2];
    NSLog(@"unwanted %@",unwanted);
    
    PFQuery *query1 = [PFQuery queryWithClassName:@"friendrequest"];
    [query1 whereKey:@"user1" equalTo:self.currentUser];
    [query1 whereKey:@"user2" equalTo:unwanted];
    [query1 whereKey:@"status" equalTo:@3];
    PFQuery *query2 = [PFQuery queryWithClassName:@"friendrequest"];
    [query1 whereKey:@"user1" equalTo:unwanted];
    [query1 whereKey:@"user2" equalTo:self.currentUser];
    [query1 whereKey:@"status" equalTo:@3];
    PFQuery *query3 = [PFQuery queryWithClassName:@"friendrequest"];
    [query1 whereKey:@"user1" equalTo:self.currentUser];
    [query1 whereKey:@"user2" equalTo:unwanted];
    [query1 whereKey:@"status" equalTo:@1];
    PFQuery *query4 = [PFQuery queryWithClassName:@"friendrequest"];
    [query1 whereKey:@"user1" equalTo:unwanted];
    [query1 whereKey:@"user2" equalTo:self.currentUser];
    [query1 whereKey:@"status" equalTo:@1];
    PFQuery *finalQuery = [PFQuery orQueryWithSubqueries:@[query1,query2,query3,query4]];
    [finalQuery findObjectsInBackgroundWithBlock:^(NSArray *objects,NSError *error){
        if(!error){
            NSLog(@"gotem %lu",(unsigned long)objects.count);
        }
        
    }];
    
}*/
- (IBAction)backgroundTap:(id)sender {
    
    [self.view endEditing:YES];
    
}

-(void)goBack{
    [self performSegueWithIdentifier:@"goBack" sender:self];
}

- (IBAction)cancel:(id)sender {
    
    NSLog(@"plase work too!!!");
    
}
@end
