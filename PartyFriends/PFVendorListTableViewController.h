//
//  PFVendorListTableViewController.h
//  PartyFriends
//
//  Created by Gilberto Silva on 12/2/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "PFVendorListTableViewCell.h"   

@interface PFVendorListTableViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic)IBOutlet  UITableView * tableView;
@property (strong, nonatomic) PFUser * currentUser;
@property (strong, nonatomic) NSMutableArray * myVendorAccounts;

@end
