//
//  MapView.h
//  PartyFriends
//
//  Created by Joshua Silva  on 6/28/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Parse/Parse.h"
#import <MapKit/MapKit.h>
#import "PFProfileInfomation.h"
#import "ProfileContainerViewController.h"

#import "LFHeatMap.h"
#import "tabBarController.h"
@import GoogleMaps;


@interface MapView : UIViewController <UIGestureRecognizerDelegate, MKMapViewDelegate, CLLocationManagerDelegate, GMSMapViewDelegate>{

    
}
@property (weak, nonatomic) IBOutlet UIButton *newsFeedButton;
@property (weak, nonatomic) IBOutlet GMSMapView *googleMapView;
@property (weak, nonatomic) IBOutlet UIView *googleMap;
@property (strong, nonatomic) GMSMapView *mapView_;



- (IBAction)checking:(id)sender;

@property (strong, nonatomic) NSMutableArray * allArray;
@property (weak, nonatomic) IBOutlet UIButton *partyFeedButton;

//@property (strong, nonatomic) IBOutlet MKMapView *heatMap;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) CLLocation * currentUsersLocation;

@property (strong, nonatomic) PFGeoPoint * myGeo;
@property (strong, nonatomic) PFUser * currentUser;
@property (strong, nonatomic) PFRelation * currentPFs;

@property (strong, nonatomic) IBOutlet UIView *rightEdgeView;
@property (strong, nonatomic) IBOutlet UIButton *PFButton;
@property (strong, nonatomic) IBOutlet UILabel *mapLabel;

@property (strong, nonatomic) NSArray * onlineUsers;
@property (strong, nonatomic) NSMutableArray * parties;

@property (weak, nonatomic) IBOutlet UIView *profileView;

// josh new properties
// sliderState
@property (weak, nonatomic) IBOutlet UISlider *sliderState;
@property (strong, nonatomic) UIImageView * heatMapView;
@property (strong, nonatomic) UIImageView * otherView;

@property (strong, nonatomic) NSMutableArray * friendsLocations;
@property (strong, nonatomic) NSMutableArray * weights;

- (void) sendContainerToBack;
- (void) ZoomUser;
- (void) newData;
- (void)customPins:(NSMutableArray * )Users;
- (void) refreshFailed;

@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *backgroundTapGesture;

@property (strong, nonatomic) IBOutlet UIButton *heatMapButton;

@property(weak, nonatomic) NSMutableArray * dictionaryArray;

@property(strong, nonatomic) NSMutableArray * annotationArray;

@property(weak, nonatomic) NSDictionary *binder;

@end
