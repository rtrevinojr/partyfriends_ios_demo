//
//  MapView.m
//  PartyFriends
//
//  Created by Joshua Silva  on 6/28/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import "MapView.h"
#import "Annotation.h"
#import "NewsFeed.h"
#import "LocationManagerSingleton.h"
#import "UIView+Toast.h"
#import "NSDate+NVTimeAgo.h"
@import GoogleMaps;


@interface MapView () <GMSMapViewDelegate>
//#define me_HughesLat 31.743899;
//#define me_HughesLong -106.317554;
#define me_HughesLat 31.701607;
#define me_HughesLong -106.284257;

@property (strong, nonatomic) PFObject * pfObject;

@end

float spanX = 0.1;
float spanY = 0.1;
float spanx2= 0.005;
float spany2= 0.005;
double magnitude = 1.0;
CLLocationCoordinate2D cood;

Annotation * AnotherAnnotation;
Annotation * firstPin;


@implementation MapView
{
    ProfileContainerViewController * childView;
    tabBarController * tabbar;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.currentUser = [PFUser currentUser];
    
    
    self.mapView.delegate = self;
    // [self.mapView setUserInteractionEnabled:NO];
    
    //GOOGLE maps
    
    //    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:-33.86
    //                                                            longitude:151.20
    //                                                            zoom:6];
    //
    //
    //
    //    self.mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+30) camera:camera];
    //
    //   // self.mapView_ = [GMSMapView mapWithFrame:self.view.bounds camera:camera];
    //    self.mapView_.myLocationEnabled = YES;
    //   // self.googleMap = mapView_;
    //    [self.googleMap addSubview:self.mapView_];
    //    self.mapView_.delegate=self;
    //
    //    //self.view = mapView_;
    //   // mapView_.delegate = self;
    //    //self.googleMapView = mapView_;
    //   // [self.googleMapView addSubview:mapView_];
    //   // [self.view addSubview:mapView_];
    //
    //
    //
    //    // Creates a marker in the center of the map.
    //    GMSMarker *marker = [[GMSMarker alloc] init];
    //    marker.position = CLLocationCoordinate2DMake(-33.86, 151.20);
    //    marker.title = @"Sydney";
    //    marker.snippet = @"Australia";
    //
    //
    //    marker.icon = [UIImage imageNamed:@"GoldBall"];
    //    marker.map = self.mapView_;
    ////    UIView * googleLogo =  mapView_.subviews[2];
    ////    googleLogo.frame = CGRectMake(25, 35 , googleLogo.frame.size.width, googleLogo.frame.size.height);
    ////    googleLogo.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
    //    //[self.view bringSubviewToFront:self.newsFeedButton];
    //
    //    self.mapView_.alpha = .5;
    
    //[self.view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
    
    self.backgroundTapGesture.delegate = self;
    
    [self.partyFeedButton setTintColor:[UIColor purpleColor]];
    
    
    CLLocationCoordinate2D cuud = CLLocationCoordinate2DMake(30.337405, -97.757093);
    
    CLRegion * syrup = [[CLCircularRegion alloc] initWithCenter:cuud radius:200 identifier:@"main"];
    
    
    
    CLLocationCoordinate2D homeCord = CLLocationCoordinate2DMake(30.384899, -97.879592);
    
    CLRegion * mapleSyrup = [[CLCircularRegion alloc] initWithCenter:homeCord radius:200 identifier:@"home"];
    
    
    [[LocationManagerSingleton sharedSingleton].locationManager startMonitoringForRegion:mapleSyrup];
    
    CLLocationCoordinate2D closeHome = CLLocationCoordinate2DMake(30.389698, -97.882107);
    CLRegion * nearHome = [[CLCircularRegion alloc] initWithCenter:closeHome radius:300 identifier:@"nearHome"];
    [[LocationManagerSingleton sharedSingleton].locationManager startMonitoringForRegion:nearHome];
    
    CLLocationCoordinate2D reneCord = CLLocationCoordinate2DMake(30.341631, -97.782373);
    CLRegion * r = [[CLCircularRegion alloc] initWithCenter:reneCord radius:200 identifier:@"r"];
    
    [[LocationManagerSingleton sharedSingleton].locationManager startMonitoringForRegion:r];
    
    [self.view sendSubviewToBack:self.profileView];
    
    [self.profileView setHidden:YES];
    [self.profileView setUserInteractionEnabled:NO];
    
    UITapGestureRecognizer *tapgesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(exitProfileView:)];
    
    [self.mapView addGestureRecognizer:tapgesture];
    [self.mapView setUserInteractionEnabled:YES];
    
    [self.mapView setShowsUserLocation:YES];
    
    self.friendsLocations = [NSMutableArray new];
    self.weights = [NSMutableArray new];
    
    [self.heatMapButton setTintColor:[UIColor redColor]];
    
    self.mapLabel.text = @" ";
    self.mapLabel.numberOfLines = 0;
    //[self.mapLabel bringSubviewToFront:self.rightEdgeView];
    
    //[self.mapView sendSubviewToBack:self.view];
    //[self.rightEdgeView bringSubviewToFront:self.mapView];
    
    //[self.heatMap sendSubviewToBack:self.view];
    //[self.rightEdgeView bringSubviewToFront:self.heatMap];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    self.rightEdgeView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
    
    UIScreenEdgePanGestureRecognizer *pan = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self
                                                                                              action:@selector(panToHeatMap:)];
    [pan setEdges:UIRectEdgeRight];
    [pan setDelegate:self];
    [self.view addGestureRecognizer:pan];
    
    [self.PFButton bringSubviewToFront:self.view];
    
    
    CLLocationCoordinate2D pancakes;
    pancakes.latitude = me_HughesLat;
    pancakes.longitude = me_HughesLong;
    
    NSTimer * updateCurrentLocation = [NSTimer scheduledTimerWithTimeInterval:20 target:self selector:@selector(updatingCurrentLocation) userInfo:nil repeats:YES];
    
    
    
    
    cood = [LocationManagerSingleton sharedSingleton].locationManager.location.coordinate;
    
    
    
    if (cood.longitude == 0 && cood.latitude == 0){
        
        MKCoordinateRegion region;
        region.center.latitude = me_HughesLat;
        region.center.longitude = me_HughesLong;
        region.span = MKCoordinateSpanMake(spanX, spanY);
        [self.mapView setRegion:region animated:YES];
        NSTimer * segueTo =[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(moveLegalLabel) userInfo:nil repeats:NO];
        NSTimer * checkAgain =[NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(moveLegalLabel) userInfo:nil repeats:NO];
    }else{
        
        MKCoordinateRegion region;
        region.center.latitude = cood.latitude;
        region.center.longitude = cood.longitude;
        region.span = MKCoordinateSpanMake(spanX, spanY);
        [self.mapView setRegion:region animated:YES];
        NSTimer * segueTo =[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(moveLegalLabel) userInfo:nil repeats:NO];
        
        NSTimer * checkAgain =[NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(moveLegalLabel) userInfo:nil repeats:NO];
        
    }
    
    //self.sliderState.thumbTintColor = [UIColor redColor];
    self.sliderState.tintColor = [UIColor redColor];
    
    // Creating the view for the heatMap Image
    self.heatMapView = [[UIImageView alloc] initWithFrame:self.view.frame];
    self.heatMapView.contentMode = UIViewContentModeCenter;
    [self.mapView addSubview:self.heatMapView];
    self.sliderState.hidden = YES;
    
    //    self.otherView = [[UIImageView alloc] initWithFrame:self.mapView_.frame];
    //    [self.view addSubview:self.otherView];
    // [self.mapView_ addSubview:self.otherView];
    
    NSLog(@"frame of self.mapView_ and self.otherView %f %f", self.mapView_.frame.size.height, self.otherView.frame.size.height);
    
    
    tabbar = (tabBarController *) self.tabBarController;
    
    
    PFGeoPoint * userLocation = [PFGeoPoint geoPointWithLatitude:cood.latitude longitude:cood.longitude];
    
    PFQuery * queryParty= [PFQuery queryWithClassName:@"pflocation"];
    [queryParty whereKey:@"location" nearGeoPoint:userLocation withinMiles:100];
    [queryParty setLimit:250];
    [queryParty orderByAscending:@"createdAt"];
    // [queryParty includeKey:@"location"];
    [queryParty findObjectsInBackgroundWithBlock:^(NSArray * geoPoints, NSError * error){
        
        if (!error) {
            //self.friendsLocations = [NSMutableArray arrayWithArray:geoPoints];
            
            //  NSLog(@"going to check the amount of parties pulled \n from the mutableArray %lu \n to see whats it returning %@ ", (unsigned long)geoPoints.count, geoPoints[5]);
            for (int i = 0; i< geoPoints.count; i++) {
                PFObject * partyObject = geoPoints[i];
                PFGeoPoint * points = partyObject[@"location"];
                
                //  NSLog(@"this the geopoint returning \n anything?!?! \n %@", points);
                
                CLLocation *location = [[CLLocation alloc] initWithLatitude:points.latitude longitude:points.longitude];
                [self.weights addObject:[NSNumber numberWithInteger:(magnitude * 10)]];
                
                [self.friendsLocations addObject:location];
                
            }
            
        }else {
            
            
            NSLog(@"error for fireMAP");
        }
        
        
    }];
    
    
    
}


-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:NO];
    //[self newData];
    
    [self.mapView removeAnnotation:AnotherAnnotation];
    
    if (tabbar.friendsLocation != nil) {
        
        MKCoordinateRegion friendRegion;
        friendRegion.center.latitude = tabbar.friendsLocation.latitude;
        friendRegion.center.longitude = tabbar.friendsLocation.longitude;
        friendRegion.span = MKCoordinateSpanMake(spanx2, spany2);
        //[self.allArray containsObject:tabbar.pinUser];
        
        NSInteger i;
        i = [self.allArray indexOfObject:tabbar.pinUser];
        
        // [[self.mapView annotations] objectAtIndex:3]]
        
        [self.mapView setRegion:friendRegion animated:YES];
        //        [self.mapView selectAnnotation:[[self.mapView annotations] objectAtIndex:i] animated:YES];
        
        Annotation *annotation;
        PFUser * user = tabbar.pinUser;
        PFObject * partyObject = user[@"lastParty"];
        [partyObject fetchIfNeeded];
        PFGeoPoint * geoPoint = partyObject[@"location"];
        CLLocationCoordinate2D waffles;
        waffles.latitude = geoPoint.latitude;
        waffles.longitude = geoPoint.longitude;
        BOOL isAnnotations = false;
        
        
        for(int i = 0; self.annotationArray.count > i; i++){
            
            annotation = [self.annotationArray objectAtIndex:i];
            
            if (annotation.coordinate.longitude == waffles.longitude && annotation.coordinate.latitude == waffles.latitude) {
                isAnnotations = true;
                break;
            }
        }
        
        if (isAnnotations) {
            
            [self.mapView selectAnnotation:annotation animated:true];
            
            
        }
        NSLog(@"MAPANN %@ \n %ld \n %@ \n %@", [self.mapView annotations], (long)i, tabbar.pinUser, [[self.mapView annotations] objectAtIndex:i]);
        
    }
    
    if (cood.longitude == 0 || cood.latitude == 0) {
        
        CLLocationCoordinate2D pancakes;
        pancakes.latitude = me_HughesLat;
        pancakes.longitude = me_HughesLong;
        
        Annotation * anotherAnnotstion = [Annotation alloc];
        anotherAnnotstion.coordinate =pancakes;
        anotherAnnotstion.title = @"PartyFriends HQ";
        anotherAnnotstion.subtitle = @"";
        
        [self.mapView addAnnotation:anotherAnnotstion];
        
    }else{
        NSLog(@" %f", cood.longitude);
        
        firstPin = [Annotation alloc];
        firstPin.coordinate = cood;
        firstPin.title = @"  Me  ";
        firstPin.subtitle = self.currentUser[@"headline"];
        firstPin.theUserObject = self.currentUser;
        
        //  [self.mapView addAnnotation:firstPin];
        
    }
    
    
    
    // NSLog(@"this is to see cood  %f", [LocationManagerSingleton sharedSingleton].locationManager.location.coordinate.longitude);
    
    
    self.myGeo = [self.currentUser objectForKey:@"location"];
    
    self.currentUsersLocation = [[CLLocation alloc]initWithLatitude:self.myGeo.latitude longitude:self.myGeo.longitude];
    
    
    CLLocationCoordinate2D waffles = self.currentUsersLocation.coordinate;
    
    Annotation * myAnnotation = [Annotation alloc];
    myAnnotation.coordinate = waffles;
    myAnnotation.title = @"Me";
    myAnnotation.subtitle = @"";
    
    
    self.parties = [NSMutableArray new];
    
    self.currentPFs = [[PFUser currentUser]
                       objectForKey:@"friends"];
    
    PFQuery * query = [self.currentPFs query];
    [query includeKey:@"lastParty"];
    [query orderByAscending:@"firstName"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        //NSLog(@"****************** Objects = %@", objects);
        
        
        if (error){
            NSLog(@"error %@ %@", error, [error userInfo]);
        }
        else{
            self.onlineUsers = objects;
            
            //NSLog(@"*** Objects.count = %lu", [objects count]);
            
            int l =0;
            for(int i = 0; objects.count>i;i++){
                PFUser *user = objects[i];
                NSLog(@"user = %@", user);
                PFObject *party = user[@"lastParty"];
                NSLog(@"last party = %@", party);
                if(party!=NULL){
                    //self.parties[l]=party;
                    [self.parties addObject:party];
                    l++;
                }
            }
            
            //    NSLog(@"  looking for this   %@  ", self.parties);
            //[self pinsForAll];
            
            //[self customPins];
        }
        
    }];
    // [self moveLegalLabel];
    
    //[self customPins];
    NSTimer * segueTo =[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(moveLegalLabel) userInfo:nil repeats:NO];
    
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    tabbar.friendsLocation = nil;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    
}

- (IBAction)newsFeedTouch:(id)sender {
    [self.tabBarController setSelectedIndex:1];
    
}


-(void)panToHeatMap:(UIGestureRecognizer*)sender{
    
    if (sender.state == UIGestureRecognizerStateEnded) {
        
        [self.tabBarController setSelectedIndex:1];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    /*
     if ([[segue identifier] isEqualToString:@"EmbedSegue"]) {
     ProfileContainerViewController *embed = (ProfileContainerViewController *) segue.destinationViewController;
     embed.labelString = @"sjhasjghakljhflkajshfkjahsdlkfjhalkjfhalkjfh!@@@@!@!@!@!@!@!@!@!@!@!@!@!@!@!2";
     } */
    
    childView = segue.destinationViewController;
    
    //    if ([[segue identifier] isEqualToString:@"profile"]) {
    //        ProfileContainerViewController * profileVC = (ProfileContainerViewController *) segue.destinationViewController;
    //        profileVC.labelString = @"Rene";
    //    }
    
}

-(void)newData{
    //self.allArray = tabbar.refreshArray;
    
    //  NSLog(@"data \n SEEING \n if the ARRAY \n is GOING \n through \n %lu",(unsigned long)self.allArray.count);
    
    
}

- (IBAction)loopTime:(id)sender {
    
    NSMutableArray * locations =[NSMutableArray new];
    
    Annotation * peersAnns;
    
    NSLog(@"how many peers in the array %lu", (unsigned long)self.onlineUsers.count);
    
    for (int i = 0; i < self.onlineUsers.count; i++) {
        
        PFUser * name = [self.onlineUsers objectAtIndex:i];
        
        PFGeoPoint * nameLocation = name[@"location"];
        
        NSLog(@"Geo point = %@", nameLocation);
        
        if (nameLocation == nil) {
            NSLog(@" has no point!  %@  ", name[@"firstName"]);
        }
        else {
            NSLog(@"done with %@", [self.onlineUsers objectAtIndex:i]);
            
            CLLocation * location = [[CLLocation alloc]initWithLatitude:nameLocation.latitude longitude:nameLocation.longitude];
            CLLocationCoordinate2D loCation = location.coordinate;
            peersAnns = [Annotation new];
            peersAnns.coordinate = loCation;
            peersAnns.title = name[@"firstName"];
            
            peersAnns.subtitle = [NSString stringWithFormat:@"number %@",[self.onlineUsers objectAtIndex:i]];
            [locations addObject:peersAnns];
            
        }
        
    }
    
    [self.mapView addAnnotations:locations];
    
}

-(void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker {
    
    PFUser * user;
    PFObject * party;
    CLLocationCoordinate2D markerLocation = marker.position;
    BOOL foundUser = false;
    
    for(int i = 0; self.allArray.count > i; i++){
        
        
        user = self.allArray[i];
        party = [user objectForKey:@"lastParty"];
        [party fetch];
        
        PFGeoPoint * location = party[@"location"];
        
        if(markerLocation.latitude == location.latitude && markerLocation.longitude == location.longitude){
            
            foundUser = true;
            break;
        }
        
    }
    
    if(foundUser){
        childView.userParty = party;
        childView.user = user;
        [childView reload];
        
    }
    else{
        
        NSLog(@"user was not found annotation:");
        
    }
    
    
    [self.profileView setHidden:NO];
    [self.profileView setUserInteractionEnabled:YES];
    
    self.mapView.alpha = .6;
    
    //[childView  reload];
    //    if (([(Annotation*)view.annotation.title  isEqual:self.currentUser[@"firstName"]])) {
    //
    //        [childView reloadForCurrentUser];}
    //    else {
    //        [childView reload];
    //    }
    // childView.NameLabel.text = view.annotation.title;
    
    
    //childView.headline.text = ((Annotation *)view.annotation).annObjectId;
    //childView.headline.text = view.annotation.subtitle;
    //childView.headline.text = view.annotation.subtitle;
    
    //childView.NameLabel.text = ((Annotation *)view.annotation).customtitle;
    //childView.headline.text = ((Annotation* )view.annotation).annObjectId;
    //childView.headline.text = view.annotation.objId;
    // childView.NameLabel.text = view.customtitle;
    //childView.headline.text = view.customsubtitle;
    //NSLog(@"this \n looking \n for what \n the user \n object is returning %@", view.theUserObject);
    
    
    //childView.NameLabel.text = view.customtitle;
    
    //childView.headline.text = view.customsubtitle;
    
    //childView.profilePartyPic = view.annImage;
    
    //childView.objectId = view.annObjectId;
    
    
    //childView.profilePartyPic = view.annImage;
    
    //childView.objectId = view.annotation.title;
    
    
    NSLog(@"callout Accessorty Control Tapped:");
    //placeName.friendsName = @"pancakes!!!";
    
    //_BarAnnotation = view.annotation.title;
    //[self performSegueWithIdentifier:@"profile" sender:view];
    
    //[self.view setHidden:YES];
    
    //self.view.userInteractionEnabled = NO;
    
    UIView * myview = [[UIView alloc] initWithFrame:CGRectMake(20, 50, 200, 200)];
    
    //    myview.layer.backgroundColor = [UIColor whiteColor].CGColor;
    //    myview.layer.cornerRadius = 20.0;
    //    myview.layer.frame = CGRectInset(self.profileView.layer.frame, 20, 20);
    //
    //    myview.layer.shadowOffset = CGSizeMake(1, 0);
    //    myview.layer.shadowColor = [[UIColor blackColor] CGColor];
    //    myview.layer.shadowRadius = 5;
    //    myview.layer.shadowOpacity = .25;
    
    self.profileView.layer.backgroundColor = [UIColor whiteColor].CGColor;
    self.profileView.layer.cornerRadius = 30.0f;
    //self.profileView.layer.frame = CGRectInset(self.profileView.layer.frame, 20, 20);
    
    self.profileView.layer.shadowOffset = CGSizeMake(1, 0);
    self.profileView.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.profileView.layer.shadowRadius = 5;
    self.profileView.layer.shadowOpacity = .25;
    
    [self.view addSubview:self.profileView];
    
    [self.mapView setScrollEnabled:NO];
    self.mapView_.settings.scrollGestures = NO;
    
    
    
}

//USING custom annotations
-(void)customPins:(NSMutableArray * )Users{
    
    
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView_ clear];
    //    if (self.allArray ==nil) {
    //       self.allArray = [NSMutableArray arrayWithArray:tabbar.pinsArray];
    //    }
    
    
    self.allArray = [NSMutableArray new];
    self.annotationArray = [NSMutableArray new];
    //    self.dictionaryArray = [NSMutableArray new];
    //    self.binder = [NSDictionary new];
    
    
    self.allArray = Users;
    PFUser * currentUserUpdate = self.allArray[0];
    PFObject * currentParty = currentUserUpdate[@"lastParty"];
    
    
    NSLog(@"loggin if anything is coming from the tabbar!!!!!!! \n \n \n \n \n \n \n j%lu \n \n \n \n \n ",(unsigned long)self.allArray.count);
    
    for (int i = 0; i < self.allArray.count; i++) {
        
        
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        
        
        PFUser * user = [self.allArray objectAtIndex:i];
        PFObject * partyObject = user[@"lastParty"];
        [partyObject fetchIfNeeded];
        //NSLog(@"*********** \n ********** \n ********* \n **************** \n ******* Other Parties count = %@", user[@"firstName"]);
        
        //  NSLog(@" \n \n \n \n asd%lu \n \n \n \n \ \n \ \n \n \n \n \n \n \n \n432 %@ \n \n \n ",(unsigned long)self.allArray.count, user.username);
        
        PFGeoPoint * nameLocation = partyObject[@"location"];
        
        if (nameLocation == nil || nameLocation.latitude ==0 || nameLocation.longitude ==0 ) {
            NSLog(@"nameLocation is nil");
        }else{
            
            CLLocation * location = [[CLLocation alloc] initWithLatitude:nameLocation.latitude longitude:nameLocation.longitude];
            CLLocationCoordinate2D userLocation = location.coordinate;
            NSDate * partyTimeStamp = partyObject[@"updatedLocationAt"];
            if(partyTimeStamp == nil){
                partyTimeStamp = partyObject.updatedAt;
            }
            NSLog(@"timeStamp %@", partyTimeStamp);
            NSDate * currentTimeStamp = [NSDate date];
            NSTimeInterval differenceInDate = [currentTimeStamp timeIntervalSinceDate:partyTimeStamp] * 1000;
            
            //                NSInteger hours = floor(differenceInDate/3600);
            NSInteger hours = floor(differenceInDate/3600);
            
            NSInteger minsTime = hours % 60;
            NSInteger lessHours = ceil(differenceInDate/3600);
            NSInteger days = hours / 24 ;
            NSString * lastUpdateString = [NSString alloc];
            NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"MMM d"];
            NSString * monthDate = [formatter stringFromDate:partyTimeStamp];
            NSDateFormatter * formatterTime = [[NSDateFormatter alloc] init];
            [formatterTime setDateFormat:@"h:mm a"];
            NSString * timeDate = [formatter stringFromDate:partyTimeStamp];
            
            NSString * locationWithUpdateString = [NSString stringWithFormat:@"Location updated: %@", [partyTimeStamp formattedAsTimeAgo]];
            
            lastUpdateString = locationWithUpdateString;
            
            //    BOOL cereal = [partyTimeStamp alertTime];
            
            //lastUpdateString = [self locationDistanceString:location];
            
            //                NSLog(@"User: %@ time for each pin %ld and hour %ld than lesshours %ld , pin number %d",user.objectId, (long)minsTime,(long)hours,(long)lessHours, i);
            //
            //                if (hours >= 24) {
            //
            //                    if (hours < 48) {
            //
            //                    lastUpdateString = [NSString stringWithFormat:@"Last Updated: Yesterday %@ ", timeDate];
            //
            //                    }else {
            //                        lastUpdateString = [NSString stringWithFormat:@"Last Updated: %@ ", monthDate];
            //
            //                    }
            //
            //                }else if( hours < 24 && hours > 1){
            //                        lastUpdateString =  [NSString stringWithFormat:@"Last Updated: %lu hours ago. ", hours];
            //
            //                }else if(hours == 1){
            //
            //                    lastUpdateString =  [NSString stringWithFormat:@"Last Updated: %lu hour ago. ", hours];
            //
            ////                    if (hou == 0) {
            ////                        lastUpdateString = [NSString stringWithFormat:@"Updated %ld mins ago", (long)minsTime];
            ////                    }else{
            ////                        if (hours == 1) {
            ////                            lastUpdateString = [NSString stringWithFormat:@"Updated an hour ago"];
            ////                        }else{
            ////                    lastUpdateString = [NSString stringWithFormat:@"Updated %ld hours ago", (long)hours];
            ////                        }
            ////
            ////
            ////
            ////                    }
            //
            //
            //                } else{
            //                    if(minsTime == 0){
            //                            lastUpdateString =  [NSString stringWithFormat:@"Last Update: Just Now "];
            //                    }
            //                    else if(minsTime == 1){
            //                            lastUpdateString =  [NSString stringWithFormat:@"Last Update: %lu minute ago. ", minsTime];
            //                    }
            //                    else{
            //                                lastUpdateString =  [NSString stringWithFormat:@"Last Update: %lu minutes ago. ", minsTime];
            //                    }
            //                }
            //Google Marker
            if ([self.currentUser.objectId isEqualToString:user.objectId]) {
                marker.icon = [UIImage imageNamed:@"GoldBall"];
            }else if ([user[@"status"] isEqual:@1]){
                marker.icon = [UIImage imageNamed:@"RedBallon"];
                
            }else if ([user[@"status"] isEqual:@2]){
                
                [UIImage imageNamed:@"BlueBallon"];
            }
            
            
            marker.position = CLLocationCoordinate2DMake(userLocation.latitude, userLocation.longitude);
            marker.title = user[@"firstName"];
            marker.snippet = lastUpdateString;
            
            marker.map = self.mapView_;
            
            
            
            
            
            Annotation * point = [[Annotation alloc]initWithTitle:user[@"firstName"] Subtitle:lastUpdateString Location:userLocation];
            point.partyObject = partyObject;
            point.theUserObject = user;
            [self.annotationArray addObject:point];
            //                [self.binder setValue:point forKey:[user objectId]];
            //                [self.dictionaryArray addObject:binder];
            
            NSLog(@"differenceInDate = %f \n numberPin %d \n %@", differenceInDate, i, point.title);
            
            
            /* Annotation * point = [Annotation new];
             point.customtitle = user[@"firstName"];
             point.customsubtitle = user[@"headline"];
             point.coordinate = userLocation;*/
            //point.theUserObject = user;
            
            
            NSLog(@" \n \n title \n \n %@", point.title);
            
            [self.mapView addAnnotation:point];
            
            // NSLog(@"*********** \n ********** \n ********* \n **************** \n ******* Other Parties count = %@", point.annObjectId);
            
        }
    }
    
    
    //    for (int i = 0; i < self.parties.count; i++) {
    //
    //
    //
    //        PFObject * name = [self.parties objectAtIndex:i];
    //        PFUser * user = name[@"createdBy"];
    //       // NSLog(@"*********** \n ********** \n ********* \n **************** \n ******* Other Parties count = %@", user[@"firstName"]);
    //
    //        PFGeoPoint * nameLocation = name[@"location"];
    //
    //        if (nameLocation == nil || nameLocation.latitude ==0 || nameLocation.longitude ==0 ) {
    //            NSLog(@"nameLocation is nil");
    //        }else{
    //
    //            CLLocation * location = [[CLLocation alloc] initWithLatitude:nameLocation.latitude longitude:nameLocation.longitude];
    //            CLLocationCoordinate2D userLocation = location.coordinate;
    //
    //            Annotation * point = [[Annotation alloc]initWithTitle:user[@"firstName"] Subtitle:user[@"headline"] Location:userLocation];
    //            point.partyObject = name;
    //            point.theUserObject = user;
    //            /* Annotation * point = [Annotation new];
    //             point.customtitle = user[@"firstName"];
    //             point.customsubtitle = user[@"headline"];
    //             point.coordinate = userLocation;*/
    //            //point.theUserObject = user;
    //
    //
    //            NSLog(@" \n \n title \n \n %@", point.title);
    //
    //            [self.mapView addAnnotation:point];
    //
    //           // NSLog(@"*********** \n ********** \n ********* \n **************** \n ******* Other Parties count = %@", point.annObjectId);
    //
    //        }
    //    }
    
}

// USING THE DEFUALT ANNOTATIONS
-(void)pinsForAll {
    
    NSLog(@"here here");
    NSMutableArray * locations =[NSMutableArray new];
    //Annotation * peersAnns;
    //MKPointAnnotation * point;
    
    NSLog(@"****************************** Other Parties count = %lu", (unsigned long) self.parties.count);
    
    
    for (int i = 0; i < self.parties.count; i++) {
        
        //peersAnns = [[Annotation alloc] init];
        
        PFObject * name = [self.parties objectAtIndex:i];
        PFUser * user = name[@"createdBy"];
        NSLog(@"%d ", i);
        
        PFGeoPoint * nameLocation = name[@"location"];
        
        NSLog(@"User = %@", user);
        NSLog(@"Geo point = %@", nameLocation);
        
        if (nameLocation == nil) {
            NSLog(@"nameLocation is nil");
        }
        else {
            NSLog(@"nameLocation is NOT nil");
            
            CLLocation * location = [[CLLocation alloc]initWithLatitude:nameLocation.latitude longitude:nameLocation.longitude];
            
            CLLocationCoordinate2D loCation = location.coordinate;
            
            
            NSLog(@"PARTY LOCATION COORDINATES = %f", loCation.latitude);
            
            //peersAnns = [Annotation new];
            
            MKPointAnnotation * point = [[MKPointAnnotation alloc] init];
            
            Annotation *peersAnns = [[Annotation alloc] init];
            
            peersAnns.coordinate = loCation;
            
            NSString * mainTitle = user[@"firstName"];
            
            
            peersAnns.title = mainTitle;
            
            peersAnns.annObjectId = [user objectId];
            
            //peersAnns.annImage = user[@"profilePic"];
            
            //PFFile * pic = [user objectForKey:@"profilePic"];
            
            /*
             [pic getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
             if (!error) {
             //UIImage *image = [UIImage imageWithData:data];
             
             UIImage * image = [UIImage imageNamed:@"PFIcon"];
             
             [peersAnns.annImage setImage:image];
             }
             else {
             
             NSLog(@"********** ERROR getting pic");
             
             UIImage * defaultimage = [UIImage imageNamed:@"PFIcon"];
             
             [peersAnns.annImage setImage:defaultimage];
             }
             }];
             */
            
            //peersAnns.annImage setImage:;
            
            
            NSLog(@"self.onlineUsers object = %@", [self.onlineUsers objectAtIndex:i]);
            
            PFObject * obj = [self.onlineUsers objectAtIndex:i];
            
            NSString * objstr = [obj objectForKey:@"headline"];
            
            NSLog(@"PFObject string = %@", objstr);
            
            
            //peersAnns.customsubtitle = [NSString stringWithFormat:@"number %@",[self.onlineUsers objectAtIndex:i]];
            
            peersAnns.subtitle = objstr;
            
            point.title = mainTitle;
            
            point.subtitle = objstr;
            
            point.coordinate = loCation;
            
            [self.mapView addAnnotation:point];
            
            //[self.mapView addAnnotation:peersAnns];
            
            //[locations addObject:peersAnns];
            
        }
    }
    
    NSLog(@"***************");
    
    
    //[self.mapView addAnnotations:locations];
    
}


- (IBAction)slider:(UISlider *)slider {
    
    
    float boost = slider.value;
    UIImage * heatmap = [LFHeatMap heatMapForMapView:self.mapView boost:boost locations:self.friendsLocations weights:self.weights];
    
    //  UIImage * heatmap = [LFHeatMap heatMapWithRect:CGRectMake(0, 0, self.mapView_.frame.size.width, self.mapView_.frame.size.height) boost:boost points:self.friendsLocations weights:self.weights];
    self.heatMapView.image = heatmap;
    //  self.otherView.image = heatmap;
    
    
}

- (void) sendContainerToBack {
    
    [self.view sendSubviewToBack:self.profileView];
    
}

/*-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 if ([segue.identifier isEqualToString:@"toNewsFeed"]) {
 NewsFeed * viewController = (NewsFeed *)segue.destinationViewController;
 
 viewController.onlineUsers = self.onlineUsers;//[[CLLocation alloc] initWithLatitude:self.yourCord.coordinate.latitude longitude:self.yourCord.coordinate.latitude];
 }}
 
 */

-(IBAction)unwindToMap:(UIStoryboardSegue *)unwindSegue{
    
    
}



-(void)updatingCurrentLocation{
    
    [self.mapView removeAnnotation:firstPin];
    [self.mapView removeAnnotation:AnotherAnnotation];
    
    cood = [LocationManagerSingleton sharedSingleton].locationManager.location.coordinate;
    
    if (cood.longitude == 0 && cood.latitude == 0) {
        
        CLLocationCoordinate2D pancakes;
        pancakes.latitude = me_HughesLat;
        pancakes.longitude = me_HughesLong;
        
        
        Annotation * anotherAnnotstion = [Annotation alloc];
        anotherAnnotstion.coordinate =pancakes;
        anotherAnnotstion.title = @"PartyFriends HQ";
        anotherAnnotstion.subtitle = @"";
        
        [self.mapView addAnnotation:anotherAnnotstion];
        
    }
    else {
        AnotherAnnotation = [Annotation alloc];
        AnotherAnnotation.coordinate = cood;
        AnotherAnnotation.title = @"  Me  ";
        AnotherAnnotation.subtitle = @"";
        AnotherAnnotation.theUserObject = self.currentUser;
        //[self.mapView addAnnotation:AnotherAnnotation];
        
    }
    
}


- (IBAction)zoom:(id)sender {
    
    
    /* [PFCloud callFunctionInBackground:@"Add"
     withParameters:@{@"parameterKey": @"parameterValue"}
     block:^(NSArray *results, NSError *error) {
     if (!error) {
     // this is where you handle the results and change the UI.
     NSLog(@"Add function Cloud");
     }
     else {
     NSLog(@"Add function error from Cloud");
     }
     }];
     
     [PFCloud callFunctionInBackground:@"Delete"
     withParameters:@{@"parameterKey": @"parameterValue"}
     block:^(NSArray *results, NSError *error) {
     if (!error) {
     // this is where you handle the results and change the UI.
     NSLog(@"Delete function from cloud");
     
     }
     else {
     NSLog(@"Delete function error");
     }
     }];
     */
    
    /* if (cood.longitude == 0 && cood.latitude == 0){
     
     MKCoordinateRegion region;
     region.center.latitude = me_HughesLat;
     region.center.longitude = me_HughesLong;
     region.span = MKCoordinateSpanMake(spanX, spanY);
     [self.mapView setRegion:region animated:YES];
     }else{
     
     MKCoordinateRegion region;
     region.center.latitude = cood.latitude;
     region.center.longitude = cood.longitude;
     region.span = MKCoordinateSpanMake(spanx2, spany2);
     [self.mapView setRegion:region animated:YES];
     
     
     }*/
    
    [self viewWillAppear:YES];
    
    NSLog(@"***********************************");
    
    /*   NSString * url = @"uber://?client_id=L58ExalNrphCN8FtEvGEOP2MRVM3QvXc&action=setPickup&pickup[latitude]=37.775818&pickup[longitude]=-122.418028&pickup[nickname]=UberHQ&pickup[formatted_address]=1455%20Market%20St%2C%20San%20Francisco%2C%20CA%2094103&dropoff[latitude]=37.802374&dropoff[longitude]=-122.405818&dropoff[nickname]=Coit%20Tower&dropoff[formatted_address]=1%20Telegraph%20Hill%20Blvd%2C%20San%20Francisco%2C%20CA%2094133&product_id=a1111c8c-c720-46c3-8534-2fcdd730040d";
     
     
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
     
     
     NSLog(@"*********************************");
     
     */
    
}



/*  if ([segue.identifier isEqualToString:@"toSettingsView"]) {
 SettingsView * Settings = (SettingsView *)segue.destinationViewController;
 
 }
 
 }
 
 */


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


/*- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
 {
 NSLog(@"view For annotation");
 
 if (![annotation isKindOfClass:[Annotation class]]) {
 MKAnnotationView *annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"loc"];
 annotationView.canShowCallout = YES;
 //annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
 
 annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeInfoDark];
 
 
 
 return annotationView;
 }
 return nil;
 }
 */

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    
    MKAnnotationView *returnedAnnotationView = nil;
    
    if (![annotation isKindOfClass:[MKUserLocation class]])
    {
        
        if ([annotation isKindOfClass:[Annotation class]]) {
            Annotation * annotationObject = annotation;
            returnedAnnotationView = [Annotation createViewAnnotationForMapView:self.mapView annotation:annotation];
            
            PFUser * userObject= annotationObject.theUserObject;
            
            if ([userObject.objectId isEqualToString:self.currentUser.objectId]) {
                // returnedAnnotationView.image = [UIImage imageNamed:@"Square"];
                returnedAnnotationView.image = [UIImage imageNamed:@"GoldBall"];
            }else if ([userObject[@"status"]  isEqual: @2]) {
                
                
                returnedAnnotationView.image = [UIImage imageNamed:@"BlueBallon"];
                
                
            }else if ([userObject[@"status"] isEqual: @1]){
                
                //returnedAnnotationView.image = [UIImage imageNamed:@"ThreeBallon"];
                returnedAnnotationView.image = [UIImage imageNamed:@"RedBallon"];
            }/*else {
              
              returnedAnnotationView.image = [UIImage imageNamed:@"YellowBallon"];
              //            UIImage * redBall = [UIImage imageNamed:@"RedBallon"];
              //            returnedAnnotationView.image = [UIImage imageNamed:@"Green Pin.png"];
              
              //returnedAnnotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
              
              // returnedAnnotationView.centerOffset = CGPointMake(0,1);
              
              }
              */
        }
        
    }
    
    return returnedAnnotationView;
}


///*- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
//{
//    NSLog(@"view For annotation");
//    // static NSString * reuse = @"loc";
//
//    if ([annotation isKindOfClass:[MKUserLocation class]])
//        return nil;
//
//    if ([annotation isKindOfClass:[Annotation class]]) {
//
//        Annotation * location = (Annotation *)annotation;
//
//
//        MKAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"MyCustomAnnotation"];
//
//    annotationView.canShowCallout = YES;
//      annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
//
//        if (annotationView == nil) {
//            annotationView = location.annotationView;
//        }else{
//
//            annotationView.annotation = annotation;
//        }
//
//        /*
//
//
//         if (annotationView == nil) {
//         annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"MyCustomAnnotation"];
//         }else {
//         annotationView.annotation = annotation;
//
//         }*/
//        //Annotation * annot = [[Annotation alloc] init
//
//        // annotationView.canShowCallout = YES;
//        //annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
//
//        //annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeInfoDark];
//
//
//
//        return annotationView;
//    }
//    else {
//        return nil;
//    }
//}
//*/



/*
 - (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(MKAnnotationView *)annotation
 {
 NSLog(@"view For annotation");
 
 if (![annotation isKindOfClass:[MKUserLocation class]]) {
 MKAnnotationView *annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"loc"];
 annotationView.canShowCallout = YES;
 annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
 
 annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeInfoDark];
 
 
 
 return annotationView;
 }
 return nil;
 }*/








- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    
    [self.profileView setHidden:NO];
    [self.profileView setUserInteractionEnabled:YES];
    
    self.mapView.alpha = .6;
    
    //[childView  reload];
    if (([(Annotation*)view.annotation.title  isEqual:self.currentUser[@"firstName"]])) {
        
        [childView reloadForCurrentUser];}
    else {
        [childView reload];
    }
    // childView.NameLabel.text = view.annotation.title;
    
    
    //childView.headline.text = ((Annotation *)view.annotation).annObjectId;
    //childView.headline.text = view.annotation.subtitle;
    //childView.headline.text = view.annotation.subtitle;
    
    //childView.NameLabel.text = ((Annotation *)view.annotation).customtitle;
    //childView.headline.text = ((Annotation* )view.annotation).annObjectId;
    //childView.headline.text = view.annotation.objId;
    // childView.NameLabel.text = view.customtitle;
    //childView.headline.text = view.customsubtitle;
    //NSLog(@"this \n looking \n for what \n the user \n object is returning %@", view.theUserObject);
    
    
    //childView.NameLabel.text = view.customtitle;
    
    //childView.headline.text = view.customsubtitle;
    
    //childView.profilePartyPic = view.annImage;
    
    //childView.objectId = view.annObjectId;
    
    
    //childView.profilePartyPic = view.annImage;
    
    //childView.objectId = view.annotation.title;
    
    
    NSLog(@"callout Accessorty Control Tapped:");
    //placeName.friendsName = @"pancakes!!!";
    
    //_BarAnnotation = view.annotation.title;
    //[self performSegueWithIdentifier:@"profile" sender:view];
    
    //[self.view setHidden:YES];
    
    //self.view.userInteractionEnabled = NO;
    
    UIView * myview = [[UIView alloc] initWithFrame:CGRectMake(20, 50, 200, 200)];
    
    //    myview.layer.backgroundColor = [UIColor whiteColor].CGColor;
    //    myview.layer.cornerRadius = 20.0;
    //    myview.layer.frame = CGRectInset(self.profileView.layer.frame, 20, 20);
    //
    //    myview.layer.shadowOffset = CGSizeMake(1, 0);
    //    myview.layer.shadowColor = [[UIColor blackColor] CGColor];
    //    myview.layer.shadowRadius = 5;
    //    myview.layer.shadowOpacity = .25;
    
    self.profileView.layer.backgroundColor = [UIColor whiteColor].CGColor;
    self.profileView.layer.cornerRadius = 30.0f;
    //self.profileView.layer.frame = CGRectInset(self.profileView.layer.frame, 20, 20);
    
    self.profileView.layer.shadowOffset = CGSizeMake(1, 0);
    self.profileView.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.profileView.layer.shadowRadius = 5;
    self.profileView.layer.shadowOpacity = .25;
    
    [self.view addSubview:self.profileView];
    
    [self.mapView setScrollEnabled:NO];
    self.mapView_.settings.scrollGestures = NO;
    
    
    //[self.mapView setUserInteractionEnabled:NO];
    
}

- (void) mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    
    NSLog(@"didSelectAnnotationView");
    
    Annotation * annotation = view.annotation;
    
    if(![annotation isKindOfClass:[MKUserLocation class]]) {
        
        childView.user = ((Annotation* )view.annotation).theUserObject;
        childView.userParty = ((Annotation *)view.annotation).partyObject;
        childView.NameLabel.text = ((Annotation *)view.annotation).subtitle;
        
        NSLog(@"this looking the index of the of a annotation %@",view.annotation );
        
    }
    
    // childView.headline.text = ((Annotation * )view.annotation).annObjectId;
    
    
}


- (IBAction)heat:(id)sender {
    
    [self.profileView setHidden:YES];
    [self.profileView setUserInteractionEnabled:NO];
    
    NSLog(@"friendsheat locations map count %lu", (unsigned long)self.friendsLocations.count);
    
    if (self.friendsLocations.count != 250) {
        [self.view makeToast:@"Low activity around your area" duration:3.0 position:CSToastPositionCenter];
        
    }else{
        
        float boost = 0.5;
        UIImage * heatmap = [LFHeatMap heatMapForMapView:self.mapView boost:boost locations:self.friendsLocations weights:self.weights];
        //UIImage * heatmap = [LFHeatMap heatMapWithRect:CGRectMake(0, 0, self.mapView_.frame.size.width, self.mapView_.frame.size.height) boost:boost locations:self.friendsLocations weights:self.weights];
        
        
        
        
        UIButton * button = (UIButton *) sender;
        button.tag = button.tag+1;
        
        NSLog(@"%lu",(unsigned long) button.tag);
        
        if (button.tag % 2 == 0) {
            //[self.view sendSubviewToBack:self.settingsView];
            
            //self.view.alpha = 1;
            self.mapView.alpha = 1;
            
            UIImage * heatImg = [[UIImage imageNamed:@"flameOff"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            // [self.heatMapButton setBackgroundImage:heatImg forState:UIControlStateNormal];
            [self.heatMapButton setImage:heatImg forState:UIControlStateNormal];
            [self.heatMapButton setTintColor:[UIColor redColor]];
            //[self.view sendSubviewToBack:self.otherView];
            self.mapView_.settings.scrollGestures = YES;
            
            
            self.mapView.userInteractionEnabled = YES;
            self.sliderState.hidden = YES;
            self.heatMapView.image = nil;
            //44[self.view sendSubviewToBack:self.otherView];
            //self.otherView.image = nil;
        }
        if (button.tag % 2 == 1) {
            //[self.view bringSubviewToFront:self.settingsView];
            
            //self.view.alpha = .6;
            self.mapView.alpha = 0.7;
            
            
            UIImage * heatImg = [[UIImage imageNamed:@"flameOn"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            
            [self.heatMapButton setImage:heatImg forState:UIControlStateNormal];
            
            self.mapView.userInteractionEnabled = NO;
            self.sliderState.hidden = NO;
            // [self.view bringSubviewToFront:self.mapView];
            self.mapView_.settings.scrollGestures = NO;
            //[self.heatMapView bringSubviewToFront:];
            // [self.view bringSubviewToFront:self.otherView];
            // self.otherView.image = heatmap;
            
            
            self.heatMapView.image = heatmap;
            
            
        }
    }
    
}



-(void)moveLegalLabel
{
    UIView * legalLink = _mapView.subviews[1];
    legalLink.frame = CGRectMake(25, 35 , legalLink.frame.size.width, legalLink.frame.size.height);
    legalLink.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
}


-(void)viewDidAppear:(BOOL)animated {
    //[self moveLegalLabel];
    
    //NSTimer * segueTo =[NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(moveLegalLabel) userInfo:nil repeats:NO];
}

- (IBAction)exitProfileView:(id)sender {
    
    NSLog(@"ProfileView outside Touch................");
    
    [self.view sendSubviewToBack:self.profileView];
    
    [self.profileView setHidden:YES];
    
    [self.profileView setUserInteractionEnabled:NO];
    
    [self.mapView setScrollEnabled:YES];
    
    self.mapView.alpha = 1;
    
    self.mapView_.settings.scrollGestures = YES;
    
    [tabbar removeKeyboard];
    
    //[self.mapView setUserInteractionEnabled:YES];
}

- (void) refreshFailed{
    
    [self.view makeToast:@"Could not refresh" duration:2.5 position:CSToastPositionBottom];
    
}

-(NSString *) locationDistanceString:(CLLocation*) partyObjectsLocation{
    CLLocation * currentUserLocation = [[CLLocation alloc] initWithLatitude:cood.latitude longitude:cood.longitude];
    
    CLLocationDistance distanceFromFriend = [currentUserLocation distanceFromLocation:partyObjectsLocation];
    double mToMi = distanceFromFriend/1609.344;
    NSString * stringDistance = [NSString new];
    
    if (mToMi < 1.0) {
        
        stringDistance = [NSString stringWithFormat:@"Less than a mile"];
    }else{
        
        stringDistance = [NSString stringWithFormat:@"%.2f miles away", mToMi];
    }
    
    return stringDistance;
    
}


- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    
    CGPoint point = mapView.center;
    
    CLLocationCoordinate2D coor = [mapView.projection coordinateForPoint:point];
    
    [self.mapView setCenter:point];
    
    NSLog(@"center of the apple maps %@ then center of google maps %@ ",NSStringFromCGPoint(mapView.center), NSStringFromCGPoint(self.mapView.center));
}



- (void) ZoomUser
{
    MKCoordinateRegion region;
    cood = [LocationManagerSingleton sharedSingleton].locationManager.location.coordinate;
    
    if (cood.longitude == 0 && cood.latitude == 0) {
        
        CLLocationDegrees  Long= me_HughesLong;
        CLLocationDegrees  Lat = me_HughesLat;
        
        
        CLLocationCoordinate2D coordinates = CLLocationCoordinate2DMake(Lat,Long);
        
        GMSCameraUpdate * updateCamera = [GMSCameraUpdate setTarget:coordinates zoom:15];
        
        [self.mapView_ animateWithCameraUpdate:updateCamera];
        
        region.center.latitude = me_HughesLat;
        region.center.longitude = me_HughesLong;
        region.span = MKCoordinateSpanMake(spanX, spanY);
        [self.mapView setRegion:region animated:YES];
    }
    else {
        
        CLLocationCoordinate2D coordinates = CLLocationCoordinate2DMake(cood.latitude, cood.longitude);
        
        GMSCameraUpdate * updateCamera = [GMSCameraUpdate setTarget:coordinates zoom:15];
        
        
        [self.mapView_ animateWithCameraUpdate:updateCamera];
        
        region.center.latitude = cood.latitude;
        region.center.longitude = cood.longitude;
        region.span = MKCoordinateSpanMake(spanx2, spany2);
        [self.mapView setRegion:region animated:YES];
        
    }
}

//-(BOOL)alertTimeCheck:(NSDate *)objectTimeStamp{
//    NSDate * now = [NSDate date];
//    NSTimeInterval secondsSince = -(int)[NSTimeIn]
//    
//
//}


@end