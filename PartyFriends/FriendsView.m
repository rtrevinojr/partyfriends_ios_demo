//
//  FriendsView.m
//  PartyFriends
//
//  Created by Joshua Silva  on 8/14/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import "FriendsView.h"

#import "PFFriendsListTableViewCell.h"
#import <ParseUI/ParseUI.h>


@interface FriendsView ()


@end
int theRow;


@implementation FriendsView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"PF FriendsView viewDidLoad");
    
    self.refreshControl = [[UIRefreshControl alloc]init];
    [self.tableView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    if(self.currentUser == nil){
    
        self.currentUser = [PFUser currentUser];
    }
    
    self.friends = [self.currentUser relationForKey:@"friends"];
    
    
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:NO];
    
    self.friends = [[PFUser currentUser] objectForKey:@"friends"];
    
    PFQuery * query = [self.friends query];
    [query orderByAscending:@"firstName"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
        if(error) {
            
            NSLog(@" error %@  %@  ", error, [error userInfo]);
            
        }
        else {
           
            self.currentFriends = [[NSMutableArray alloc] initWithArray:objects];
            [self.tableView reloadData];
        }
        
    }];
    
    
    
    
  /*  NSString * roleName = [NSString stringWithFormat:@"Role-%@", self.currentUser.objectId];
    
    PFQuery *query = [PFRole query];
    [query whereKey:@"name" equalTo:roleName];
    [query findObjectsInBackgroundWithBlock:^(NSArray * objects, NSError * error){
        NSLog(@"here");
        if (error.code == 101 || objects.count==0) {
            NSLog(@"here2");
            
            PFACL *acl = [PFACL ACL];
            [acl setPublicReadAccess:false];
            [acl setPublicWriteAccess:false];
            [acl setReadAccess:true forUser:[PFUser currentUser]];
            [acl setWriteAccess:true forUser:[PFUser currentUser]];
            //NSString * roleName = [NSString stringWithFormat:@"Role-%@", self.currentUser.objectId];
            PFRole *role = [PFRole roleWithName:roleName];
            [role setACL:acl];
            [role saveInBackgroundWithBlock:^(BOOL succeeded, NSError * error){
                
                if (!error) {
                    PFQuery *query = [PFRole query];
                    [query whereKey:@"name" equalTo:roleName];
                    [query findObjectsInBackgroundWithBlock:^(NSArray * objects, NSError * error){
                        NSLog(@"double checking");
                        if (error) {
                            NSLog(@"double checking");
                        }else if(objects.count>0){
                            NSLog(@"double checking2");
                            
                            
                            self.rolling = objects[0];
                         
                            
                        }else{

                        }
                    }];
                  
                }
                
            }];
        }else{
            self.rolling = objects[0];
            }
    }];

    
    
    
    
    
    
    */
    
    
    
    
    [self.tableView reloadData];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)refreshTable {
    //TODO: refresh your data
    [self.refreshControl endRefreshing];
    
    //[self requeryTable];
    
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return [self.currentFriends count];
}


- (PFFriendsListTableViewCell * ) tableView: (UITableView *) tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    static NSString * cellIdentifier = @"friendlistcell";
    PFFriendsListTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[PFFriendsListTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    PFUser * friend = [self.currentFriends objectAtIndex:indexPath.row];
    
    NSString * first = [friend objectForKey:@"firstName"];
    NSString * last = [friend objectForKey:@"lastName"];
    NSString * fullName = [NSString stringWithFormat:@"%@ %@", first, last];
    
    
    CALayer *imageLayer = cell.friendProfileImage.layer;
    [imageLayer setCornerRadius:5];
    [imageLayer setBorderWidth:0];
    [imageLayer setMasksToBounds:YES];
    [cell.friendProfileImage.layer setCornerRadius:cell.friendProfileImage.frame.size.width/2];
    [cell.friendProfileImage.layer setMasksToBounds:YES];
    
    PFObject * photo = friend[@"profilePicture"];
    
    if (photo !=nil) {
        [photo fetchInBackgroundWithBlock:^(PFObject * photoObject, NSError * error){
            if (!error) {
                PFFile * pic =[photoObject objectForKey:@"photo"];
                PFImageView * userImage =[PFImageView new];
                userImage.image = [UIImage imageNamed:@"userPic"];
                userImage.file = (PFFile *) pic;
                
                [userImage loadInBackground:^(UIImage * image, NSError * error){
                    if (!error) {
                        image = [self squareImageFromImage:image scaledToSize:image.size.height];
                        [cell.friendProfileImage setImage:image];
                    }else {
                        UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
                        [cell.friendProfileImage setImage:defaultimg];
                        
                    }
                    
                }];
                
            }
            
            
        }];
    }else{
        
        UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
        [cell.friendProfileImage setImage:defaultimg];
        
    }
    
    
    
    
    
    
//    if (friend[@"profilePic"] != nil) {
//        
//        PFFile * pic = [friend objectForKey:@"profilePic"];
//        
//        
//        [pic getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
//            if (!error) {
//                UIImage *image = [UIImage imageWithData:data];
//                CALayer *imageLayer = cell.friendProfileImage.layer;
//                [imageLayer setCornerRadius:5];
//                [imageLayer setBorderWidth:1];
//                [imageLayer setMasksToBounds:YES];
//                [cell.friendProfileImage.layer setCornerRadius:cell.friendProfileImage.frame.size.width/2];
//                [cell.friendProfileImage.layer setMasksToBounds:YES];
//                [cell.friendProfileImage setImage:image];
//            }
//            else {
//                
//                UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
//                
//                CALayer *imageLayer = cell.friendProfileImage.layer;
//                [imageLayer setCornerRadius:5];
//                [imageLayer setBorderWidth:1];
//                [imageLayer setMasksToBounds:YES];
//                [cell.friendProfileImage.layer setCornerRadius:cell.friendProfileImage.frame.size.width/2];
//                [cell.friendProfileImage.layer setMasksToBounds:YES];
//                [cell.friendProfileImage setImage:defaultimg];
//            }
//        }];
//        
//    }
//    else {
//        UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
//        
//        CALayer *imageLayer = cell.friendProfileImage.layer;
//        [imageLayer setCornerRadius:5];
//        [imageLayer setBorderWidth:1];
//        [imageLayer setMasksToBounds:YES];
//        [cell.friendProfileImage.layer setCornerRadius:cell.friendProfileImage.frame.size.width/2];
//        [cell.friendProfileImage.layer setMasksToBounds:YES];
//        [cell.friendProfileImage setImage:defaultimg];
//    }
    
    
    cell.friendNameLabel.text = fullName;
    
    
    return cell;
    
}


/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 
    static NSString * cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

    PFUser * name=[self.currentFriends objectAtIndex:indexPath.row];
    
    [self.index indexAtPosition:indexPath.row];
    
    cell.textLabel.text = name[@"firstName"];
    
    UILabel * remove = [[UILabel alloc] init];
    remove.frame = CGRectMake(cell.bounds.origin.x + 200, cell.bounds.origin.y + 20 , 70, 30);
    [remove setText:@"Delete!"];
    [remove setTextColor:[UIColor whiteColor]];
    [remove setBackgroundColor:[UIColor redColor]];
    [cell.contentView addSubview:remove];
    
    
    //UIButton * remove = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    remove.frame = CGRectMake(cell.bounds.origin.x + 200, cell.bounds.origin.y + 20 , 70, 30);
    [remove setTitle: @"Remove" forState:UIControlStateNormal];
    
    [remove addTarget:self action:@selector(remove:) forControlEvents:UIControlEventTouchUpInside];
    remove.backgroundColor = [UIColor redColor];
    
    [remove setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
        //[self.statusButton setAttributedTitle:[[NSAttributedString alloc]  initWithString:@"Going Out" attributes:nil] forState:UIControlStateNormal];
    
   // remove.tag = 1;
    //[cell.contentView addSubview:remove];
    
    return cell;
}
*/



-(void)waffles{


    NSLog(@"waffles");
}

-(IBAction)remove:(id)sender{
    
    PFUser * unwanted = [self.currentFriends objectAtIndex:_index.row];
    NSString * message = [NSString stringWithFormat:@"Are you aure you want to delete %@", unwanted[@"firstName"]];
    
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Gaaaayyyyy!!" message:message delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    
    [alert show];
    
   /*[self.friends removeObject:unwanted];
    
    
    [self.currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        if(error)
        {
            NSLog(@"%@%@ æaa8āåãērÿžqgkłńgmbvčxœûütrwhfjpdß  %ld" ,error,error.userInfo, (long)error.code);
        }else{
    
            [self.currentFriends removeObjectAtIndex:_index.row];

    UIButton * removeBtn = (UIButton *) sender;
    [removeBtn setHidden:YES];
            
            [self.tableView reloadData];
    
        }

    }];
    */

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == [alertView firstOtherButtonIndex]) {
        
        
        

        PFUser * wanted = [self.currentFriends objectAtIndex:theRow];
        NSString * wantedID = wanted.objectId;
        
        
            NSLog(@"unwanted %@",wanted);
        
        [PFCloud callFunctionInBackground:@"Delete" withParameters:@{@"user2Id":wantedID} block:^(NSString * results, NSError * error){
        
            if (!error) {
                NSLog(@"the person that was deleted %@", wanted[@"firstName"]);
                [self.currentFriends removeObject:wanted];
                [self.tableView reloadData];
            }else{
                NSLog(@"ERRRROROOROROROORROROROROR");
            
            }
        
        
        
        }];
        
  /*      PFQuery *query1 = [PFQuery queryWithClassName:@"friendrequest"];
        [query1 whereKey:@"user1" equalTo:self.currentUser];
        [query1 whereKey:@"user2" equalTo:wanted];
        [query1 whereKey:@"status" equalTo:@3];
        PFQuery *query2 = [PFQuery queryWithClassName:@"friendrequest"];
        [query2 whereKey:@"user1" equalTo:wanted];
        [query2 whereKey:@"user2" equalTo:self.currentUser];
        [query2 whereKey:@"status" equalTo:@3];
        PFQuery *query3 = [PFQuery queryWithClassName:@"friendrequest"];
        [query3 whereKey:@"user1" equalTo:self.currentUser];
        [query3 whereKey:@"user2" equalTo:wanted];
        [query3 whereKey:@"status" equalTo:@1];
        PFQuery *query4 = [PFQuery queryWithClassName:@"friendrequest"];
        [query4 whereKey:@"user1" equalTo:wanted];
        [query4 whereKey:@"user2" equalTo:self.currentUser];
        [query4 whereKey:@"status" equalTo:@1];
//        PFQuery *query5 = [PFQuery queryWithClassName:@"friendrequest"];
//        [query5 whereKey:@"user1" equalTo:self.currentUser];
//        [query5 whereKey:@"user2" equalTo:wanted];
//        [query5 whereKey:@"status" equalTo:@4];
//        PFQuery *query6 = [PFQuery queryWithClassName:@"friendrequest"];
//        [query6 whereKey:@"user1" equalTo:wanted];
//        [query6 whereKey:@"user2" equalTo:self.currentUser];
//        [query6 whereKey:@"status" equalTo:@4];
        PFQuery *finalQuery = [PFQuery orQueryWithSubqueries:@[query1,query2,query3,query4]];
        [finalQuery findObjectsInBackgroundWithBlock:^(NSArray *objects,NSError *error){
            if(!error){
                NSLog(@"gotem %@",objects);
//                for (int n = 0; objects.count>n; n++) {
//                    PFObject *request = objects[n];
//                    PFUser *user1 = [request objectForKey:@"user1"];
//                    if(user1 = );
//                    
//                    
//                    
//                }
                
                PFObject *request = objects[0];
                PFUser *user2 = [request objectForKey:@"user2"];
                NSLog(@"%@",user2);

                [self.friends removeObject:user2];
//                PFRelation *relation = [self.currentUser relationForKey:@"friends"];
//                [relation removeObject:user2];
                
                [request setValue:@4 forKey:@"status"];
                [request saveInBackgroundWithBlock:^(BOOL succedded,NSError *error){
                    if (error) {
                        NSLog(@"%@",error);
                    }
                    else{
                        NSLog(@"Updated request.");
                    }
                }];
            }
            else{
                NSLog(@"******* %@",error);
            }
            
        }];
   */
        

        
        //[self.friends removeObject:u];
        
        
//        [self.currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
//            
//            if(error)
//            {
//                NSLog(@"%@%@   %ld" ,error,error.userInfo, (long)error.code);
//            }else{
//                
//              //  [self.currentFriends removeObjectAtIndex:theRow];
//                
//               // UIButton * removeBtn = (UIButton *) sender;
//                //[removeBtn setHidden:YES];
//                
//                NSLog(@"the person is gone");
//                
//                [self.tableView reloadData];
//                
//            }
//            
//        }];
        
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
        [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    

    PFUser * unwanted = [self.currentFriends objectAtIndex:indexPath.row];
    
    
    
    theRow = indexPath.row;
    
    NSLog(@"%d    %ld", theRow, (long)indexPath.row);
    NSString * message = [NSString stringWithFormat:@"Are you sure you want to remove %@ as a friend?", unwanted[@"firstName"]];
    
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Delete!!" message:message delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    
    [alert show];
    



}


-(void)whatever:(PFUser * ) unwanted{


    
    NSLog(@"unwanted %@",unwanted);
    
    PFQuery *query1 = [PFQuery queryWithClassName:@"friendrequest"];
    [query1 whereKey:@"user1" equalTo:self.currentUser];
    [query1 whereKey:@"user2" equalTo:unwanted];
    [query1 whereKey:@"status" equalTo:@3];
    PFQuery *query2 = [PFQuery queryWithClassName:@"friendrequest"];
    [query2 whereKey:@"user1" equalTo:unwanted];
    [query2 whereKey:@"user2" equalTo:self.currentUser];
    [query2 whereKey:@"status" equalTo:@3];
    PFQuery *query3 = [PFQuery queryWithClassName:@"friendrequest"];
    [query3 whereKey:@"user1" equalTo:self.currentUser];
    [query3 whereKey:@"user2" equalTo:unwanted];
    [query3 whereKey:@"status" equalTo:@1];
    PFQuery *query4 = [PFQuery queryWithClassName:@"friendrequest"];
    [query4 whereKey:@"user1" equalTo:unwanted];
    [query4 whereKey:@"user2" equalTo:self.currentUser];
    [query4 whereKey:@"status" equalTo:@1];
    PFQuery *finalQuery = [PFQuery orQueryWithSubqueries:@[query1,query2,query3,query4]];
    [finalQuery findObjectsInBackgroundWithBlock:^(NSArray *objects,NSError *error){
        if(!error){
            NSLog(@"gotem %lu",(unsigned long)objects.count);
        }
        
    }];



}

- (UIImage *)squareImageFromImage:(UIImage *)image scaledToSize:(CGFloat)newSize {
    CGAffineTransform scaleTransform;
    CGPoint origin;
    
    if (image.size.width > image.size.height) {
        CGFloat scaleRatio = newSize / image.size.height;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(-(image.size.width - image.size.height) / 2.0f, 0);
    } else {
        CGFloat scaleRatio = newSize / image.size.width;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(0, -(image.size.height - image.size.width) / 2.0f);
    }
    
    CGSize size = CGSizeMake(newSize, newSize);
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(size, YES, 0);
    } else {
        UIGraphicsBeginImageContext(size);
    }
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextConcatCTM(context, scaleTransform);
    
    [image drawAtPoint:origin];
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
