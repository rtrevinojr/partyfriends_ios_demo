//
//  PFHostingDetailViewController.h
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 11/16/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Parse/Parse.h>


@interface PFHostingDetailViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>


@property (weak, nonatomic) IBOutlet UITextField *headlineTextField;

@property (weak, nonatomic) IBOutlet UITextField *descriptionTextField;

@property (weak, nonatomic) IBOutlet UITextField *addressTextField;



@property (weak, nonatomic) IBOutlet UIButton *savePartyBtn;

@property (strong, nonatomic) NSArray * allFriends;

@property (strong, nonatomic) NSMutableArray * pickFriends;


@property (weak, nonatomic) IBOutlet UIDatePicker *hostingPartyDatePicker;

@property (strong, nonatomic) IBOutlet UITableView *tableView;


@property (strong, nonatomic) PFUser * currentUser;

@property (strong, nonatomic) PFRelation * allFriendsRelation;

@property (strong, nonatomic) PFRole * rolling;

@property (strong, nonatomic) PFRole * role;
@property (strong, nonatomic) PFRelation * userRelation;

@end
