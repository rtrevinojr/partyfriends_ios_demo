//
//  PFNewsFeedTableViewCell.h
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 11/11/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Parse/Parse.h>


@interface PFNewsFeedTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *movingLabel;

@property (weak, nonatomic) IBOutlet UILabel *profileName;
@property (weak, nonatomic) IBOutlet UILabel *profileHeadline;
@property (weak, nonatomic) IBOutlet UILabel *profileSubheadline;
@property (strong, nonatomic) IBOutlet UILabel *thridText;


@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (strong, nonatomic) IBOutlet UIImageView *batteryImage;
@property (strong, nonatomic) IBOutlet UILabel *batteryNumber;

@property (strong, nonatomic) IBOutlet UILabel *alertLabel;

@property (strong, nonatomic) NSString * objectId;
@property (strong, nonatomic) IBOutlet UIButton *tugButton;

@property (strong, nonatomic) IBOutlet UIImageView *statusButton;

@property (strong, nonatomic) PFObject * partyObject;

@property (strong, nonatomic) PFObject * postObject;

@property (strong, nonatomic) PFUser * userObject;

@property (weak, nonatomic) IBOutlet UILabel *partyCommentCount;

@property (weak, nonatomic) IBOutlet UILabel *pointsLabel;

@property (strong, nonatomic) IBOutlet UIButton *commentButton;

@end
