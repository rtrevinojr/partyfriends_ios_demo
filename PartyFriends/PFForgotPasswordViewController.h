//
//  PFForgotPasswordViewController.h
//  PartyFriends
//
//  Created by Joshua Silva  on 1/14/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>


@interface PFForgotPasswordViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *emailAddress;
@property (strong, nonatomic) IBOutlet UITextView *emailTextView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
