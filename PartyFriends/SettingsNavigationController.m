//
//  SettingsNavigationController.m
//  PartyFriends
//
//  Created by Joshua Silva  on 2/17/16.
//  Copyright © 2016 Joshua Silva . All rights reserved.
//

#import "SettingsNavigationController.h"
#import "Gradient.h"

@interface SettingsNavigationController (RotationAll)


@end

@implementation SettingsNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    CAGradientLayer * randomLayer = [Gradient gold];
//    
//    randomLayer.frame = CGRectMake(0, -20, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height + 20);
//    
//    [randomLayer setStartPoint:CGPointMake(0.0, 0.5)];
//    [randomLayer setEndPoint:CGPointMake(1.0, 0.5)];
//    
//    
//    [self.navigationBar.layer insertSublayer:randomLayer atIndex:0];
    
    
//    CAGradientLayer * randomLayer = [Gradient gold];
//    randomLayer.frame = CGRectMake(0, -20, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height + 20);
//    [randomLayer setStartPoint:CGPointMake(0.0, 0.5)];
//    [randomLayer setEndPoint:CGPointMake(1.0, 0.5)];
//    CGSize goldSize = CGSizeMake(randomLayer.bounds.size.width, randomLayer.bounds.size.height);
//    
//    UIGraphicsBeginImageContext(goldSize);
//    [randomLayer renderInContext:UIGraphicsGetCurrentContext()];
//    UIImage *gradientImage =UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    
//    [self.navigationBar setBackgroundImage:gradientImage forBarMetrics:UIBarMetricsDefault];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    CAGradientLayer * randomLayer = [Gradient gold];
    
    randomLayer.frame = CGRectMake(0, -20, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height + 20);
    
    [randomLayer setStartPoint:CGPointMake(0.0, 0.5)];
    [randomLayer setEndPoint:CGPointMake(1.0, 0.5)];
    
    
    [self.navigationController.navigationBar.layer insertSublayer:randomLayer atIndex:1];

}

//-(NSUInteger)supportedInterfaceOrientations {
//    return UIInterfaceOrientationMaskAll;
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
