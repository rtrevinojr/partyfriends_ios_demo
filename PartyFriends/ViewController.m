
//  ViewController.m
//  VendorProfile
//
//  Created by Gilberto Silva on 11/30/15.
//  Copyright © 2015 Gilberto Silva. All rights reserved.
//

#import "ViewController.h"
#import "VendorCell.h"
#import "PFVendorEditController.h"
//#import "VendorCell.m"
#import "Gradient.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.currentUser = [PFUser currentUser];
    
    CGSize barSize = CGSizeMake(self.navigationBar.frame.size.width, self.navigationBar.frame.size.height +20);
    
    // CAGradientLayer * whiteLayer = [Gradient white];
    
    CAGradientLayer * randomLayer = [Gradient gold];
    
    //  whiteLayer.frame = CGRectMake(0, -20, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height + 20);
    
    randomLayer.frame = CGRectMake(0, -20, self.navigationBar.frame.size.width, self.navigationBar.frame.size.height + 20);
    
    //[whiteLayer setStartPoint:CGPointMake(0.0, 0.5)];
    // [whiteLayer setEndPoint:CGPointMake(1.0, 0.5)];
    //    [randomLayer setStartPoint:CGPointMake(0.0, 0.5)];
    //    [randomLayer setEndPoint:CGPointMake(1.0, 0.5)];
    
    [randomLayer setStartPoint:CGPointMake(-1.5, 0.0)];
    [randomLayer setEndPoint:CGPointMake(2.5, 0.0)];
    UIGraphicsBeginImageContext(barSize);
    [randomLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * anotherImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.navigationBar setBackgroundImage:anotherImage forBarMetrics:UIBarMetricsDefault];
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:NO];
    
    PFRelation *vendorsAccounts = [self.currentUser relationForKey:@"vendors"];
    PFQuery *query = [vendorsAccounts query];
    [query orderByAscending:@"createdAt"];
    [query findObjectsInBackgroundWithBlock:^(NSArray * objects, NSError * error){
        if(!error){
            self.vendorAccounts = [[NSMutableArray alloc] initWithArray:objects];
            [self.tableView reloadData];
            NSLog(@"Vendor count = %lu",self.vendorAccounts.count);
        }
    }];
    
    
    
    
}

-(IBAction)backToView:(UIStoryboard *) segue{
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.vendorAccounts count] + 1 ;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    
    return NO;
    
    
}
- (IBAction)editButton:(id)sender {
    
    VendorCell * cell = (VendorCell *) [[sender superview] superview];
    
    self.vendorObject = cell.vendorCellObject;
    
    
    [self performSegueWithIdentifier:@"toVendorEdit" sender:self];
}

- (VendorCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * cellId = @"cell";
    VendorCell * cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
    
    cell.editButton.hidden = false;
    cell.createNewButton.hidden = false;
    cell.nameLabel.hidden = false;
    
    if(indexPath.row == self.vendorAccounts.count){
        // cell.textLabel.text =  @"Cell";
        cell.nameLabel.hidden = true;
        //        cell.createNewButton.hidden = true;
        cell.editButton.hidden = true;
    }
    else{
        PFObject * vendor = [self.vendorAccounts objectAtIndex:indexPath.row];
        cell.createNewButton.hidden = true;
        cell.vendorCellObject = vendor;
        cell.nameLabel.text = vendor[@"businessName"];
    }
    
    return cell;
}

-(void) tableView: (UITableView *) tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"toVendorEdit"]) {
        
        //PFVendorContainerViewController * vendorVC = [segue destinationViewController];
        
        PFVendorEditController * vendorVC = [segue destinationViewController];
        
        vendorVC.vendorObejct = self.vendorObject;
        
    }
}


- (IBAction)backButton:(id)sender {
    [self performSegueWithIdentifier:@"backToSetting" sender:self];
    
}


@end









