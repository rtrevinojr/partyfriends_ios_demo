//
//  PFVendorAddTableViewController.h
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 12/9/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PFVendorAddTableViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>


@property (strong, nonatomic) NSArray * promoterFriends;

@property (strong, nonatomic) NSArray * currentFriends;


@end
