//
//  PFVendorSelectPromoTableViewCell.h
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 12/10/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Parse/Parse.h>


@interface PFVendorSelectPromoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *promoterFriendChoiceImageView;
@property (weak, nonatomic) IBOutlet UILabel *promoterFriendNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *promoterFriendAddButton;

@property (strong, nonatomic) PFObject * promoterPFObject;

@end
