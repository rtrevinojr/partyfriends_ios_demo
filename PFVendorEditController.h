//
//  PFVendorEditController.h
//  VendorProfile
//
//  Created by Gilberto Silva on 11/30/15.
//  Copyright © 2015 Gilberto Silva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface PFVendorEditController : UIViewController



@property (strong, nonatomic) IBOutlet UISegmentedControl *vendorTabSelector;
@property (strong, nonatomic) PFObject * vendorObejct;
-(void)sendInfo;


@end
