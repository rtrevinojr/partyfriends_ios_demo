//
//  LocationManagerSingleton.m
//  PartyFriends
//
//  Created by Joshua Silva  on 8/18/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import "LocationManagerSingleton.h"
#import <Parse/Parse.h>
@import GoogleMaps;


@implementation LocationManagerSingleton{
    GMSPlacesClient * _placesClient;
}


@synthesize locationManager;

-(id) init {
    self = [super init];
    
    if(self) {
        self.locationManager = [CLLocationManager new];
        [self.locationManager setDelegate:self];
        [self.locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
        
        [self.locationManager requestAlwaysAuthorization];
        
        if (![CLLocationManager locationServicesEnabled]){
            NSLog(@"location services are disabled");
            //return;
        }
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied){
            NSLog(@"location services are blocked by the user");
            UIAlertController * locationDenied = [UIAlertController alertControllerWithTitle:@"Background Location Access Denied" message:@"Background location updates are needed to share your location with friends only if you are in Party or Exclusive" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
            [locationDenied addAction:cancelAction];
            
            
            UIAlertAction * settingAction = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                NSLog(@"asdhfgjk");
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
              
                
            }];;
            [locationDenied addAction:settingAction];
            [locationDenied addAction:cancelAction];
            
           // [UIAlertController presentViewController:locationDenied comletion:nil];
            
            
            //return;
        }
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways){
            NSLog(@"location services are enabled");
            
            
            
        }
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined){
            NSLog(@"about to show a dialog requesting permission");
            [self.locationManager requestAlwaysAuthorization];
        }
        
        
        CLLocationCoordinate2D cood = CLLocationCoordinate2DMake(30.403677, -97.854031);
        
        CLRegion * waffles = [[CLCircularRegion alloc] initWithCenter:cood radius:200 identifier:@"waffles"];
        CLLocationCoordinate2D otherCood = CLLocationCoordinate2DMake(30.359383, -97.791205);
        
        CLRegion * cereal=[[CLCircularRegion alloc] initWithCenter:otherCood radius:200 identifier:@"cereal"];
        
        [self.locationManager startMonitoringForRegion:waffles];
        [self.locationManager startMonitoringForRegion:cereal];
        self.currentUser = [PFUser currentUser];
        
   
        [self.locationManager startUpdatingLocation];
        [self.locationManager  startMonitoringVisits];
        
        _placesClient = [[GMSPlacesClient alloc] init];
        
    }
    return self;
    
}




+ (LocationManagerSingleton*)sharedSingleton {
    static LocationManagerSingleton* sharedSingleton;
    if(!sharedSingleton) {
        @synchronized(sharedSingleton) {
            sharedSingleton = [LocationManagerSingleton new];
        }
    }
    
    return sharedSingleton;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    //handle your location updates here

    
    
}

-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region{
    
    
    if ([region.identifier isEqualToString:@"waffles"]) {
        UILocalNotification * notification = [UILocalNotification  new];
        notification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
        notification.alertBody = @"Leaving the 1st region";
        
        [[UIApplication sharedApplication] scheduleLocalNotification:notification ];
        
        
    }
    
 
    /*else{
        
        UILocalNotification * note = [UILocalNotification  new];
                NSString * random = [NSString stringWithFormat:@"Random EXIT ?! %@", region.identifier];
        note.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
        note.alertBody = random;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:note ];
        
    }
    */

    
}


-(void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region{
    
    
    if ([region.identifier isEqualToString:@"waffles"]) {
        UILocalNotification * noti = [UILocalNotification  new];
        noti.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
        noti.alertBody = @"Entering the 1st region";
        
       // [[UIApplication sharedApplication] scheduleLocalNotification:noti ];
        
        
        
    }

    
    if([region.identifier isEqualToString:@"home"]){
        
        if ([self.currentUser[@"status"] isEqual:@1] || [self.currentUser[@"status"] isEqual:@2]) {
            
        UILocalNotification * homeNote = [UILocalNotification new];
        
        homeNote.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
            homeNote.alertTitle = @"Are you Home?";
        homeNote.alertBody = @"Reminder you are still online. Go offline to stop sharing your location";
        
        [[UIApplication sharedApplication] scheduleLocalNotification:homeNote];
//        [self.currentUser saveInBackgroundWithBlock:^(BOOL succeed, NSError * error){
//            if (error) {
//                UILocalNotification * errorNote = [UILocalNotification new];
//                
//                errorNote.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
//                errorNote.alertBody = @"error :(";
//                
//                [[UIApplication sharedApplication] scheduleLocalNotification:errorNote];
//            }else{
//                UILocalNotification * goodNote = [UILocalNotification new];
//                
//                goodNote.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
//                goodNote.alertBody = @"yay! check parse!";
//                
//                [[UIApplication sharedApplication] scheduleLocalNotification:goodNote];
//            
//            
//            }
//        
//        
//        }];
        }
    }
    /*else{
    
        UILocalNotification * note = [UILocalNotification  new];
        NSString * random = [NSString stringWithFormat:@"Random ENTER ?! %@", region.identifier];
        note.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
        note.alertBody = random;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:note];
        
    }
    */
    
    if([region.identifier isEqualToString:@"nearHome"]){
        
        UILocalNotification * close = [UILocalNotification new];
        close.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
        close.alertBody = @"near the home! -- ENTER";
        
       // [[UIApplication sharedApplication] scheduleLocalNotification:close];
    }
    if ([region.identifier isEqualToString:@"r"]) {
        
        UILocalNotification * far = [UILocalNotification new];
        far.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
        far.alertBody = @"near the spot -- ENTER";
        
        //[[UIApplication sharedApplication] scheduleLocalNotification:far];
    }
    
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    //handle your heading updates here- I would suggest only handling the nth update, because they
    //come in fast and furious and it takes a lot of processing power to handle all of them
}

-(void)locationManager:(CLLocationManager *)manager didFinishDeferredUpdatesWithError:(nullable NSError *)error{



}

-(void)locationManager:(CLLocationManager *)manager didVisit:(CLVisit *)visit
{
    
    UILocalNotification *n = [[UILocalNotification alloc] init];
    if ([visit.departureDate isEqual: [NSDate distantFuture]]) {
        
        n.alertBody = [NSString stringWithFormat:@"didVisit: We arrived somewhere!"];
        if ([self.currentUser[@"status"] isEqual:@1] || [self.currentUser[@"status"] isEqual:@2]) {
            
            
                UILocalNotification *v= [[UILocalNotification alloc] init];
            
            NSNumber * Long = [NSNumber numberWithDouble:[LocationManagerSingleton sharedSingleton].locationManager.location.coordinate.longitude];
            NSNumber * lat = [NSNumber numberWithDouble:[LocationManagerSingleton sharedSingleton].locationManager.location.coordinate.latitude];
            CLLocation * location = [[CLLocation alloc] initWithLatitude:[LocationManagerSingleton sharedSingleton].locationManager.location.coordinate.latitude longitude:[LocationManagerSingleton sharedSingleton].locationManager.location.coordinate.longitude];
            int batteryLevel = [[UIDevice currentDevice] batteryLevel] * 100;
            NSNumber * battery = [NSNumber numberWithInt:batteryLevel];
            
            
            
            NSMutableDictionary * params = [NSMutableDictionary new];
            params[@"headline"] = @"";
            params[@"lat"] = lat;
            params[@"long"] = Long;
            params[@"batteryPercentage"] = battery;
            
            
            [_placesClient currentPlaceWithCallback:^(GMSPlaceLikelihoodList *likelihoodList, NSError *error) {
                if (error != nil) {
                    NSLog(@"Current Place error %@", [error localizedDescription]);
                    [PFCloud callFunctionInBackground:@"CreateParty" withParameters:params block:^(NSString * results, NSError * error){
                        
                        if (!error) {
                            NSLog(@"the results ARE %@: ", results);
                        }else{
                            NSLog(@"create  ERROR: %@", error);
                            NSLog(@"theres an erRor %@", error);
                        }
                        
                    }];
                    
                    
                    
                    return;
                }
                
                
                for (GMSPlaceLikelihood *likelihood in likelihoodList.likelihoods) {
                    
                    if (likelihood.likelihood >= .70){
                        GMSPlace * myPlace = likelihood.place;
                        
                        params[@"googlePlacesName"] = myPlace.name;
                        params[@"googlePlacesID"] = myPlace.placeID;
                        params[@"address"] = myPlace.addressComponents;
                        params[@"googlePlaces"] = @YES;
                        
                        [PFCloud callFunctionInBackground:@"CreateParty" withParameters:params block:^(NSString * results, NSError * error){
                            
                            if (!error) {
                                
                                
                            }else{
                                NSLog(@"create  ERROR: %@", error);
                                
                                UILocalNotification *o= [[UILocalNotification alloc] init];
                                o.alertBody = @"CHECK! AGAIN!";
                                o.soundName = UILocalNotificationDefaultSoundName;
                                o.fireDate = [NSDate date];
                                //  [[UIApplication sharedApplication] scheduleLocalNotification:o];
                                NSLog(@"create  ERROR: %@", error);
                                
                                
                            }
                            
                        }];
                        break;
                    }
                    GMSPlace* place = likelihood.place;
                    NSLog(@"Current Place name %@ at likelihood %g", place.name, likelihood.likelihood);
                    NSLog(@"Current Place address %@", place.formattedAddress);
                    NSLog(@"Current Place attributions %@", place.attributions);
                    NSLog(@"Current PlaceID %@", place.placeID);
                    
                    if ([params objectForKey:@"googlePlacesName"] == nil) {
                        CLGeocoder * waffles = [CLGeocoder new];
                        [waffles reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
                            if (error) {
                                NSLog(@"Error %@", error.description);
                                NSLog(@"syrup!!! error %@", error);
                                [PFCloud callFunctionInBackground:@"CreateParty" withParameters:params block:^(NSString * results, NSError * error){
                                    
                                    if (!error) {
                                        NSLog(@"the results ARE %@: ", results);
                                    }else{
                                        NSLog(@"create  ERROR: %@", error);
                                        NSLog(@"theres an erRor %@", error);
                                    }
                                    
                                }];
                                
                            } else {
                                CLPlacemark * placemark = [placemarks lastObject];
                                
                                NSString * applePlacemark = placemark.areasOfInterest[0];
                                if (applePlacemark != nil) {
                                    params[@"applePlaceMark"] = applePlacemark;
                                }else{
                                    params[@"applePlaceMark"] = placemark.name;
                                }
                                
                                [PFCloud callFunctionInBackground:@"CreateParty" withParameters:params block:^(NSString * results, NSError * error){
                                    
                                    if (!error) {
                                        NSLog(@"the results ARE %@: ", results);
                                    }else{
                                        NSLog(@"create  ERROR: %@", error);
                                        NSLog(@"theres an erRor %@", error);
                                    }
                                    
                                }];
                                
                                
                                
                                
                            }
                        }];
                        
                        
                    }
                    
                    
                    
                }
                
            }];
            
            
//            CLGeocoder * waffles = [CLGeocoder new];
//            [waffles reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
//                if (error) {
//                    NSLog(@"Error %@", error.description);
//
//                    [PFCloud callFunctionInBackground:@"CreateParty" withParameters:params block:^(NSString * results, NSError * error){
//                        
//                        if (!error) {
//                            NSLog(@"the results ARE %@: ", results);
//                    UILocalNotification *o= [[UILocalNotification alloc] init];
//                    o.alertBody = @"Check Parse for update";
//                    o.soundName = UILocalNotificationDefaultSoundName;
//                    o.fireDate = [NSDate date];
//                   // [[UIApplication sharedApplication] scheduleLocalNotification:o];
//                            
//                        }else{
//                            NSLog(@"create  ERROR: %@", error);
//                            
//                    UILocalNotification *o= [[UILocalNotification alloc] init];
//                    o.alertBody = @"CHECK! AGAIN!";
//                    o.soundName = UILocalNotificationDefaultSoundName;
//                    o.fireDate = [NSDate date];
//                  //  [[UIApplication sharedApplication] scheduleLocalNotification:o];
//                    NSLog(@"create  ERROR: %@", error);
//                            
//                            
//                        }
//                        
//                    }];
//                    
//                } else {
//                    CLPlacemark * placemark = [placemarks lastObject];
//                    
//                    NSString * applePlacemark = placemark.areasOfInterest[0];
//                    
//                    
//                    params[@"applePlaceMark"] = applePlacemark;
//                    
//                    [PFCloud callFunctionInBackground:@"CreateParty" withParameters:params block:^(NSString * results, NSError * error){
//                        
//                        if (!error) {
//                            NSLog(@"the results ARE %@: ", results);
//                            
//                    UILocalNotification *o= [[UILocalNotification alloc] init];
//                    o.alertBody = @"CheckParse";
//                    o.soundName = UILocalNotificationDefaultSoundName;
//                    o.fireDate = [NSDate date];
//                    //[[UIApplication sharedApplication] scheduleLocalNotification:o];
//                        }else{
//                            NSLog(@"create  ERROR: %@", error);
//                            
//                    UILocalNotification *o= [[UILocalNotification alloc] init];
//                        o.alertBody = @"CHECK! AGAIN!";
//                        o.soundName = UILocalNotificationDefaultSoundName;
//                        o.fireDate = [NSDate date];
//                        //[[UIApplication sharedApplication] scheduleLocalNotification:o];
//                                                NSLog(@"create  ERROR: %@", error);
//                            
//                        }
//                        
//                    }];
//                    
//                    
//                }
//            }];
//
//            v.soundName = UILocalNotificationDefaultSoundName;
//            v.fireDate = [NSDate date];
//            [[UIApplication sharedApplication] scheduleLocalNotification:v];
//            
        }
        
    } else {
        n.alertBody = @"didVisit: We left somewhere!";
        
        //self.currentUser[@"moving"] = @YES;
        //[self.currentUser saveInBackground];
    }
    n.soundName = UILocalNotificationDefaultSoundName;
    n.fireDate = [NSDate date];
    //[[UIApplication sharedApplication] scheduleLocalNotification:n];
}


-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"%@",error);
}


@end
