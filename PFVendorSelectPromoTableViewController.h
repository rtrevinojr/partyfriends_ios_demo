//
//  PFVendorSelectPromoTableViewController.h
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 12/10/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface PFVendorSelectPromoTableViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource>


@property (strong, nonatomic) NSArray * friendsList;

@property (strong, nonatomic) NSArray * vendorList;

@property (strong, nonatomic) PFObject * vendorObject;


@end
