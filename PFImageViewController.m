
//
//  PFImageViewController.m
//  PartyFriends
//
//  Created by Rene Trevino Jr. on 11/10/15.
//  Copyright © 2015 Joshua Silva . All rights reserved.
//

#import "PFImageViewController.h"
#import "Reach.h"
#import "UIView+Toast.h"
#import "Parse/Parse.h"
#import <ParseUI/ParseUI.h>


@implementation PFImageViewController


-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];

    NSLog(@"the number of the picture array! %u", self.navigationController.viewControllers.count);
    
    if (self.navigationController.viewControllers.count == 1) {
        UIButton * backButtonView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        [backButtonView addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
        [backButtonView setBackgroundImage:[UIImage imageNamed:@"GoldBackButton"] forState:UIControlStateNormal];
        
        UIBarButtonItem * backBUTTON = [[UIBarButtonItem alloc] initWithCustomView:backButtonView];
        self.navigationItem.leftBarButtonItem = backBUTTON;
        [self.scrollView flashScrollIndicators];
    }

}

- (void) viewDidLoad {
    
    [super viewDidLoad];
    
    PFUser * user = [PFUser currentUser];
    
    
    CALayer * imageLayer = self.imageView.layer;
    [imageLayer setCornerRadius:5];
    [imageLayer setBorderWidth:0];
    [imageLayer setMasksToBounds:YES];
    [self.imageView.layer setCornerRadius:self.imageView.frame.size.width/2];
    [self.imageView.layer setMasksToBounds:YES];
    
    
    if (self.navigationController.viewControllers.count == 1) {
    
        
    }
  
    PFObject * photo = user[@"profilePicture"];
    
    if (photo !=nil) {
        [photo fetchInBackgroundWithBlock:^(PFObject * photoObject, NSError * error){
            if (!error) {
                PFFile * pic =[photoObject objectForKey:@"photo"];
                PFImageView * userImage =[PFImageView new];
                userImage.image = [UIImage imageNamed:@"userPic"];
                userImage.file = (PFFile *) pic;
                
                [userImage loadInBackground:^(UIImage * image, NSError * error){
                    if (!error) {
                        image = [self squareImageFromImage:image scaledToSize:image.size.height];
                        
                        [self.imageView setImage:image];
                    }else {
                        UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
                        [self.imageView setImage:defaultimg];
                    
                    }
                    
                }];
                
            }
            
            
        }];
    }else{
        
               UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
        [self.imageView setImage:defaultimg];
        
    }
    

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"arrow-left"]
                                   style:UIBarButtonItemStylePlain
                                   target:self action:@selector(goBack)];
    
//    if (user[@"profilePic"] != nil) {
//        PFFile * pic = [user objectForKey:@"profilePic"];
//        [pic getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
//            if (!error) {
//                UIImage *image = [UIImage imageWithData:data];
////                CALayer *imageLayer = self.imageView.layer;
////                [imageLayer setCornerRadius:5];
////                [imageLayer setBorderWidth:0];
////                [imageLayer setMasksToBounds:YES];
////                [self.imageView.layer setCornerRadius:self.imageView.frame.size.width/2];
////                [self.imageView.layer setMasksToBounds:YES];
//                [self.imageView setImage:image];
//            }
//            else {
//                UIImage * defaultimg = [UIImage imageNamed:@"58.png"];
////                CALayer *imageLayer = self.imageView.layer;
////                [imageLayer setCornerRadius:5];
////                [imageLayer setBorderWidth:0];
////                [imageLayer setMasksToBounds:YES];
////                [self.imageView.layer setCornerRadius:self.imageView.frame.size.width/2];
////                [self.imageView.layer setMasksToBounds:YES];
//                [self.imageView setImage:defaultimg];
//            }
//        }];
//        
//    }
//    else {
//        UIImage * defaultimg = [UIImage imageNamed:@"NoImage"];
//        
////        CALayer *imageLayer = self.imageView.layer;
////        [imageLayer setCornerRadius:5];
////        [imageLayer setBorderWidth:0];
////        [imageLayer setMasksToBounds:YES];
////        [self.imageView.layer setCornerRadius:self.imageView.frame.size.width/2];
////        [self.imageView.layer setMasksToBounds:YES];
//        [self.imageView setImage:defaultimg];
//    }
    
    

    

    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    }
}


- (void) goBack
{   [self performSegueWithIdentifier:@"goBack" sender:self];
    //[self.navigationController popViewControllerAnimated:YES];
}



- (IBAction)saveProfileImage:(id)sender {
    
//    NSData *imageData = UIImagePNGRepresentation(image);
//    PFFile *imageFile = [PFFile fileWithName:@"image.png" data:imageData];
//    [imageFile saveInBackground];
//    
//    PFUser *user = [PFUser currentUser];
//    [user setObject:imageFile forKey:@"profilePic"];
//    [user saveInBackground];
    
    
//    NSData *imageData = UIImageJPEGRepresentation(resizedImage, 0.05f);
//    PFFile *profPic = [PFFile fileWithName:@"profile.png" data:imageData];
//    PFQuery *query = [PFQuery queryWithClassName:@"UserInfo"];
//    [query whereKey:@"username" equalTo:[[PFUser currentUser] valueForKey:@"username"]];
//    PFObject *object =  [[NSArray arrayWithArray:[query findObjects]]lastObject];
//    [object setValue:profPic forKey:@"profilePicture"];

    
    
//IMAGE FOR THE PARTYFRIENDS ACCOUNT
        // UIImage * image = [UIImage imageNamed:@"PFAcc"];
        //NSData * PFicon = UIImagePNGRepresentation(image);
        //NSLog(@"ahfjhfjasl e34jnn 3h4br3j4hb5245 \n jgh\n %@", image);
    
    if ([Reachability internetCheck]) {
        [self.activityIndicator startAnimating];
        
    
    NSData *imageData = UIImagePNGRepresentation(self.imageView.image);
    PFFile *file = [PFFile fileWithName:@"profilePicture.png" data:imageData];
    //[file saveInBackground];
    
    //PFQuery * query = [PFUser query];
        
        PFObject *photo = [PFObject objectWithClassName:@"photo"];
        [photo setValue:file forKey:@"photo"];
        [photo saveInBackgroundWithBlock:^(BOOL success, NSError * error)
        {
            if (!error) {
                [self.view makeToast:@"Picture Saved" duration:3 position:CSToastPositionCenter];
                [self.activityIndicator stopAnimating];
            }else{
            [self.view makeToast:@"Failed to Save: Try Again" duration:3 position:CSToastPositionCenter];
            [self.activityIndicator stopAnimating];
            }
        
            
        }];
    
//    PFUser * currentUser = [PFUser currentUser];
//    
//    [currentUser setValue:file forKey:@"profilePic"];
//    
//    [currentUser save];
    
    //[currentUser fetch];
    
    }else if (![Reachability internetCheck]) {
        
        [self.view makeToast:@"Try again: No internet connection" duration:3 position:CSToastPositionCenter];
    }
    
    
    
}



- (IBAction)selectPhoto:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    
    
}

- (IBAction)takePhoto:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    picker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
    
    picker.cameraViewTransform = CGAffineTransformIdentity;
    
    
    UIView *cameraOverlayView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 100.0f, 5.0f, 100.0f, 35.0f)];
    [cameraOverlayView setBackgroundColor:[UIColor blackColor]];
    UIButton *emptyBlackButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 100.0f, 35.0f)];
    [emptyBlackButton setBackgroundColor:[UIColor blackColor]];
    [emptyBlackButton setEnabled:YES];
    [cameraOverlayView addSubview:emptyBlackButton];
    
    
    picker.cameraOverlayView = cameraOverlayView;
    
    //picker.cameraViewTransform = YES;
    
    //[picker setShowsCameraControls:FALSE];
    
    [self presentViewController:picker animated:YES completion:NULL];
}



- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.imageView.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    
}

- (UIImage *)squareImageFromImage:(UIImage *)image scaledToSize:(CGFloat)newSize {
    CGAffineTransform scaleTransform;
    CGPoint origin;
    
    if (image.size.width > image.size.height) {
        CGFloat scaleRatio = newSize / image.size.height;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(-(image.size.width - image.size.height) / 2.0f, 0);
    } else {
        CGFloat scaleRatio = newSize / image.size.width;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(0, -(image.size.height - image.size.width) / 2.0f);
    }
    
    CGSize size = CGSizeMake(newSize, newSize);
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(size, YES, 0);
    } else {
        UIGraphicsBeginImageContext(size);
    }
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextConcatCTM(context, scaleTransform);
    
    [image drawAtPoint:origin];
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}



@end
