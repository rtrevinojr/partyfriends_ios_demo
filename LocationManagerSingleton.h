//
//  LocationManagerSingleton.h
//  PartyFriends
//
//  Created by Joshua Silva  on 8/18/15.
//  Copyright (c) 2015 Joshua Silva . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <Parse/Parse.h>

@interface LocationManagerSingleton : NSObject <CLLocationManagerDelegate>
@property(strong, nonatomic) CLLocationManager * locationManager;
@property(strong, nonatomic) PFUser * currentUser;

+(LocationManagerSingleton *) sharedSingleton;


@end
